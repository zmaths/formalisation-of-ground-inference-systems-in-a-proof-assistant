theory Propo_DPLL
imports Main Partial_Clausal_Logic

begin

datatype 'v marked_lit = is_marked: Marked (lit_of: "'v literal") | True_lit (lit_of: "'v literal")
type_synonym 'v annotated_literals = "'v marked_lit list"

definition lits_of :: "'a annotated_literals \<Rightarrow> 'a interp" where
"lits_of Ls = lit_of ` (set Ls)"

definition lits_of_l :: "'a annotated_literals \<Rightarrow> 'a literal list" where
"lits_of_l Ls = map lit_of Ls"

lemma lits_of_cons[simp]:
  "lits_of (L # Ls) = insert (lit_of L) (lits_of Ls)"
  unfolding lits_of_def by auto

instantiation marked_lit :: (type) uminus
begin

definition uminus_marked_lit :: "'a marked_lit \<Rightarrow> 'a marked_lit" where
  "uminus L = (if is_marked L then Marked else True_lit) (- lit_of L)"

instance ..

end

lemma
  uminus_marked_lit_Marked[simp]: "- Marked L = Marked (-L)" and
  uminus_marked_lit_True_lit[simp]: "- True_lit L = True_lit (-L)"
  unfolding uminus_marked_lit_def by simp_all

lemma uminus_of_uminus_id[simp]:
  "- (- (x:: 'v marked_lit)) = x"
  by (simp add: uminus_marked_lit_def)

lemma uminus_not_id[simp]:
  "x \<noteq> - (x:: 'v marked_lit)"
  by (case_tac x, auto)

definition "defined_lit L I \<equiv> Marked L \<in> set I \<or> True_lit L \<in> set I \<or>  Marked (-L) \<in> set I \<or> True_lit (-L) \<in> set I"

lemma Marked_True_lit_in_iff_in_lits_of:
  "defined_lit L I \<longleftrightarrow> (L \<in> lits_of I \<or> - L \<in> lits_of I)"
  unfolding lits_of_def
proof -
  have "\<forall>M l f. \<exists>m. \<forall>ma Ma la fa. ((l\<Colon>'a literal) \<notin> f ` M \<or> (m\<Colon>'a marked_lit) \<in> M) \<and> (l \<notin> f ` M \<or> f m = l) \<and> ((ma\<Colon>'a marked_lit) \<notin> Ma \<or> (la\<Colon>'a literal) \<noteq> fa ma \<or> la \<in> fa ` Ma)"
    by blast (* 11 ms *)
  then obtain mm :: "'a marked_lit set \<Rightarrow> 'a literal \<Rightarrow> ('a marked_lit \<Rightarrow> 'a literal) \<Rightarrow> 'a marked_lit" where
    f1: "\<And>l f M m Ma la fa. (l \<notin> f ` M \<or> mm M l f \<in> M) \<and> (l \<notin> f ` M \<or> f (mm M l f) = l) \<and> ((m\<Colon>'a marked_lit) \<notin> Ma \<or> (la\<Colon>'a literal) \<noteq> fa m \<or> la \<in> fa ` Ma)"
    by moura (* 43 ms *)
  hence "\<And>l M. l \<notin> lit_of ` M \<or> mm M l lit_of = True_lit l \<or> mm M l lit_of = Marked l"
    by (metis (no_types) marked_lit.exhaust_sel) (* 11 ms *)
  thus "defined_lit L I = (L \<in> lit_of ` set I \<or> - L \<in> lit_of ` set I)"
    using f1 by (metis (no_types) defined_lit_def marked_lit.sel(1) marked_lit.sel(2)) (* 35 ms *)
qed



abbreviation "undefined_lit L I \<equiv> \<not>defined_lit L I"

definition true_annotated :: "'a annotated_literals \<Rightarrow> 'a clause \<Rightarrow> bool" (infix "\<Turnstile>d" 49) where
  "I \<Turnstile>d C \<longleftrightarrow> (lits_of I) \<Turnstile> C"

definition true_annotated_propo :: "'a annotated_literals \<Rightarrow> 'a propo \<Rightarrow> bool" (infix "\<Turnstile>ds" 49) where
  "I \<Turnstile>ds CC \<longleftrightarrow> (\<forall>C \<in> CC. I \<Turnstile>d C)"


lemma [simp]:
  "I \<Turnstile>ds {}"
  unfolding true_annotated_propo_def by auto
lemma [iff]:
  "I \<Turnstile>ds {C} \<longleftrightarrow> I \<Turnstile>d C"
  unfolding true_annotated_propo_def by auto

lemma true_annotated_propo_true_cls:
  "I \<Turnstile>ds CC \<longleftrightarrow> (lits_of I) \<Turnstile>s CC"
  unfolding true_annotated_propo_def Ball_def true_annotated_def true_clss_def by auto

lemma true_annotated_iff_marked_or_true_lit:
  "(defined_lit L I) \<longleftrightarrow> ((lits_of I) \<Turnstile>l L \<or> (lits_of I) \<Turnstile>l -L)"
  unfolding defined_lit_def by (smt imageE imageI lits_of_def marked_lit.exhaust_sel marked_lit.sel(1) marked_lit.sel(2) true_lit_def)

definition CNot ::"'v clause \<Rightarrow> 'v propo" where "CNot \<psi> = { {#-L#} | L.  L \<in># \<psi> }"

lemma in_CNot_uminus[simp]:
  shows "{#L#} \<in> CNot \<psi> \<longleftrightarrow> (-L \<in># \<psi>)"
  using assms unfolding CNot_def by force

lemma consistent_CNot_not:
  assumes "consistent_interp I"
  shows "I \<Turnstile>s CNot \<phi> \<Longrightarrow> ~I \<Turnstile> \<phi>"
  using assms unfolding consistent_interp_def true_clss_def true_cls_def CNot_def by auto


lemma total_not_CNot:
  assumes "total_over_m I {\<phi>}"
  shows "~I \<Turnstile> \<phi> \<Longrightarrow> I \<Turnstile>s CNot \<phi>"
  using assms unfolding total_over_m_def total_over_set_def true_clss_def true_cls_def CNot_def apply simp
  apply clarify
  by (metis (no_types, hide_lams) Clausal_Logic.uminus_of_uminus_id atm_iff_pos_or_neg_lit mem_set_of_iff multiset_not_empty neq0_conv true_cls_def true_cls_empty true_lit_def uminus_Neg)

lemma atms_of_m_CNot_atms_of[simp]:
  "atms_of_m (CNot C) = atms_of C"
  unfolding atms_of_m_def CNot_def apply auto
  apply (metis (full_types) atm_of_lit_in_atms_of atm_of_uminus less_not_refl)
  unfolding atms_of_def by (metis atm_of_uminus imageE mem_set_of_iff multi_member_last)

lemma [simp]:
  "\<not>[] \<Turnstile>d \<psi>"
  unfolding true_annotated_def lits_of_def true_cls_def by simp

lemma true_clss_singleton_lit_of_implies_un:
  "I \<Turnstile>s (\<lambda>a. {#lit_of a#}) ` set MLs \<Longrightarrow> I = lits_of MLs \<union> I"
  unfolding true_clss_def lits_of_def by auto

lemma true_annotated_true_propo:
  "MLs \<Turnstile>d \<psi> \<Longrightarrow> set (map (\<lambda>a. {#lit_of a#}) MLs) \<Turnstile>p \<psi>"
  unfolding true_annotated_def true_propo_def by (metis Propo_DPLL.true_clss_singleton_lit_of_implies_un Un_iff image_set true_cls_def true_lit_def)

lemma true_annotated_propo_true_propo:
  "MLs \<Turnstile>ds \<psi> \<Longrightarrow> set (map (\<lambda>a. {#lit_of a#}) MLs) \<Turnstile>ps \<psi>"
  unfolding true_annotated_propo_def Ball_def true_propo_def true_propo_propo_def by (metis Propo_DPLL.true_clss_singleton_lit_of_implies_un list.set_map true_annotated_def true_cls_union_increase true_clss_def)

fun backtrack_split :: "'v annotated_literals \<Rightarrow> 'v annotated_literals \<times> 'v annotated_literals" where
"backtrack_split [] = ([], [])" |
"backtrack_split (True_lit L # mlits) = apfst ((op #) (True_lit L)) (backtrack_split mlits)" |
"backtrack_split (Marked L # mlits) = ([], Marked L # mlits)"

lemma backtrack_split_fst_not_marked: "a \<in> set (fst (backtrack_split l)) \<Longrightarrow> ~ is_marked a"
  by (induct l, auto) (case_tac aa, auto)

lemma backtrack_split_snd_hd_marked: "snd (backtrack_split l) \<noteq> [] \<Longrightarrow> is_marked (hd (snd (backtrack_split l)))"
  by (induct l, auto) (case_tac a, auto)

lemma backtrack_split_list_eq:
  "l = fst (backtrack_split l) @ (snd (backtrack_split l))"
  by (induct l, auto) (case_tac a, auto)


type_synonym 'v state = "'v annotated_literals \<times> 'v propo"
value "filter"

inductive dpll :: "'v state \<Rightarrow> 'v state \<Rightarrow> bool" where
propagate: "C + {#L#} \<in> snd S \<Longrightarrow> fst S \<Turnstile>ds CNot C \<Longrightarrow> undefined_lit L (fst S) \<Longrightarrow> dpll S (True_lit L # fst S, snd S)" |
decided: "undefined_lit L (fst S) \<Longrightarrow> atm_of L \<in> atms_of_m (snd S) \<Longrightarrow> dpll S (Marked L # fst S, snd S)" |
backtrack: "backtrack_split (fst S)  = (M', L # M) \<Longrightarrow> is_marked L \<Longrightarrow> D \<in> snd S \<Longrightarrow> fst S \<Turnstile>ds CNot D \<Longrightarrow> dpll S (True_lit (- (lit_of L)) # M, snd S)"

definition "final_dpll_state S \<longleftrightarrow> (fst S \<Turnstile>d snd S \<or> ((\<forall>L \<in> set (fst S). \<not>is_marked L) \<and> fst S \<Turnstile>ds CNot (snd S)))"

lemma "consistent_interp (lits_of I) \<Longrightarrow> I \<Turnstile>ds N \<Longrightarrow> satisfiable N"
  by (simp add: true_annotated_propo_true_cls)

lemma consistent_add_undefined_lit_consistent:
  "consistent_interp (lits_of Ls) \<Longrightarrow> undefined_lit L Ls \<Longrightarrow> consistent_interp (insert L (lits_of Ls))"
  unfolding consistent_interp_def by (auto simp add: Marked_True_lit_in_iff_in_lits_of)

lemma defined_lit_map:
  "defined_lit L Ls \<longleftrightarrow> atm_of L \<in> set (map (atm_of o lit_of) Ls)"
  unfolding defined_lit_def apply auto
  using image_iff apply fastforce
  using image_iff apply fastforce
  using image_iff apply fastforce
  using image_iff apply fastforce
  by (metis literal.exhaust_sel marked_lit.collapse uminus_Neg uminus_Pos)

abbreviation "no_dup L == distinct (map (\<lambda>l. atm_of (lit_of l)) L)"
lemma dpll_no_dup_inv:
  assumes "dpll S S'"
  and "no_dup (fst S)"
  shows "no_dup (fst S')"
  using assms
proof (induct rule: dpll.induct)
  case (decided L S)
  thus ?case using defined_lit_map by force
next
  case (propagate C L S)
  thus ?case using defined_lit_map by force
next
  case (backtrack S M' L M D) note extracted = this(1) and no_dup = this(5)
  have no_dup': "no_dup M" using no_dup backtrack_split_list_eq[of "fst S"] unfolding extracted by auto
  thus ?case using no_dup no_dup' backtrack_split_list_eq[of "fst S"] unfolding extracted by auto
qed

lemma dpll_consistent_interp_inv:
  assumes "dpll S S'"
  and "consistent_interp (lits_of (fst S))"
  and "no_dup (fst S)"
  shows "consistent_interp (lits_of (fst S'))"
  using assms
proof (induct rule: dpll.induct)
  case (decided L S)
  thus ?case using consistent_add_undefined_lit_consistent by auto
next
  case (propagate C L S)
  thus ?case using consistent_add_undefined_lit_consistent by auto
next
  case (backtrack S M' L M D) note extracted = this(1) and cons = this(5) and no_dup = this(6)
  have no_dup': "no_dup M" using no_dup backtrack_split_list_eq[of "fst S"] unfolding extracted by auto
  hence "insert (lit_of L) (lits_of M) \<subseteq> lits_of (fst S)" using backtrack_split_list_eq[of "fst S"] unfolding extracted lits_of_def by auto
  hence cons: "consistent_interp (insert (lit_of L) (lits_of M))" using consistent_interp_subset cons by blast
  also have L: "lit_of L \<notin> (lits_of M)"
    proof -
      have "atm_of (lit_of L) \<notin> (\<lambda>m. atm_of (lit_of m)) ` set M"
        using no_dup backtrack_split_list_eq[of "fst S"] unfolding extracted by force (* 9 ms *)
      hence "lit_of L \<notin> lit_of ` set M"
        by force (* 0.7 ms *)
      thus ?thesis
        using lits_of_def by blast (* 0.7 ms *)
    qed
  moreover have mL: "- lit_of L \<notin> (lits_of M)"
      proof -
      have "atm_of (-lit_of L) \<notin> (\<lambda>m. atm_of (lit_of m)) ` set M"
        using no_dup backtrack_split_list_eq[of "fst S"] unfolding extracted by force
      hence "-lit_of L \<notin> lit_of ` set M"
        by force (* 0.7 ms *)
      thus ?thesis
        using lits_of_def by blast (* 0.7 ms *)
    qed
  ultimately show ?case by simp
qed


(*TODO Move*)


  
lemma true_propo_propo_left_right':
  assumes "A \<Turnstile>ps B"
  and "A \<union> B \<Turnstile>ps M"
  and "atms_of_m M \<subseteq> atms_of_m A"
  and "atms_of_m B \<subseteq> atms_of_m A"
  shows "A \<Turnstile>ps M"
  using assms unfolding true_propo_propo_def atms_of_m_def atms_of_def by (smt assms(3) assms(4) atms_of_m_union sup.orderE total_over_m_def true_clss_union)

lemma true_annotated_propo_true_propo_propo:
  "A \<Turnstile>ds \<Psi> \<Longrightarrow> (\<lambda>a. {#lit_of a#}) ` set A \<Turnstile>ps \<Psi>"
  unfolding true_propo_propo_def true_annotated_propo_def Ball_def true_annotated_def lits_of_def true_clss_def by (metis (full_types) Propo_DPLL.true_clss_singleton_lit_of_implies_un lits_of_def true_cls_union_increase true_clss_def)

lemma [simp]:
  "atms_of_m ((\<lambda>a. {#lit_of a#}) ` set M') = atm_of ` lit_of `set M'"
  unfolding atms_of_m_def apply auto
  using image_iff apply force
  by fastforce

lemma true_propo_propo_contradiction_true_propo_false:
  "C \<in> D \<Longrightarrow> D \<Turnstile>ps CNot C \<Longrightarrow> D \<Turnstile>p {#}"
  unfolding true_propo_propo_def true_propo_def apply (auto simp add: total_over_m_union)
  unfolding total_over_m_def by (metis atms_of_m_insert atms_of_m_CNot_atms_of atms_of_m_union consistent_CNot_not in_atms_of_in_atms_of_m sup.orderE total_over_m_def total_over_m_union true_clss_def)

lemma true_propo_propo_false_left_right:
  assumes "{{#L#}} \<union> B \<Turnstile>p {#}"
  shows "B \<Turnstile>ps CNot {#L#}"
  unfolding true_propo_propo_def true_propo_def
proof (clarify)
  fix I
  assume tot: "total_over_m I (B \<union> CNot {#L#})"
  and cons: "consistent_interp I"
  and I: "I \<Turnstile>s B"
  have "total_over_m I ({{#L#}} \<union> B)" using tot unfolding total_over_m_def atms_of_m_union by auto
  hence "~I \<Turnstile>s insert {#L#} B"
    using assms cons unfolding true_propo_propo_def true_propo_def CNot_def apply simp
    by (metis add.right_neutral total_over_m_insert total_over_m_sum)
  hence "\<not>I \<Turnstile>s {{#L#}} \<or> \<not>I \<Turnstile>s B"  unfolding true_clss_def by auto
  hence "\<not>I \<Turnstile>s {{#L#}}" using I by auto
  thus "I \<Turnstile>s CNot {#L#}"
    unfolding CNot_def true_clss_def Ball_def apply auto
    unfolding total_over_m_def total_over_set_def by (metis `total_over_m I ({{#L#}} \<union> B)` insertI1 insert_DiffM insert_is_Un multi_member_last total_over_m_def total_over_set_literal_defined)
qed
  (*END TODO*)


lemma atms_of_m_CNot_atms_of_m [simp]: "atms_of_m (CNot CC) = atms_of_m {CC}"
  unfolding atms_of_m_def CNot_def apply auto
  apply (metis (full_types) atm_of_uminus less_not_refl)
  by (metis atm_of_uminus multi_member_last)

lemma total_over_m_CNot_toal_over_m[simp]: "total_over_m I (CNot C) = total_over_m I {C}"
  unfolding total_over_m_def total_over_set_def by auto

lemma true_propo_plus_CNot:
  assumes CC_L: "A \<Turnstile>p CC + {#L#}"
  and CNot_CC: "A \<Turnstile>ps CNot CC"
  and incl: "CC + {#L#} \<in> A"
  shows "A \<Turnstile>p {#L#}"
  unfolding true_propo_propo_def true_propo_def CNot_def total_over_m_def
proof (clarify)
  fix I
  assume tot: "total_over_set I (atms_of_m (A \<union> {{#L#}}))"
  and cons: "consistent_interp I"
  and I: "I \<Turnstile>s A"
  have "total_over_set I (atms_of_m A)" using incl tot unfolding total_over_set_atm_of atms_of_m_union by auto
  have "atms_of (CC + {#L#}) \<subseteq> atms_of_m A" using incl unfolding atms_of_m_def atms_of_def by fastforce+
  hence "total_over_m I (A \<union> {CC + {#L#}})" using incl tot unfolding total_over_m_def total_over_set_atm_of atms_of_m_union by auto
  hence "I \<Turnstile> CC + {#L#}" using CC_L cons I unfolding true_propo_def by auto
  have "total_over_m I (A \<union> CNot CC)"
    using tot unfolding total_over_m_union apply (auto simp add: `total_over_set I (atms_of_m A)` total_over_m_def)
    by (metis `total_over_m I (A \<union> {CC + {#L#}})` atms_of_m_CNot_atms_of atms_of_m_CNot_atms_of_m total_over_m_def total_over_m_sum total_over_m_union)
  hence "I \<Turnstile>s CNot CC" using CNot_CC cons I unfolding true_propo_propo_def by auto
  hence "\<not>A \<Turnstile>p CC" by (meson I `total_over_m I (A \<union> {CC + {#L#}})` cons consistent_CNot_not total_over_m_sum total_over_m_union true_propo_def)
  hence "\<not>I \<Turnstile> CC" using `I \<Turnstile>s CNot CC` cons consistent_CNot_not by blast
  thus "I \<Turnstile>l L" using `I \<Turnstile> CC + {#L#}` by blast
qed


fun get_all_marked_decomposition :: "'a marked_lit list \<Rightarrow> 'a marked_lit set \<Rightarrow> ('a marked_lit list \<times> 'a marked_lit set) list" where
"get_all_marked_decomposition (Marked L # Ls) seen = (Marked L # Ls, seen) # get_all_marked_decomposition Ls {}" |
"get_all_marked_decomposition (True_lit L # Ls) seen = get_all_marked_decomposition Ls ({True_lit L} \<union> seen)" |
"get_all_marked_decomposition [] seen = [([], seen)]"

lemma get_all_marked_decomposition_never_empty[simp]: "(get_all_marked_decomposition M seen = []) \<longleftrightarrow> False"
  by (induct M arbitrary: seen, simp) (case_tac a, auto)
lemma get_all_marked_decomposition_never_empty_sym[simp]: "([] = get_all_marked_decomposition M seen) \<longleftrightarrow> False"
  using get_all_marked_decomposition_never_empty[of M seen] by presburger

lemma get_all_marked_decomposition_True_lit[simp]: "(get_all_marked_decomposition (True_lit L # Ls) seen) = (\<lambda>(Ls, seen). (Ls, {True_lit L} \<union> seen)) (hd (get_all_marked_decomposition Ls seen)) # tl (get_all_marked_decomposition Ls seen)"
  apply (induct Ls arbitrary: seen, auto)
  by (case_tac a) auto

lemma get_all_marked_decomposition_decomp: "hd (get_all_marked_decomposition S seen) = (a, c) \<Longrightarrow> set S \<union> seen = set a \<union> c \<union> seen"
proof (induct S arbitrary: a  c seen)
  case Nil
  thus ?case by simp
next
  case (Cons x A) note H = this
  show ?case
    proof (case_tac x)
      fix x1
      assume x: "x = Marked x1"
      have h: "hd (get_all_marked_decomposition (x # A) seen) = (x # A, seen)" unfolding x by simp
      have A: "a = x # A" "c = seen" using H(2) h unfolding x by auto
      show ?case unfolding A by auto
    next
      fix x2
      assume x: "x = True_lit x2"
      obtain Ls' seen' where L: "hd (get_all_marked_decomposition A seen) = (Ls', seen')" by (case_tac "hd (get_all_marked_decomposition A seen)") auto
      have s: "set A \<union> seen = set Ls' \<union> seen \<union> seen'" using L H by fastforce
      have "get_all_marked_decomposition (x # A) seen = (Ls',  {True_lit x2} \<union> seen') # tl (get_all_marked_decomposition A seen)"
        using get_all_marked_decomposition_True_lit[of x2 A seen] H unfolding x L by auto
      hence a: "a = Ls'" "c = {True_lit x2} \<union> seen'" using H(2) by auto
      show ?case using s unfolding x a by auto
    qed
qed

lemma get_all_marked_decomposition_backtrack_split:
  "backtrack_split S = (M, L # M') \<Longrightarrow> hd (get_all_marked_decomposition S seen) = (L # M', set M \<union> seen)"
  apply (induct S arbitrary: M L M', auto)
  apply (case_tac a)
  apply auto[1]
  apply (case_tac "(get_all_marked_decomposition S seen)")
  apply (simp_all del: get_all_marked_decomposition.simps(2))
  by (smt Un_commute Un_insert_right fst_apfst list.simps(15) old.prod.case prod.collapse prod.sel(1) snd_apfst snd_conv)

lemma get_all_marked_decomposition_nil_backtrack_split_snd_nil:
  "get_all_marked_decomposition S seen = [([], A)] \<Longrightarrow> snd (backtrack_split S) = []"
  apply (induct S arbitrary: seen, auto)
  by (case_tac a, auto)

definition "all_decomposition_implies N (S:: ('a marked_lit list \<times> 'a marked_lit set) list) \<longleftrightarrow> (\<forall>(Ls, seen) \<in> set S. (\<lambda>a. {#lit_of a#}) ` set Ls \<union> N \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` seen)"

lemma dpll_vars_in_snd_inv:
  assumes "dpll S S'"
  and "atm_of ` (set (map lit_of (fst S))) \<subseteq> atms_of_m (snd S)"
  shows "atm_of ` (set (map lit_of (fst S'))) \<subseteq> atms_of_m (snd S')"
  using assms apply (induct rule: dpll.induct)
  apply (simp_all add: in_m_in_literals union_commute)
  apply auto
  using backtrack_split_list_eq apply (metis contra_subsetD in_set_conv_decomp list.set_map list.simps(9) map_append snd_eqD)
  using backtrack_split_list_eq by (metis append_Cons append_assoc contra_subsetD imageI in_set_conv_decomp snd_conv)

lemma atms_of_m_lit_of_atms_of: "atms_of_m ((\<lambda>a. {#lit_of a#}) ` c) = atm_of ` lit_of ` c"
  unfolding atms_of_m_def using image_iff by force+

lemma tl_get_all_marked_decomposition_seen:
  "tl (get_all_marked_decomposition M'' seen) = [] \<longleftrightarrow> tl (get_all_marked_decomposition M'' {}) = []"
  apply (induct M'' arbitrary: seen, simp_all)
  by (case_tac a) auto

lemma length_get_all_marked_decomposition_ind_of_seen:
  "length (get_all_marked_decomposition M'' seen) = length (get_all_marked_decomposition M'' {})"
  apply (induct M'' arbitrary: seen, auto)
  apply (case_tac a, auto)
  by metis

lemma get_all_marked_decomposition_remove_unmarked_length:
  assumes "\<forall>l \<in> set M'. \<not>is_marked l"
  shows "length (get_all_marked_decomposition (M' @ M'') seen) = length (get_all_marked_decomposition M'' seen)"
  using assms apply (induct M' arbitrary: M'' seen, auto)
  apply (case_tac a, auto)
  by (metis length_get_all_marked_decomposition_ind_of_seen)

lemma get_all_marked_decomposition_not_is_marked_length:
  assumes "\<forall>l \<in> set M'. \<not>is_marked l"
  shows "1 + length (get_all_marked_decomposition (True_lit (-L) # M) {}) = length (get_all_marked_decomposition (M' @ Marked L # M) {})"
  using assms apply (induct M')
  using length_get_all_marked_decomposition_ind_of_seen apply auto[1]
  apply (case_tac a)
  apply simp
  using get_all_marked_decomposition_remove_unmarked_length length_get_all_marked_decomposition_ind_of_seen by fastforce

lemma get_all_marked_decomposition_last_choice:
  assumes "tl (get_all_marked_decomposition (M' @ Marked L # M) {}) \<noteq> []"
  and "\<forall>l \<in> set M'. \<not>is_marked l"
  and "hd (tl (get_all_marked_decomposition (M' @ Marked L # M) {})) = (M0', M0)" 
  shows "hd (get_all_marked_decomposition (True_lit (-L) # M) {}) = ( M0', {True_lit (-L)} \<union> M0)"
  using assms
proof (induct M')
  case Nil note H = this
  have n: "get_all_marked_decomposition M {} \<noteq> []" using H(1) by auto
  show ?case using H(3) unfolding get_all_marked_decomposition_True_lit by simp
next
  case (Cons a M') note H = this
  have n: "get_all_marked_decomposition M {} \<noteq> []" by auto
  have "tl (get_all_marked_decomposition (M' @ Marked L # M) {}) \<noteq> []" using H(2) by (metis H(3) append_Cons get_all_marked_decomposition.simps(2) insertI1 list.simps(15) marked_lit.collapse(2) tl_get_all_marked_decomposition_seen)
  also have  "\<forall>l\<in>set M'. \<not> is_marked l" using H(3) by auto
  moreover have "hd (tl (get_all_marked_decomposition (M' @ Marked L # M) {})) = (M0', M0)" using H(4) by (metis (no_types, lifting) H(3) append_Cons get_all_marked_decomposition_True_lit list.sel(3) list.set_intros(1) marked_lit.collapse(2))
  ultimately have 1: "hd (get_all_marked_decomposition (True_lit (- L) # M) {}) = (M0', {True_lit (- L)} \<union> M0)" using H(1) by auto
  show ?case unfolding 1 by auto
qed

lemma get_all_marked_decomposition_marked_vars:
 assumes "(L # A, B) \<in> set (get_all_marked_decomposition M seen)"
 shows "is_marked L"
 using assms apply (induct M arbitrary: seen, auto)
 by (case_tac a, auto)

lemma get_all_marked_decomposition_marked_var_or_empty:
 assumes "x \<in> set (get_all_marked_decomposition M seen)"
 shows "length (fst x) \<ge> 1 \<or> fst x = []"
 using assms apply (induct M arbitrary: seen, auto)
 by (case_tac a, auto)

lemma get_all_marked_decomposition_except_last_choice_equal:
  assumes "\<forall>l \<in> set M'. \<not>is_marked l"
  shows "tl (get_all_marked_decomposition (True_lit (-L) # M) {}) = tl (tl (get_all_marked_decomposition (M' @ Marked L # M) {}))"
  using assms
proof (induct M')
  case Nil
  show ?case using get_all_marked_decomposition_True_lit[of "-L"]  by fastforce (* 43 ms *)
next
  case (Cons a M')
  thus ?case
    apply (auto, case_tac a, simp)
    using get_all_marked_decomposition_True_lit by (metis (no_types, lifting) list.sel(3))
qed

lemma get_all_marked_decomposition_hd_hd:
  assumes "get_all_marked_decomposition Ls seen =  (M, C) # (M0, M0') # l"
  shows "set (tl M) = set M0 \<union> M0' \<and> is_marked (hd M)"
  using assms 
proof (induct Ls arbitrary: seen M C M0 M0' l)
  case (Nil)
  thus ?case by simp
next
  case (Cons a Ls seen M C M0 M0' l) note IH = this(1) and g = this(2)
  { fix L
    assume a: "a = Marked L"
    have "M = Marked L # Ls" and "seen = C" and "get_all_marked_decomposition Ls {} = (M0, M0') # l" using g unfolding a by auto
    hence "set Ls = set M0 \<union> M0' "
      using get_all_marked_decomposition_decomp[of Ls "{}" M0 M0'] by force
    hence "set (tl M) = set M0 \<union> M0' \<and> is_marked (hd M)" using `M = Marked L # Ls` by auto
  } 
  also {
    fix L
    assume a: "a = True_lit L"
    have "get_all_marked_decomposition Ls (insert (True_lit L) seen) = (M, C) # (M0, M0') # l" using g unfolding a by simp
    hence "set (tl M) = set M0 \<union> M0' \<and> is_marked (hd M)" using IH by auto
  }
  ultimately show "set (tl M) = set M0 \<union> M0' \<and> is_marked (hd M)" by (case_tac a, auto)
qed

lemma dppl_propagate_is_conclusion:
  assumes "dpll S S'"
  and "all_decomposition_implies (snd S) (get_all_marked_decomposition (fst S) {})"
  and "atm_of ` lit_of ` set (fst S) \<subseteq> atms_of_m (snd S)"
  shows "all_decomposition_implies (snd S') (get_all_marked_decomposition (fst S') {})"
  using assms
proof (induct rule: dpll.induct)
  case (decided L S)
  thus ?case unfolding all_decomposition_implies_def by simp
next
  case (propagate C L S) note inS = this(1) and cnot = this(2) and IH = this(4) and undef = this(3) and atms_incl = this(5)
  have b: "backtrack_split (fst (True_lit L # fst S, snd S)) = (True_lit L # fst (backtrack_split (fst S)), snd (backtrack_split (fst S)))" by (metis apfst_conv backtrack_split.simps(2) fst_conv prod.collapse)
  let ?I = "set (map (\<lambda>a. {#lit_of a#}) (fst S)) \<union> (snd S) "
  have "?I \<Turnstile>p C + {#L#}"  by (auto simp add: inS)
  also have "(fst S) \<Turnstile>ds CNot C" using true_annotated_true_propo cnot by auto
  hence "(\<lambda>a. {#lit_of a#}) ` set (fst S) \<Turnstile>ps CNot C" using true_annotated_propo_true_propo by auto
  hence "?I \<Turnstile>ps CNot C" by auto
  ultimately have "?I \<Turnstile>p {#L#}" using true_propo_plus_CNot[of ?I C L] inS by auto
  {
    assume "get_all_marked_decomposition (fst S) {} = []"
    hence ?case using  IH unfolding b all_decomposition_implies_def by (simp del: get_all_marked_decomposition.simps(2))
  }
  also {
    assume n: "get_all_marked_decomposition (fst S) {} \<noteq> []"
    have 1: "\<And>a b. (a, b) \<in> set (tl (get_all_marked_decomposition (fst S) {})) \<Longrightarrow> ((\<lambda>a. {#lit_of a#}) ` set a \<union> snd S) \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` b"
      using IH unfolding all_decomposition_implies_def by (fastforce simp add: list.set_sel(2) n)
    have 2: "\<And>a c. hd (get_all_marked_decomposition (fst S) {}) = (a, c) \<Longrightarrow> ((\<lambda>a. {#lit_of a#}) ` set a \<union> snd S) \<Turnstile>ps ((\<lambda>a. {#lit_of a#}) ` c)"
      proof -
        fix a c
        assume S: "hd (get_all_marked_decomposition (fst S) {}) = (a, c)"
        have "hd (get_all_marked_decomposition (fst S) {}) \<in> set (get_all_marked_decomposition (fst S) {})" by (fastforce simp add: list.set_sel(2) n)
        thus "((\<lambda>a. {#lit_of a#}) ` set a \<union> snd S) \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` c"
          using IH unfolding S all_decomposition_implies_def by (fastforce simp add: list.set_sel(2) n)
      qed
    have 3: "\<And>a c. hd (get_all_marked_decomposition (fst S) {}) = (a, c) \<Longrightarrow> ((\<lambda>a. {#lit_of a#}) ` set a \<union> snd S) \<Turnstile>p {#L#}"
      proof -
        fix a c
        assume h: "hd (get_all_marked_decomposition (fst S) {}) = (a, c)"
        have h': "set (fst S) = set a \<union> c" using get_all_marked_decomposition_decomp using h n by blast
        have s: "(\<lambda>a. {#lit_of a#}) ` set a \<union> snd S \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` c" using IH h unfolding all_decomposition_implies_def using "2" by auto
        have "(\<lambda>a. {#lit_of a#}) ` set a \<union> (\<lambda>a. {#lit_of a#}) ` c \<union> snd S \<Turnstile>ps CNot C"
          using `?I \<Turnstile>ps CNot C` h' by (simp add: image_Un)
        hence I: "set (map (\<lambda>a. {#lit_of a#})  a) \<union> snd S \<union> (\<lambda>a. {#lit_of a#}) ` c \<Turnstile>ps CNot C" by (simp add: Un_commute Un_left_commute)
        have st: "atms_of_m ((\<lambda>a. {#lit_of a#}) ` c) \<subseteq> atm_of ` set (map lit_of (fst S))"
          proof -
            have t: "atm_of ` set (map lit_of (fst S)) = atm_of ` lit_of ` (set (fst S))" by auto
            show "atms_of_m ((\<lambda>a. {#lit_of a#}) ` c) \<subseteq> atm_of ` set (map lit_of (fst S))"
              unfolding t h' atms_of_m_lit_of_atms_of by (auto simp add: image_Un)
          qed
        hence st: "atms_of_m ((\<lambda>a. {#lit_of a#}) ` c) \<subseteq> atms_of_m (snd S)" using atms_incl by auto
        hence "atms_of_m (CNot C) \<subseteq> atms_of_m (set (map (\<lambda>a. {#lit_of a#})  a) \<union> snd S)" and "atms_of_m ((\<lambda>a. {#lit_of a#}) ` c) \<subseteq> atms_of_m (set (map (\<lambda>a. {#lit_of a#}) a) \<union> snd S)"
          using inS unfolding atms_of_m_CNot_atms_of using atms_of_m_insert in_atms_of_in_atms_of_m apply fastforce
          using st by (metis atms_of_m_union le_supI2)
        hence "(\<lambda>a. {#lit_of a#}) ` set a \<union> snd S \<Turnstile>ps CNot C" using `?I \<Turnstile>ps CNot C` true_propo_propo_left_right'[OF _ I] s  unfolding h' by auto
        thus "((\<lambda>a. {#lit_of a#}) ` set a \<union> snd S) \<Turnstile>p {#L#}" by (smt Un_insert_right image_insert inS inf_sup_aci(5) insertI1 list.set_map list.simps(15) mk_disjoint_insert true_propo_in true_propo_plus_CNot)
      qed
    have ?case
      unfolding all_decomposition_implies_def apply (simp add: n del: get_all_marked_decomposition.simps, auto)
      apply (case_tac "hd (get_all_marked_decomposition (fst S) {})", auto)
      using 1 2 3 unfolding all_decomposition_implies_def true_propo_propo_insert by blast+
  }
  ultimately show ?case by auto
next
  case (backtrack S M' L M D) note extracted = this(1) and D = this(3) and cnot = this(4) and cons = this(4)  and IH = this(5) and atms_incl = this(6)
  have S: "fst S = M' @ L # M" using backtrack_split_list_eq[of "fst S"] unfolding extracted by auto
  have M': "\<forall>l \<in> set M'. \<not>is_marked l" using extracted by (metis backtrack_split_fst_not_marked fst_conv)
  have L: "is_marked L" using extracted by (simp add: backtrack.hyps(2))
  have n: "get_all_marked_decomposition (fst S) {} \<noteq> []" by auto
  have 1: "(\<lambda>a. {#lit_of a#}) ` set (L # M) \<union> snd S \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` set M'" using IH unfolding all_decomposition_implies_def using get_all_marked_decomposition_backtrack_split extracted list.set_sel(1)[of "get_all_marked_decomposition (fst S) {}"] by fastforce
  have "(\<lambda>a. {#lit_of a#}) ` set (L # M) \<union> (\<lambda>a. {#lit_of a#}) ` set M' \<Turnstile>ps CNot D" using true_annotated_propo_true_propo_propo[OF cnot]  get_all_marked_decomposition_decomp[OF get_all_marked_decomposition_backtrack_split[OF extracted]] by (metis (no_types)  image_Un sup_bot_right)
  hence 2: "(\<lambda>a. {#lit_of a#}) ` set (L # M) \<union> snd S \<union> (\<lambda>a. {#lit_of a#}) ` set M' \<Turnstile>ps CNot D"
    by (metis (no_types, lifting) Un_assoc Un_left_commute true_propo_propo_union_l_r)
  have "set M' \<subseteq> set (fst S)" using get_all_marked_decomposition_decomp[OF get_all_marked_decomposition_backtrack_split[OF extracted]] by auto
  hence "set (map (\<lambda>a. {#lit_of a#}) (L # M)) \<union> snd S \<Turnstile>ps CNot D" using D true_propo_propo_left_right'[OF 1 2] apply simp
    proof -
      assume a1: "atm_of ` lit_of ` set M' \<subseteq> atms_of_m (insert {#lit_of L#} ((\<lambda>a. {#lit_of a#}) ` set M \<union> snd S)) \<Longrightarrow> insert {#lit_of L#} ((\<lambda>a. {#lit_of a#}) ` set M \<union> snd S) \<Turnstile>ps CNot D"
      have f2: "atms_of_m (snd S) \<union> atm_of ` lit_of ` set (fst S) \<subseteq> atms_of_m (snd S)"
        by (simp add: atms_incl) (* 1 ms *)
      have "set M' \<union> set (fst S) \<subseteq> set (fst S)"
        by (simp add: `set M' \<subseteq> set (fst S)`) (* 5 ms *)
      hence "\<And>M. atm_of ` lit_of ` set M' \<subseteq> atms_of_m (snd S \<union> M)"
        using f2 by auto (* 7 ms *)
      hence "atm_of ` lit_of ` set M' \<subseteq> atms_of_m (snd S \<union> (\<lambda>m. {#lit_of m#}) ` set M \<union> {m. m = {#lit_of L#}})"
        by (metis Un_assoc) (* 58 ms *)
      thus "insert {#lit_of L#} ((\<lambda>m. {#lit_of m#}) ` set M \<union> snd S) \<Turnstile>ps CNot D"
        using a1 by (metis Un_commute insert_def) (* 134 ms *)
    qed
  hence "set (map (\<lambda>a. {#lit_of a#}) (L # M)) \<union> snd S \<Turnstile>p {#}"  using true_propo_propo_contradiction_true_propo_false D by blast
  hence "(\<lambda>a. {#lit_of a#}) ` set M \<union> snd S \<Turnstile>ps CNot {#lit_of L#}" using true_propo_propo_false_left_right by auto
  hence IL: "(\<lambda>a. {#lit_of a#}) ` set M \<union> snd S \<Turnstile>ps {{#-lit_of L#}}" unfolding CNot_def by (smt Collect_cong count_single less_nat_zero_code multi_member_last singleton_conv)
  have n': "get_all_marked_decomposition (fst (True_lit (- lit_of L) # M, snd S)) {} \<noteq> []" by auto
  hence n'': "get_all_marked_decomposition M {} \<noteq> []" by fastforce
  show ?case unfolding S all_decomposition_implies_def
    proof
      fix x
      assume x: "x \<in> set (get_all_marked_decomposition (fst (True_lit (- lit_of L) # M, snd S)) {})"
      let ?hd = "hd (get_all_marked_decomposition (True_lit (- lit_of L) # M) {})"
      let ?tl = "tl (get_all_marked_decomposition (True_lit (- lit_of L) # M) {})"
      have "x = ?hd \<or> x \<in> set ?tl"
        using x by (case_tac "(get_all_marked_decomposition (fst (True_lit (- lit_of L) # M, snd S)) {})") auto
      also { assume x': "x \<in> set ?tl"
        have L': "Marked (lit_of L) = L" using L by (case_tac L, auto)
        have "x \<in> set (get_all_marked_decomposition (M' @ L # M) {})"
          using x' unfolding get_all_marked_decomposition_except_last_choice_equal[OF M', of "lit_of L" M] L' x' by (metis list.set_sel(2) tl_Nil)
        hence "case x of (Ls, seen) \<Rightarrow> (\<lambda>a. {#lit_of a#}) ` set Ls \<union> snd (True_lit (- lit_of L) # M, snd S) \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` seen"
          using L apply (case_tac L)
          using IH unfolding S L' all_decomposition_implies_def by auto
      }
      moreover {
        assume x': "x = ?hd"
        have tl: "tl (get_all_marked_decomposition (M' @ L # M) {}) \<noteq> []"
          proof -
            have "length (get_all_marked_decomposition (True_lit (- lit_of L) # M) {}) \<noteq> 0"
              using n' by force (* 1 ms *)
            thus ?thesis
              by (metis (no_types) L Nitpick.size_list_simp(2) S Suc_eq_plus1 get_all_marked_decomposition_not_is_marked_length[OF M'] add.commute add_right_imp_eq marked_lit.collapse(1) n) (* 86 ms *)
          qed
        obtain M0' M0 where L0: "hd (tl (get_all_marked_decomposition (M' @ L # M) {})) = (M0, M0')"
          apply (case_tac "hd (tl (get_all_marked_decomposition (M' @ L # M) {}))")
          using get_all_marked_decomposition_marked_vars[of _ _ _ "(M' @ L # M)" _] by metis
        have x'': "x = (M0, {True_lit (-lit_of L)} \<union> M0')"
          unfolding x' using get_all_marked_decomposition_last_choice tl M' L0 by (metis L marked_lit.collapse(1))
        obtain l_get_all_marked_decomposition where "get_all_marked_decomposition (fst S) {} = (L# M, set M') # (M0, M0') # l_get_all_marked_decomposition"
        using get_all_marked_decomposition_backtrack_split[OF extracted] by (metis (no_types) L0 S hd_Cons_tl inf_sup_aci(5) n sup_bot.left_neutral tl)
        hence M: "set M = set M0 \<union> M0'" using get_all_marked_decomposition_hd_hd[of _ _ "L#M"] by auto
 
        have "(\<lambda>a. {#lit_of a#}) ` (set M0 \<union> M0') \<union> snd S = (\<lambda>a. {#lit_of a#}) ` set M0 \<union> snd S \<union> (\<lambda>a. {#lit_of a#}) ` M0'" by auto
        hence IL': "(\<lambda>a. {#lit_of a#}) ` set M0 \<union> snd S \<union> (\<lambda>a. {#lit_of a#}) ` M0' \<Turnstile>ps  {{#- lit_of L#}}" using IL unfolding M by simp
        have H: "(\<lambda>a. {#lit_of a#}) ` set M0 \<union> snd S \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` M0'" 
          using IH x'' unfolding all_decomposition_implies_def by (metis (no_types, lifting) L0 S list.set_sel(1) list.set_sel(2) old.prod.case tl tl_Nil)
 
        have "case x of (Ls, seen) \<Rightarrow> (\<lambda>a. {#lit_of a#}) ` set Ls \<union> snd (True_lit (- lit_of L) # M, snd S) \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` seen"
          unfolding x'' apply simp
          by (metis H IL' insert_def singleton_conv true_propo_propo_left_right)
      }
      ultimately show "case x of (Ls, seen) \<Rightarrow> (\<lambda>a. {#lit_of a#}) ` set Ls \<union> snd (True_lit (- lit_of L) # M, snd S) \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` seen" by blast
    qed
qed

lemma get_all_marked_decomposition_length_1_fst_empty_or_length_1:
  assumes "get_all_marked_decomposition M seen = (a, b) # []"
  shows "a = [] \<or> (length a = 1 \<and> is_marked (hd a) \<and> hd a \<in> set M)"
  using assms by (induct M arbitrary: seen, simp) (case_tac aa, auto)
  
lemma get_all_marked_decomposition_fst_empty_or_hd_in_M:
  assumes "get_all_marked_decomposition M seen = (a, b) # l"
  shows "a = [] \<or> (is_marked (hd a) \<and> hd a \<in> set M)"
  using assms by (induct M arbitrary: seen, simp) (case_tac aa, auto)
  
lemma get_all_marked_decomposition_snd_union:
  "set M \<union> seen = Union (snd ` set (get_all_marked_decomposition M seen)) \<union> {L |L. is_marked L \<and> L \<in> set M}" (is "?M M = ?U M seen \<union> ?Ls M")  
proof (induct M arbitrary: seen)
  case Nil
  thus ?case by simp
next
  case (Cons L M seen)
  show ?case
    proof (case_tac L)
      fix a
      assume L: "L = Marked a"
      hence "L \<in> ?Ls (L#M)" by auto
      also have "?U (L#M) seen = ?U M {} \<union> seen" unfolding L by auto
      moreover have "set M = \<Union>(snd ` set (get_all_marked_decomposition M {})) \<union> {L |L. is_marked L \<and> L \<in> set M}" using Cons.hyps[of "{}"] by auto
      ultimately show ?case by auto
   next
      fix a
      assume L: "L = True_lit a"
      also have "?U (L#M) seen = ?U M ({L} \<union> seen)" unfolding L by auto
      moreover have "set M \<union> ({L} \<union> seen) = \<Union>(snd ` set (get_all_marked_decomposition M ({L} \<union> seen))) \<union> {L |L. is_marked L \<and> L \<in> set M}" using Cons.hyps[of "{L} \<union> seen"] by auto
      ultimately show ?case by auto   
   qed
qed


lemma all_decomposition_implies_proapagated_lits_are_implied:
  assumes "all_decomposition_implies N (get_all_marked_decomposition M seen)"
  shows "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` (set M \<union> seen)" (is "?I \<Turnstile>ps ?A")
proof -
  have "?I \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` {L |L. is_marked L \<and> L \<in> set M}" using all_in_true_propo_propo[of "(\<lambda>a. {#lit_of a#}) ` {L |L. is_marked L \<and> L \<in> set M}" ?I] by auto
  also have "?I \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` Union (snd ` set (get_all_marked_decomposition M seen))"
    using assms
    proof (induct "length (get_all_marked_decomposition M seen)" arbitrary: seen M)
      case 0
      thus ?case by auto
    next
      case (Suc n) note IH = this(1) and length = this(2)
      { 
        assume "length (get_all_marked_decomposition M seen) \<le> 1"
        then obtain a b where g: "get_all_marked_decomposition M seen = (a, b) # []"
          by (case_tac "get_all_marked_decomposition M seen", auto)          
        also {
          assume "a = []"
          hence ?case using Suc.prems unfolding all_decomposition_implies_def g by auto
        }
        moreover {
          assume l: "length a = 1" and m: "is_marked (hd a)" and hd: "hd a \<in> set M"
          hence "(\<lambda>a. {#lit_of a#}) (hd a) \<in> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M}" by auto
          hence "(\<lambda>a. {#lit_of a#}) ` set a \<union> N \<subseteq> N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M}" 
            using l by (case_tac a, auto)
          hence ?case 
            unfolding g apply simp 
            proof -
              have "(\<lambda>m. {#lit_of m#}) ` set a \<union> N \<Turnstile>ps (\<lambda>m. {#lit_of m#}) ` b"
                using Suc.prems unfolding all_decomposition_implies_def g by simp (* 4 ms *)
              thus "N \<union> {{#lit_of m#} |m. is_marked m \<and> m \<in> set M} \<Turnstile>ps (\<lambda>m. {#lit_of m#}) ` b"
                using `(\<lambda>a. {#lit_of a#}) \` set a \<union> N \<subseteq> N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M}` true_propo_propo_subset by blast (* 10 ms *)
            qed 
        }
        ultimately have ?case using get_all_marked_decomposition_length_1_fst_empty_or_length_1 by blast   
      }
      moreover {
        assume "length (get_all_marked_decomposition M seen) > 1"
        then obtain Ls0 seen0 M' where Ls0: "get_all_marked_decomposition M seen = (Ls0, seen0) # get_all_marked_decomposition M' {}" and length': "length (get_all_marked_decomposition M' {}) = n" and M'_in_M: "set M' \<subseteq> set M"
          using length apply (induct M arbitrary: seen, simp_all)
          apply (case_tac a) by simp_all blast+
        {
          assume "n = 0"
          hence "get_all_marked_decomposition M' {} = []" using length' by auto
          hence ?case using Suc.prems unfolding all_decomposition_implies_def Ls0 by auto
        }
        also {
          assume n: "n > 0"
          then obtain Ls1 seen1 l where Ls1: "get_all_marked_decomposition M' {} = (Ls1, seen1) # l" using length' apply (induct M' arbitrary: seen, simp) by(case_tac a, auto simp del:get_all_marked_decomposition.simps(2))
          
          have "all_decomposition_implies N (get_all_marked_decomposition M' {})" using Suc.prems unfolding Ls0 all_decomposition_implies_def by auto
          hence N: "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M'} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` \<Union>(snd ` set (get_all_marked_decomposition M' {}))" using IH length' by auto     
          
          have l: "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M'} \<subseteq> N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M}" using M'_in_M by auto
          hence \<Psi>N: "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` \<Union>(snd ` set (get_all_marked_decomposition M' {}))" using true_propo_propo_subset[OF l N] by auto           
          have "is_marked (hd Ls0)" and LS: "set (tl Ls0) = set Ls1 \<union> seen1" using get_all_marked_decomposition_hd_hd[of M seen] unfolding Ls0 Ls1 by auto
          
          have LSM: "set Ls1 \<union> seen1 = set M'" using get_all_marked_decomposition_decomp[of M' "{}"] Ls1 by auto
          have M': "set M' = Union (snd ` set (get_all_marked_decomposition M' {})) \<union> {L |L. is_marked L \<and> L \<in> set M'}" using get_all_marked_decomposition_snd_union by auto
         
          {
            assume "Ls0 \<noteq> []"
            hence "hd Ls0 \<in> set M" using get_all_marked_decomposition_fst_empty_or_hd_in_M Ls0 by blast
            hence "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M} \<Turnstile>p (\<lambda>a. {#lit_of a#}) (hd Ls0)" 
              using `is_marked (hd Ls0)` by (metis (mono_tags, lifting) UnCI mem_Collect_eq true_propo_in)
          } note hd_Ls0 = this
          
          have l: "(\<lambda>a. {#lit_of a#}) ` (\<Union>(snd ` set (get_all_marked_decomposition M' {})) \<union> {L |L. is_marked L \<and> L \<in> set M'}) =  (\<lambda>a. {#lit_of a#}) ` \<Union>(snd ` set (get_all_marked_decomposition M' {})) \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M'}" by auto
          have "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M'} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` (\<Union>(snd ` set (get_all_marked_decomposition M' {})) \<union> {L |L. is_marked L \<and> L \<in> set M'})"  unfolding l using N by (auto intro: all_in_true_propo_propo simp add: true_propo_propo_union_and)    
          hence "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M'} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` set (tl Ls0)" using M' unfolding LS LSM  by auto
          hence t: "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M'} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` set (tl Ls0)" by (auto intro: all_in_true_propo_propo simp add: true_propo_propo_union_and)    
          hence "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` set (tl Ls0)" using M'_in_M true_propo_propo_subset[OF _ t, of "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M}"] by auto
          hence "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` set Ls0" using hd_Ls0 apply (case_tac Ls0, auto) using true_propo_propo_insert by auto
          
          also have "(\<lambda>a. {#lit_of a#}) ` set Ls0 \<union> N \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` seen0" using Suc.prems unfolding Ls0 all_decomposition_implies_def by simp
          ultimately have \<Psi>: "N \<union> {{#lit_of L#} |L. is_marked L \<and> L \<in> set M} \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` seen0" by (meson all_in_true_propo_propo true_propo_propo_left_right true_propo_propo_union_and true_propo_propo_union_l_r)
          
          have un: "(\<lambda>a. {#lit_of a#}) ` (seen0 \<union> (\<Union>x\<in>set (get_all_marked_decomposition M' {}). snd x)) =  (\<lambda>a. {#lit_of a#}) ` seen0 \<union> (\<lambda>a. {#lit_of a#}) ` (\<Union>x\<in>set (get_all_marked_decomposition M' {}). snd x)" by auto
          
          have ?case unfolding Ls0 using \<Psi> \<Psi>N un by (simp add: true_propo_propo_union_and)
        }
        ultimately have ?case by auto
      }
      ultimately show ?case by arith
   qed
  ultimately show "?I \<Turnstile>ps ?A" unfolding get_all_marked_decomposition_snd_union[of M seen] by (simp add: image_Un true_propo_propo_left_right)
qed 
 

(*lemma 2.9.3*)
lemma 
  assumes marked: "\<forall>x \<in> set M. \<not> is_marked x"
  and DN: "D \<in> N" and D: "M \<Turnstile>ds CNot D"
  and inv: "all_decomposition_implies N (get_all_marked_decomposition M seen)"
  and atm_incl: "atm_of ` lit_of ` (set M \<union> seen)  \<subseteq> atms_of_m N"
  shows "unsatisfiable N"
proof (rule ccontr)
  assume "\<not> unsatisfiable N"
  then obtain I where I: "I \<Turnstile>s N" and cons: "consistent_interp I" and tot: "total_over_m I N" unfolding satisfiable_def by auto
  have I_D: "I \<Turnstile> D" using I DN unfolding true_clss_def Ball_def true_cls_def true_lit_def by auto
  
  have l0: "{{#lit_of L#} |L. is_marked L \<and> L \<in> set M} = {}" using marked by auto  
  have "atms_of_m (N \<union> {} \<union> (\<lambda>a. {#lit_of a#}) ` (set M \<union> seen)) = atms_of_m N" using atm_incl unfolding atms_of_m_def apply auto
    proof -
      fix \<chi> :: "'a literal" and a :: "'a marked_lit"
      assume a1: "a \<in> seen"
      assume a2: "(0\<Colon>nat) < (if lit_of a = \<chi> then 1 else 0)"
      assume a3: "atm_of ` lit_of ` (set M \<union> seen) \<subseteq> {atm_of \<chi> |\<chi>. \<exists>\<psi>. \<psi> \<in> N \<and> \<chi> \<in># \<psi>}"
      have f4: "lit_of a = \<chi>"
        using a2 by presburger (* 1 ms *)
      have "\<And>l. l \<notin> lit_of ` (set M \<union> seen) \<or> (\<exists>la. atm_of l = atm_of la \<and> (\<exists>m. m \<in> N \<and> la \<in># m))"
        using a3 by blast (* 4 ms *)
      thus "\<exists>l. atm_of \<chi> = atm_of l \<and> (\<exists>m. m \<in> N \<and> l \<in># m)"
        using f4 a1 by blast (* 1 ms *)
    next
      fix \<chi> :: "'a literal" and a :: "'a marked_lit"
      assume a1: "a \<in> set M"
      assume a2: "(0\<Colon>nat) < (if lit_of a = \<chi> then 1 else 0)"
      assume a3: "atm_of ` lit_of ` (set M \<union> seen) \<subseteq> {atm_of \<chi> |\<chi>. \<exists>\<psi>. \<psi> \<in> N \<and> \<chi> \<in># \<psi>}"
      have f4: "lit_of a = \<chi>"
        using a2 by presburger (* 1 ms *)
      have "\<And>l. l \<notin> lit_of ` (seen \<union> set M) \<or> (\<exists>la. atm_of l = atm_of la \<and> (\<exists>m. m \<in> N \<and> la \<in># m))"
        using a3 by blast (* 4 ms *)
      thus "\<exists>l. atm_of \<chi> = atm_of l \<and> (\<exists>m. m \<in> N \<and> l \<in># m)"
        using f4 a1 by blast (* 1 ms *)
    qed
  hence "total_over_m I (N \<union> {} \<union> (\<lambda>a. {#lit_of a#}) ` (set M \<union> seen))" using tot unfolding total_over_m_def by auto
  hence "I \<Turnstile>s (\<lambda>a. {#lit_of a#}) ` (set M \<union> seen)" using all_decomposition_implies_proapagated_lits_are_implied[OF inv] cons I unfolding true_propo_propo_def l0 by auto
  hence IM: "I \<Turnstile>s (\<lambda>a. {#lit_of a#}) ` set M" unfolding true_clss_def Ball_def true_cls_def true_lit_def by auto
  {
    fix K
    assume "K \<in># D"
    hence "-K \<in> lits_of M" using D unfolding true_annotated_propo_def Ball_def CNot_def true_annotated_def true_cls_def true_lit_def by (metis (mono_tags, lifting) count_single less_not_refl mem_Collect_eq)
    hence " -K \<in> I" using IM unfolding true_clss_def Ball_def true_cls_def true_lit_def lits_of_def using IM Propo_DPLL.true_clss_singleton_lit_of_implies_un lits_of_def by fastforce
  }
  hence "~ I \<Turnstile> D" using cons unfolding true_cls_def true_lit_def consistent_interp_def by auto
  thus False using I_D by blast
qed

lemma dpll_same_clauses:
  assumes "dpll S S'"
  shows "snd S = snd S'"
  using assms by (induct rule: dpll.induct, auto)
  
lemma rtranclp_dpll_inv:
  assumes "rtranclp dpll S S'" 
  and inv: "all_decomposition_implies (snd S) (get_all_marked_decomposition (fst S) {})"
  and atm_incl: "atm_of ` lit_of ` (set (fst S))  \<subseteq> atms_of_m (snd S)"
  shows "all_decomposition_implies (snd S') (get_all_marked_decomposition (fst S') {})"
  and "atm_of ` lit_of ` (set (fst S'))  \<subseteq> atms_of_m (snd S')"
  and "snd S = snd S'"
  using assms 
proof (induct rule: rtranclp.induct)
  case (rtrancl_refl)
  fix S :: "'a marked_lit list \<times> 'a literal multiset set"
  assume "all_decomposition_implies (snd S) (get_all_marked_decomposition (fst S) {})"
  and "atm_of ` lit_of ` (set (fst S))  \<subseteq> atms_of_m (snd S)"
  thus "all_decomposition_implies (snd S) (get_all_marked_decomposition (fst S) {})" 
  and "atm_of ` lit_of ` set (fst S) \<subseteq> atms_of_m (snd S)" 
  and "snd S = snd S" by auto
next
  case (rtrancl_into_rtrancl S S' S'') note dpllStar = this(1) and IH = this(2,3,4) and dpll = this(5)
  also assume inv: "all_decomposition_implies (snd S) (get_all_marked_decomposition (fst S) {})"
  and atm_incl: "atm_of ` lit_of ` (set (fst S))  \<subseteq> atms_of_m (snd S)"
  ultimately have decomp: "all_decomposition_implies (snd S') (get_all_marked_decomposition (fst S') {})"
  and atm_incl': "atm_of ` lit_of ` set (fst S') \<subseteq> atms_of_m (snd S')"
  and snd: "snd S = snd S'" by auto
  show "snd S = snd S''" using dpll_same_clauses[OF dpll] snd by metis
  
  show "all_decomposition_implies (snd S'') (get_all_marked_decomposition (fst S'') {})" using  dppl_propagate_is_conclusion[OF dpll] decomp atm_incl' by auto
  show "atm_of ` lit_of ` set (fst S'') \<subseteq> atms_of_m (snd S'')" using dpll_vars_in_snd_inv[OF dpll]  atm_incl atm_incl' by auto
qed
  
lemma rtranclp_dpll_inv':
  assumes "rtranclp dpll S S'" 
  and inv: "fst S = []"
  shows "all_decomposition_implies (snd S') (get_all_marked_decomposition (fst S') {})"
  and "atm_of ` lit_of ` (set (fst S'))  \<subseteq> atms_of_m (snd S')"
  and "snd S = snd S'"
  using assms rtranclp_dpll_inv[OF assms(1)] unfolding all_decomposition_implies_def by auto
  
lemma strong_completeness:
  assumes "set M \<Turnstile>s N"
  and "consistent_interp (set M)"
  and "distinct M"
  shows "rtranclp dpll ([], N) (map True_lit M, N)"
proof (induct M)
  case Nil
  thus ?case by auto
next
  case (Cons L M)
  assume "consistent_interp (set (L # M))"
  and "distinct (L # M)"
  hence "undefined_lit L (map True_lit M)" by (smt comp_eq_dest_lhs consistent_interp_def defined_lit_map distinct.simps(2) imageE insert_iff list.set(2) literal.exhaust_sel marked_lit.sel(2) set_map uminus_literal_def)
(*Failed to replay Z3 proof step: "rewrite"
  proposition:
    SMT.fun_app (if is_pos  then Neg else Pos) (SMT.fun_app atm_of ) = (if is_pos  then SMT.fun_app Neg (SMT.fun_app atm_of ) else SMT.fun_app Pos (SMT.fun_app atm_of ))*)  
  thus ?case
end
