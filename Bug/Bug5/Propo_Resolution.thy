theory Propo_Resolution

imports Partial_Clausal_Logic
begin


section \<open>Inference Rule\<close>

inductive simplifier  :: "'v propo \<Rightarrow> 'v propo \<Rightarrow> bool" for N :: "'v clause set" where
tautology_deletion: "A + {#Pos P#} + {#Neg P#} \<in> N  \<Longrightarrow> simplifier N (Set.remove (A + {#Pos P#} + {#Neg P#}) N)" |
condensation: "A + {#Pos P#} + {#Pos P#} \<in> N  \<Longrightarrow> simplifier N (Set.remove (A + {#Pos P#} + {#Pos P#}) N \<union>{A + {#Pos P#}})" |
subsumption: "A \<in> N \<Longrightarrow> B \<in> N \<Longrightarrow> A <# B \<Longrightarrow> simplifier N (Set.remove B N)"


lemma simplifier_preserves_un_sat:
  fixes N N' :: "'v propo"
  assumes "simplifier N N'"
  and "total_over_m I N"
  shows "I \<Turnstile>s N \<longleftrightarrow> I \<Turnstile>s N'"
  using assms
proof (induct rule: simplifier.induct)
  case (subsumption A B) note H = this
  show ?case
    proof
      assume "I \<Turnstile>s N"
      thus "I \<Turnstile>s Set.remove B N" by (simp add: true_clss_def)
    next
      assume a: "I \<Turnstile>s Set.remove B N"
      hence "I \<Turnstile> A" unfolding true_clss_def using H(1,3) by auto
      hence "I \<Turnstile> B" using H(3) mset_less_def subsumption_imp_eval by blast
      thus "I \<Turnstile>s N" by (metis a member_remove true_clss_def)
    qed
next
  case (tautology_deletion A P) note incl = this(1) and total = this(2)
  {
    assume "I \<Turnstile>s Set.remove (A + {#Pos P#} + {#Neg P#}) N"
    also
    have "Pos P \<in> I \<or> - Pos P \<in> I" by (metis add.commute tautology_deletion.hyps total total_over_m_def total_over_set_literal_defined uminus_Neg uminus_Pos)
    hence "I \<Turnstile> A + {#Pos P#} + {#Neg P#}" using total unfolding true_cls_def by auto
    ultimately have "I \<Turnstile>s N" by (metis insertE insert_Diff remove_def tautology_deletion.hyps true_clss_def)
  }
  thus ?case by (metis model_remove)
next
  case (condensation A P)
  assume "A + {#Pos P#} + {#Pos P#} \<in> N"
  hence "\<And>L. \<not> L \<Turnstile>s N \<or> L \<Turnstile>s {A + {#Pos P#} + {#Pos P#}}"
    by (metis (no_types) Un_insert_right mk_disjoint_insert sup_bot.right_neutral true_clss_union)
  moreover
  {  assume "\<not> I \<Turnstile>s N \<and> \<not> I \<Turnstile>s insert (A + {#Pos P#}) (N - {A + {#Pos P#} + {#Pos P#}})"
     hence ?case
       by (simp add: remove_def)
  }
  ultimately show ?case
    by (metis (no_types) Un_insert_right insert_Diff_single remove_def sup_bot.right_neutral true_cls_union true_clss_singleton true_clss_union)
qed



inductive inference :: "'v propo \<Rightarrow> 'v propo \<Rightarrow> bool" where
resolution: "{#Pos p#} + C \<in> N \<Longrightarrow> {#Neg p#} + D \<in> N \<Longrightarrow> inference N (N \<union> ({C + D}))" |
factoring: "{#L#} + {#L#} + C \<in> N \<Longrightarrow> inference N (N \<union> {C +{#L#}})" |
simplifier: "simplifier N N' \<Longrightarrow> inference N N'"


lemma inference_preserves_un_sat:
  fixes N N' :: "'v propo"
  assumes "inference N N'"
  and  "total_over_m I N"
  and consistent: "consistent_interp I"
  shows "I \<Turnstile>s N \<longleftrightarrow> I \<Turnstile>s N'"
  using assms(1-3)
proof (induct rule: inference.induct)
  fix p C D and N :: "'v propo"
  assume posp: "{#Pos p#} + C \<in> N"
  and negp: "{#Neg p#} + D \<in> N"
  and total: "total_over_m I N"
  hence "Pos p \<in> I \<or> - Pos p \<in> I" using total total_over_set_literal_defined unfolding total_over_m_def  by metis
  hence "Pos p \<in> I \<or> Neg p \<in> I" by auto
  also {
    assume "Pos p \<in> I" and IN: "I \<Turnstile>s N"
    hence "Neg p \<notin> I" using consistent unfolding consistent_interp_def by auto
    hence "I \<Turnstile> D" using negp IN unfolding true_clss_def by auto
    hence "(I \<Turnstile>s N \<union> {C + D})" using IN unfolding true_clss_def by auto
  }
  moreover {
    assume "Neg p \<in> I" and IN: "I \<Turnstile>s N"
    hence "Pos p \<notin> I" using consistent unfolding consistent_interp_def by auto
    hence "I \<Turnstile> C" using posp IN unfolding true_clss_def by auto
    hence "(I \<Turnstile>s N \<union> {C + D})" using IN unfolding true_clss_def by auto
  }
  moreover {
    assume "I \<Turnstile>s N \<union> {C + D}"
    hence "I \<Turnstile>s N" unfolding true_clss_def  by auto
  }
  ultimately show "I \<Turnstile>s N \<longleftrightarrow> I \<Turnstile>s N \<union> {C + D}" by blast
next
  fix L C N
  assume "{#L, L#} + C \<in> N"
  and "total_over_m I N"
  and "consistent_interp I"
  thus "I \<Turnstile>s N \<longleftrightarrow> I \<Turnstile>s N \<union> {C + {#L#}}" using true_clss_def by fastforce
next 
  fix N N'
  assume " simplifier N N'"
  and "total_over_m I N"
  and "consistent_interp I"
  thus "(I \<Turnstile>s N) = (I \<Turnstile>s N')" using simplifier_preserves_un_sat by metis
qed

lemma simplifier_preserves_atms_of_m:
  fixes N N' :: "'v propo"
  assumes "simplifier N N'"
  shows "atms_of_m N' \<subseteq> atms_of_m N"
  using assms apply (induct rule: simplifier.induct, auto simp add: atms_of_m_remove_incl)
  by (smt Set.set_insert Un_left_absorb atms_of_m_plus atms_of_m_union inf_sup_aci(6) insert_Diff_single insert_is_Un remove_def)

  
lemma inference_preserves_atms_of_m:
  fixes N N' :: "'v propo"
  assumes "inference N N'"
  shows "atms_of_m N' \<subseteq> atms_of_m N"
  using assms apply (induct rule: inference.induct, auto simp add: simplifier_preserves_atms_of_m)
  unfolding atms_of_m_def by fastforce+

lemma inference_preserves_total:
  fixes N N' :: "'v propo"
  assumes "inference N N'"
  shows "total_over_m I N \<Longrightarrow> total_over_m I N'"
  using assms inference_preserves_atms_of_m unfolding total_over_m_def total_over_set_def by auto 

lemma rtranclp_inference_preserves_total:
  fixes N N' :: "'v propo"
  assumes "rtranclp inference N N'"
  shows "total_over_m I N \<Longrightarrow> total_over_m I N'"
  using assms by (induct rule: rtranclp.induct, auto simp add: inference_preserves_total)


  
lemma rtranclp_inference_preserves_un_sat:
  assumes "rtranclp inference N N'"
  and  "total_over_m I N"
  and consistent: "consistent_interp I"
  shows "I \<Turnstile>s N \<longleftrightarrow> I \<Turnstile>s N'"
  using assms apply (induct rule: rtranclp.induct)
  apply (simp add: inference_preserves_un_sat)
  using inference_preserves_un_sat rtranclp_inference_preserves_total by blast


lemma simplifier_preserves_finite:
  assumes "simplifier \<psi> \<psi>'" and "finite \<psi>"
  shows "finite \<psi>'"
  using assms by (induct rule: simplifier.induct, simp_all add: remove_def)
  
lemma inference_preserves_finite:
  assumes "inference\<^sup>*\<^sup>* \<psi> \<psi>'" and "finite \<psi>"
  shows "finite \<psi>'"
  using assms apply (induct rule: rtranclp.induct, auto simp add: simplifier_preserves_finite)
  by (metis (no_types, hide_lams) Un_insert_right finite.simps inference.simps simplifier_preserves_finite sup_bot.right_neutral)


lemma simplifier_preserves_unsat:
  assumes "simplifier \<psi> \<psi>'" and "unsatisfiable \<psi>"
  shows "unsatisfiable \<psi>'"
  unfolding true_clss_def apply auto
  using assms 
proof (induct rule:simplifier.induct) 
  case (subsumption A B I) note H = this
  obtain a where "a\<in>Set.remove B \<psi> \<and> I \<Turnstile> a" using H by fastforce
  hence "a \<in> \<psi>  \<and> I \<Turnstile> a" by auto
  hence "\<not> consistent_interp I" using H(4,5,6,7) unfolding true_clss_def  sorry
  thus False using H(5) subsumption.prems(1) by blast
next
  case (condensation A P I) note H = this
  obtain a where "a \<in> \<psi> \<or> a = A + {#Pos P#}" and I: "I \<Turnstile> a" using H by fastforce
  also {
    assume a: "a = A + {#Pos P#}"
    have "I \<Turnstile> A + {#Pos P#} + {#Pos P#}" using I unfolding a true_cls_def true_lit_def by auto 
    hence "\<not> consistent_interp I" using H(5) unfolding true_clss_def  sorry
    hence False using H(3) condensation.prems(1) by metis
  }
  moreover {
    assume "a \<in> \<psi>"
    hence "\<not> consistent_interp I" using condensation.prems unfolding true_clss_def  sorry
    hence False using H(3) condensation.prems(1) by metis    
  }
  ultimately show ?case by auto
next
  case (tautology_deletion A P I) note H = this
  {
     assume \<psi>: "\<psi> = {A + {#Pos P#} + {#Neg P#}}"
     hence "{Pos P} \<Turnstile> A + {#Pos P#} + {#Neg P#}" unfolding true_cls_def by auto
     also have "consistent_interp {Pos P}" unfolding consistent_interp_def by auto
     ultimately have False using H(4) unfolding \<psi> sorry
  }
  also{
     assume \<psi>: "\<psi> \<noteq> {A + {#Pos P#} + {#Neg P#}}"
     hence "{Pos P} \<Turnstile> A + {#Pos P#} + {#Neg P#}" unfolding true_cls_def by auto
     have "\<not> consistent_interp I"  using H(2,4) \<psi> unfolding true_clss_def sorry
     hence False using H(2) by metis
  }
  ultimately show False by auto
qed

lemma inference_preserves_unsat:
  assumes "inference\<^sup>*\<^sup>* \<psi> \<psi>'" and "unsatisfiable \<psi>"
  shows "unsatisfiable \<psi>'"
  using assms apply (induct rule: rtranclp.induct)
  apply auto
  using inference.cases simplifier_preserves_unsat by (metis total_over_m_union true_clss_union) 



datatype 'v sem_tree = Node "'v" "'v sem_tree" "'v sem_tree" | Leaf

fun sem_tree_size :: "'v sem_tree \<Rightarrow> nat" where
"sem_tree_size (Leaf) = 0" |
"sem_tree_size (Node _ ag ad) = 1 + sem_tree_size ag + sem_tree_size ad"

lemma sem_tree_size:
    "(\<And>xs:: 'v sem_tree. \<forall>ys:: 'v sem_tree. sem_tree_size ys < sem_tree_size xs \<longrightarrow> P ys \<Longrightarrow> P xs) \<Longrightarrow> P xs"
  by (fact measure_induct)


fun partial_interps :: "'v sem_tree \<Rightarrow> 'v interp \<Rightarrow> 'v propo \<Rightarrow> bool" where
"partial_interps Leaf I \<psi> = (\<exists>\<chi>. \<not> I \<Turnstile> \<chi> \<and> \<chi> \<in> \<psi> \<and> total_over_m I {\<chi>})" |
"partial_interps (Node v ag ad) I \<psi> = (partial_interps ag (I \<union> {Pos v}) \<psi> \<and> partial_interps ad (I\<union> {Neg v}) \<psi>)"

lemma simplifier_preserve_partial_tree:
  assumes "simplifier N N'"
  and "partial_interps t I N"
  shows "partial_interps t I N'"
  using assms apply (induct t arbitrary: I)
  apply auto
  using simplifier.cases 
sorry
lemma inference_preserve_partial_tree:
  assumes "inference N N'"
  and "partial_interps t I N"
  shows "partial_interps t I N'"
  using assms apply (induct t arbitrary: I, auto)
  using inference.cases
sledgehammer[e, verbose, overlord, isar_proof, dont_minimize] (inference.cases Un_iff partial_interps.simps(1) simplifier_preserve_partial_tree)
using Un_iff partial_interps.simps(1) simplifier_preserve_partial_tree by blast

(*TODO Jasmin "e": Try this: by (metis Un_iff partial_interps.simps(1) simplifier_preserve_partial_tree) (> 1.0 s, timed out).
Warning: Isar proof construction failed. 

"e": Try this: using Un_iff partial_interps.simps(1) simplifier_preserve_partial_tree by blast (1.2 s).
Warning: Isar proof construction failed.
\<rightarrow> timing gives 10s *)

end