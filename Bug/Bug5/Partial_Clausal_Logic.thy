(*  Copy PAste of:
    Title:       Clausal Logic
    Author:      Jasmin Blanchette <blanchette at in.tum.de>, 2014
    Author:      Dmitriy Traytel <traytel at in.tum.de>, 2014
    Maintainer:  Jasmin Blanchette <blanchette at in.tum.de>
*)

header {* Clausal Logic *}

theory Partial_Clausal_Logic
imports Multiset_More
begin

text {*
Resolution operates of clauses, which are disjunctions of literals. The material formalized here
corresponds roughly to Sections 2.1 (``Formulas and Clauses'') and 2.2 (``Herbrand
Interpretations'') of Bachmair and Ganzinger, excluding the formula and term syntax.
*}


subsection {* Literals *}

text {*
Literals consist of a polarity (positive or negative) and an atom, of type @{typ 'a}.
*}

datatype 'a literal =
  is_pos: Pos (atm_of: 'a)
| Neg (atm_of: 'a)

abbreviation is_neg :: "'a literal \<Rightarrow> bool" where "is_neg L \<equiv> \<not> is_pos L"

lemma Pos_atm_of_iff[simp]: "Pos (atm_of L) = L \<longleftrightarrow> is_pos L"
  by auto (metis literal.disc(1))

lemma Neg_atm_of_iff[simp]: "Neg (atm_of L) = L \<longleftrightarrow> is_neg L"
  by auto (metis literal.disc(2))

lemma ex_lit_cases: "(\<exists>L. P L) \<longleftrightarrow> (\<exists>A. P (Pos A) \<or> P (Neg A))"
  by (metis literal.exhaust)

instantiation literal :: (type) uminus
begin

definition uminus_literal :: "'a literal \<Rightarrow> 'a literal" where
  "uminus L = (if is_pos L then Neg else Pos) (atm_of L)"

instance ..

end

lemma
  uminus_Pos[simp]: "- Pos A = Neg A" and
  uminus_Neg[simp]: "- Neg A = Pos A"
  unfolding uminus_literal_def by simp_all

instantiation literal :: (preorder) preorder
begin

definition less_literal :: "'a literal \<Rightarrow> 'a literal \<Rightarrow> bool" where
  "less_literal L M \<longleftrightarrow> atm_of L < atm_of M \<or> atm_of L \<le> atm_of M \<and> is_neg L < is_neg M"

definition less_eq_literal :: "'a literal \<Rightarrow> 'a literal \<Rightarrow> bool" where
  "less_eq_literal L M \<longleftrightarrow> atm_of L < atm_of M \<or> atm_of L \<le> atm_of M \<and> is_neg L \<le> is_neg M"

instance
  apply intro_classes
  unfolding less_literal_def less_eq_literal_def by (auto intro: order_trans simp: less_le_not_le)

end

instantiation literal :: (order) order
begin

instance
  apply intro_classes
  unfolding less_eq_literal_def by (auto intro: literal.expand)

end

lemma pos_less_neg[simp]: "Pos A < Neg A"
  unfolding less_literal_def by simp

lemma pos_less_pos_iff[simp]: "Pos A < Pos B \<longleftrightarrow> A < B"
  unfolding less_literal_def by simp

lemma pos_less_neg_iff[simp]: "Pos A < Neg B \<longleftrightarrow> A \<le> B"
  unfolding less_literal_def by (auto simp: less_le_not_le)

lemma neg_less_pos_iff[simp]: "Neg A < Pos B \<longleftrightarrow> A < B"
  unfolding less_literal_def by simp

lemma neg_less_neg_iff[simp]: "Neg A < Neg B \<longleftrightarrow> A < B"
  unfolding less_literal_def by simp

lemma pos_le_neg[simp]: "Pos A \<le> Neg A"
  unfolding less_eq_literal_def by simp

lemma pos_le_pos_iff[simp]: "Pos A \<le> Pos B \<longleftrightarrow> A \<le> B"
  unfolding less_eq_literal_def by (auto simp: less_le_not_le)

lemma pos_le_neg_iff[simp]: "Pos A \<le> Neg B \<longleftrightarrow> A \<le> B"
  unfolding less_eq_literal_def by (auto simp: less_imp_le)

lemma neg_le_pos_iff[simp]: "Neg A \<le> Pos B \<longleftrightarrow> A < B"
  unfolding less_eq_literal_def by simp

lemma neg_le_neg_iff[simp]: "Neg A \<le> Neg B \<longleftrightarrow> A \<le> B"
  unfolding less_eq_literal_def by (auto simp: less_imp_le)

lemma leq_imp_less_eq_atm_of: "L \<le> M \<Longrightarrow> atm_of L \<le> atm_of M"
  by (metis less_eq_literal_def less_le_not_le)

instantiation literal :: (linorder) linorder
begin

instance
  apply intro_classes
  unfolding less_eq_literal_def less_literal_def by auto

end

instantiation literal :: (wellorder) wellorder
begin

instance
proof intro_classes
  fix P :: "'a literal \<Rightarrow> bool" and L :: "'a literal"
  assume ih: "\<And>L. (\<And>M. M < L \<Longrightarrow> P M) \<Longrightarrow> P L"
  have "\<And>x. (\<And>y. y < x \<Longrightarrow> P (Pos y) \<and> P (Neg y)) \<Longrightarrow> P (Pos x) \<and> P (Neg x)"
    by (rule conjI[OF ih ih])
      (auto simp: less_literal_def atm_of_def split: literal.splits intro: ih)
  hence "\<And>A. P (Pos A) \<and> P (Neg A)"
    by (rule less_induct) blast
  thus "P L"
    by (cases L) simp+
qed

end


subsection {* Clauses *}

text {*
Clauses are (finite) multisets of literals.
*}

type_synonym 'a clause = "'a literal multiset"
type_synonym 'v propo = "'v clause set"


abbreviation poss :: "'a multiset \<Rightarrow> 'a clause" where "poss AA \<equiv> {#Pos A. A \<in># AA#}"
abbreviation negs :: "'a multiset \<Rightarrow> 'a clause" where "negs AA \<equiv> {#Neg A. A \<in># AA#}"

lemma image_replicate_mset[simp]: "{#f A. A \<in># replicate_mset n A#} = replicate_mset n (f A)"
  by (induct n) (simp, subst replicate_mset_Suc, simp)

lemma Max_in_lits: "C \<noteq> {#} \<Longrightarrow> Max (set_of C) \<in># C"
  by (rule Max_in[OF finite_set_of, unfolded mem_set_of_iff set_of_eq_empty_iff])

lemma Max_atm_of_set_of_commute: "C \<noteq> {#} \<Longrightarrow> Max (atm_of ` set_of C) = atm_of (Max (set_of C))"
  by (rule mono_Max_commute[symmetric])
    (auto simp: mono_def atm_of_def less_eq_literal_def less_literal_def)

lemma Max_pos_neg_less_multiset:
  assumes max: "Max (set_of C) = Pos A" and neg: "Neg A \<in># D"
  shows "C \<subset># D"
proof -
  have "Max (set_of C) < Neg A"
    using max by simp
  thus ?thesis
    using neg by (metis (no_types) ex_gt_imp_less_multiset Max_less_iff[OF finite_set_of]
      all_not_in_conv mem_set_of_iff)
qed

lemma pos_Max_imp_neg_notin: "Max (set_of C) = Pos A \<Longrightarrow> \<not> Neg A \<in># C"
  using Max_pos_neg_less_multiset[unfolded multiset_linorder.not_le[symmetric]] by blast

lemma less_eq_Max_lit: "C \<noteq> {#} \<Longrightarrow> C \<subseteq># D \<Longrightarrow> Max (set_of C) \<le> Max (set_of D)"
proof (unfold le_multiset\<^sub>H\<^sub>O)
  assume ne: "C \<noteq> {#}" and ex_gt: "\<forall>x. count D x < count C x \<longrightarrow> (\<exists>y > x. count C y < count D y)"
  from ne have "Max (set_of C) \<in># C"
    by (fast intro: Max_in_lits)
  hence "\<exists>l. l \<in># D \<and> \<not> l < Max (set_of C)"
    using ex_gt by (metis not_less0 not_less_iff_gr_or_eq)
  hence "\<not> Max (set_of D) < Max (set_of C)"
    by (metis Max.coboundedI[OF finite_set_of] le_less_trans mem_set_of_iff)
  thus ?thesis
    by simp
qed

definition atms_of :: "'a clause \<Rightarrow> 'a set" where
  "atms_of C = atm_of ` set_of C"

lemma atms_of_empty[simp]: "atms_of {#} = {}"
  unfolding atms_of_def by simp

lemma atms_of_singleton[simp]: "atms_of {#L#} = {atm_of L}"
  unfolding atms_of_def by auto

lemma finite_atms_of[iff]: "finite (atms_of C)"
  unfolding atms_of_def by simp

lemma atm_of_lit_in_atms_of: "L \<in># C \<Longrightarrow> atm_of L \<in> atms_of C"
  unfolding atms_of_def by simp

lemma atms_of_plus[simp]: "atms_of (C + D) = atms_of C \<union> atms_of D"
  unfolding atms_of_def image_def by auto

lemma pos_lit_in_atms_of: "Pos A \<in># C \<Longrightarrow> A \<in> atms_of C"
  unfolding atms_of_def by (metis image_iff literal.sel(1) mem_set_of_iff)

lemma neg_lit_in_atms_of: "Neg A \<in># C \<Longrightarrow> A \<in> atms_of C"
  unfolding atms_of_def by (metis image_iff literal.sel(2) mem_set_of_iff)

lemma atm_imp_pos_or_neg_lit: "A \<in> atms_of C \<Longrightarrow> Pos A \<in># C \<or> Neg A \<in># C"
  unfolding atms_of_def image_def mem_Collect_eq
  by (metis Neg_atm_of_iff Pos_atm_of_iff mem_set_of_iff)

lemma atm_iff_pos_or_neg_lit: "A \<in> atms_of L \<longleftrightarrow> Pos A \<in># L \<or> Neg A \<in># L"
  by (auto intro: pos_lit_in_atms_of neg_lit_in_atms_of dest: atm_imp_pos_or_neg_lit)

lemma lits_subseteq_imp_atms_subseteq: "set_of C \<subseteq> set_of D \<Longrightarrow> atms_of C \<subseteq> atms_of D"
  unfolding atms_of_def by blast

lemma atms_empty_iff_empty: "atms_of C = {} \<longleftrightarrow> C = {#}"
  unfolding atms_of_def image_def Collect_empty_eq
  by (metis all_not_in_conv set_of_eq_empty_iff)

lemma
  atms_of_poss[simp]: "atms_of (poss AA) = set_of AA" and
  atms_of_negg[simp]: "atms_of (negs AA) = set_of AA"
  unfolding atms_of_def image_def by auto

lemma less_eq_Max_atms_of: "C \<noteq> {#} \<Longrightarrow> C \<subseteq># D \<Longrightarrow> Max (atms_of C) \<le> Max (atms_of D)"
  unfolding atms_of_def
  by (metis Max_atm_of_set_of_commute le_multiset_empty_right leq_imp_less_eq_atm_of
    less_eq_Max_lit)

lemma le_multiset_Max_in_imp_Max:
  "Max (atms_of D) = A \<Longrightarrow> C \<subseteq># D \<Longrightarrow> A \<in> atms_of C \<Longrightarrow> Max (atms_of C) = A"
  by (metis Max.coboundedI[OF finite_atms_of] atms_of_def empty_iff eq_iff image_subsetI
    less_eq_Max_atms_of set_of_empty subset_Compl_self_eq)

lemma atm_of_Max_lit[simp]: "C \<noteq> {#} \<Longrightarrow> atm_of (Max (set_of C)) = Max (atms_of C)"
  unfolding atms_of_def Max_atm_of_set_of_commute ..

lemma Max_lit_eq_pos_or_neg_Max_atm:
  "C \<noteq> {#} \<Longrightarrow> Max (set_of C) = Pos (Max (atms_of C)) \<or> Max (set_of C) = Neg (Max (atms_of C))"
  by (metis Neg_atm_of_iff Pos_atm_of_iff atm_of_Max_lit)

lemma atms_less_imp_lit_less_pos: "(\<And>B. B \<in> atms_of C \<Longrightarrow> B < A) \<Longrightarrow> L \<in># C \<Longrightarrow> L < Pos A"
  unfolding atms_of_def less_literal_def by force

lemma atms_less_eq_imp_lit_less_eq_neg: "(\<And>B. B \<in> atms_of C \<Longrightarrow> B \<le> A) \<Longrightarrow> L \<in># C \<Longrightarrow> L \<le> Neg A"
  unfolding less_eq_literal_def by (simp add: atm_of_lit_in_atms_of)

subsection {* Herbrand Interpretations *}

text {*
A Herbrand interpretation is a set of ground atoms that are to be considered true.
*}

type_synonym 'a interp = "'a literal set"

definition true_lit :: "'a interp \<Rightarrow> 'a literal \<Rightarrow> bool" (infix "\<Turnstile>l" 50) where
  "I \<Turnstile>l L \<longleftrightarrow> L \<in> I"

declare true_lit_def[simp]

definition consistent_interp :: "'a literal set \<Rightarrow> bool" where
"consistent_interp I = (\<forall>L. \<not>(L \<in> I \<and> - L \<in> I))"

definition total_over_set :: "'a interp \<Rightarrow> 'a set \<Rightarrow> bool" where
"total_over_set I S = (\<forall>l\<in>S. Pos l \<in> I \<or> Neg l \<in> I)"


definition true_cls :: "'a interp \<Rightarrow> 'a clause \<Rightarrow> bool" (infix "\<Turnstile>" 50) where
  "I \<Turnstile> C \<longleftrightarrow> (\<exists>L. L \<in># C \<and> I \<Turnstile>l L)"

lemma true_cls_empty[iff]: "\<not> I \<Turnstile> {#}"
  unfolding true_cls_def by simp

lemma true_cls_singleton[iff]: "I \<Turnstile> {#L#} \<longleftrightarrow> I \<Turnstile>l L"
  unfolding true_cls_def by simp

lemma true_cls_union[iff]: "I \<Turnstile> C + D \<longleftrightarrow> I \<Turnstile> C \<or> I \<Turnstile> D"
  unfolding true_cls_def by auto

lemma true_cls_mono: "set_of C \<subseteq> set_of D \<Longrightarrow> I \<Turnstile> C \<Longrightarrow> I \<Turnstile> D"
  unfolding true_cls_def subset_eq by (metis mem_set_of_iff)


lemma true_cls_replicate_mset[iff]: "I \<Turnstile> replicate_mset n L \<longleftrightarrow> n \<noteq> 0 \<and> I \<Turnstile>l L"
  by (induct n) auto


definition true_clss :: "'a interp \<Rightarrow> 'a clause set \<Rightarrow> bool" (infix "\<Turnstile>s" 50) where
  "I \<Turnstile>s CC \<longleftrightarrow> (\<forall>C \<in> CC. I \<Turnstile> C)"

lemma true_clss_empty[iff]: "I \<Turnstile>s {}"
  unfolding true_clss_def by blast

lemma true_clss_singleton[iff]: "I \<Turnstile>s {C} \<longleftrightarrow> I \<Turnstile> C"
  unfolding true_clss_def by blast

lemma true_clss_union[iff]: "I \<Turnstile>s CC \<union> DD \<longleftrightarrow> I \<Turnstile>s CC \<and> I \<Turnstile>s DD"
  unfolding true_clss_def by blast

lemma true_clss_mono: "DD \<subseteq> CC \<Longrightarrow> I \<Turnstile>s CC \<Longrightarrow> I \<Turnstile>s DD"
  unfolding true_clss_def by blast

definition atms_of_m :: "'a literal multiset set \<Rightarrow> 'a set" where
"atms_of_m \<psi>s = {(atm_of \<chi>) | \<chi>. \<exists>\<psi>. \<psi> \<in> \<psi>s \<and> \<chi> :# \<psi>}"

definition total_over_m  :: "'a literal set \<Rightarrow> 'a clause set \<Rightarrow> bool" where
"total_over_m I \<psi>s = total_over_set I (atms_of_m \<psi>s)"

abbreviation satisfiable :: "'a clause set \<Rightarrow> bool" where
  "satisfiable CC \<equiv> \<exists>I. (I \<Turnstile>s CC \<and> consistent_interp I \<and> total_over_m I CC)"

abbreviation unsatisfiable :: "'a clause set \<Rightarrow> bool" where  
  "unsatisfiable CC \<equiv> \<not> satisfiable CC"

definition true_cls_mset :: "'a interp \<Rightarrow> 'a clause multiset \<Rightarrow> bool" (infix "\<Turnstile>m" 50) where
  "I \<Turnstile>m CC \<longleftrightarrow> (\<forall>C. C \<in># CC \<longrightarrow> I \<Turnstile> C)"

lemma true_cls_mset_empty[iff]: "I \<Turnstile>m {#}"
  unfolding true_cls_mset_def by auto

lemma true_cls_mset_singleton[iff]: "I \<Turnstile>m {#C#} \<longleftrightarrow> I \<Turnstile> C"
  unfolding true_cls_mset_def by auto

lemma true_cls_mset_union[iff]: "I \<Turnstile>m CC + DD \<longleftrightarrow> I \<Turnstile>m CC \<and> I \<Turnstile>m DD"
  unfolding true_cls_mset_def by auto

lemma true_cls_mset_image_mset[iff]: "I \<Turnstile>m image_mset f A \<longleftrightarrow> (\<forall>x . x \<in># A \<longrightarrow> I \<Turnstile> f x)"
  unfolding true_cls_mset_def by auto

lemma true_cls_mset_mono: "set_of DD \<subseteq> set_of CC \<Longrightarrow> I \<Turnstile>m CC \<Longrightarrow> I \<Turnstile>m DD"
  unfolding true_cls_mset_def subset_iff by auto

lemma true_clss_set_of[iff]: "I \<Turnstile>s set_of CC \<longleftrightarrow> I \<Turnstile>m CC"
  unfolding true_clss_def true_cls_mset_def by auto



lemma in_m_in_literals:
  assumes "{#A#} + D \<in> \<psi>s"
  shows "atm_of A \<in> atms_of_m \<psi>s"
  unfolding atms_of_m_def using assms by fastforce
  

lemma multiset_not_empty:
  assumes "M \<noteq> {#}"
  and "x \<in># M"
  shows "\<exists>A. x = Pos A \<or> x = Neg A"
  using assms literal.exhaust_sel by blast

lemma atms_of_m_empty:
  fixes \<psi> :: "'v propo"
  assumes "finite \<psi>" and "atms_of_m \<psi> = {}"
  shows "\<psi> = {} \<or> \<psi> = {{#}}"
  using assms
proof (induct rule: finite_induct)
  assume "atms_of_m {} = {}"
  thus "{} = {} \<or> {} = {{#}}" by auto
next
  fix x :: "'v clause" and F :: "'v propo"
  assume "finite F"
  and "x \<notin> F"
  and ai: "atms_of_m (insert x F) = {}"
  and IH: "atms_of_m F = {} \<Longrightarrow> F = {} \<or> F = {{#}}"
  have aixF: "atms_of_m (insert x F) =  atms_of_m {x} \<union>  atms_of_m F" unfolding atms_of_m_def by auto
  hence "atms_of_m {x} = {}" using ai by auto
  hence "x = {#}" unfolding atms_of_m_def by (auto simp add: multiset_eq_iff)

  also have "F = {} \<or> F = {{#}}" using aixF ai IH by auto
  ultimately show "insert x F = {} \<or> insert x F = {{#}}" by auto
qed


lemma total_over_set_literal_defined:
  assumes "{#A#} + D \<in> \<psi>s"
  and "total_over_set I (atms_of_m \<psi>s)"
  shows "A \<in> I \<or> -A \<in> I"
  using assms unfolding total_over_set_def by (metis (no_types) Neg_atm_of_iff in_m_in_literals literal.collapse(1) uminus_Neg uminus_Pos)


lemma model_remove: "I \<Turnstile>s N \<Longrightarrow> I \<Turnstile>s Set.remove a N"
by (simp add: true_clss_def)

lemma subsumption_imp_eval:
  assumes "A \<le> B"
  shows "I \<Turnstile> A \<Longrightarrow> I \<Turnstile> B"
  using assms by (metis mset_le_exists_conv true_cls_union)

  
lemma atms_of_m_union[simp]:
  "atms_of_m (\<psi>s \<union> \<chi>s) = atms_of_m \<psi>s \<union> atms_of_m \<chi>s"
  unfolding atms_of_m_def by auto

lemma atms_of_m_insert:
  "atms_of_m (insert \<psi>s \<chi>s) = atms_of_m {\<psi>s} \<union> atms_of_m \<chi>s"
  unfolding atms_of_m_def by auto
  
lemma atms_of_m_plus[simp]:
  fixes C D :: "'a literal multiset"
  shows "atms_of_m {C + D} = atms_of_m {C} \<union> atms_of_m {D}"
  unfolding atms_of_m_def by auto

lemma [simp]:
  "atms_of_m {\<chi>} = atms_of \<chi>"
  unfolding atms_of_m_def atms_of_def by auto

  
lemma tot_over_m_remove:  
  assumes "total_over_m (I \<union> {L}) {\<psi>}"
  and L: "\<not> L \<in># \<psi> \<and> \<not> -L \<in># \<psi>"
  shows "total_over_m I {\<psi>}"
  unfolding total_over_m_def total_over_set_def
proof 
  fix l
  assume l: "l \<in> atms_of_m {\<psi>}"
  hence "Pos l \<in> I \<or> Neg l \<in> I \<or> l = atm_of L"
     using assms unfolding total_over_m_def total_over_set_def by auto
  also have "atm_of L \<notin> atms_of_m {\<psi>}" 
    proof (rule ccontr)
      assume "\<not> atm_of L \<notin> atms_of_m {\<psi>}"
      hence "atm_of L \<in> atms_of \<psi>" by auto
      hence "Pos (atm_of L) \<in># \<psi> \<or> Neg (atm_of L) \<in># \<psi>" using atm_imp_pos_or_neg_lit by metis
      hence "L \<in># \<psi> \<or> - L \<in># \<psi>" by (case_tac L, auto)
      thus False using L by auto
    qed   
  ultimately show  "Pos l \<in> I \<or> Neg l \<in> I" using l by metis
qed

lemma total_over_m_sum:
  shows "total_over_m I {C + D} \<longleftrightarrow> (total_over_m I {C} \<and> total_over_m I {D})"
  using assms unfolding total_over_m_def total_over_set_def by auto

lemma total_over_m_union:
  "total_over_m I (A \<union> B) \<longleftrightarrow> (total_over_m I A \<and> total_over_m I B)"
  unfolding total_over_m_def total_over_set_def by auto

lemma atms_of_m_remove_incl: 
  shows "atms_of_m (Set.remove a \<psi>) \<subseteq> atms_of_m \<psi>"
  unfolding atms_of_m_def unfolding remove_def by auto
end
