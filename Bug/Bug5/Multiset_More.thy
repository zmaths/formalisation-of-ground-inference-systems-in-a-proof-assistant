(*  Title:       More about Multisets
    Author:      Jasmin Blanchette <blanchette at in.tum.de>, 2014
    Author:      Dmitriy Traytel <traytel at in.tum.de>, 2014
    Maintainer:  Jasmin Blanchette <blanchette at in.tum.de>
*)

header {* More about Multisets *}

theory Multiset_More
imports "~~/src/HOL/Library/Multiset"
begin

text {*
Isabelle's theory of finite multisets is not as developed as other areas, such as lists and sets.
The present theory introduces some missing concepts and lemmas. Most of it is expected to move to
Isabelle's library.
*}


subsection {* Miscellaneous Definitions and Facts *}

text {*
This subsection gathers various definitions and lemmas about multisets.
*}

syntax (xsymbols)
  "_comprehension2_mset" :: "'a \<Rightarrow> 'b \<Rightarrow> 'b multiset \<Rightarrow> 'a multiset" ("({#_/. _ \<in># _#})")
translations
  "{#e. x \<in># M#}" == "CONST image_mset (\<lambda>x. e) M"

lemma elem_multiset_of_set[simp, intro]: "finite A \<Longrightarrow> x \<in># multiset_of_set A \<longleftrightarrow> x \<in> A"
 by (induct A rule: finite_induct) simp_all

declare
  image_mset.compositionality[simp]
  image_mset.id[simp]

lemma image_mset_id[simp]: "image_mset id x = x"
  unfolding id_def by auto

lemma image_mset_cong[cong]: "(\<And>x. x \<in># \<Sigma> \<Longrightarrow> f x = g x) \<Longrightarrow> {#f x. x \<in># \<Sigma>#} = {#g x. x \<in># \<Sigma>#}"
  by (induct \<Sigma>) auto

lemma image_mset_cong_pair:
  "(\<forall>x y. (x, y) \<in># \<Sigma> \<longrightarrow> f x y = g x y) \<Longrightarrow> {#f x y. (x, y) \<in># \<Sigma>#} = {#g x y. (x, y) \<in># \<Sigma>#}"
  by (metis image_mset_cong split_cong)

lemma comp_fun_commute_plus_mset[simp]: "comp_fun_commute (op + \<Colon> 'a multiset \<Rightarrow> _ \<Rightarrow> _)"
  by default (simp add: add_ac comp_def)

declare comp_fun_commute.fold_mset_insert[OF comp_fun_commute_plus_mset, simp]

lemma in_mset_fold_plus_iff[iff]: "x \<in># Multiset.fold (op +) \<Sigma> TT \<longleftrightarrow> x \<in># \<Sigma> \<or> (\<exists>T. T \<in># TT \<and> x \<in># T)"
  by (induct TT) auto

abbreviation Union_mset :: "'a multiset multiset \<Rightarrow> 'a multiset" where
  "Union_mset \<Sigma>\<Sigma> \<equiv> msetsum \<Sigma>\<Sigma>"

notation (xsymbols) Union_mset ("\<Union>#_" [900] 900)

lemma set_of_Union_mset[simp]: "set_of (\<Union># \<Sigma>\<Sigma>) = (\<Union>\<Sigma> \<in> set_of \<Sigma>\<Sigma>. set_of \<Sigma>)"
  by (induct \<Sigma>\<Sigma>) auto

lemma in_Union_mset_iff[iff]: "x \<in># \<Union># \<Sigma>\<Sigma> \<longleftrightarrow> (\<exists>\<Sigma>. \<Sigma> \<in># \<Sigma>\<Sigma> \<and> x \<in># \<Sigma>)"
  by (induct \<Sigma>\<Sigma>) auto

lemma msetsum_diff:
  fixes \<Sigma>\<Sigma> TT :: "('a \<Colon> ordered_cancel_comm_monoid_diff) multiset"
  shows "TT \<le> \<Sigma>\<Sigma> \<Longrightarrow> msetsum (\<Sigma>\<Sigma> - TT) = msetsum \<Sigma>\<Sigma> - msetsum TT"
  by (metis add_diff_cancel_left' msetsum.union
    ordered_cancel_comm_monoid_diff_class.add_diff_inverse)

definition replicate_mset :: "nat \<Rightarrow> 'a \<Rightarrow> 'a multiset" where
  "replicate_mset n x = ((op + {#x#}) ^^ n) {#}"

lemma replicate_mset_0[simp]: "replicate_mset 0 x = {#}"
  unfolding replicate_mset_def by simp

lemma replicate_mset_Suc[simp]: "replicate_mset (Suc n) x = replicate_mset n x + {#x#}"
  unfolding replicate_mset_def by (induct n) (auto intro: add.commute)

lemma in_replicate_mset[simp]: "x \<in># replicate_mset n y \<longleftrightarrow> n > 0 \<and> x = y"
  unfolding replicate_mset_def by (induct n) simp_all

lemma count_replicate_mset[simp]: "count (replicate_mset n x) y = (if y = x then n else 0)"
  unfolding replicate_mset_def by (induct n) simp_all

lemma set_of_replicate_mset_subset[simp]:
  "set_of (replicate_mset n x) = (if n = 0 then {} else {x})"
  by (auto split: if_splits)

lemma filter_mset_less[simp]: "{#x \<in># \<Sigma>. P x#} \<le> \<Sigma>"
  by (simp add: mset_less_eqI)

lemma filter_eq_replicate_mset: "{#y \<in># D. y = x#} = replicate_mset (count D x) x"
  by (induct D) simp_all

lemma ball_set_of_iff[simp]: "(\<forall>x \<in> set_of \<Sigma>. P x) \<longleftrightarrow> (\<forall>x. x \<in># \<Sigma> \<longrightarrow> P x)"
  by auto

lemma in_image_mset[iff]: "y \<in># {#f x. x \<in># \<Sigma>#} \<longleftrightarrow> y \<in> f ` set_of \<Sigma>"
  by (metis mem_set_of_iff set_of_image_mset)

lemma mcard_1_singleton:
  assumes card: "mcard AA = 1"
  shows "\<exists>A. AA = {#A#}"
  using card by (cases AA) auto


subsection {* Multiset Ordering *}

text {*
Isabelle's multiset theory introduces the multiset ordering, as @{term "op \<subset>#"} (and
@{term "op \<subseteq>#"}), but proves few facts to work with it. This subsection proves various lemmas,
including alternative characterizations, in a general fashion so that it can work with other
underlying orderings than @{term "op <"}.
*}

context order
begin

lemma reflp_le: "reflp (op \<le>)"
  unfolding reflp_def by simp

lemma antisymP_le: "antisymP (op \<le>)"
  unfolding antisym_def by auto

lemma transp_le: "transp (op \<le>)"
  unfolding transp_def by auto

lemma irreflp_less: "irreflp (op <)"
  unfolding irreflp_def by simp

lemma antisymP_less: "antisymP (op <)"
  unfolding antisym_def by auto

lemma transp_less: "transp (op <)"
  unfolding transp_def by auto

lemmas le_trans = transp_le[unfolded transp_def, rule_format]

lemma order_mult: "class.order
  (\<lambda>\<Sigma> T. (\<Sigma>, T) \<in> mult {(x, y). x < y} \<or> \<Sigma> = T)
  (\<lambda>\<Sigma> T. (\<Sigma>, T) \<in> mult {(x, y). x < y})"
  (is "class.order ?le ?less")
proof -
  have irrefl: "\<And>M :: 'a multiset. \<not> ?less M M"
  proof
    fix M :: "'a multiset"
    have "trans {(x'::'a, x). x' < x}"
      by (rule transI) simp
    moreover
    assume "(M, M) \<in> mult {(x, y). x < y}"
    ultimately have "\<exists>I J K. M = I + J \<and> M = I + K
      \<and> J \<noteq> {#} \<and> (\<forall>k\<in>set_of K. \<exists>j\<in>set_of J. (k, j) \<in> {(x, y). x < y})"
      by (rule mult_implies_one_step)
    then obtain I J K where "M = I + J" and "M = I + K"
      and "J \<noteq> {#}" and "(\<forall>k\<in>set_of K. \<exists>j\<in>set_of J. (k, j) \<in> {(x, y). x < y})" by blast
    then have aux1: "K \<noteq> {#}" and aux2: "\<forall>k\<in>set_of K. \<exists>j\<in>set_of K. k < j" by auto
    have "finite (set_of K)" by simp
    moreover note aux2
    ultimately have "set_of K = {}"
      by (induct rule: finite_induct)
       (simp, metis (mono_tags) insert_absorb insert_iff insert_not_empty less_irrefl less_trans)
    with aux1 show False by simp
  qed
  have trans: "\<And>K M N :: 'a multiset. ?less K M \<Longrightarrow> ?less M N \<Longrightarrow> ?less K N"
    unfolding mult_def by (blast intro: trancl_trans)
  show "class.order ?le ?less"
    by default (auto simp add: le_multiset_def irrefl dest: trans)
qed

text {* The Dershowitz--Manna ordering: *}

definition less_multiset\<^sub>D\<^sub>M where
  "less_multiset\<^sub>D\<^sub>M \<Sigma> T \<longleftrightarrow>
   (\<exists>X Y. X \<noteq> {#} \<and> X \<le> T \<and> \<Sigma> = (T - X) + Y \<and> (\<forall>k. k \<in># Y \<longrightarrow> (\<exists>a. a \<in># X \<and> k < a)))"


text {* The Huet--Oppen ordering: *}

definition less_multiset\<^sub>H\<^sub>O where
  "less_multiset\<^sub>H\<^sub>O \<Sigma> T \<longleftrightarrow> \<Sigma> \<noteq> T \<and> (\<forall>y. count T y < count \<Sigma> y \<longrightarrow> (\<exists>x. y < x \<and> count \<Sigma> x < count T x))"

lemma mult_imp_less_multiset\<^sub>H\<^sub>O: "(\<Sigma>, T) \<in> mult {(x, y). x < y} \<Longrightarrow> less_multiset\<^sub>H\<^sub>O \<Sigma> T"
proof (unfold mult_def less_multiset\<^sub>H\<^sub>O_def, induct rule: trancl_induct)
  case (base U)
  then show ?case unfolding mult1_def by force
next
  case (step T U)
  from step(2) obtain M0 a K where
    *: "U = M0 + {#a#}" "T = M0 + K" "\<And>b. b \<in># K \<Longrightarrow> b < a"
    unfolding mult1_def by blast
  then have count_K_a: "count K a = 0" by auto
  with step(3) *(1,2) have "\<Sigma> \<noteq> U" by (force dest: *(3) split: if_splits)
  moreover
  { assume "count U a \<le> count \<Sigma> a"
    with count_K_a have "count T a < count \<Sigma> a" unfolding *(1,2) by auto
      with step(3) obtain z where z: "z > a" "count \<Sigma> z < count T z" by blast
      with * have "count T z \<le> count U z" by force
      with z have "\<exists>z > a. count \<Sigma> z < count U z" by auto
  } note count_a = this
  { fix y
    assume count_y: "count U y < count \<Sigma> y"
    have "\<exists>x>y. count \<Sigma> x < count U x"
    proof (cases "y = a")
      case True
      with count_y count_a show ?thesis by auto
    next
      case False
      show ?thesis
      proof (cases "y \<in># K")
        case True
        with *(3) have "y < a" by simp
        then show ?thesis by (cases "count U a \<le> count \<Sigma> a") (auto dest: count_a intro: less_trans)
      next
        case False
        with `y \<noteq> a` have "count U y = count T y" unfolding *(1,2) by simp
        with count_y step(3) obtain z where z: "z > y" "count \<Sigma> z < count T z" by auto
        show ?thesis
        proof (cases "z \<in># K")
          case True
          with *(3) have "z < a" by simp
          with z(1) show ?thesis
            by (cases "count U a \<le> count \<Sigma> a") (auto dest!: count_a intro: less_trans)
        next
          case False
          with count_K_a have "count T z \<le> count U z" unfolding * by auto
          with z show ?thesis by auto
        qed
      qed
    qed
  }
  ultimately show ?case by blast
qed

lemma less_multiset\<^sub>D\<^sub>M_imp_mult:
  "less_multiset\<^sub>D\<^sub>M \<Sigma> T \<Longrightarrow> (\<Sigma>, T) \<in> mult {(x, y). x < y}"
proof -
  assume "less_multiset\<^sub>D\<^sub>M \<Sigma> T"
  then obtain X Y where
    "X \<noteq> {#}" and "X \<le> T" and "\<Sigma> = T - X + Y" and "\<forall>k. k \<in># Y \<longrightarrow> (\<exists>a. a \<in># X \<and> k < a)"
    unfolding less_multiset\<^sub>D\<^sub>M_def by blast
  then have "(T - X + Y, T - X + X) \<in> mult {(x, y). x < y}"
    by (intro one_step_implies_mult) (auto simp: Bex_def trans_def)
  with `\<Sigma> = T - X + Y` `X \<le> T` show "(\<Sigma>, T) \<in> mult {(x, y). x < y}"
    by (metis ordered_cancel_comm_monoid_diff_class.diff_add)
qed

lemma less_multiset\<^sub>H\<^sub>O_imp_less_multiset\<^sub>D\<^sub>M: "less_multiset\<^sub>H\<^sub>O \<Sigma> T \<Longrightarrow> less_multiset\<^sub>D\<^sub>M \<Sigma> T"
unfolding less_multiset\<^sub>D\<^sub>M_def
proof (intro iffI exI conjI)
  assume "less_multiset\<^sub>H\<^sub>O \<Sigma> T"
  then obtain z where z: "count \<Sigma> z < count T z"
    unfolding less_multiset\<^sub>H\<^sub>O_def by (auto simp: multiset_eq_iff nat_neq_iff)
  def X \<equiv> "T - \<Sigma>"
  def Y \<equiv> "\<Sigma> - T"
  from z show "X \<noteq> {#}" unfolding X_def by (auto simp: multiset_eq_iff not_less_eq_eq Suc_le_eq)
  from z show "X \<le> T" unfolding X_def by auto
  show "\<Sigma> = (T - X) + Y" unfolding X_def Y_def multiset_eq_iff count_union count_diff by force
  show "\<forall>k. k \<in># Y \<longrightarrow> (\<exists>a. a \<in># X \<and> k < a)"
  proof (intro allI impI)
    fix k
    assume "k \<in># Y"
    then have "count T k < count \<Sigma> k" unfolding Y_def by auto
    with `less_multiset\<^sub>H\<^sub>O \<Sigma> T` obtain a where "k < a" and "count \<Sigma> a < count T a"
      unfolding less_multiset\<^sub>H\<^sub>O_def by blast
    then show "\<exists>a. a \<in># X \<and> k < a" unfolding X_def by auto
  qed
qed

lemma mult_less_multiset\<^sub>D\<^sub>M: "(\<Sigma>, T) \<in> mult {(x, y). x < y} \<longleftrightarrow> less_multiset\<^sub>D\<^sub>M \<Sigma> T"
  by (metis less_multiset\<^sub>D\<^sub>M_imp_mult less_multiset\<^sub>H\<^sub>O_imp_less_multiset\<^sub>D\<^sub>M mult_imp_less_multiset\<^sub>H\<^sub>O)

lemma mult_less_multiset\<^sub>H\<^sub>O: "(\<Sigma>, T) \<in> mult {(x, y). x < y} \<longleftrightarrow> less_multiset\<^sub>H\<^sub>O \<Sigma> T"
  by (metis less_multiset\<^sub>D\<^sub>M_imp_mult less_multiset\<^sub>H\<^sub>O_imp_less_multiset\<^sub>D\<^sub>M mult_imp_less_multiset\<^sub>H\<^sub>O)

lemmas mult\<^sub>D\<^sub>M = mult_less_multiset\<^sub>D\<^sub>M[unfolded less_multiset\<^sub>D\<^sub>M_def]
lemmas mult\<^sub>H\<^sub>O = mult_less_multiset\<^sub>H\<^sub>O[unfolded less_multiset\<^sub>H\<^sub>O_def]

end

context linorder
begin

lemma total_le: "total {(a \<Colon> 'a, b). a \<le> b}"
  unfolding total_on_def by auto

lemma total_less: "total {(a \<Colon> 'a, b). a < b}"
  unfolding total_on_def by auto

lemma linorder_mult: "class.linorder
  (\<lambda>\<Sigma> T. (\<Sigma>, T) \<in> mult {(x, y). x < y} \<or> \<Sigma> = T)
  (\<lambda>\<Sigma> T. (\<Sigma>, T) \<in> mult {(x, y). x < y})"
proof -
  interpret o: order
    "(\<lambda>\<Sigma> T. (\<Sigma>, T) \<in> mult {(x, y). x < y} \<or> \<Sigma> = T)"
    "(\<lambda>\<Sigma> T. (\<Sigma>, T) \<in> mult {(x, y). x < y})"
    by (rule order_mult)
  show ?thesis by unfold_locales (auto 0 3 simp: mult\<^sub>H\<^sub>O not_less_iff_gr_or_eq)
qed

end

lemma less_multiset_less_multiset\<^sub>H\<^sub>O:
  "\<Sigma> \<subset># T \<longleftrightarrow> less_multiset\<^sub>H\<^sub>O \<Sigma> T"
  unfolding less_multiset_def mult\<^sub>H\<^sub>O less_multiset\<^sub>H\<^sub>O_def ..

lemmas less_multiset\<^sub>D\<^sub>M = mult\<^sub>D\<^sub>M[folded less_multiset_def]
lemmas less_multiset\<^sub>H\<^sub>O = mult\<^sub>H\<^sub>O[folded less_multiset_def]

lemma le_multiset\<^sub>H\<^sub>O:
  fixes \<Sigma> T :: "('a \<Colon> linorder) multiset"
  shows "\<Sigma> \<subseteq># T \<longleftrightarrow> (\<forall>y. count T y < count \<Sigma> y \<longrightarrow> (\<exists>x. y < x \<and> count \<Sigma> x < count T x))"
  by (auto simp: le_multiset_def less_multiset\<^sub>H\<^sub>O)

lemma wf_less_multiset: "wf {(\<Sigma> \<Colon> ('a \<Colon> wellorder) multiset, T). \<Sigma> \<subset># T}"
  unfolding less_multiset_def by (auto intro: wf_mult wf)

lemma order_multiset: "class.order
  (le_multiset :: ('a \<Colon> order) multiset \<Rightarrow> ('a \<Colon> order) multiset \<Rightarrow> bool)
  (less_multiset :: ('a \<Colon> order) multiset \<Rightarrow> ('a \<Colon> order) multiset \<Rightarrow> bool)"
  by unfold_locales

lemma linorder_multiset: "class.linorder
  (le_multiset :: ('a \<Colon> linorder) multiset \<Rightarrow> ('a \<Colon> linorder) multiset \<Rightarrow> bool)
  (less_multiset :: ('a \<Colon> linorder) multiset \<Rightarrow> ('a \<Colon> linorder) multiset \<Rightarrow> bool)"
  by unfold_locales (fastforce simp add: less_multiset\<^sub>H\<^sub>O le_multiset_def not_less_iff_gr_or_eq)

interpretation multiset_linorder: linorder
  "le_multiset :: ('a \<Colon> linorder) multiset \<Rightarrow> ('a \<Colon> linorder) multiset \<Rightarrow> bool"
  "less_multiset :: ('a \<Colon> linorder) multiset \<Rightarrow> ('a \<Colon> linorder) multiset \<Rightarrow> bool"
  by (rule linorder_multiset)

interpretation multiset_wellorder: wellorder
  "le_multiset :: ('a \<Colon> wellorder) multiset \<Rightarrow> ('a \<Colon> wellorder) multiset \<Rightarrow> bool"
  "less_multiset :: ('a \<Colon> wellorder) multiset \<Rightarrow> ('a \<Colon> wellorder) multiset \<Rightarrow> bool"
  by unfold_locales (blast intro: wf_less_multiset[unfolded wf_def, rule_format])

lemma le_multiset_total:
  fixes \<Sigma> T :: "('a \<Colon> linorder) multiset"
  shows "\<not> \<Sigma> \<subseteq># T \<Longrightarrow> T \<subseteq># \<Sigma>"
  by (metis multiset_linorder.le_cases)

lemma less_eq_imp_le_multiset:
  fixes \<Sigma> T :: "('a \<Colon> linorder) multiset"
  shows "\<Sigma> \<le> T \<Longrightarrow> \<Sigma> \<subseteq># T"
  unfolding le_multiset_def less_multiset\<^sub>H\<^sub>O
  by (auto dest: leD simp add: less_eq_multiset.rep_eq)

lemma less_multiset_right_total:
  fixes \<Sigma> :: "('a \<Colon> linorder) multiset"
  shows "\<Sigma> \<subset># \<Sigma> + {#undefined#}"
  unfolding le_multiset_def less_multiset\<^sub>H\<^sub>O by simp

lemma le_multiset_empty_left[simp]:
  fixes \<Sigma> :: "('a \<Colon> linorder) multiset"
  shows "{#} \<subseteq># \<Sigma>"
  by (simp add: less_eq_imp_le_multiset)

lemma le_multiset_empty_right[simp]:
  fixes \<Sigma> :: "('a \<Colon> linorder) multiset"
  shows "\<Sigma> \<noteq> {#} \<Longrightarrow> \<not> \<Sigma> \<subseteq># {#}"
  by (metis le_multiset_empty_left multiset_order.antisym)

lemma less_multiset_empty_left[simp]:
  fixes \<Sigma> :: "('a \<Colon> linorder) multiset"
  shows "\<Sigma> \<noteq> {#} \<Longrightarrow> {#} \<subset># \<Sigma>"
  by (simp add: less_multiset\<^sub>H\<^sub>O)

lemma less_multiset_empty_right[simp]:
  fixes \<Sigma> :: "('a \<Colon> linorder) multiset"
  shows "\<not> \<Sigma> \<subset># {#}"
  using le_empty less_multiset\<^sub>D\<^sub>M by blast

lemma
  fixes \<Sigma> T :: "('a \<Colon> linorder) multiset"
  shows
    le_multiset_plus_left[simp]: "T \<subseteq># (\<Sigma> + T)" and
    le_multiset_plus_right[simp]: "\<Sigma> \<subseteq># (\<Sigma> + T)"
  by (metis less_eq_imp_le_multiset mset_le_add_left union_commute)+

lemma
  fixes \<Sigma> T :: "('a \<Colon> linorder) multiset"
  shows
    less_multiset_plus_plus_left_iff[simp]: "\<Sigma> + T \<subset># \<Sigma>' + T \<longleftrightarrow> \<Sigma> \<subset># \<Sigma>'" and
    less_multiset_plus_plus_right_iff[simp]: "\<Sigma> + T \<subset># \<Sigma> + T' \<longleftrightarrow> T \<subset># T'"
  unfolding less_multiset\<^sub>H\<^sub>O by auto

lemma add_eq_self_empty_iff: "\<Sigma> + T = \<Sigma> \<longleftrightarrow> T = {#}"
  by (metis add.commute add_diff_cancel_right' monoid_add_class.add.left_neutral)

lemma
  fixes \<Sigma> T :: "('a \<Colon> linorder) multiset"
  shows
    less_multiset_plus_left_nonempty[simp]: "\<Sigma> \<noteq> {#} \<Longrightarrow> T \<subset># \<Sigma> + T" and
    less_multiset_plus_right_nonempty[simp]: "T \<noteq> {#} \<Longrightarrow> \<Sigma> \<subset># \<Sigma> + T"
  by (metis empty_neutral(2) less_multiset_empty_left less_multiset_plus_plus_right_iff
    union_commute)+

lemma ex_gt_imp_less_multiset: "(\<exists>y \<Colon> 'a \<Colon> linorder. y \<in># T \<and> (\<forall>x. x \<in># \<Sigma> \<longrightarrow> x < y)) \<Longrightarrow> \<Sigma> \<subset># T"
  unfolding less_multiset\<^sub>H\<^sub>O by (metis less_irrefl less_nat_zero_code not_gr0)

lemma ex_gt_count_imp_less_multiset:
  "(\<forall>y \<Colon> 'a \<Colon> linorder. y \<in># \<Sigma> + T \<longrightarrow> y \<le> x) \<Longrightarrow> count \<Sigma> x < count T x \<Longrightarrow> \<Sigma> \<subset># T"
  unfolding less_multiset\<^sub>H\<^sub>O by (metis UnCI comm_monoid_diff_class.diff_cancel dual_order.asym
    less_imp_diff_less mem_set_of_iff order.not_eq_order_implies_strict set_of_union)

lemma union_less_diff_plus: "U \<le> \<Sigma> \<Longrightarrow> T \<subset># U \<Longrightarrow> \<Sigma> - U + T \<subset># \<Sigma>"
  by (drule ordered_cancel_comm_monoid_diff_class.diff_add[symmetric]) (metis union_less_mono2)


subsection {* Multiset Extension of Multiset Ordering *}

text {*
The @{text "op \<subset>##"} and @{text "op \<subseteq>##"} operators are introduced as the multiset extension of
the multiset orderings of @{term "op \<subset>#"} and @{term "op \<subseteq>#"}.
*}

definition
  less_mset_mset :: "('a \<Colon> order) multiset multiset \<Rightarrow> 'a multiset multiset \<Rightarrow> bool" (infix "<##" 50)
where
  "M' <## M \<longleftrightarrow> (M', M) \<in> mult {(x', x). x' <# x}"

definition
  le_mset_mset :: "('a \<Colon> order) multiset multiset \<Rightarrow> 'a multiset multiset \<Rightarrow> bool" (infix "<=##" 50)
where
  "M' <=## M \<longleftrightarrow> M' <## M \<or> M' = M"

notation (xsymbols) less_mset_mset (infix "\<subset>##" 50)
notation (xsymbols) le_mset_mset (infix "\<subseteq>##" 50)

lemmas less_mset_mset\<^sub>D\<^sub>M = order.mult\<^sub>D\<^sub>M[OF order_multiset, folded less_mset_mset_def]
lemmas less_mset_mset\<^sub>H\<^sub>O = order.mult\<^sub>H\<^sub>O[OF order_multiset, folded less_mset_mset_def]

interpretation multiset_multiset_order: order
  "le_mset_mset :: ('a \<Colon> linorder) multiset multiset \<Rightarrow> ('a \<Colon> linorder) multiset multiset \<Rightarrow> bool"
  "less_mset_mset :: ('a \<Colon> linorder) multiset multiset \<Rightarrow> ('a \<Colon> linorder) multiset multiset \<Rightarrow> bool"
  unfolding less_mset_mset_def[abs_def] le_mset_mset_def[abs_def] less_multiset_def[abs_def]
  by (rule order.order_mult)+ default

interpretation multiset_multiset_linorder: linorder
  "le_mset_mset :: ('a \<Colon> linorder) multiset multiset \<Rightarrow> ('a \<Colon> linorder) multiset multiset \<Rightarrow> bool"
  "less_mset_mset :: ('a \<Colon> linorder) multiset multiset \<Rightarrow> ('a \<Colon> linorder) multiset multiset \<Rightarrow> bool"
  unfolding less_mset_mset_def[abs_def] le_mset_mset_def[abs_def]
  by (rule linorder.linorder_mult[OF linorder_multiset])

lemma wf_less_mset_mset: "wf {(\<Sigma> \<Colon> ('a \<Colon> wellorder) multiset multiset, T). \<Sigma> \<subset>## T}"
  unfolding less_mset_mset_def by (auto intro: wf_mult wf_less_multiset)

interpretation multiset_multiset_wellorder: wellorder
  "le_mset_mset :: ('a \<Colon> wellorder) multiset multiset \<Rightarrow> ('a \<Colon> wellorder) multiset multiset \<Rightarrow> bool"
  "less_mset_mset :: ('a \<Colon> wellorder) multiset multiset \<Rightarrow> ('a \<Colon> wellorder) multiset multiset \<Rightarrow> bool"
  by unfold_locales (blast intro: wf_less_mset_mset[unfolded wf_def, rule_format])

lemma union_less_mset_mset_mono2: "B \<subset>## D ==> C + B \<subset>## C + (D::'a::order multiset multiset)"
apply (unfold less_mset_mset_def mult_def)
apply (erule trancl_induct)
 apply (blast intro: mult1_union)
apply (blast intro: mult1_union trancl_trans)
done

lemma union_less_mset_mset_diff_plus:
  fixes \<Sigma>\<Sigma> UU TT :: "('a \<Colon> wellorder) multiset multiset"
  shows "U \<le> \<Sigma> \<Longrightarrow> T \<subset>## U \<Longrightarrow> \<Sigma> - U + T \<subset>## \<Sigma>"
  by (drule ordered_cancel_comm_monoid_diff_class.diff_add[symmetric])
    (metis union_less_mset_mset_mono2)

lemma ex_gt_imp_less_mset_mset:
  "(\<exists>y \<Colon> 'a \<Colon> linorder multiset. y \<in># T \<and> (\<forall>x. x \<in># \<Sigma> \<longrightarrow> x \<subset># y)) \<Longrightarrow> \<Sigma> \<subset>## T"
  by (metis less_mset_mset\<^sub>H\<^sub>O less_nat_zero_code multiset_linorder.not_less_iff_gr_or_eq not_gr0)

end
