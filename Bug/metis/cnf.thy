theory cnf
imports Main Transformation
begin
text \<open>Given the previous definition about abstract rewriting and theorem about them, we now have the detailed rule making the transformation into CNF/DNF.\<close>

section "Rewrite Rules"
text \<open>The idea of Christoph Weidenbach's book is to remove gradually the operators: first equivalencies, then implication, after that the unused true/false and finally the reorganizing the or/and.  We will prove each transformation seperately.\<close>


subsection \<open>Elimination of the equivalences\<close>

text \<open>The first transformation consists in removing every equivalence symbol.\<close>
fun elim_equiv :: "'v propo \<Rightarrow> 'v propo \<Rightarrow> bool" where
"elim_equiv (FEq \<phi> \<psi>) (FAnd (FImp \<phi>' \<psi>')  (FImp \<psi>'' \<phi>'')) = (\<phi> = \<phi>' \<and> \<phi>' = \<phi>'' \<and> \<psi> = \<psi>' \<and> \<psi> = \<psi>'')" |
"elim_equiv _ _ = False"

lemma elim_equiv_transformation_consistent:
"(A \<Turnstile> (FEq \<phi> \<psi>)) = (A \<Turnstile> (FAnd (FImp \<phi> \<psi>) (FImp \<psi> \<phi>)))"
  by auto


lemma elim_equiv_explicit: "elim_equiv \<phi> \<psi> \<longrightarrow> (\<forall>A. (A\<Turnstile>\<phi>) = (A\<Turnstile>\<psi>))"
  by (induct \<phi> \<psi> rule: elim_equiv.induct, auto)

lemma elim_equiv_consistent: "preserves_un_sat elim_equiv"
  unfolding preserves_un_sat_def by (simp add: elim_equiv_explicit)


lemma elimEquv_lifted_consistant:
  "preserves_un_sat (full_propo_rew elim_equiv)"
  by (simp add: elim_equiv_consistent)


text \<open>This function ensures that there is no equivalencies left in the formula tested by @{term no_equiv_symb}.\<close>
fun no_equiv_symb :: "'v propo \<Rightarrow> bool" where
"no_equiv_symb (FEq _ _) = False" |
"no_equiv_symb _ = True"


text \<open>Given the definition of @{term no_equiv_symb}, it does not depend on the formula, but only on the connective used.\<close>
lemma no_equiv_symb_conn_characterization[simp]:
  fixes c :: "'v connective" and l :: "'v propo list"
  assumes corr: "wf_conn c l"
  shows "no_equiv_symb (conn c l) \<longleftrightarrow> c \<noteq> CEq"
proof (case_tac c rule: connective_cases_arity)
  fix x
  assume c: "c = CT \<or> c = CF \<or> c = CVar x"
  hence "l = []" using corr by auto
  hence "no_equiv_symb (conn c l) " using c by auto
  also have "c \<noteq> CEq" using c by auto
  ultimately show " no_equiv_symb (conn c l) = (c \<noteq> CEq)" by auto
next
  assume c: "c \<in> binary_connectives"
  obtain a b where "l = [a, b]" using corr c list_length2_decomp wf_conn_bin_list_length by metis
  thus "no_equiv_symb (conn c l) = (c \<noteq> CEq)" using c unfolding binary_connectives_def by auto
next
  assume c: "c = CNot"
  obtain \<phi> where "l = [\<phi>]" using corr c wf_conn_Not_decomp by auto
  thus "no_equiv_symb (conn c l) = (c \<noteq> CEq)" using c corr c wf_conn_Not_decomp unfolding binary_connectives_def by auto
qed


definition no_equiv where "no_equiv = all_subformula_st no_equiv_symb"

lemma no_equiv_Eq[simp]:
  fixes \<phi> \<psi> :: "'v propo"
  shows
    "\<not> no_equiv (FEq \<phi> \<psi>)"
    "no_equiv FT"
    "no_equiv FF"
  using no_equiv_symb.simps(1) all_subformula_st_test_symb_true_phi unfolding no_equiv_def apply metis
  by auto

text \<open>The following lemma helps to reconstruct @{term no_equiv} expressions: this representation is easier to use than the set definition.\<close>


lemma all_subformula_st_decomp_explicit_no_equiv[simp]:
fixes \<phi> \<psi> :: "'v propo"
shows
  "no_equiv (FNot \<phi>) \<longleftrightarrow> no_equiv \<phi>"
  "no_equiv (FAnd \<phi> \<psi>) \<longleftrightarrow> (no_equiv \<phi> \<and> no_equiv \<psi>)"
  "no_equiv (FOr \<phi> \<psi>) \<longleftrightarrow> (no_equiv \<phi> \<and> no_equiv \<psi>)"
  "no_equiv (FImp \<phi> \<psi>) \<longleftrightarrow> (no_equiv \<phi> \<and> no_equiv \<psi>)"
  by (auto simp add: no_equiv_def)



text \<open>A theorem to show the link between the rewrite relation @{term elim_equiv} and the function @{term no_equiv_symb}. This theorem is one of the assumption we need to characterize the transformation.\<close>
lemma no_equiv_elim_equiv_step:
  fixes \<phi> :: "'v propo"
  assumes no_equiv: "\<not> no_equiv \<phi>"
  shows "\<exists>\<psi> \<psi>'. \<psi> \<preceq> \<phi> \<and> elim_equiv \<psi> \<psi>' "
proof -
  have test_symb_false_nullary: "\<forall>x::'v. no_equiv_symb FF \<and> no_equiv_symb FT \<and> no_equiv_symb (FVar x)" unfolding no_equiv_def by auto
  also {
    fix c:: "'v connective" and  l :: "'v propo list" and \<psi> :: "'v propo"
      assume a1: "elim_equiv (conn c l) \<psi>"
      have "\<And>p pa. \<not> elim_equiv (p\<Colon>'v propo) pa \<or> \<not> no_equiv_symb p"
        using elim_equiv.elims(2) no_equiv_symb.simps(1) by blast
      hence "elim_equiv (conn c l) \<psi> \<Longrightarrow> \<not>no_equiv_symb (conn c l) " using a1 by metis
  }
  moreover have  H': "\<forall>\<psi>. \<not>elim_equiv FT \<psi>" "\<forall>\<psi>. \<not>elim_equiv FF \<psi>" "\<forall>\<psi> x. \<not>elim_equiv (FVar x) \<psi>"
    by simp_all
  moreover have "\<And>\<phi>. \<not> no_equiv_symb \<phi> \<Longrightarrow> \<exists>\<psi>. elim_equiv \<phi> \<psi>"
    apply (case_tac \<phi>, auto)
    using elim_equiv.simps(1) by blast
  hence "\<And>\<phi>'. \<phi>' \<preceq> \<phi> \<Longrightarrow> \<not>no_equiv_symb \<phi>' \<Longrightarrow>  \<exists> \<psi>. elim_equiv \<phi>' \<psi>" by force
  ultimately show "?thesis" using no_test_symb_step_exists no_equiv test_symb_false_nullary unfolding no_equiv_def by blast
qed

text \<open>Given all the previous theorem and the characterization, once we have rewritten everything, there is no equivalence symbol any more.\<close>
lemma no_equiv_full_propo_rew_elim_equiv: "full_propo_rew elim_equiv \<phi> \<psi> \<Longrightarrow> no_equiv \<psi>"
  using full_propo_rew_subformula no_equiv_elim_equiv_step by blast



subsection \<open>Eliminate Implication\<close>

text \<open>After that, we can eliminate the implication symbols.\<close>
fun elim_imp :: "'v propo \<Rightarrow> 'v propo \<Rightarrow> bool" where
"elim_imp (FImp \<phi> \<psi>) (FOr (FNot \<phi>') \<psi>') = (\<phi> = \<phi>' \<and> \<psi> = \<psi>')" |
"elim_imp _ _ = False"

lemma elim_imp_transformation_consistent:
"(A \<Turnstile> (FImp \<phi> \<psi>)) = (A \<Turnstile> (FOr (FNot \<phi>) \<psi>))"
  by auto


lemma elim_imp_explicit: "elim_imp \<phi> \<psi> \<longrightarrow> (\<forall>A. (A\<Turnstile>\<phi>) = (A\<Turnstile>\<psi>))"
  by (induct \<phi> \<psi> rule: elim_imp.induct, auto)

lemma elim_imp_consistent: "preserves_un_sat elim_imp"
  unfolding preserves_un_sat_def by (simp add: elim_imp_explicit)


lemma elim_imp_lifted_consistant:
"preserves_un_sat (full_propo_rew elim_imp)"
  by (simp add: elim_imp_consistent)


fun no_imp_symb where
"no_imp_symb (FImp _ _) = False" |
"no_imp_symb _ = True"

lemma no_imp_symb_conn_characterization:
  "wf_conn c l \<Longrightarrow> no_imp_symb (conn c l) \<longleftrightarrow> c\<noteq>CImp"
proof (case_tac c rule: connective_cases_arity)
  fix x
  assume "wf_conn c l"
  and c: "c = CT \<or> c = CF \<or> c = CVar x"
  hence "l = []" by auto
  hence "no_imp_symb (conn c l) " using c by auto
  also have "c \<noteq> CImp" using c by auto
  ultimately show "?thesis" by auto
next
  assume corr: "wf_conn c l"
  and c: "c \<in> binary_connectives"
  obtain a b where "l = [a, b]" using corr c list_length2_decomp wf_conn_bin_list_length by metis
  thus "?thesis" using c unfolding binary_connectives_def by auto
next
  assume corr: "wf_conn c l"
  and c: "c = CNot"
  obtain \<phi> where "l = [\<phi>]" using corr c wf_conn_Not_decomp by auto
  thus "?thesis" using c unfolding binary_connectives_def by auto
qed


definition no_imp where "no_imp = all_subformula_st no_imp_symb"
declare no_imp_def[simp]

lemma no_imp_Imp[simp]:
  "\<not>no_imp (FImp \<phi> \<psi>)"
  "no_imp FT"
  "no_imp FF"
  unfolding no_imp_def by auto


lemma all_subformula_st_decomp_explicit_imp:
fixes \<phi> \<psi> :: "'v propo"
shows
  "no_imp (FNot \<phi>) \<longleftrightarrow> no_imp \<phi>"
  "no_imp (FAnd \<phi> \<psi>) \<longleftrightarrow> (no_imp \<phi> \<and> no_imp \<psi>)"
  "no_imp (FOr \<phi> \<psi>) \<longleftrightarrow> (no_imp \<phi> \<and> no_imp \<psi>)"
  by auto


text \<open>Invariant of the @{term elim_imp} transformation\<close>
lemma elim_imp_no_equiv:
  "elim_imp \<phi> \<psi> \<Longrightarrow> no_equiv \<phi> \<Longrightarrow>  no_equiv \<psi>"
  by (induct \<phi> \<psi> rule: elim_imp.induct, auto)

lemma elim_imp_inv:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "full_propo_rew elim_imp \<phi> \<psi> "
  and "no_equiv \<phi>"
  shows "no_equiv \<psi>"
proof -
  {
     fix \<phi> \<psi> :: "'v propo"
     have H: "elim_imp \<phi> \<psi> \<Longrightarrow> no_equiv \<phi> \<Longrightarrow>  no_equiv \<psi>"
       using elim_imp_no_equiv by metis
  }
  also {
     fix c :: "'v connective" and l l' :: "'v propo list"
     have "wf_conn c l \<Longrightarrow> wf_conn c l' \<Longrightarrow> no_equiv_symb (conn c l) \<longleftrightarrow> no_equiv_symb (conn c l')"
       using no_equiv_symb_conn_characterization by auto
  }
  ultimately show "no_equiv \<psi>" using full_propo_rew_inv_stay_conn assms unfolding no_equiv_def by blast
qed


lemma no_no_imp_elim_imp_step_exists:
  fixes \<phi> :: "'v propo"
  assumes no_equiv: "\<not> no_imp \<phi>"
  shows "\<exists>\<psi> \<psi>'. \<psi> \<preceq> \<phi> \<and> elim_imp \<psi> \<psi>'"
proof -
  have test_symb_false_nullary: "\<forall>x. no_imp_symb FF \<and> no_imp_symb FT \<and> no_imp_symb (FVar (x:: 'v))"
    by auto
  also {
     fix c:: "'v connective" and  l :: "'v propo list" and \<psi> :: "'v propo"
     have H: "elim_imp (conn c l) \<psi> \<Longrightarrow> \<not>no_imp_symb (conn c l) "
       by (auto elim: elim_imp.elims(2))
  }
  moreover have  H': "\<forall>\<psi>. \<not>elim_imp FT \<psi>" "\<forall>\<psi>. \<not>elim_imp FF \<psi>" "\<forall>\<psi> x. \<not>elim_imp (FVar x) \<psi>"
    by simp+
  moreover have "\<And>\<phi>. \<not> no_imp_symb \<phi> \<Longrightarrow> \<exists>\<psi>. elim_imp \<phi> \<psi>"
    apply (case_tac \<phi>, auto)
    using elim_imp.simps(1) by blast
  hence "(\<And>\<phi>'. \<phi>' \<preceq> \<phi> \<Longrightarrow> \<not>no_imp_symb \<phi>' \<Longrightarrow>  \<exists> \<psi>. elim_imp \<phi>' \<psi>)" by force
  ultimately show "?thesis" using no_test_symb_step_exists no_equiv test_symb_false_nullary unfolding no_imp_def by blast
qed


lemma no_imp_full_propo_rew_elim_imp: "full_propo_rew elim_imp \<phi> \<psi> \<Longrightarrow> no_imp \<psi>"
  using full_propo_rew_subformula no_no_imp_elim_imp_step_exists by blast


subsection "Eliminate all the True and False in the formula"
text \<open>Contrary to the book, we have to give the transformation and the ``commutative'' transformation. The latter is implicit in the book.\<close>
inductive elimTB where
ElimTB1: "elimTB (FAnd \<phi> FT) \<phi>" |
ElimTB1': "elimTB (FAnd FT \<phi>) \<phi>" |

ElimTB2: "elimTB (FAnd \<phi> FF) FF" |
ElimTB2': "elimTB (FAnd FF \<phi>) FF" |

ElimTB3: "elimTB (FOr \<phi> FT) FT" |
ElimTB3': "elimTB (FOr FT \<phi>) FT" |

ElimTB4: "elimTB (FOr \<phi> FF) \<phi>" |
ElimTB4': "elimTB (FOr FF \<phi>) \<phi>" |

ElimTB5: "elimTB (FNot FT) FF" |
ElimTB6: "elimTB (FNot FF) FT"


lemma elimTB_consistent: "preserves_un_sat elimTB"
proof -
  {
    fix \<phi> \<psi>:: "'b propo"
    have "elimTB \<phi> \<psi> \<Longrightarrow> (\<forall>A. (A\<Turnstile>\<phi>) = (A\<Turnstile>\<psi>))" by (induct_tac rule: elimTB.inducts, auto)
  }
  thus ?thesis using preserves_un_sat_def by auto
qed

fun no_FT_FF where
"no_FT_FF FT = False" |
"no_FT_FF FF = False" |
"no_FT_FF (FVar _) = True" |
"no_FT_FF (FNot \<phi>) = no_FT_FF \<phi>" |
"no_FT_FF (FAnd \<phi> \<psi>) = (no_FT_FF \<phi> \<and> no_FT_FF \<psi>)" |
"no_FT_FF (FOr \<phi> \<psi>) = (no_FT_FF \<phi> \<and> no_FT_FF \<psi>)" |
"no_FT_FF (FImp \<phi> \<psi>) = (no_FT_FF \<phi> \<and> no_FT_FF \<psi>)" |
"no_FT_FF (FEq \<phi> \<psi>) = (no_FT_FF \<phi> \<and> no_FT_FF \<psi>)"

inductive no_T_F_symb :: "'v propo \<Rightarrow> bool" where
no_T_F_symb_comp: "c \<noteq> CF \<Longrightarrow> c \<noteq> CT \<Longrightarrow> wf_conn c l \<Longrightarrow> (\<forall>\<phi> \<in> set l. \<phi>\<noteq>FT \<and> \<phi>\<noteq>FF) \<Longrightarrow> no_T_F_symb (conn c l) "


lemma wf_conn_no_T_F_symb_iff[simp]:
  "wf_conn c \<psi>s \<Longrightarrow> no_T_F_symb (conn c \<psi>s) \<longleftrightarrow> (c \<noteq> CF \<and> c \<noteq> CT \<and> (\<forall>\<psi>\<in>set \<psi>s. \<psi> \<noteq> FF \<and> \<psi> \<noteq> FT))"
  using conn_inj no_T_F_symb.simps by blast

lemma wf_conn_no_T_F_symb_iff_explicit[simp]:
"no_T_F_symb (FAnd \<phi> \<psi>) \<longleftrightarrow> (\<forall> \<xi> \<in>set [\<phi>, \<psi>]. \<xi> \<noteq> FF \<and> \<xi> \<noteq> FT)"
"no_T_F_symb (FOr \<phi> \<psi>) \<longleftrightarrow> (\<forall> \<xi> \<in>set [\<phi>, \<psi>]. \<xi> \<noteq> FF \<and> \<xi> \<noteq> FT)"
"no_T_F_symb (FEq \<phi> \<psi>) \<longleftrightarrow> (\<forall> \<xi> \<in>set [\<phi>, \<psi>]. \<xi> \<noteq> FF \<and> \<xi> \<noteq> FT)"
"no_T_F_symb (FImp \<phi> \<psi>) \<longleftrightarrow> (\<forall> \<xi> \<in>set [\<phi>, \<psi>]. \<xi> \<noteq> FF \<and> \<xi> \<noteq> FT)"
  using wf_conn_no_T_F_symb_iff wf_conn_helper_facts(5) apply fastforce
  using wf_conn_no_T_F_symb_iff wf_conn_helper_facts(6) apply fastforce
  using wf_conn_no_T_F_symb_iff wf_conn_helper_facts(8) apply fastforce
  using wf_conn_no_T_F_symb_iff wf_conn_helper_facts(7) apply fastforce
done


lemma no_T_F_symb_false[simp]:
  fixes c :: "'v connective"
  shows
    "\<not>no_T_F_symb (FT:: 'v propo)"
    "\<not>no_T_F_symb (FF:: 'v propo)"
proof -
  {
     fix \<psi> :: "'v propo"
     have "no_T_F_symb \<psi> \<Longrightarrow> \<psi> = FT \<Longrightarrow> False"
       apply (induct rule: no_T_F_symb.induct)
       by (metis conn.simps(1) conn_inj wf_conn_nullary)
  }
  thus "\<not>no_T_F_symb (FT:: 'v propo)"  by auto
next
  {
     fix \<psi> :: "'v propo"
     have "no_T_F_symb \<psi> \<Longrightarrow> \<psi> = FF \<Longrightarrow> False"
       apply (induct rule: no_T_F_symb.induct)
       by (metis conn.simps(2) conn_inj wf_conn_nullary)
  }
  thus "\<not>no_T_F_symb (FF:: 'v propo)"  by auto
qed

lemma no_T_F_symb_bool[simp]:
  fixes x :: "'v"
  shows "no_T_F_symb (FVar x)"
  using no_T_F_symb_comp wf_conn_nullary by (metis connective.distinct(3, 15) conn.simps(3) empty_iff list.set(1))


lemma no_T_F_symb_fnot_imp:
  "\<not>no_T_F_symb (FNot \<phi>) \<Longrightarrow> \<phi> = FT \<or> \<phi> = FF"
proof (rule ccontr)
  assume n: " \<not> no_T_F_symb (FNot \<phi>) "
  assume "\<not> (\<phi> = FT \<or> \<phi> = FF) "
  hence "\<forall>\<phi>' \<in> set [\<phi>]. \<phi>'\<noteq>FT \<and> \<phi>'\<noteq>FF" by auto
  also have "wf_conn CNot [\<phi>]" by simp
  ultimately have "no_T_F_symb (FNot \<phi>)" using no_T_F_symb.intros by (metis conn.simps(4) connective.distinct(17) connective.distinct(5))
  thus "False" using n by blast
qed

lemma no_T_F_symb_fnot[simp]:
  "no_T_F_symb (FNot \<phi>) \<longleftrightarrow> \<not>(\<phi> = FT \<or> \<phi> = FF)"
  apply auto
  using no_T_F_symb.simps no_T_F_symb_fnot_imp by (metis conn_inj_not(2) list.set_intros(1))+

text \<open>Actually it is not possible to remover every @{term FT} and @{term FF}: if the formula is equal to true or false, we can not remove it.\<close>
inductive no_T_F_symb_except_toplevel where
no_T_F_symb_except_toplevel_true[simp]: "no_T_F_symb_except_toplevel FT" |
no_T_F_symb_except_toplevel_false[simp]: "no_T_F_symb_except_toplevel FF" |
noTrue_no_T_F_symb_except_toplevel: "no_T_F_symb \<phi> \<Longrightarrow> no_T_F_symb_except_toplevel \<phi>"

lemma no_T_F_symb_except_toplevel_bool[simp]:
  fixes x :: "'v"
  shows "no_T_F_symb_except_toplevel (FVar x)"
  by (auto simp add: noTrue_no_T_F_symb_except_toplevel)


lemma no_T_F_symb_except_toplevel_not_decom:
  "\<phi>\<noteq>FT \<Longrightarrow> \<phi>\<noteq>FF \<Longrightarrow> no_T_F_symb_except_toplevel (FNot \<phi>)"
proof -
  fix \<phi> :: "'v propo"
  assume "\<phi> \<noteq> FT " and "\<phi> \<noteq> FF "
  also have "wf_conn CNot [\<phi>]" by simp
  ultimately have *: "no_T_F_symb (FNot \<phi>)" by (metis no_T_F_symb_fnot)
  thus "no_T_F_symb_except_toplevel (FNot \<phi>)" using noTrue_no_T_F_symb_except_toplevel by blast
qed

lemma no_T_F_symb_except_toplevel_bin_decom:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "\<phi> \<noteq> FT " and "\<phi> \<noteq> FF" and "\<psi> \<noteq> FT " and "\<psi> \<noteq> FF "
  and c: "c\<in> binary_connectives"
  shows "no_T_F_symb_except_toplevel (conn c [\<phi>, \<psi>])"
  by (metis (no_types, lifting) assms c conn.simps(4) list.discI noTrue_no_T_F_symb_except_toplevel wf_conn_no_T_F_symb_iff no_T_F_symb_fnot set_ConsD wf_conn_binary wf_conn_helper_facts(1) wf_conn_list_decomp(1,2))


lemma no_T_F_symb_except_toplevel_if_is_a_true_false:
  fixes l :: "'v propo list" and c :: "'v connective"
  assumes corr: "wf_conn c l"
  and "FT \<in> set l \<or> FF \<in> set l"
  shows "\<not>no_T_F_symb_except_toplevel (conn c l)"
  by (metis assms empty_iff no_T_F_symb_except_toplevel.simps wf_conn_no_T_F_symb_iff set_empty wf_conn_list(1) wf_conn_list(2))


lemma no_T_F_symb_except_top_level_false_example[simp]:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "\<phi> = FT \<or> \<psi> = FT \<or> \<phi> = FF \<or> \<psi> = FF"
  shows
    "\<not> no_T_F_symb_except_toplevel (FAnd \<phi> \<psi>)"
    "\<not> no_T_F_symb_except_toplevel (FOr \<phi> \<psi>)"
    "\<not> no_T_F_symb_except_toplevel (FImp \<phi> \<psi>)"
    "\<not> no_T_F_symb_except_toplevel (FEq \<phi> \<psi>)"
  using assms no_T_F_symb_except_toplevel_if_is_a_true_false unfolding binary_connectives_def
    by (metis (no_types) conn.simps(5-8) insert_iff list.simps(14-15) wf_conn_helper_facts(5-8))+


lemma no_T_F_symb_except_top_level_false_not[simp]:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "\<phi> = FT \<or> \<phi> = FF"
  shows
    "\<not> no_T_F_symb_except_toplevel (FNot \<phi>)"
proof -
  have "wf_conn CNot [\<phi>]" by simp
  also have "FT \<in> set [\<phi>] \<or> FF \<in> set [\<phi>]" using assms by auto
  ultimately show "\<not>no_T_F_symb_except_toplevel (FNot \<phi>)"
    using no_T_F_symb_except_toplevel_if_is_a_true_false by fastforce
qed

text \<open>This is the local extension of @{const no_T_F_symb_except_toplevel}.\<close>
definition no_T_F_except_top_level where
"no_T_F_except_top_level = all_subformula_st no_T_F_symb_except_toplevel"

text \<open>This is another property we will use. While this version might seem to be the one we want to prove, it is not since @{term FT} can not be reduced.\<close>
definition no_T_F where
"no_T_F = all_subformula_st no_T_F_symb"

lemma no_T_F_except_top_level_false:
  fixes l :: "'v propo list" and c :: "'v connective"
  assumes corr: "wf_conn c l"
  and "FT \<in> set l \<or> FF \<in> set l"
  shows "\<not>no_T_F_except_top_level (conn c l)"
  by (simp add: all_subformula_st_decomp assms(2) corr no_T_F_except_top_level_def no_T_F_symb_except_toplevel_if_is_a_true_false)


lemma no_T_F_except_top_level_false_example[simp]:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "\<phi> = FT \<or> \<psi> = FT \<or> \<phi> = FF \<or> \<psi> = FF"
  shows
    "\<not> no_T_F_except_top_level (FAnd \<phi> \<psi>)"
    "\<not> no_T_F_except_top_level (FOr \<phi> \<psi>)"
    "\<not> no_T_F_except_top_level (FEq \<phi> \<psi>)"
    "\<not> no_T_F_except_top_level (FImp \<phi> \<psi>)"
  by (metis all_subformula_st_test_symb_true_phi assms no_T_F_except_top_level_def no_T_F_symb_except_top_level_false_example)+


lemma no_T_F_symb_except_toplevel_no_T_F_symb:
  "no_T_F_symb_except_toplevel \<phi> \<Longrightarrow> \<phi> \<noteq> FF \<Longrightarrow> \<phi>\<noteq> FT \<Longrightarrow> no_T_F_symb \<phi>"
  by (induct rule:no_T_F_symb_except_toplevel.induct, auto)

text \<open>The two following lemmas give the precise link between the two definitions.\<close>
lemma no_T_F_symb_except_toplevel_all_subformula_st_no_T_F_symb:
  "no_T_F_except_top_level \<phi> \<Longrightarrow> \<phi> \<noteq> FF \<Longrightarrow> \<phi>\<noteq> FT \<Longrightarrow> no_T_F \<phi>"
  unfolding no_T_F_except_top_level_def no_T_F_def apply (induct \<phi>, simp_all)
  using no_T_F_symb_fnot apply fastforce
  by (metis no_T_F_symb_except_top_level_false_example no_T_F_symb_except_toplevel_no_T_F_symb propo.distinct(8,10,12,13,19,22,23,25))+


lemma no_T_F_no_T_F_except_top_level:
  "no_T_F \<phi> \<Longrightarrow> no_T_F_except_top_level \<phi>"
  unfolding no_T_F_except_top_level_def no_T_F_def
  unfolding all_subformula_st_def apply auto
  using noTrue_no_T_F_symb_except_toplevel by blast

lemma [simp]:  "no_T_F_except_top_level FF" "no_T_F_except_top_level FT"
  unfolding no_T_F_except_top_level_def by auto

lemma  no_T_F_no_T_F_except_top_level'[simp]:
  "no_T_F_except_top_level \<phi> \<longleftrightarrow> (\<phi> = FF \<or> \<phi> = FT \<or> no_T_F \<phi>)"
  apply auto
  using no_T_F_symb_except_toplevel_all_subformula_st_no_T_F_symb no_T_F_no_T_F_except_top_level by blast+


lemma no_T_F_bin_decomp[simp]:
  assumes c: "c \<in> binary_connectives"  
  shows "no_T_F (conn c [\<phi>, \<psi>]) \<longleftrightarrow> (no_T_F \<phi> \<and> no_T_F \<psi>)"
proof -
  have "wf_conn c [\<phi>, \<psi>]" using c by auto
  hence "no_T_F (conn c [\<phi>, \<psi>]) \<longleftrightarrow> (no_T_F_symb (conn c [\<phi>, \<psi>]) \<and> no_T_F \<phi> \<and> no_T_F \<psi>)"
    unfolding no_T_F_def using all_subformula_st_comp c subformula_in_binary_conn(1) subformula_in_binary_conn(2)
      proof -
        have "\<forall>c ps. \<not> wf_conn (c\<Colon>'a connective) ps \<or> (\<forall>p. \<not> all_subformula_st p (conn c ps) \<or> p (conn c ps) \<and> (\<forall>pa. pa \<notin> set ps \<or> all_subformula_st p pa)) \<and> (\<forall>p. (\<not> p (conn c ps) \<or> (\<exists>pa. pa \<in> set ps \<and> \<not> all_subformula_st p pa)) \<or> all_subformula_st p (conn c ps))"
          by (metis all_subformula_st_decomp) (* 18 ms *)
        then obtain pp :: "'a connective \<Rightarrow> 'a propo list \<Rightarrow> ('a propo \<Rightarrow> bool) \<Rightarrow> 'a propo" where
          f1: "\<forall>c ps. \<not> wf_conn c ps \<or> (\<forall>p. \<not> all_subformula_st p (conn c ps) \<or> p (conn c ps) \<and> (\<forall>pa. pa \<notin> set ps \<or> all_subformula_st p pa)) \<and> (\<forall>p. (\<not> p (conn c ps) \<or> pp c ps p \<in> set ps \<and> \<not> all_subformula_st p (pp c ps p)) \<or> all_subformula_st p (conn c ps))"
          by (metis (no_types)) (* 32 ms *)
        have f2: "\<And>p pa. last [p\<Colon>'a propo, pa] = pa"
          by simp (* 0.6 ms *)
        { assume "\<psi> \<noteq> \<phi>"
          have "[\<phi>, \<psi>] \<noteq> []"
            by blast (* 0.1 ms *)
          hence "all_subformula_st no_T_F_symb \<psi> \<or> all_subformula_st no_T_F_symb (conn c [\<phi>, \<psi>]) = (no_T_F_symb (conn c [\<phi>, \<psi>]) \<and> all_subformula_st no_T_F_symb \<phi> \<and> all_subformula_st no_T_F_symb \<psi>)"
            using f2 f1 by (metis (no_types) `wf_conn c [\<phi>, \<psi>]` last_in_set) (* 15 ms *) }
        moreover
        { assume "all_subformula_st no_T_F_symb \<psi>"
          moreover
          { assume "all_subformula_st no_T_F_symb \<phi> \<and> all_subformula_st no_T_F_symb \<psi>"
            moreover
            { assume "no_T_F_symb (conn c [\<phi>, \<psi>]) \<and> all_subformula_st no_T_F_symb \<phi> \<and> all_subformula_st no_T_F_symb \<psi>"
              hence "all_subformula_st no_T_F_symb (conn c [\<phi>, \<psi>]) = (no_T_F_symb (conn c [\<phi>, \<psi>]) \<and> all_subformula_st no_T_F_symb \<phi> \<and> all_subformula_st no_T_F_symb \<psi>)"
                using `wf_conn c [\<phi>, \<psi>]` all_subformula_st_comp by blast (* 1 ms *) }
            ultimately have "all_subformula_st no_T_F_symb (conn c [\<phi>, \<psi>]) = (no_T_F_symb (conn c [\<phi>, \<psi>]) \<and> all_subformula_st no_T_F_symb \<phi> \<and> all_subformula_st no_T_F_symb \<psi>)"
              using f1 `wf_conn c [\<phi>, \<psi>]` by blast (* 1 ms *) }
          ultimately have "all_subformula_st no_T_F_symb \<phi> \<longrightarrow> all_subformula_st no_T_F_symb (conn c [\<phi>, \<psi>]) = (no_T_F_symb (conn c [\<phi>, \<psi>]) \<and> all_subformula_st no_T_F_symb \<phi> \<and> all_subformula_st no_T_F_symb \<psi>)"
            by fastforce (* 0.5 ms *) }
        ultimately show "all_subformula_st no_T_F_symb (conn c [\<phi>, \<psi>]) = (no_T_F_symb (conn c [\<phi>, \<psi>]) \<and> all_subformula_st no_T_F_symb \<phi> \<and> all_subformula_st no_T_F_symb \<psi>)"
          using f1 by (metis `wf_conn c [\<phi>, \<psi>]` list.set_intros(1)) (* 23 ms *)
      qed
  thus "no_T_F (conn c [\<phi>, \<psi>]) \<longleftrightarrow> (no_T_F \<phi> \<and> no_T_F \<psi>)"
    using c  `wf_conn c [\<phi>, \<psi>]` all_subformula_st_decomp list.discI no_T_F_def no_T_F_symb_except_toplevel_bin_decom no_T_F_symb_except_toplevel_no_T_F_symb no_T_F_symb_false(1,2) wf_conn_helper_facts(2,3) wf_conn_list(1) wf_conn_list(2) by metis
qed

lemma no_T_F_bin_decomp_expanded[simp]:
  assumes c: "c = CAnd \<or> c = COr \<or> c = CEq \<or> c = CImp"  shows "no_T_F (conn c [\<phi>, \<psi>]) \<longleftrightarrow> (no_T_F \<phi> \<and> no_T_F \<psi>)"
  using no_T_F_bin_decomp assms unfolding binary_connectives_def by blast

lemma no_T_F_comp_expanded_explicit[simp]:
  fixes \<phi> \<psi> :: "'v propo"
  shows "no_T_F (FAnd \<phi> \<psi>) \<longleftrightarrow> (no_T_F \<phi> \<and> no_T_F \<psi>)"
    "no_T_F (FOr \<phi> \<psi>) \<longleftrightarrow> (no_T_F \<phi> \<and> no_T_F \<psi>)"
    "no_T_F (FEq \<phi> \<psi>) \<longleftrightarrow> (no_T_F \<phi> \<and> no_T_F \<psi>)"
    "no_T_F (FImp \<phi> \<psi>) \<longleftrightarrow> (no_T_F \<phi> \<and> no_T_F \<psi>)"
  using assms conn.simps(5-8) no_T_F_bin_decomp_expanded by (metis (no_types))+

lemma no_T_F_comp_not[simp]:
  fixes \<phi> \<psi> :: "'v propo"
  shows "no_T_F (FNot \<phi>) \<longleftrightarrow> no_T_F \<phi>"
proof -
  have "wf_conn CNot [\<phi>]" by auto
  hence "no_T_F (conn CNot [\<phi>]) \<longleftrightarrow> (no_T_F_symb (FNot \<phi>) \<and> no_T_F \<phi>)" unfolding no_T_F_def by simp
  also have "no_T_F \<phi> \<Longrightarrow> (\<phi> \<noteq> FF \<and> \<phi> \<noteq> FT)"  by (metis all_subformula_st_test_symb_true_phi no_T_F_def no_T_F_symb_false(1) no_T_F_symb_false(2))
  hence "no_T_F \<phi> \<Longrightarrow> no_T_F_symb (FNot \<phi>)" using no_T_F_symb_fnot by blast  
  ultimately show "?thesis" by auto
qed


lemma no_T_F_decomp:
  fixes \<phi> \<psi> :: "'v propo"
  assumes \<phi>: "no_T_F (FAnd \<phi> \<psi>) \<or> no_T_F (FOr \<phi> \<psi>) \<or> no_T_F (FEq \<phi> \<psi>) \<or> no_T_F (FImp \<phi> \<psi>)"
  shows  "no_T_F \<psi> " and "no_T_F \<phi>"
  using assms by auto


lemma no_T_F_decomp_not:
  fixes \<phi> :: "'v propo"
  assumes \<phi>: "no_T_F (FNot \<phi>)"
  shows  "no_T_F \<phi>"
  using assms by auto

lemma no_T_F_symb_except_toplevel_step_exists:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "no_equiv \<phi>" and "no_imp \<phi>"
  shows "\<psi> \<preceq> \<phi> \<Longrightarrow> \<not> no_T_F_symb_except_toplevel \<psi> \<Longrightarrow> \<exists>\<psi>'. elimTB \<psi> \<psi>'"
proof (induct \<psi> rule: propo_induct_arity)
  case (nullary \<phi>' x)
  hence "False" using no_T_F_symb_except_toplevel_true no_T_F_symb_except_toplevel_false by auto
  thus "Ex (elimTB \<phi>')" by blast
next
  case (unary \<psi>)
  hence "\<psi> = FF \<or> \<psi> = FT" using  no_T_F_symb_except_toplevel_not_decom by blast
  thus "Ex (elimTB (FNot \<psi>))" using ElimTB5 ElimTB6 by blast
next
  fix \<psi>1 \<psi>2 \<phi>' :: "'v propo"
  assume IH1: "(\<psi>1 \<preceq> \<phi> \<Longrightarrow> \<not> no_T_F_symb_except_toplevel \<psi>1 \<Longrightarrow> Ex (elimTB \<psi>1))"
  and IH2: "(\<psi>2 \<preceq> \<phi> \<Longrightarrow> \<not> no_T_F_symb_except_toplevel \<psi>2 \<Longrightarrow> Ex (elimTB \<psi>2))"
  and F\<phi>: "\<phi>' \<preceq> \<phi>"
  and \<phi>':  "\<phi>' = FAnd \<psi>1 \<psi>2 \<or> \<phi>' = FOr \<psi>1 \<psi>2 \<or> \<phi>' = FImp \<psi>1 \<psi>2 \<or> \<phi>' = FEq \<psi>1 \<psi>2"
  and n: " \<not> no_T_F_symb_except_toplevel \<phi>'"

  {
    assume "\<phi>' = FImp \<psi>1 \<psi>2 \<or> \<phi>' = FEq \<psi>1 \<psi>2"
    hence "False" using n F\<phi> subformula_all_subformula_st assms by (metis (no_types) no_equiv_Eq(1) no_equiv_def no_imp_Imp(1) no_imp_def)
    hence "Ex (elimTB \<phi>')" by blast
  }
  also {
    assume \<phi>': "\<phi>' = FAnd \<psi>1 \<psi>2 \<or> \<phi>' = FOr \<psi>1 \<psi>2"
    hence "\<psi>1 = FT \<or> \<psi>2 = FT \<or> \<psi>1 = FF \<or> \<psi>2 = FF"
      using no_T_F_symb_except_toplevel_bin_decom conn.simps(5,6) n unfolding binary_connectives_def by fastforce+
    hence "Ex (elimTB \<phi>')" using elimTB.intros \<phi>' by blast
  }
  ultimately show "Ex (elimTB \<phi>')" using \<phi>' by blast
qed


lemma no_T_F_except_top_level_rew:
  fixes \<phi> :: "'v propo"
  assumes noTB: "\<not> no_T_F_except_top_level \<phi>" and no_equiv: "no_equiv \<phi>" and no_imp: "no_imp \<phi>"
  shows "\<exists>\<psi> \<psi>'. \<psi> \<preceq> \<phi> \<and> elimTB \<psi> \<psi>' "
proof -
  have test_symb_false_nullary: "\<forall>x. no_T_F_symb_except_toplevel (FF:: 'v propo) \<and> no_T_F_symb_except_toplevel FT \<and> no_T_F_symb_except_toplevel (FVar (x:: 'v))" by auto
  also {
     fix c:: "'v connective" and  l :: "'v propo list" and \<psi> :: "'v propo"
     have H: "elimTB (conn c l) \<psi> \<Longrightarrow> \<not>no_T_F_symb_except_toplevel (conn c l) "
       by (case_tac "(conn c l)" rule: elimTB.cases, auto)
  }
  moreover {
     fix x :: "'v"
     have  H': "no_T_F_except_top_level FT" " no_T_F_except_top_level FF" "no_T_F_except_top_level (FVar x)"
       by (auto simp add: no_T_F_except_top_level_def test_symb_false_nullary)
  }
  moreover {
     fix \<psi>
     have " \<psi> \<preceq>\<phi> \<Longrightarrow> \<not> no_T_F_symb_except_toplevel \<psi> \<Longrightarrow> \<exists>\<psi>'. elimTB \<psi> \<psi>'"
       using no_T_F_symb_except_toplevel_step_exists no_equiv no_imp by blast
  }
  ultimately show "?thesis" using no_test_symb_step_exists noTB unfolding no_T_F_except_top_level_def by blast
qed

lemma elimTB_inv:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "full_propo_rew elimTB \<phi> \<psi> "
  and "no_equiv \<phi>" and "no_imp \<phi>"
  shows "no_equiv \<psi>" and "no_imp \<psi>"
proof -
  {
     fix \<phi> \<psi> :: "'v propo"
     have H: "elimTB \<phi> \<psi> \<Longrightarrow> no_equiv \<phi> \<Longrightarrow>  no_equiv \<psi>"
       by (induct \<phi> \<psi> rule: elimTB.induct, auto)
  }
  also {
     fix c :: "'v connective" and l l' :: "'v propo list"
     have " wf_conn c l \<Longrightarrow> wf_conn c l' \<Longrightarrow> no_equiv_symb (conn c l) \<longleftrightarrow> no_equiv_symb (conn c l')"
       using no_equiv_symb_conn_characterization by auto
  }
  ultimately show "no_equiv \<psi>" using full_propo_rew_inv_stay_conn assms unfolding no_equiv_def by blast
next
  {
     fix \<phi> \<psi> :: "'v propo"
     have H: "elimTB \<phi> \<psi> \<Longrightarrow> no_imp \<phi> \<Longrightarrow> no_imp \<psi>"
       by (induct \<phi> \<psi> rule: elimTB.induct, auto)
  }
  also {
     fix c :: "'v connective" and l l' :: "'v propo list"
     have " wf_conn c l \<Longrightarrow> wf_conn c l' \<Longrightarrow> no_imp_symb (conn c l) \<longleftrightarrow> no_imp_symb (conn c l')"
       using no_imp_symb_conn_characterization by auto
  }
  ultimately show "no_imp \<psi>" using full_propo_rew_inv_stay_conn assms unfolding no_imp_def by blast
qed



lemma elimTB_full_propo_rew:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "no_equiv \<phi>" and "no_imp \<phi>" and "full_propo_rew elimTB \<phi> \<psi>"
  shows "no_T_F_except_top_level \<psi>"
  using full_propo_rew_subformula no_T_F_except_top_level_rew assms elimTB_inv by fastforce

subsection "PushNeg"
text \<open>Push the negation inside the formula, until the litteral.\<close>
inductive pushNeg where
PushNeg1: "pushNeg (FNot (FAnd \<phi> \<psi>)) (FOr (FNot \<phi>) (FNot \<psi>))" |
PushNeg2: "pushNeg (FNot (FOr \<phi> \<psi>)) (FAnd (FNot \<phi>) (FNot \<psi>))" |
PushNeg3: "pushNeg (FNot (FNot \<phi>)) \<phi>"


lemma pushNeg_transformation_consistent:
"(A \<Turnstile> (FNot (FAnd \<phi> \<psi>))) = (A \<Turnstile> (FOr (FNot \<phi>) (FNot \<psi>)))"
"(A \<Turnstile> (FNot (FOr \<phi> \<psi>))) = (A \<Turnstile> (FAnd (FNot \<phi>) (FNot \<psi>)))"
"(A \<Turnstile> (FNot (FNot \<phi>))) = (A \<Turnstile> \<phi>)"
  by auto


lemma pushNeg_explicit: "pushNeg \<phi> \<psi> \<Longrightarrow> (\<forall>A. (A\<Turnstile>\<phi>) = (A\<Turnstile>\<psi>))"
  by (induct \<phi> \<psi> rule: pushNeg.induct, auto)

lemma pushNeg_consistent: "preserves_un_sat pushNeg"
  unfolding preserves_un_sat_def by (simp add: pushNeg_explicit)


lemma pushNeg_lifted_consistant:
"preserves_un_sat (full_propo_rew pushNeg)"
  by (simp add: pushNeg_consistent)

fun simple where
"simple FT = True" |
"simple FF = True" |
"simple (FVar _) = True" |
"simple _ = False"

lemma simple_decomp:
  "simple \<phi> \<longleftrightarrow> (\<phi> = FT \<or> \<phi> = FF \<or> (\<exists>x. \<phi> = FVar x))"
  by (case_tac \<phi>, auto)

lemma subformula_conn_decomp_simple:
  "simple \<psi> \<Longrightarrow> \<phi> \<preceq> FNot \<psi> \<longleftrightarrow> (\<phi> = FNot \<psi> \<or> \<phi> = \<psi>)"
proof -  
  assume s: "simple \<psi>"
  have "wf_conn CNot [\<psi>] \<Longrightarrow> \<phi> \<preceq> conn CNot [\<psi>] \<longleftrightarrow> (\<phi> = conn CNot [\<psi>] \<or> (\<exists> \<psi>\<in> set [\<psi>]. \<phi> \<preceq> \<psi>))"
    using subformula_conn_decomp  wf_conn_helper_facts(1) by blast
  also have "wf_conn CNot [\<psi>]"  by auto
  ultimately have "\<phi> \<preceq> conn CNot [\<psi>] \<longleftrightarrow> (\<phi> = conn CNot [\<psi>] \<or> (\<exists> \<psi>\<in> set [\<psi>]. \<phi> \<preceq> \<psi>))" by metis
  thus "\<phi> \<preceq> FNot \<psi> \<longleftrightarrow> (\<phi> = FNot \<psi> \<or> \<phi> = \<psi>)" using s by (auto simp add: simple_decomp)
qed  

lemma subformula_conn_decomp_explicit[simp]:
  "\<phi> \<preceq> FNot FT \<longleftrightarrow> (\<phi> = FNot FT \<or> \<phi> = FT)"
  "\<phi> \<preceq> FNot FF \<longleftrightarrow> (\<phi> = FNot FF \<or> \<phi> = FF)"
  "\<phi> \<preceq> FNot (FVar x) \<longleftrightarrow> (\<phi> = FNot (FVar x) \<or> \<phi> = FVar x)"
  by (auto simp add: subformula_conn_decomp_simple)
 
  
fun simple_not_symb where
"simple_not_symb (FNot \<phi>) = (simple \<phi>)" |
"simple_not_symb _ = True"

definition simple_not where
"simple_not = all_subformula_st simple_not_symb"
declare simple_not_def[simp]

lemma simple_not_Not[simp]:
  "\<not> simple_not (FNot (FAnd \<phi> \<psi>))"
  "\<not> simple_not (FNot (FOr \<phi> \<psi>))"
  by auto

lemma simple_not_step_exists:
  fixes \<phi> :: "'v propo"
  assumes "no_equiv \<phi>" and "no_imp \<phi>"
  shows "\<psi> \<preceq>\<phi> \<Longrightarrow> \<not> simple_not_symb \<psi> \<Longrightarrow> \<exists>\<psi>'. pushNeg \<psi> \<psi>'"
  apply (induct \<psi>)
  apply auto
  apply (case_tac \<psi>, auto)
            using PushNeg1 PushNeg2 PushNeg3 apply blast+
  by (metis assms(1,2) no_imp_Imp(1) no_equiv_Eq(1) no_imp_def no_equiv_def subformula_in_subformula_not subformula_all_subformula_st)+



lemma simple_not_rew:
  fixes \<phi> :: "'v propo"
  assumes noTB: "\<not> simple_not \<phi>" and no_equiv: "no_equiv \<phi>" and no_imp: "no_imp \<phi>"
  shows "\<exists>\<psi> \<psi>'. \<psi> \<preceq> \<phi> \<and> pushNeg \<psi> \<psi>'"
proof -
  have test_symb_false_nullary: "\<forall>x. simple_not_symb (FF:: 'v propo) \<and> simple_not_symb FT \<and> simple_not_symb (FVar (x:: 'v))"   by auto
  also {
     fix c:: "'v connective" and  l :: "'v propo list" and \<psi> :: "'v propo"
     have H: "pushNeg (conn c l) \<psi> \<Longrightarrow> \<not>simple_not_symb (conn c l)"
       by (case_tac "(conn c l)" rule: pushNeg.cases, simp_all)
  }
  moreover {
     fix x :: "'v"
     have  H': "simple_not FT" "simple_not FF" "simple_not (FVar x)"
       by simp_all
  }
  moreover {
     fix \<psi>
     have  "\<psi> \<preceq>\<phi> \<Longrightarrow> \<not> simple_not_symb \<psi> \<Longrightarrow> \<exists>\<psi>'. pushNeg \<psi> \<psi>'"
       using simple_not_step_exists no_equiv no_imp by blast
  }
  ultimately show "?thesis" using no_test_symb_step_exists noTB unfolding simple_not_def by blast
qed


lemma no_T_F_except_top_level_pushNeg1:
  "no_T_F_except_top_level (FNot (FAnd \<phi> \<psi>)) \<Longrightarrow> no_T_F_except_top_level (FOr (FNot \<phi>) (FNot \<psi>))"
  using no_T_F_symb_except_toplevel_all_subformula_st_no_T_F_symb  no_T_F_comp_not no_T_F_decomp(1) no_T_F_decomp(2) no_T_F_no_T_F_except_top_level by (metis no_T_F_comp_expanded_explicit(2) propo.distinct(17) propo.distinct(5))


lemma no_T_F_except_top_level_pushNeg2:
  "no_T_F_except_top_level (FNot (FOr \<phi> \<psi>)) \<Longrightarrow> no_T_F_except_top_level (FAnd (FNot \<phi>) (FNot \<psi>))"
  by auto


lemma no_T_F_symb_pushNeg:
  "no_T_F_symb (FOr (FNot \<phi>') (FNot \<psi>'))"
  "no_T_F_symb (FAnd (FNot \<phi>') (FNot \<psi>'))"
  "no_T_F_symb (FNot (FNot \<phi>'))" 
  by auto

  


lemma propo_rew_step_pushNeg_no_T_F_symb:
  "propo_rew_step pushNeg \<phi> \<psi> \<Longrightarrow> no_T_F_except_top_level \<phi> \<Longrightarrow> no_T_F_symb \<phi> \<Longrightarrow> no_T_F_symb \<psi>"
  apply (induct rule: propo_rew_step.induct)
  apply(cases rule:pushNeg.cases)
  apply simp_all
  apply (metis no_T_F_symb_pushNeg(1))
  apply (metis no_T_F_symb_pushNeg(2))
  apply (metis all_subformula_st_test_symb_true_phi no_T_F_decomp_not no_T_F_def no_T_F_symb_false(1,2))
proof -
  fix \<phi> \<phi>'::"'a propo" and c::"'a connective" and \<xi> \<xi>'::"'a propo list"
  assume rel: "propo_rew_step pushNeg \<phi> \<phi>'"
  and IH: "no_T_F \<phi> \<Longrightarrow> no_T_F_symb \<phi> \<Longrightarrow> no_T_F_symb \<phi>'"
  and wf: "wf_conn c (\<xi> @ \<phi> # \<xi>')"
  and n: "conn c (\<xi> @ \<phi> # \<xi>') = FF \<or> conn c (\<xi> @ \<phi> # \<xi>') = FT \<or> no_T_F (conn c (\<xi> @ \<phi> # \<xi>'))"
  and x: "c \<noteq> CF \<and> c \<noteq> CT \<and> \<phi> \<noteq> FF \<and> \<phi> \<noteq> FT \<and> (\<forall>\<psi>\<Colon>'a propo\<in>set \<xi> \<union> set \<xi>'. \<psi> \<noteq> FF \<and> \<psi> \<noteq> FT)"
  hence "c \<noteq> CF \<and> c \<noteq> CF \<and> wf_conn c (\<xi> @ \<phi>' # \<xi>')"
    using wf_conn_no_arity_change_helper  wf_conn_no_arity_change by metis
  also have n': "no_T_F (conn c (\<xi> @ \<phi> # \<xi>'))" using n by (simp add: local.wf wf_conn_list(1) wf_conn_list(2))
  moreover
  {
    have "no_T_F \<phi>" using n' by (metis Un_iff all_subformula_st_decomp_rec list.set_intros(1) local.wf no_T_F_def set_append)
    also have "no_T_F_symb \<phi>" using calculation by (simp add: all_subformula_st_test_symb_true_phi no_T_F_def)
    ultimately have "\<phi>' \<noteq> FF \<and> \<phi>' \<noteq> FT" using IH no_T_F_symb_false(1) no_T_F_symb_false(2) by blast
    hence "\<forall>\<psi>\<in> set (\<xi> @ \<phi>' # \<xi>'). \<psi> \<noteq> FF \<and> \<psi> \<noteq> FT" using x by auto
  }
  ultimately show "no_T_F_symb (conn c (\<xi> @ \<phi>' # \<xi>'))" by (simp add: x)
qed

lemma propo_rew_step_pushNeg_no_T_F:
  "propo_rew_step pushNeg \<phi> \<psi> \<Longrightarrow> no_T_F \<phi> \<Longrightarrow> no_T_F \<psi>"
  apply (induct rule: propo_rew_step.induct)
  apply (metis (no_types, lifting) no_T_F_symb_except_toplevel_all_subformula_st_no_T_F_symb no_T_F_def no_T_F_except_top_level_pushNeg1 no_T_F_except_top_level_pushNeg2 no_T_F_no_T_F_except_top_level all_subformula_st_decomp_explicit(3) pushNeg.simps simple.simps(1) simple.simps(2) simple.simps(5) simple.simps(6))
proof -
  fix \<phi> \<phi>' :: "'v propo" and c :: "'v connective" and \<xi> \<xi>' :: "'v propo list"
  assume rel: "propo_rew_step pushNeg \<phi> \<phi>'"
  and IH: "no_T_F \<phi> \<Longrightarrow> no_T_F \<phi>'"
  and corr: "wf_conn c (\<xi> @ \<phi> # \<xi>')"
  and no_T_F: "no_T_F (conn c (\<xi> @ \<phi> # \<xi>'))"
  also have corr': "wf_conn c (\<xi> @ \<phi>' # \<xi>')" using wf_conn_no_arity_change wf_conn_no_arity_change_helper corr by metis
  ultimately show "no_T_F (conn c (\<xi> @ \<phi>' # \<xi>'))" unfolding no_T_F_def
    apply(simp add: all_subformula_st_decomp corr corr')
    using all_subformula_st_test_symb_true_phi no_T_F_symb_false(1) no_T_F_symb_false(2) by blast
qed


lemma pushNeg_inv:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "full_propo_rew pushNeg \<phi> \<psi> "
  and "no_equiv \<phi>" and "no_imp \<phi>" and "no_T_F_except_top_level \<phi>"
  shows "no_equiv \<psi>" and "no_imp \<psi>" and "no_T_F_except_top_level \<psi>"
proof -
  {
    fix \<phi> \<psi> :: "'v propo"
    have "propo_rew_step pushNeg \<phi> \<psi> \<Longrightarrow> no_T_F_except_top_level \<phi> \<Longrightarrow>  no_T_F_except_top_level \<psi>"
      proof -
        assume rel: "propo_rew_step pushNeg \<phi> \<psi>"
        and  no: "no_T_F_except_top_level \<phi>"
        {
          assume "\<phi> = FT \<or> \<phi> = FF"
          from rel this have False
            apply (induct rule: propo_rew_step.induct)
              using pushNeg.cases apply blast
            using wf_conn_list(1) wf_conn_list(2) by auto
          hence "no_T_F_except_top_level \<psi>" by blast
        }
        also {
          assume "\<phi> \<noteq> FT \<and> \<phi> \<noteq> FF"
          hence "no_T_F \<phi>" by (metis no no_T_F_symb_except_toplevel_all_subformula_st_no_T_F_symb)
          hence "no_T_F \<psi>" using propo_rew_step_pushNeg_no_T_F rel by auto
          hence "no_T_F_except_top_level \<psi>" by (simp add: no_T_F_no_T_F_except_top_level)
        }
        ultimately show "no_T_F_except_top_level \<psi>" by metis
      qed
  }
  hence "(\<And>(c\<Colon>'v connective) (\<xi>\<Colon>'v propo list) (\<zeta>\<Colon>'v propo) (\<xi>'\<Colon>'v propo list) \<zeta>'\<Colon>'v propo.
        \<zeta> \<preceq>\<phi> \<Longrightarrow> propo_rew_step pushNeg \<zeta> \<zeta>' \<Longrightarrow>
        wf_conn c (\<xi> @ \<zeta> # \<xi>') \<Longrightarrow> no_T_F_symb_except_toplevel (conn c (\<xi> @ \<zeta> # \<xi>')) \<Longrightarrow>
        no_T_F_symb_except_toplevel \<zeta>' \<Longrightarrow> no_T_F_symb_except_toplevel (conn c (\<xi> @ \<zeta>' # \<xi>'))) \<Longrightarrow>
        no_T_F_except_top_level \<psi>"
        using full_propo_rew_inv_stay_with_inc assms subformula_refl unfolding no_T_F_except_top_level_def by blast
  also {
    fix c :: "'v connective" and \<xi> \<xi>' :: "'v propo list" and \<zeta> \<zeta>' :: "'v propo"
     have "\<zeta> \<preceq>\<phi> \<Longrightarrow>propo_rew_step pushNeg \<zeta> \<zeta>' \<Longrightarrow> wf_conn c (\<xi> @ \<zeta> # \<xi>') \<Longrightarrow>
     no_T_F_symb_except_toplevel (conn c (\<xi> @ \<zeta> # \<xi>')) \<Longrightarrow> no_T_F_symb_except_toplevel \<zeta>' \<Longrightarrow>
     no_T_F_symb_except_toplevel (conn c (\<xi> @ \<zeta>' # \<xi>'))"
     proof
       assume rel: "propo_rew_step pushNeg \<zeta> \<zeta>'"
       and incl: "\<zeta> \<preceq> \<phi>"
       and corr: "wf_conn c (\<xi> @ \<zeta> # \<xi>')"
       and no_T_F: "no_T_F_symb_except_toplevel (conn c (\<xi> @ \<zeta> # \<xi>'))"
       and n: "no_T_F_symb_except_toplevel \<zeta>'"
       have p: "no_T_F_symb (conn c (\<xi> @ \<zeta> # \<xi>'))"
         using corr wf_conn_list(1) wf_conn_list(2) no_T_F_symb_except_toplevel_no_T_F_symb no_T_F by blast

       have l: "\<forall>\<phi>\<in>set (\<xi> @ \<zeta> # \<xi>'). \<phi> \<noteq> FT \<and> \<phi> \<noteq> FF" using corr wf_conn_no_T_F_symb_iff p by blast
       from rel incl have "\<zeta>'\<noteq>FT \<and>\<zeta>'\<noteq>FF"
         apply (induction \<zeta> \<zeta>' rule: propo_rew_step.induct)
         apply (cases rule:pushNeg.cases)
         apply auto
         apply (metis assms(4) no_T_F_symb_except_top_level_false_not no_T_F_except_top_level_def all_subformula_st_test_symb_true_phi subformula_in_subformula_not subformula_all_subformula_st)
         apply (metis assms(4) no_T_F_symb_except_top_level_false_not no_T_F_except_top_level_def all_subformula_st_test_symb_true_phi subformula_in_subformula_not subformula_all_subformula_st)
         apply (metis append_is_Nil_conv list.distinct(1) wf_conn_no_arity_change_helper wf_conn_list(1) wf_conn_no_arity_change)
         by (metis append_is_Nil_conv list.distinct(1) wf_conn_list(2) wf_conn_no_arity_change wf_conn_no_arity_change_helper)
       hence "\<forall>\<phi>\<in>set (\<xi> @ \<zeta>' # \<xi>'). \<phi> \<noteq> FT \<and> \<phi> \<noteq> FF" using l by auto
       also have "c\<noteq>CT \<and> c \<noteq> CF" using corr by auto
       ultimately show "no_T_F_symb (conn c (\<xi> @ \<zeta>' # \<xi>'))" by (meson corr no_T_F_symb_comp wf_conn_no_arity_change wf_conn_no_arity_change_helper)
     qed
  }
  ultimately show "no_T_F_except_top_level \<psi>" unfolding no_T_F_except_top_level_def  by blast
next
  {
    fix \<phi> \<psi> :: "'v propo"
    have H: "pushNeg \<phi> \<psi> \<Longrightarrow> no_equiv \<phi> \<Longrightarrow>  no_equiv \<psi>"
      by (induct \<phi> \<psi> rule: pushNeg.induct, auto)
  }
  also {
     fix c :: "'v connective" and l l' :: "'v propo list"
     have " wf_conn c l \<Longrightarrow> wf_conn c l' \<Longrightarrow> no_equiv_symb (conn c l) = no_equiv_symb (conn c l')"
      using no_equiv_symb_conn_characterization by auto
  }
  ultimately show "no_equiv \<psi>" using full_propo_rew_inv_stay_conn assms unfolding no_equiv_def by blast
next
  {
    fix \<phi> \<psi> :: "'v propo"
    have H: "pushNeg \<phi> \<psi> \<Longrightarrow> no_imp \<phi> \<Longrightarrow>  no_imp \<psi>"
      by (induct \<phi> \<psi> rule: pushNeg.induct, auto)
  }
  also {
    fix c :: "'v connective" and l l' :: "'v propo list"
     have "wf_conn c l \<Longrightarrow> wf_conn c l' \<Longrightarrow> no_imp_symb (conn c l) = no_imp_symb (conn c l')"
      using no_imp_symb_conn_characterization by auto
  }
  ultimately show "no_imp \<psi>" using full_propo_rew_inv_stay_conn assms unfolding no_imp_def by blast
qed


lemma pushNeg_full_propo_rew:
  fixes \<phi> \<psi> :: "'v propo"
  assumes "no_equiv \<phi>" and "no_imp \<phi>" and "full_propo_rew pushNeg \<phi> \<psi>" and "no_T_F_except_top_level \<phi>"
  shows "simple_not \<psi>"
  using assms full_propo_rew_subformula pushNeg_inv(1) pushNeg_inv(2) simple_not_rew by blast

subsection \<open>Push inside\<close>

inductive push_c_inside_c' :: "'v connective \<Rightarrow> 'v connective \<Rightarrow> 'v propo \<Rightarrow> 'v propo \<Rightarrow> bool" for c c':: "'v connective" where
push_c_inside_c'_l[intro]: "c = CAnd \<or> c = COr \<Longrightarrow> c' = CAnd \<or> c' = COr \<Longrightarrow> push_c_inside_c' c c' (conn c [conn c' [\<phi>1, \<phi>2], \<psi>]) (conn c' [conn c [\<phi>1, \<psi>], conn c [\<phi>2, \<psi>]])" |
push_c_inside_c'_r[intro]: "c = CAnd \<or> c = COr \<Longrightarrow> c' = CAnd \<or> c' = COr \<Longrightarrow> push_c_inside_c' c c' (conn c [\<psi>, conn c' [\<phi>1, \<phi>2]]) (conn c' [conn c [\<psi>, \<phi>1], conn c [\<psi>, \<phi>2]])"


lemma push_c_inside_c'_explicit: "push_c_inside_c' c c' \<phi> \<psi> \<Longrightarrow> (\<forall>A. (A\<Turnstile>\<phi>) = (A\<Turnstile>\<psi>))"
  by (induct \<phi> \<psi> rule: push_c_inside_c'.induct, auto)
  

lemma push_c_inside_c'_consistent: "preserves_un_sat (push_c_inside_c' c c')"
  unfolding preserves_un_sat_def by (simp add: push_c_inside_c'_explicit)

lemma propo_rew_step_push_c_inside_c'[simp]:
 "\<not>propo_rew_step (push_c_inside_c' c c') FT \<psi>" "\<not>propo_rew_step (push_c_inside_c' c c') FF \<psi>"
 proof -
  {
    {
      fix \<phi> \<psi>
      have "push_c_inside_c' c c' \<phi> \<psi> \<Longrightarrow> \<phi> = FT \<or> \<phi> = FF \<Longrightarrow> False"
        by (induct rule:push_c_inside_c'.induct, auto)
    } note H = this   
    fix \<phi>
    have "propo_rew_step (push_c_inside_c' c c') \<phi> \<psi> \<Longrightarrow> \<phi> = FT \<or> \<phi> = FF\<Longrightarrow> False"
      apply (induct rule:propo_rew_step.induct, auto simp add: wf_conn_list(1) wf_conn_list(2))
        using H by blast+
  }
  thus "\<not>propo_rew_step (push_c_inside_c' c c') FT \<psi>" 
    "\<not>propo_rew_step (push_c_inside_c' c c') FF \<psi>" by blast+
qed


inductive not_c_in_c'_symb:: "'v connective \<Rightarrow> 'v connective \<Rightarrow> 'v propo  \<Rightarrow> bool" for c c' where
not_c_in_c'_symb_l[simp]: "wf_conn c [conn c' [\<phi>, \<phi>'], \<psi>] \<Longrightarrow> wf_conn c' [\<phi>, \<phi>'] \<Longrightarrow> not_c_in_c'_symb c c' (conn c [conn c' [\<phi>, \<phi>'], \<psi>])" |
not_c_in_c'_symb_r[simp]: "wf_conn c [\<psi>, conn c' [\<phi>, \<phi>']] \<Longrightarrow> wf_conn c' [\<phi>, \<phi>'] \<Longrightarrow> not_c_in_c'_symb c c' (conn c [\<psi>, conn c' [\<phi>, \<phi>']])"

abbreviation "c_in_c'_symb c c' \<phi> == \<not>not_c_in_c'_symb c c' \<phi>"

(*TODO: wf_conn.simps in [simp] ?*)
lemma c_in_c'_symb_simp:
  "not_c_in_c'_symb c c' \<xi> \<Longrightarrow> \<xi> = FF \<or> \<xi> = FT \<or> \<xi> = FVar x \<or> \<xi> = FNot FF \<or> \<xi> = FNot FT  \<or> \<xi> = FNot (FVar x)\<Longrightarrow> False"
  apply (induct rule: not_c_in_c'_symb.induct)
  apply (simp add: wf_conn_list(1-3))
  apply (auto simp add: wf_conn.simps wf_conn_list(1-3))
  using conn_inj_not(2) wf_conn_binary by fastforce+

lemma  c_in_c'_symb_simp'[simp]:
  "~not_c_in_c'_symb c c' FF" 
  "~not_c_in_c'_symb c c' FT" 
  "~not_c_in_c'_symb c c' (FVar x)"   
  "~not_c_in_c'_symb c c' (FNot FF)" 
  "~not_c_in_c'_symb c c' (FNot FT)" 
  "~not_c_in_c'_symb c c' (FNot (FVar x))"   
  using c_in_c'_symb_simp by metis+
  
definition c_in_c'_only where
"c_in_c'_only c c' = all_subformula_st (c_in_c'_symb c c')"

lemma c_in_c'_only_simp[simp]:
  "c_in_c'_only c c' FF"
  "c_in_c'_only c c' FT"
  "c_in_c'_only c c' (FVar x)"
  "c_in_c'_only c c' (FNot FF)"
  "c_in_c'_only c c' (FNot FT)"  
  "c_in_c'_only c c' (FNot (FVar x))"
  unfolding c_in_c'_only_def by auto
 
  
lemma not_c_in_c'_symb_commute:
  "not_c_in_c'_symb c c' \<xi> \<Longrightarrow> wf_conn c [\<phi>, \<psi>] \<Longrightarrow> \<xi> = conn c [\<phi>, \<psi>] \<Longrightarrow> not_c_in_c'_symb c c' (conn c [\<psi>, \<phi>])"
proof (induct rule: not_c_in_c'_symb.induct)
  case (not_c_in_c'_symb_r \<phi>' \<phi>'' \<psi>') note H = this
  hence \<psi>: "\<psi> = conn c' [\<phi>'', \<psi>']" using conn_inj by auto
  have "wf_conn c [conn c' [\<phi>'', \<psi>'], \<phi>]" using H(1) wf_conn_no_arity_change length_Cons by metis 
  thus "not_c_in_c'_symb c c' (conn c [\<psi>, \<phi>])" unfolding \<psi> using not_c_in_c'_symb.intros(1) H by auto
next
  case (not_c_in_c'_symb_l \<phi>' \<phi>'' \<psi>') note H = this
  hence "\<phi> = conn c' [\<phi>', \<phi>'']" using conn_inj by auto
  also have "wf_conn c [\<psi>', conn c' [\<phi>', \<phi>'']]" using H(1) wf_conn_no_arity_change length_Cons by metis 
  ultimately show  "not_c_in_c'_symb c c' (conn c [\<psi>, \<phi>])" using not_c_in_c'_symb.intros(2) conn_inj not_c_in_c'_symb_l.hyps not_c_in_c'_symb_l.prems(1,2) by blast 
qed

lemma not_c_in_c'_symb_commute':
  "wf_conn c [\<phi>, \<psi>] \<Longrightarrow> c_in_c'_symb c c' (conn c [\<phi>, \<psi>])  \<longleftrightarrow> c_in_c'_symb c c' (conn c [\<psi>, \<phi>])"
  using not_c_in_c'_symb_commute wf_conn_no_arity_change by (metis length_Cons)
  
lemma not_c_in_c'_comm:
  assumes wf: "wf_conn c [\<phi>, \<psi>]"
  shows "c_in_c'_only c c' (conn c [\<phi>, \<psi>]) \<longleftrightarrow> c_in_c'_only c c' (conn c [\<psi>, \<phi>])" (is "?A \<longleftrightarrow> ?B")
proof -
  have "?A \<longleftrightarrow> (c_in_c'_symb c c' (conn c [\<phi>, \<psi>]) \<and> (\<forall>\<xi> \<in> set [\<phi>, \<psi>]. all_subformula_st (c_in_c'_symb c c') \<xi>))" using all_subformula_st_decomp wf unfolding c_in_c'_only_def  by fastforce
  also have "\<dots> \<longleftrightarrow> (c_in_c'_symb c c' (conn c [\<psi>, \<phi>]) \<and> (\<forall>\<xi> \<in> set [\<psi>, \<phi>]. all_subformula_st (c_in_c'_symb c c') \<xi>))"  using not_c_in_c'_symb_commute' wf by auto
  also 
  have "wf_conn c [\<psi>, \<phi>]" using wf_conn_no_arity_change wf by (metis length_Cons)
  hence "(c_in_c'_symb c c' (conn c [\<psi>, \<phi>]) \<and> (\<forall>\<xi> \<in> set [\<psi>, \<phi>]. all_subformula_st (c_in_c'_symb c c') \<xi>)) \<longleftrightarrow> ?B" using all_subformula_st_decomp  unfolding c_in_c'_only_def by fastforce
  finally show ?thesis .
qed

lemma not_c_in_c'_simp[simp]:
  fixes \<phi>1 \<phi>2 \<psi> :: "'v propo" and x :: "'v"
  shows 
  "c_in_c'_symb c c' FT"
  "c_in_c'_symb c c' FF"
  "c_in_c'_symb c c' (FVar x)"
  "wf_conn c [conn c' [\<phi>1, \<phi>2], \<psi>] \<Longrightarrow> wf_conn c' [\<phi>1, \<phi>2] \<Longrightarrow> \<not> c_in_c'_only c c' (conn c [conn c' [\<phi>1, \<phi>2], \<psi>])"
  apply (simp_all add: c_in_c'_only_def)
proof
  assume "all_subformula_st (c_in_c'_symb c c') (conn c [conn c' [\<phi>1, \<phi>2], \<psi>])"
  and wf: "wf_conn c [conn c' [\<phi>1, \<phi>2], \<psi>]" "wf_conn c' [\<phi>1, \<phi>2]"
  hence "c_in_c'_symb c c' (conn c [conn c' [\<phi>1, \<phi>2], \<psi>])" using all_subformula_st_test_symb_true_phi by blast 
  thus False using not_c_in_c'_symb_l local.wf by blast
qed

lemma [simp]:
  fixes c c'  :: "'v connective" and \<psi> :: "'v propo"
  shows "c_in_c'_symb c c' (FNot \<psi>)"
proof -
  {
    fix \<xi> :: "'v propo"
    have "not_c_in_c'_symb c c' \<xi> \<Longrightarrow> \<xi> = FNot \<psi> \<Longrightarrow> False"
      apply (induct rule: not_c_in_c'_symb.induct)
      using conn_inj_not(2) by blast+
  }
 thus ?thesis by auto
qed  
  
lemma c_in_c'_symb_step_exists:
  fixes \<phi> :: "'v propo"
  assumes c: "c = CAnd \<or> c = COr" and c':"c' = CAnd \<or> c' = COr"
  shows "\<psi> \<preceq>\<phi> \<Longrightarrow> \<not> c_in_c'_symb c c' \<psi> \<Longrightarrow> \<exists>\<psi>'. push_c_inside_c' c c' \<psi> \<psi>'"
  apply (induct \<psi> rule: propo_induct_arity)
  apply auto[2]
proof -
  fix \<psi>1 \<psi>2 \<phi>':: "'v propo"
  assume IH\<psi>1: "\<psi>1 \<preceq> \<phi> \<Longrightarrow> \<not> c_in_c'_symb c c' \<psi>1 \<Longrightarrow> Ex (push_c_inside_c' c c' \<psi>1)"
  and IH\<psi>2: "\<psi>1 \<preceq> \<phi> \<Longrightarrow> \<not> c_in_c'_symb c c' \<psi>1 \<Longrightarrow> Ex (push_c_inside_c' c c' \<psi>1)"
  and \<phi>': "\<phi>' = FAnd \<psi>1 \<psi>2 \<or> \<phi>' = FOr \<psi>1 \<psi>2 \<or> \<phi>' = FImp \<psi>1 \<psi>2 \<or> \<phi>' = FEq \<psi>1 \<psi>2"
  and in\<phi>: "\<phi>' \<preceq> \<phi>" and  n0: "\<not>c_in_c'_symb c c' \<phi>'"
  hence n: "not_c_in_c'_symb c c' \<phi>'" by auto
  {
    assume \<phi>': "\<phi>' = conn c [\<psi>1, \<psi>2]"
    obtain a b where "\<psi>1 = conn c' [a, b] \<or> \<psi>2 = conn c' [a, b]"
      using n \<phi>' 
      proof (induct rule: not_c_in_c'_symb.induct)
        fix \<phi> \<phi>' \<psi>
        assume wf: "wf_conn c [conn c' [\<phi>, \<phi>'], \<psi>]" 
        and IH: "(\<And>a b. \<psi>1 = conn c' [a, b] \<or> \<psi>2 = conn c' [a, b] \<Longrightarrow> thesis)" 
        and "conn c [conn c' [\<phi>, \<phi>'], \<psi>] = conn c [\<psi>1, \<psi>2]"
        also have "wf_conn c [\<psi>1, \<psi>2]" using wf_conn_no_arity_change wf by (metis length_Cons) 
        ultimately have "\<psi>1 = conn c' [\<phi>, \<phi>']" using conn_inj by blast
        thus "thesis" using IH by metis
      next
        fix \<phi> \<phi>' \<psi>
        assume wf: "wf_conn c [\<psi>, conn c' [\<phi>, \<phi>']]" 
        and IH: "(\<And>a b. \<psi>1 = conn c' [a, b] \<or> \<psi>2 = conn c' [a, b] \<Longrightarrow> thesis)" 
        and "conn c [\<psi>, conn c' [\<phi>, \<phi>']] = conn c [\<psi>1, \<psi>2]"
        also have "wf_conn c [\<psi>1, \<psi>2]" using wf_conn_no_arity_change wf by (metis length_Cons) 
        ultimately have "\<psi>2 = conn c' [\<phi>, \<phi>']" using conn_inj by blast
        thus "thesis" using IH by metis
      qed 
    hence "Ex (push_c_inside_c' c c' \<phi>')" 
      using \<phi>' apply auto 
      using push_c_inside_c'.intros(1) c c' apply blast
      using push_c_inside_c'.intros(2) c c' by blast
  }
  also {
    assume \<phi>': "\<phi>' \<noteq> conn c [\<psi>1, \<psi>2]" 
    have "\<forall>\<phi> c ca. \<exists>\<phi>1 \<psi>1 \<psi>2 \<psi>1' \<psi>2' \<phi>2'. conn (c\<Colon>'v connective) [\<phi>1, conn ca [\<psi>1, \<psi>2]] = \<phi> \<or> conn c [conn ca [\<psi>1', \<psi>2'], \<phi>2'] = \<phi> \<or> c_in_c'_symb c ca \<phi>" by (metis not_c_in_c'_symb.cases)
    hence "Ex (push_c_inside_c' c c' \<phi>')"
      by (metis (no_types) c c' n push_c_inside_c'_l push_c_inside_c'_r)
  }
  ultimately show "Ex (push_c_inside_c' c c' \<phi>')" by blast
qed

declare[[show_types]]
lemma c_in_c'_symb_rew:
  fixes \<phi> :: "'v propo"
  assumes noTB: "\<not> c_in_c'_only c c' \<phi>"
  and c: "c = CAnd \<or> c = COr" and c':"c' = CAnd \<or> c' = COr"
  shows "\<exists>\<psi> \<psi>'. \<psi> \<preceq> \<phi> \<and> push_c_inside_c' c c' \<psi> \<psi>' "
proof -
  { (*TODO Jasmin: why is metis unable to instanciate test_symb and r?*)
    fix test_symb r
    have " \<forall>x\<Colon>'v. (test_symb\<Colon>'v propo \<Rightarrow> bool) FF \<and> test_symb FT \<and> test_symb (FVar x) \<Longrightarrow>
  (\<forall>\<phi>'\<Colon>'v propo. \<phi>' \<preceq> (\<phi>\<Colon>'v propo) \<longrightarrow> \<not> test_symb \<phi>' \<longrightarrow> (\<exists>\<psi>\<Colon>'v propo. (r\<Colon>'v propo \<Rightarrow> 'v propo \<Rightarrow> bool) \<phi>' \<psi>)) \<longrightarrow> (\<not> all_subformula_st test_symb \<phi>) \<longrightarrow> (\<exists>(\<psi>\<Colon>'v propo) \<psi>'\<Colon>'v propo. \<psi> \<preceq> \<phi> \<and> r \<psi> \<psi>')"
      using no_test_symb_step_exists  by blast
 } note a = this

  have test_symb_false_nullary: "\<forall>x. c_in_c'_symb c c' (FF:: 'v propo) \<and> c_in_c'_symb c c' FT \<and> c_in_c'_symb c c' (FVar (x:: 'v))"  by auto
  moreover {
    fix x :: "'v"
    have  H': "c_in_c'_symb c c' FT" "c_in_c'_symb c c' FF" "c_in_c'_symb c c' (FVar x)"
      by simp+
  }
  moreover {
    fix \<psi>
    have "\<psi> \<preceq> \<phi> \<Longrightarrow> \<not> c_in_c'_symb c c' \<psi> \<Longrightarrow> \<exists>\<psi>'. push_c_inside_c' c c' \<psi> \<psi>'"
      by (auto simp add: assms(2) c' c_in_c'_symb_step_exists)
  }
  ultimately show "?thesis" using noTB a  unfolding c_in_c'_only_def by metis
qed

end

