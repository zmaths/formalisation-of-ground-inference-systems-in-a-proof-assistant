(*  Title:       More about Multisets
    Author:      Jasmin Blanchette <blanchette at in.tum.de>, 2014, 2015
    Author:      Dmitriy Traytel <traytel at in.tum.de>, 2014
    Author:      Mathias Fleury <mathias.fleury@ens-rennes.fr>, 2015
    Maintainer:  Jasmin Blanchette <blanchette at in.tum.de>
*)

header {* More about Multisets *}

theory Multiset_More
imports "~~/src/HOL/Library/Multiset_Order"
begin

text {*
Isabelle's theory of finite multisets is not as developed as other areas, such as lists and sets.
The present theory introduces some missing concepts and lemmas. Some of it is expected to move to
Isabelle's library.
*}


subsection {* Basic Setup *}

declare
  diff_single_trivial [simp]
  ball_set_of_iff [simp]
  in_image_mset [iff]
  image_mset_cong [cong]
  image_mset.compositionality [simp]

abbreviation not_Melem where
  "not_Melem x A \<equiv> ~ (x \<in># A)" -- "non-membership"

notation
  not_Melem  ("op ~:#") and
  not_Melem  ("(_/ ~:# _)" [51, 51] 50)

notation (xsymbols)
  not_Melem  ("op \<notin>#") and
  not_Melem  ("(_/ \<notin># _)" [51, 51] 50)

notation (HTML output)
  not_Melem  ("op \<notin>#") and
  not_Melem  ("(_/ \<notin># _)" [51, 51] 50)


subsection {* Lemmas about cardinality*}

text {*
This sections adds various lemmas about mcard. Most lemmas have a finite set equivalent.
*}

lemma mcard_Suc_Diff1: "x \<in># \<Sigma> \<Longrightarrow> Suc (mcard (\<Sigma> - {#x#})) = mcard \<Sigma>"
  by (metis add_Suc insert_DiffM mcard_plus mcard_singleton monoid_add_class.add.left_neutral)

lemma mcard_Diff_singleton: "x \<in># \<Sigma> \<Longrightarrow> mcard (\<Sigma> - {#x#}) = mcard \<Sigma> - 1"
  by (simp add: mcard_Suc_Diff1 [symmetric])

lemma mcard_Diff_singleton_if: "mcard (A - {#x#}) = (if x \<in># A then mcard A - 1 else mcard A)"
  by (simp add: mcard_Diff_singleton)

lemma psubset_mcard_mono: "A < B ==> mcard A < mcard B"
  by (metis mset_less_size size_eq_mcard)

lemma mcard_Un_Int:
  "mcard A + mcard B = mcard (A #\<union> B) + mcard (A #\<inter> B)"
  by (metis (no_types, hide_lams) Multiset.diff_right_commute add.assoc add_left_cancel mcard_plus monoid_add_class.add.right_neutral multiset_inter_commute multiset_inter_def sup_commute sup_empty sup_multiset_def)

lemma mcard_Un_disjoint:
  assumes "A #\<inter> B = {#}"
  shows "mcard (A #\<union> B) = mcard A + mcard B"
  using assms mcard_Un_Int [of A B] by simp

lemma mcard_Diff_subset_Int:
  shows "mcard (\<Sigma> - \<Sigma>') = mcard \<Sigma> - mcard (\<Sigma> #\<inter> \<Sigma>')"
proof -
  have "\<Sigma> - \<Sigma>' = \<Sigma> - \<Sigma> #\<inter> \<Sigma>'" by (metis Multiset.diff_cancel Multiset.diff_right_commute inf_idem multiset_inter_commute multiset_inter_def)
  thus ?thesis by (simp add: mcard_Diff_subset)
qed

lemma diff_mcard_le_mcard_Diff:  "mcard \<Sigma> - mcard \<Sigma>' \<le> mcard (\<Sigma> - \<Sigma>')"
proof-
  have "mcard \<Sigma> - mcard \<Sigma>' \<le> mcard \<Sigma> - mcard (\<Sigma> #\<inter> \<Sigma>')" using mcard_mono diff_le_mono2 inf_le2 by blast
  also have "\<dots> = mcard(\<Sigma>-\<Sigma>')" using assms by(simp add: mcard_Diff_subset_Int)
  finally show ?thesis .
qed

lemma mcard_Diff1_less: "x\<in># \<Sigma> \<Longrightarrow> mcard (\<Sigma> - {#x#}) < mcard \<Sigma>"
  apply (rule Suc_less_SucD)
  by (simp add: mcard_Suc_Diff1)

lemma mcard_Diff2_less: "x\<in># \<Sigma> \<Longrightarrow> y\<in># \<Sigma> \<Longrightarrow> mcard (\<Sigma> - {#x#} - {#y#}) < mcard \<Sigma>"
  by (metis less_imp_diff_less mcard_Diff1_less mcard_Diff_subset_Int)+

lemma mcard_Diff1_le: "mcard (\<Sigma> - {#x#}) \<le> mcard \<Sigma>"
  apply (case_tac "x \<in># \<Sigma>")
  by (simp_all add: mcard_Diff1_less less_imp_le)

lemma mcard_psubset: "\<Sigma> \<le> \<Sigma>' \<Longrightarrow> mcard \<Sigma> < mcard \<Sigma>' \<Longrightarrow> \<Sigma> < \<Sigma>'"
  using less_irrefl mset_less_def by blast


subsection {* Multiset Extension of Multiset Ordering *}

text {*
The @{text "op \<subset>##"} and @{text "op \<subseteq>##"} operators are introduced as the multiset extension of
the multiset orderings of @{term "op \<subset>#"} and @{term "op \<subseteq>#"}.
*}

definition
  less_mset_mset :: "('a \<Colon> order) multiset multiset \<Rightarrow> 'a multiset multiset \<Rightarrow> bool" (infix "<##" 50)
where
  "M' <## M \<longleftrightarrow> (M', M) \<in> mult {(x', x). x' <# x}"

definition
  le_mset_mset :: "('a \<Colon> order) multiset multiset \<Rightarrow> 'a multiset multiset \<Rightarrow> bool" (infix "<=##" 50)
where
  "M' <=## M \<longleftrightarrow> M' <## M \<or> M' = M"

notation (xsymbols) less_mset_mset (infix "\<subset>##" 50)
notation (xsymbols) le_mset_mset (infix "\<subseteq>##" 50)

lemmas less_mset_mset\<^sub>D\<^sub>M = order.mult\<^sub>D\<^sub>M[OF order_multiset, folded less_mset_mset_def]
lemmas less_mset_mset\<^sub>H\<^sub>O = order.mult\<^sub>H\<^sub>O[OF order_multiset, folded less_mset_mset_def]

interpretation multiset_multiset_order: order
  "le_mset_mset :: ('a \<Colon> linorder) multiset multiset \<Rightarrow> ('a \<Colon> linorder) multiset multiset \<Rightarrow> bool"
  "less_mset_mset :: ('a \<Colon> linorder) multiset multiset \<Rightarrow> ('a \<Colon> linorder) multiset multiset \<Rightarrow> bool"
  unfolding less_mset_mset_def[abs_def] le_mset_mset_def[abs_def] less_multiset_def[abs_def]
  by (rule order.order_mult)+ default

interpretation multiset_multiset_linorder: linorder
  "le_mset_mset :: ('a \<Colon> linorder) multiset multiset \<Rightarrow> ('a \<Colon> linorder) multiset multiset \<Rightarrow> bool"
  "less_mset_mset :: ('a \<Colon> linorder) multiset multiset \<Rightarrow> ('a \<Colon> linorder) multiset multiset \<Rightarrow> bool"
  unfolding less_mset_mset_def[abs_def] le_mset_mset_def[abs_def]
  by (rule linorder.linorder_mult[OF linorder_multiset])

lemma wf_less_mset_mset: "wf {(\<Sigma> \<Colon> ('a \<Colon> wellorder) multiset multiset, T). \<Sigma> \<subset>## T}"
  unfolding less_mset_mset_def by (auto intro: wf_mult wf_less_multiset)

interpretation multiset_multiset_wellorder: wellorder
  "le_mset_mset :: ('a \<Colon> wellorder) multiset multiset \<Rightarrow> ('a \<Colon> wellorder) multiset multiset \<Rightarrow> bool"
  "less_mset_mset :: ('a \<Colon> wellorder) multiset multiset \<Rightarrow> ('a \<Colon> wellorder) multiset multiset \<Rightarrow> bool"
  by unfold_locales (blast intro: wf_less_mset_mset[unfolded wf_def, rule_format])

lemma union_less_mset_mset_mono2: "B \<subset>## D ==> C + B \<subset>## C + (D::'a::order multiset multiset)"
apply (unfold less_mset_mset_def mult_def)
apply (erule trancl_induct)
 apply (blast intro: mult1_union)
apply (blast intro: mult1_union trancl_trans)
done

lemma union_less_mset_mset_diff_plus:
  "U \<le> \<Sigma> \<Longrightarrow> T \<subset>## U \<Longrightarrow> \<Sigma> - U + T \<subset>## \<Sigma>"
  by (drule ordered_cancel_comm_monoid_diff_class.diff_add[symmetric])
    (metis union_less_mset_mset_mono2)

lemma ex_gt_imp_less_mset_mset:
  "(\<exists>y \<Colon> 'a \<Colon> linorder multiset. y \<in># T \<and> (\<forall>x. x \<in># \<Sigma> \<longrightarrow> x \<subset># y)) \<Longrightarrow> \<Sigma> \<subset>## T"
  by (metis less_mset_mset\<^sub>H\<^sub>O less_nat_zero_code multiset_linorder.not_less_iff_gr_or_eq not_gr0)

end
