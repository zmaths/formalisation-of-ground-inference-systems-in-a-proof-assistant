(*  Based on
    Title:       Clausal Logic
    Author:      Jasmin Blanchette <blanchette at in.tum.de>, 2014
    Author:      Dmitriy Traytel <traytel at in.tum.de>, 2014
    Maintainer:  Jasmin Blanchette <blanchette at in.tum.de>
*)

chapter {* Clausal Logic *}

theory Partial_Clausal_Logic
imports Clausal_Logic
begin

text {*
Resolution operates of clauses, which are disjunctions of literals. The material formalized here
corresponds roughly to Sections 2.1 (``Formulas and Clauses'') and 2.2 (``Herbrand
Interpretations'') of Bachmair and Ganzinger, excluding the formula and term syntax.
*}

subsection {* Literals *}

text {*
Literals consist of a polarity (positive or negative) and an atom, of type @{typ 'a}.
*}



subsection {* Clauses *}

text {*
Clauses are (finite) multisets of literals.
*}

type_synonym 'a clause = "'a literal multiset"
type_synonym 'v propo = "'v clause set"

subsection {* Partial Interpretations *}

type_synonym 'a interp = "'a literal set"

definition true_lit :: "'a interp \<Rightarrow> 'a literal \<Rightarrow> bool" (infix "\<Turnstile>l" 50) where
  "I \<Turnstile>l L \<longleftrightarrow> L \<in> I"

declare true_lit_def[simp]

subsubsection \<open>Consistency\<close>
definition consistent_interp :: "'a literal set \<Rightarrow> bool" where
"consistent_interp I = (\<forall>L. \<not>(L \<in> I \<and> - L \<in> I))"

lemma consistent_interp_subset:
  assumes "A \<subseteq> B"
  and "consistent_interp B"
  shows "consistent_interp A"
  using assms unfolding consistent_interp_def by auto

lemma consistent_interp_change_insert[simp]:
  "a \<notin> A \<Longrightarrow> -a \<notin> A \<Longrightarrow> consistent_interp (insert (-a) A) \<longleftrightarrow> consistent_interp (insert a A)"
  unfolding consistent_interp_def by fastforce 

subsubsection \<open>Atoms\<close> 
definition atms_of_m :: "'a literal multiset set \<Rightarrow> 'a set" where
"atms_of_m \<psi>s = {atm_of \<chi> | \<chi>. \<exists>\<psi>. \<psi> \<in> \<psi>s \<and> \<chi> :# \<psi>}"

definition atms_of_s :: "'a literal set \<Rightarrow> 'a set" where
  "atms_of_s C = atm_of ` C"

lemma atms_of_m_emtpy_set[simp]:
  "atms_of_m {} = {}"
  unfolding atms_of_m_def by auto

lemma atms_of_m_mono:
  "A \<subseteq> B \<Longrightarrow> atms_of_m A \<subseteq> atms_of_m B"
  unfolding atms_of_m_def by auto
lemma atms_of_m_finite[simp]:
  "finite \<psi>s \<Longrightarrow> finite (atms_of_m \<psi>s)"
  unfolding atms_of_m_def by auto

lemma atms_of_m_union[iff]:
  "atms_of_m (\<psi>s \<union> \<chi>s) = atms_of_m \<psi>s \<union> atms_of_m \<chi>s"
  unfolding atms_of_m_def by auto

lemma atms_of_m_insert:
  "atms_of_m (insert \<psi>s \<chi>s) = atms_of_m {\<psi>s} \<union> atms_of_m \<chi>s"
  unfolding atms_of_m_def by auto
  
lemma atms_of_m_plus[iff]:
  fixes C D :: "'a literal multiset"
  shows "atms_of_m {C + D} = atms_of_m {C} \<union> atms_of_m {D}"
  unfolding atms_of_m_def by auto

lemma atms_of_m_singleton[simp]: "atms_of_m {L} = atms_of L"
  unfolding atms_of_m_def atms_of_def by auto

lemma in_atms_of_in_atms_of_m[simp]:
  "A \<in> \<psi> \<Longrightarrow> atms_of A \<subseteq> atms_of_m \<psi>"
  unfolding atms_of_m_def atms_of_def by fastforce

lemma in_m_in_literals:
  assumes "{#A#} + D \<in> \<psi>s"
  shows "atm_of A \<in> atms_of_m \<psi>s"
  unfolding atms_of_m_def using assms by fastforce
  
lemma atms_of_s_union:
  "atms_of_s (Ia \<union> {L}) = atms_of_s Ia \<union> {atm_of L}"
  unfolding atms_of_s_def by auto
  
lemma in_atms_of_s_decomp[iff]:
  "P \<in> atms_of_s I = (Pos P \<in> I \<or> Neg P \<in> I)" (is "?P = ?Q")
proof
  assume "?P"
  thus "?Q" unfolding atms_of_s_def by (metis image_iff literal.exhaust_sel)
next
  assume "?Q"
  thus "?P" unfolding atms_of_s_def by force
qed 

  
subsubsection \<open>Totality\<close>
definition total_over_set :: "'a interp \<Rightarrow> 'a set \<Rightarrow> bool" where
"total_over_set I S = (\<forall>l\<in>S. Pos l \<in> I \<or> Neg l \<in> I)"

definition total_over_m  :: "'a literal set \<Rightarrow> 'a clause set \<Rightarrow> bool" where
"total_over_m I \<psi>s = total_over_set I (atms_of_m \<psi>s)"

lemma total_over_m_subset:
  "A \<subseteq> B \<Longrightarrow> total_over_m I B \<Longrightarrow> total_over_m I A"
  using atms_of_m_mono[of A] unfolding total_over_m_def total_over_set_def by auto   
  
lemma total_over_m_sum:
  shows "total_over_m I {C + D} \<longleftrightarrow> (total_over_m I {C} \<and> total_over_m I {D})"
  using assms unfolding total_over_m_def total_over_set_def by auto

lemma total_over_m_union:
  "total_over_m I (A \<union> B) \<longleftrightarrow> (total_over_m I A \<and> total_over_m I B)"
  unfolding total_over_m_def total_over_set_def by auto
  
lemma total_over_m_insert:
  "total_over_m I (insert a A) \<longleftrightarrow> (total_over_m I {a} \<and> total_over_m I A)"
  unfolding total_over_m_def total_over_set_def using atms_of_m_insert by fastforce
  
lemma atms_of_m_remove_incl: 
  shows "atms_of_m (Set.remove a \<psi>) \<subseteq> atms_of_m \<psi>"
  unfolding atms_of_m_def unfolding remove_def by auto
lemma total_over_m_extension:
  fixes I :: "'v literal set" and A :: "'v propo"
  assumes "total_over_m I A"
  shows "\<exists>I'. total_over_m (I \<union> I') (A\<union>B) \<and> (\<forall>x\<in>I'. atm_of x \<in> atms_of_m B \<and> atm_of x \<notin> atms_of_m A)"
proof -
  let ?I' = "{Pos v |v. v\<in> atms_of_m B \<and> v \<notin> atms_of_m A}"
  have "(\<forall>x\<in>?I'. atm_of x \<in> atms_of_m B \<and> atm_of x \<notin> atms_of_m A)" by auto
  also have "total_over_m (I \<union> ?I') (A\<union>B)"
    unfolding total_over_m_def total_over_set_def apply auto
    by (meson assms total_over_m_def total_over_set_def)+
  ultimately show ?thesis by blast
qed

lemma total_over_m_consistent_extension:
  fixes I :: "'v literal set" and A :: "'v propo"
  assumes "total_over_m I A"
  and cons: "consistent_interp I"
  shows "\<exists>I'. total_over_m (I \<union> I') (A \<union> B) \<and> (\<forall>x\<in>I'. atm_of x \<in> atms_of_m B \<and> atm_of x \<notin> atms_of_m A) \<and> consistent_interp (I \<union> I')"
proof -
  let ?I' = "{Pos v |v. v\<in> atms_of_m B \<and> v \<notin> atms_of_m A \<and> Pos v \<notin> I \<and> Neg v \<notin> I}"
  have "(\<forall>x\<in>?I'. atm_of x \<in> atms_of_m B \<and> atm_of x \<notin> atms_of_m A)" by auto
  also have "total_over_m (I \<union> ?I') (A\<union>B)"
    unfolding total_over_m_def total_over_set_def apply auto
    by (meson assms total_over_m_def total_over_set_def)+
  moreover have "consistent_interp (I \<union> ?I')"
    using cons unfolding consistent_interp_def apply auto
    by (metis Clausal_Logic.uminus_of_uminus_id uminus_Pos)
  ultimately show ?thesis by blast
qed

lemma total_over_set_atms_of[simp]:
  "total_over_set Ia (atms_of_s Ia)"
  unfolding total_over_set_def atms_of_s_def by (metis image_iff literal.exhaust_sel) 
  

lemma total_over_set_literal_defined:
  assumes "{#A#} + D \<in> \<psi>s"
  and "total_over_set I (atms_of_m \<psi>s)"
  shows "A \<in> I \<or> -A \<in> I"
  using assms unfolding total_over_set_def by (metis (no_types) Neg_atm_of_iff in_m_in_literals literal.collapse(1) uminus_Neg uminus_Pos)

lemma tot_over_m_remove:  
  assumes "total_over_m (I \<union> {L}) {\<psi>}"
  and L: "\<not> L \<in># \<psi> \<and> \<not> -L \<in># \<psi>"
  shows "total_over_m I {\<psi>}"
  unfolding total_over_m_def total_over_set_def
proof 
  fix l
  assume l: "l \<in> atms_of_m {\<psi>}"
  hence "Pos l \<in> I \<or> Neg l \<in> I \<or> l = atm_of L"
     using assms unfolding total_over_m_def total_over_set_def by auto
  also have "atm_of L \<notin> atms_of_m {\<psi>}" 
    proof (rule ccontr)
      assume "\<not> atm_of L \<notin> atms_of_m {\<psi>}"
      hence "atm_of L \<in> atms_of \<psi>" by auto
      hence "Pos (atm_of L) \<in># \<psi> \<or> Neg (atm_of L) \<in># \<psi>" using atm_imp_pos_or_neg_lit by metis
      hence "L \<in># \<psi> \<or> - L \<in># \<psi>" by (case_tac L, auto)
      thus False using L by auto
    qed   
  ultimately show  "Pos l \<in> I \<or> Neg l \<in> I" using l by metis
qed

lemma total_union:
  assumes "total_over_m I \<psi>"
  shows "total_over_m (I \<union> I') \<psi>"
  using assms unfolding total_over_m_def total_over_set_def by auto

lemma total_union_2:
  assumes "total_over_m I \<psi>"
  and "total_over_m I' \<psi>'"
  shows "total_over_m (I \<union> I') (\<psi> \<union> \<psi>')"
  using assms unfolding total_over_m_def total_over_set_def by auto

subsubsection \<open>Interpretations\<close>
definition true_cls :: "'a interp \<Rightarrow> 'a clause \<Rightarrow> bool" (infix "\<Turnstile>" 50) where
  "I \<Turnstile> C \<longleftrightarrow> (\<exists>L. L \<in># C \<and> I \<Turnstile>l L)"

lemma true_cls_empty[iff]: "\<not> I \<Turnstile> {#}"
  unfolding true_cls_def by simp

lemma true_cls_singleton[iff]: "I \<Turnstile> {#L#} \<longleftrightarrow> I \<Turnstile>l L"
  unfolding true_cls_def by simp

lemma true_cls_union[iff]: "I \<Turnstile> C + D \<longleftrightarrow> I \<Turnstile> C \<or> I \<Turnstile> D"
  unfolding true_cls_def by auto

lemma true_cls_mono: "set_of C \<subseteq> set_of D \<Longrightarrow> I \<Turnstile> C \<Longrightarrow> I \<Turnstile> D"
  unfolding true_cls_def subset_eq by (metis mem_set_of_iff)

lemma true_cls_union_increase: 
 assumes "I \<Turnstile> \<psi>"
 shows "I \<union> I' \<Turnstile> \<psi>"
 using assms unfolding true_cls_def by auto

lemma true_cls_replicate_mset[iff]: "I \<Turnstile> replicate_mset n L \<longleftrightarrow> n \<noteq> 0 \<and> I \<Turnstile>l L"
  by (induct n) auto

definition true_clss :: "'a interp \<Rightarrow> 'a clause set \<Rightarrow> bool" (infix "\<Turnstile>s" 50) where
  "I \<Turnstile>s CC \<longleftrightarrow> (\<forall>C \<in> CC. I \<Turnstile> C)"

lemma true_clss_empty[iff]: "I \<Turnstile>s {}"
  unfolding true_clss_def by blast

lemma true_clss_singleton[iff]: "I \<Turnstile>s {C} \<longleftrightarrow> I \<Turnstile> C"
  unfolding true_clss_def by blast

lemma true_clss_union[iff]: "I \<Turnstile>s CC \<union> DD \<longleftrightarrow> I \<Turnstile>s CC \<and> I \<Turnstile>s DD"
  unfolding true_clss_def by blast

lemma true_clss_mono: "DD \<subseteq> CC \<Longrightarrow> I \<Turnstile>s CC \<Longrightarrow> I \<Turnstile>s DD"
  unfolding true_clss_def by blast
  
lemma true_clss_union_increase: 
 assumes "I \<Turnstile>s \<psi>"
 shows "I \<union> I' \<Turnstile>s \<psi>"
 using assms true_cls_union_increase unfolding true_clss_def by auto

 (*TODO: simp rule?*)
lemma model_remove: "I \<Turnstile>s N \<Longrightarrow> I \<Turnstile>s Set.remove a N"
  by (simp add: true_clss_def)
  
lemma notin_vars_union_true_cls_true_cls:
  assumes "\<forall>x\<in>I'. atm_of x \<notin> atms_of_m A"
  and "atms_of L \<subseteq> atms_of_m A"
  and "I \<union> I' \<Turnstile> L"
  shows "I \<Turnstile> L"
  using assms unfolding true_cls_def true_lit_def by (metis Un_iff atm_of_lit_in_atms_of contra_subsetD)
  
lemma notin_vars_union_true_clss_true_clss:
  assumes "\<forall>x\<in>I'. atm_of x \<notin> atms_of_m A"
  and "atms_of_m L \<subseteq> atms_of_m A"
  and "I \<union> I' \<Turnstile>s L"
  shows "I \<Turnstile>s L"
  using assms unfolding true_clss_def true_lit_def Ball_def by (meson in_atms_of_in_atms_of_m notin_vars_union_true_cls_true_cls subset_trans)
  
subsubsection \<open>Satisfiability\<close>
definition satisfiable :: "'a clause set \<Rightarrow> bool" where
  "satisfiable CC \<equiv> \<exists>I. (I \<Turnstile>s CC \<and> consistent_interp I \<and> total_over_m I CC)"

abbreviation unsatisfiable :: "'a clause set \<Rightarrow> bool" where  
  "unsatisfiable CC \<equiv> \<not> satisfiable CC"

subsubsection \<open>Multiset of clauses\<close>  
definition true_cls_mset :: "'a interp \<Rightarrow> 'a clause multiset \<Rightarrow> bool" (infix "\<Turnstile>m" 50) where
  "I \<Turnstile>m CC \<longleftrightarrow> (\<forall>C. C \<in># CC \<longrightarrow> I \<Turnstile> C)"

lemma true_cls_mset_empty[iff]: "I \<Turnstile>m {#}"
  unfolding true_cls_mset_def by auto

lemma true_cls_mset_singleton[iff]: "I \<Turnstile>m {#C#} \<longleftrightarrow> I \<Turnstile> C"
  unfolding true_cls_mset_def by auto

lemma true_cls_mset_union[iff]: "I \<Turnstile>m CC + DD \<longleftrightarrow> I \<Turnstile>m CC \<and> I \<Turnstile>m DD"
  unfolding true_cls_mset_def by auto

lemma true_cls_mset_image_mset[iff]: "I \<Turnstile>m image_mset f A \<longleftrightarrow> (\<forall>x . x \<in># A \<longrightarrow> I \<Turnstile> f x)"
  unfolding true_cls_mset_def by auto

lemma true_cls_mset_mono: "set_of DD \<subseteq> set_of CC \<Longrightarrow> I \<Turnstile>m CC \<Longrightarrow> I \<Turnstile>m DD"
  unfolding true_cls_mset_def subset_iff by auto

lemma true_clss_set_of[iff]: "I \<Turnstile>s set_of CC \<longleftrightarrow> I \<Turnstile>m CC"
  unfolding true_clss_def true_cls_mset_def by auto

lemma true_clss_union_decrease:
  assumes II': "I \<union> I' \<Turnstile> \<psi>"
  and H: "\<forall>v \<in> I'. atm_of v \<notin> atms_of \<psi>"
  shows "I \<Turnstile> \<psi>"
  using atm_of_lit_in_atms_of assms unfolding true_lit_def true_cls_def by auto

lemma multiset_not_empty:
  assumes "M \<noteq> {#}"
  and "x \<in># M"
  shows "\<exists>A. x = Pos A \<or> x = Neg A"
  using assms literal.exhaust_sel by blast

lemma atms_of_m_empty:
  fixes \<psi> :: "'v propo"
  assumes "finite \<psi>" and "atms_of_m \<psi> = {}"
  shows "\<psi> = {} \<or> \<psi> = {{#}}"
  using assms
proof (induct rule: finite_induct)
  assume "atms_of_m {} = {}"
  thus "{} = {} \<or> {} = {{#}}" by auto
next
  fix x :: "'v clause" and F :: "'v propo"
  assume "finite F"
  and "x \<notin> F"
  and ai: "atms_of_m (insert x F) = {}"
  and IH: "atms_of_m F = {} \<Longrightarrow> F = {} \<or> F = {{#}}"
  have aixF: "atms_of_m (insert x F) =  atms_of_m {x} \<union>  atms_of_m F" unfolding atms_of_m_def by auto
  hence "atms_of_m {x} = {}" using ai by auto
  hence "x = {#}" unfolding atms_of_m_def by (auto simp add: multiset_eq_iff)

  also have "F = {} \<or> F = {{#}}" using aixF ai IH by auto
  ultimately show "insert x F = {} \<or> insert x F = {{#}}" by auto
qed


lemma consistent_interp_disjoint:
 assumes consI: "consistent_interp I"
 and disj: "atms_of_s A \<inter> atms_of_s I = {}"
 and consA: "consistent_interp A"
 shows "consistent_interp (A \<union> I)"
 unfolding consistent_interp_def 
proof (rule ccontr)
  assume " \<not> (\<forall>L. \<not> (L \<in> A \<union> I \<and> - L \<in> A \<union> I))"
  then obtain L where "L \<in> A \<union> I \<and> - L \<in> A \<union> I" by metis
  also have "\<And>L. \<not> (L \<in> A \<and> -L \<in> I)" 
    proof (rule ccontr)
      fix L
      assume " \<not> \<not> (L \<in> A \<and> - L \<in> I)"
      hence "atm_of L \<in> atms_of_s A \<and> atm_of (-L) \<in> atms_of_s I"
        unfolding atms_of_s_def by (auto simp del: atm_of_uminus)
      hence "atm_of L \<in> atms_of_s A \<and> atm_of L \<in> atms_of_s I"  by simp
      thus False using disj  by auto
    qed
  ultimately show False using consA consI unfolding consistent_interp_def by (metis (full_types) Un_iff literal.exhaust_sel uminus_Neg uminus_Pos) 
qed

lemma total_remove_unused:
  assumes "total_over_m I \<psi>"
  shows "total_over_m {v| v. v \<in> I \<and> atm_of v \<in> atms_of_m \<psi>} \<psi>"
  unfolding total_over_m_def by (metis (no_types, lifting) assms literal.sel(1) literal.sel(2) mem_Collect_eq total_over_m_def total_over_set_def)

  
lemma total_over_set_atm_of:
  fixes I :: "'v interp" and K :: "'v set"
  shows "total_over_set I K  \<longleftrightarrow> (\<forall>l \<in> K. l \<in> (atm_of ` I))"
  unfolding total_over_set_def by (metis atms_of_s_def in_atms_of_s_decomp) 
  
subsubsection \<open>Tautology\<close>  
definition "tautology (\<psi>:: 'v clause) \<equiv> (\<forall>I. total_over_set I (atms_of \<psi>) \<longrightarrow> I \<Turnstile> \<psi>)"
lemma tautology_Pos_Neg_add[simp]: "tautology (A + {#Pos p, Neg p#})"
  unfolding tautology_def
proof
  fix I
  {
    assume "total_over_set I (atms_of (A + {#Pos p, Neg p#}))"
    hence "Pos p \<in> I \<or> Neg p \<in> I" unfolding total_over_m_def total_over_set_def by auto
    hence "I \<Turnstile> A + {#Pos p, Neg p#}" by auto
  }
  thus "total_over_set I (atms_of (A + {#Pos p, Neg p#})) \<longrightarrow> I \<Turnstile> A + {#Pos p, Neg p#}" by auto
qed

lemma tautology_Pos_Neg[simp]:
  assumes "Pos P \<in># A" and "Neg P \<in># A"
  shows  "tautology A"
proof -
  obtain A' where "A = A' + {#Pos P, Neg P#}"
    proof -
      assume a1: "\<And>A'. A = A' + {#Pos P, Neg P#} \<Longrightarrow> thesis"
      have "A - {#Pos P#} + {#Pos P#} = A"
        by (simp add: assms(1)) (* 0.4 ms *)
      hence "\<And>l m. A - {#l#} + m = A - {#Pos P#} - {#l#} + ({#Pos P#} + m) \<or> l = Pos P"
        by (metis (no_types) add.assoc diff_union_swap) (* 113 ms *)
      thus ?thesis
        using a1 by (metis assms(2) insert_DiffM2 literal.distinct(2)) (* 38 ms *)
    qed 
  thus ?thesis using tautology_Pos_Neg_add by metis
qed  

lemma tautology_minus[simp]:
  assumes "L \<in># A" and "-L \<in># A"
  shows  "tautology A"
   by (metis assms literal.exhaust tautology_Pos_Neg uminus_Neg uminus_Pos)

lemma tautology_exists_Pos_Neg:
  assumes "tautology \<psi>"
  shows "\<exists>p. Pos p \<in># \<psi> \<and> Neg p \<in># \<psi>"
proof (rule ccontr)
  assume p: "\<not> (\<exists>p. Pos p \<in># \<psi> \<and> Neg p \<in># \<psi>)"
  let ?I = "{-L | L. L \<in># \<psi>}"
  have "total_over_set ?I (atms_of \<psi>)" unfolding total_over_set_def using atm_imp_pos_or_neg_lit by force
  also have "\<not> ?I \<Turnstile> \<psi>" unfolding true_cls_def true_lit_def apply auto
     by (metis p literal.exhaust neq0_conv uminus_Neg uminus_Pos)
  ultimately show False using assms unfolding tautology_def by auto
qed

lemma tautology_decomp:
  "tautology \<psi> \<longleftrightarrow> (\<exists>p. Pos p \<in># \<psi> \<and> Neg p \<in># \<psi>)"
  using tautology_exists_Pos_Neg by auto

lemma minus_interp_tautology:
  assumes "{-L | L. L\<in># \<chi>} \<Turnstile> \<chi>"
  shows "tautology \<chi>"
proof -
  obtain L where "L \<in># \<chi> \<and> -L \<in># \<chi>"
    using assms unfolding true_cls_def true_lit_def by auto
  thus ?thesis using tautology_decomp literal.exhaust uminus_Neg uminus_Pos by metis  
qed

lemma remove_literal_in_model_tautology:
  assumes "I \<union> {Pos P} \<Turnstile> \<phi>"
  and "I \<union> {Neg P} \<Turnstile> \<phi>"
  shows "I \<Turnstile> \<phi> \<or> tautology \<phi>"
  using assms unfolding true_cls_def by auto
  
lemma satisfiable_decreasing:
  assumes "satisfiable (\<psi> \<union> \<psi>')"
  shows "satisfiable \<psi>"
  using assms total_over_m_union unfolding satisfiable_def by blast 

lemma tautology_imp_tautology:
  fixes \<chi> \<chi>' :: "'v clause"
  assumes "\<forall>I. total_over_m I {\<chi>} \<longrightarrow> I \<Turnstile> \<chi> \<longrightarrow> I \<Turnstile> \<chi>'" and "tautology \<chi>"
  shows "tautology \<chi>'" unfolding tautology_def apply clarify
proof -
  fix I ::"'v literal set"
  assume totI: "total_over_set I (atms_of \<chi>')"
  let ?I' = "{Pos v |v. v\<in> atms_of \<chi> \<and> v \<notin> atms_of_s I}"
  have totI': "total_over_m (I \<union> ?I') {\<chi>}" unfolding total_over_m_def total_over_set_def by auto
  hence \<chi>: "I \<union> ?I' \<Turnstile> \<chi>" using assms(2) unfolding total_over_m_def tautology_def by simp
  hence "I \<union> (?I'- I) \<Turnstile> \<chi>'" using assms(1) totI' by auto
  also have "\<And>L. L \<in># \<chi>' \<Longrightarrow> L \<notin> ?I'"
    proof -
      fix L :: "'v literal"
      assume a1: "L \<in># \<chi>'"
      have "\<forall>v. v \<notin> atms_of \<chi>' \<or> Pos v \<in> I \<or> Neg v \<in> I"
        by (meson totI total_over_set_def) (* 8 ms *)
      thus "L \<notin> {Pos v |v. v \<in> atms_of \<chi> \<and> v \<notin> atms_of_s I}"
        using a1 pos_lit_in_atms_of by force (* 63 ms *)
    qed 
  ultimately show "I \<Turnstile> \<chi>'" unfolding true_lit_def true_cls_def by auto
qed

subsubsection \<open>Clause and propositions\<close>
definition true_clause :: "'a clause \<Rightarrow> 'a clause \<Rightarrow> bool" (infix "\<Turnstile>f" 49) where 
"\<psi> \<Turnstile>f \<chi> \<equiv> (\<forall>I. total_over_m I ({\<psi>} \<union> {\<chi>}) \<longrightarrow> consistent_interp I \<longrightarrow> I \<Turnstile> \<psi> \<longrightarrow> I \<Turnstile> \<chi>)"

definition true_clause_propo :: "'a clause \<Rightarrow> 'a propo \<Rightarrow> bool" (infix "\<Turnstile>fs" 49) where 
"\<psi> \<Turnstile>fs \<chi> \<equiv> (\<forall>I. total_over_m I ({\<psi>} \<union> \<chi>) \<longrightarrow> consistent_interp I \<longrightarrow> I \<Turnstile> \<psi> \<longrightarrow> I \<Turnstile>s \<chi>)"

definition true_propo :: "'a propo \<Rightarrow> 'a clause \<Rightarrow> bool" (infix "\<Turnstile>p" 49) where 
"N \<Turnstile>p \<chi> \<equiv> (\<forall>I. total_over_m I (N \<union> {\<chi>}) \<longrightarrow> consistent_interp I \<longrightarrow> I \<Turnstile>s N \<longrightarrow> I \<Turnstile> \<chi>)"

definition true_propo_propo :: "'a propo \<Rightarrow> 'a propo \<Rightarrow> bool" (infix "\<Turnstile>ps" 49) where 
"N \<Turnstile>ps N' \<equiv> (\<forall>I. total_over_m I (N \<union> N') \<longrightarrow> consistent_interp I \<longrightarrow> I \<Turnstile>s N \<longrightarrow> I \<Turnstile>s N')"

lemma true_clause_refl[simp]:
  "A \<Turnstile>f A "
  unfolding true_clause_def by auto
lemma [simp]:
  "a \<Turnstile>f C \<Longrightarrow> insert a A \<Turnstile>p C"
  unfolding true_clause_def true_propo_def true_clss_def using total_over_m_union by fastforce
  
lemma true_clause_propo_empty[simp]:
  "N \<Turnstile>fs {}"
  unfolding true_clause_propo_def by auto
  
lemma true_prop_true_clause[simp]:
  "{\<phi>} \<Turnstile>p \<psi> \<longleftrightarrow> \<phi> \<Turnstile>f \<psi>"
  unfolding true_clause_def true_propo_def by auto

lemma true_propo_propo_true_propo[simp]:
  "N \<Turnstile>ps {\<psi>} \<longleftrightarrow> N \<Turnstile>p \<psi>"
  unfolding true_propo_propo_def true_propo_def by auto
  
lemma true_propo_propo_true_clause_propo[simp]:
  "{\<chi>} \<Turnstile>ps \<psi> \<longleftrightarrow> \<chi> \<Turnstile>fs \<psi>"
  unfolding true_propo_propo_def true_clause_propo_def by auto
  
lemma true_propo_propo_empty[simp]:
  "N \<Turnstile>ps {}"
  unfolding true_propo_propo_def by auto
  
lemma [simp]:
  "A \<Turnstile>p CC \<Longrightarrow> A \<union> B  \<Turnstile>p CC"
  unfolding true_propo_def total_over_m_union by fastforce

lemma [simp]:
  "B \<Turnstile>p CC \<Longrightarrow> A \<union> B  \<Turnstile>p CC"
  unfolding true_propo_def total_over_m_union by fastforce

lemma [simp]:
  "A \<Turnstile>p CC \<longrightarrow> A \<Turnstile>p CC + CC'"
  unfolding true_propo_def total_over_m_union total_over_m_sum by blast

lemma [simp]:
  "A \<Turnstile>p CC' \<longrightarrow> A \<Turnstile>p CC + CC'"
  unfolding true_propo_def total_over_m_union total_over_m_sum by blast

lemma true_propo_propo_union_l[simp]:
  "A \<Turnstile>ps CC \<Longrightarrow> A \<union> B  \<Turnstile>ps CC"
  unfolding true_propo_propo_def total_over_m_union by fastforce

lemma true_propo_propo_union_l_r[simp]:
  "B \<Turnstile>ps CC \<Longrightarrow> A \<union> B  \<Turnstile>ps CC"
  unfolding true_propo_propo_def total_over_m_union by fastforce

lemma true_propo_in[simp]:
  "CC \<in> A \<Longrightarrow> A \<Turnstile>p CC"
  unfolding true_propo_def true_clss_def total_over_m_union by fastforce
  
lemma [simp]:
  "A \<Turnstile>p C \<Longrightarrow> insert a A \<Turnstile>p C"
  unfolding true_clause_def true_propo_def true_clss_def using total_over_m_union by (metis Un_iff insert_is_Un sup.commute)

lemma [simp]:
  "A \<Turnstile>ps C \<Longrightarrow> insert a A \<Turnstile>ps C"
  unfolding true_propo_def true_propo_propo_def true_clss_def using total_over_m_union by (metis (mono_tags) Un_iff insert_is_Un sup.commute)
   
lemma true_propo_propo_union_and:
  "A \<Turnstile>ps C \<union> D \<longleftrightarrow> (A \<Turnstile>ps C \<and> A \<Turnstile>ps D)"
proof
  {
    fix A C D
     assume A: "A \<Turnstile>ps C \<union> D"
     have "A \<Turnstile>ps C"
         unfolding true_propo_propo_def true_propo_def insert_def total_over_m_insert
       proof (clarify)
         fix I
         assume tot: "total_over_m I (A \<union> C)"
         and cons: "consistent_interp I"
         and I: "I \<Turnstile>s A"
         obtain I' where tot': "total_over_m (I \<union> I') (A \<union> C \<union> D)"
         and cons': "consistent_interp (I \<union> I')"
         and H: "\<forall>x\<in>I'. atm_of x \<in> atms_of_m D \<and> atm_of x \<notin> atms_of_m (A \<union> C)"
           using total_over_m_consistent_extension[OF tot cons] by auto
         also have "I \<union> I' \<Turnstile>s A" using I by (simp add: true_clss_union_increase)
         ultimately have "I \<union> I' \<Turnstile>s C \<union> D" using A unfolding true_propo_propo_def true_propo_def insert_def by (auto simp add: total_over_m_union)
         hence "I \<union> I' \<Turnstile>s C \<union> D" unfolding Ball_def true_cls_def true_clss_def by auto
         thus "I \<Turnstile>s C" using notin_vars_union_true_clss_true_clss[of I'] H unfolding atms_of_m_union by auto
       qed
   } note H = this
  assume "A \<Turnstile>ps C \<union> D"
  thus "A \<Turnstile>ps C \<and> A \<Turnstile>ps D" using H[of A] Un_commute[of C D] by metis
next
  assume "A \<Turnstile>ps C \<and> A \<Turnstile>ps D"
  thus "A \<Turnstile>ps C \<union> D"
    unfolding true_propo_propo_def true_propo_def insert_def total_over_m_insert by (auto simp add: total_over_m_union)
qed


lemma true_propo_propo_insert:
  "A \<Turnstile>ps insert L Ls \<longleftrightarrow> (A \<Turnstile>p L \<and> A \<Turnstile>ps Ls)"
  using true_propo_propo_union_and[of A "{L}" "Ls"] by auto

lemma true_propo_propo_subset:
  "A \<subseteq> B \<Longrightarrow> A \<Turnstile>ps CC \<Longrightarrow> B  \<Turnstile>ps CC"
  using total_over_m_subset[of A B] unfolding true_propo_propo_def true_clss_def total_over_m_union by fastforce

lemma all_in_true_propo_propo: "\<forall>x \<in> B. x \<in> A \<Longrightarrow> A \<Turnstile>ps B"
  unfolding true_propo_propo_def true_clss_def by auto
  
lemma true_propo_propo_left_right:
  assumes "A \<Turnstile>ps B"
  and "A \<union> B \<Turnstile>ps M"
  shows "A \<Turnstile>ps M \<union> B"
  using assms unfolding true_propo_propo_def by (auto simp add: total_over_m_union)

lemma satisfiable_carac[iff]: "(\<exists>I. consistent_interp I \<and> I \<Turnstile>s \<phi>) \<longleftrightarrow> satisfiable \<phi>" (is "(\<exists>I. ?Q I) \<longleftrightarrow> ?S")
proof
  assume "?S"
  thus "\<exists>I. ?Q I" unfolding satisfiable_def by auto
next
  assume "\<exists>I. ?Q I"
  then obtain I where cons: "consistent_interp I" and I: "I \<Turnstile>s \<phi>" by metis
  let ?I' = "{Pos v |v. v \<notin> atms_of_s I \<and> v \<in> atms_of_m \<phi>}"
  have "consistent_interp (I \<union> ?I')" unfolding consistent_interp_def apply auto
    using cons unfolding consistent_interp_def apply auto
    by (metis atm_of_uminus atms_of_s_def image_eqI in_atms_of_s_decomp literal.sel(1))
  also have "total_over_m (I \<union> ?I') \<phi>" unfolding total_over_m_def total_over_set_def by auto
  moreover have "I \<union> ?I' \<Turnstile>s \<phi>" using I unfolding Ball_def true_clss_def true_cls_def true_lit_def by auto
  ultimately show ?S unfolding satisfiable_def by blast
qed

lemma satisfiable_carac'[simp]: "(consistent_interp I \<and> I \<Turnstile>s \<phi>) \<longrightarrow> satisfiable \<phi>"
  using satisfiable_carac by metis

    
subsection \<open>Subsumptions\<close> 
lemma subsumption_total_over_m:
  assumes "A < B"
  shows "total_over_m I {B} \<Longrightarrow> total_over_m I {A}"
  using assms atms_of_m_plus  unfolding mset_less_def total_over_m_def total_over_set_def by ( auto simp add: mset_le_exists_conv)
  
lemma subsumption_imp_eval:
  assumes "A \<le> B"
  shows "I \<Turnstile> A \<Longrightarrow> I \<Turnstile> B"
  using assms by (metis mset_le_exists_conv true_cls_union)

lemma subsumption_chained:
  assumes "\<forall>I. total_over_m I {D} \<longrightarrow> I \<Turnstile> D \<longrightarrow> I \<Turnstile> \<phi>"
  and "C \<le> D"
  shows "(\<forall>I. total_over_m I {C} \<longrightarrow> I \<Turnstile> C \<longrightarrow> I \<Turnstile> \<phi>) \<or> tautology \<phi>"
  using assms
proof (induct "card {Pos v | v. v \<in> atms_of D \<and> v \<notin> atms_of C}" arbitrary: D rule: nat_less_induct)
  fix D
  assume IH: "\<forall>m<card {Pos v |v. v \<in> atms_of D \<and> v \<notin> atms_of C}. \<forall>x. m = card {Pos v |v. v \<in> atms_of x \<and> v \<notin> atms_of C} \<longrightarrow> (\<forall>I. total_over_m I {x} \<longrightarrow> I \<Turnstile> x \<longrightarrow> I \<Turnstile> \<phi>) \<longrightarrow> C \<le> x \<longrightarrow> (\<forall>I. total_over_m I {C} \<longrightarrow> I \<Turnstile> C \<longrightarrow> I \<Turnstile> \<phi>) \<or> tautology \<phi>"
  and H: "\<forall>I. total_over_m I {D} \<longrightarrow> I \<Turnstile> D \<longrightarrow> I \<Turnstile> \<phi>"
  and incl: "C \<le> D" 
  {
    assume "card {Pos v |v. v \<in> atms_of D \<and> v \<notin> atms_of C} = 0"
    hence "atms_of D \<subseteq> atms_of C" by auto
    hence "\<forall>I. total_over_m I {C} \<longrightarrow> total_over_m I {D}" unfolding total_over_m_def total_over_set_def by auto
    also have "\<forall>I. I \<Turnstile> C \<longrightarrow> I \<Turnstile> D" using incl subsumption_imp_eval by blast
    ultimately have ?thesis using H by auto
    
  }
  also {
    assume card: "card {Pos v |v. v \<in> atms_of D \<and> v \<notin> atms_of C} > 0"
    let ?atms = "{Pos v |v. v \<in> atms_of D \<and> v \<notin> atms_of C}"
    have "finite ?atms" by auto
    then obtain L where  L: "L \<in> ?atms"   
      by (induct ?atms rule: finite.induct, metis card card_gt_0_iff, auto) 
    let ?D' = "D - replicate_mset (count D L) L - replicate_mset (count D (-L)) (-L)"   
    have atms_of_D: "atms_of_m {D} \<subseteq> atms_of_m {?D'} \<union> {atm_of L}" unfolding atms_of_m_def 
      proof 
        fix x
        assume "x \<in> {atm_of \<chi> |\<chi>. \<exists>\<psi>. \<psi> \<in> {D} \<and> \<chi> \<in># \<psi>}"
        then obtain \<chi> where x: "x = atm_of \<chi>" and D: "\<chi> \<in># D" by auto
        {
          assume "x = atm_of L"
          hence "x \<in> {atm_of L}" by auto
        }
        also {
          assume "x \<noteq> atm_of L"
          hence "\<chi> \<noteq> L \<or> \<chi> \<noteq> - L" by (case_tac L, auto)
          hence "\<chi> \<in># ?D' \<or> x = atm_of L" using D x `L \<in> {Pos v |v. v \<in> atms_of D \<and> v \<notin> atms_of C}` by auto
          hence "x \<in> {atm_of \<chi> |\<chi>. \<exists>\<psi>. \<psi> \<in> {?D'} \<and> \<chi> \<in># \<psi>} \<union> {atm_of L}" using x by blast  
        }
        thus "x \<in> {atm_of \<chi> |\<chi>. \<exists>\<psi>. \<psi> \<in> {?D'} \<and> \<chi> \<in># \<psi>} \<union> {atm_of L}" by blast
      qed

    {
      fix I
      assume "total_over_m I {?D'}"
      hence tot: "total_over_m (I \<union> {L}) {D}" unfolding total_over_m_def total_over_set_def using atms_of_D by auto 

      assume IDL: "I \<Turnstile> ?D'"
      hence "I \<union> {L} \<Turnstile> D" unfolding true_cls_def true_lit_def by force
      hence "I \<union> {L} \<Turnstile> \<phi>" using H tot by auto
      
      
      also 
        have tot': "total_over_m (I \<union> {-L}) {D}" unfolding total_over_m_def total_over_set_def by (metis Un_insert_right insert_iff tot total_over_m_def total_over_set_def uminus_Neg uminus_Pos)
        have "I \<union> {-L} \<Turnstile> D" using IDL unfolding true_cls_def true_lit_def by force 
        hence "I \<union> {-L} \<Turnstile> \<phi>"  using H tot' by auto
      ultimately have "I \<Turnstile> \<phi> \<or> tautology \<phi>"  unfolding tautology_def
        proof -
          { fix LL :: "'a literal set"
            obtain aa :: "'a literal \<Rightarrow> 'a" where "Pos (aa L) = L"
              using L by force (* 1 ms *)
            hence "\<not> total_over_set LL (atms_of \<phi>) \<or> I \<Turnstile> \<phi> \<or> LL \<Turnstile> \<phi>"
              by (metis (full_types) `I \<union> {- L} \<Turnstile> \<phi>` `I \<union> {L} \<Turnstile> \<phi>` remove_literal_in_model_tautology tautology_def uminus_Pos) (* 16 ms *) }
          thus "I \<Turnstile> \<phi> \<or> (\<forall>L. total_over_set L (atms_of \<phi>) \<longrightarrow> L \<Turnstile> \<phi>)"
            by blast (* 0.6 ms *)
        qed
    } note H' = this
  
  
    have "L \<notin># C \<and> -L \<notin># C" using L atm_iff_pos_or_neg_lit by force
    hence C_in_D': "C \<le> ?D'"
      proof -
        assume a1: "L \<notin># C \<and> - L \<notin># C"
        have f2: "\<forall>m ma. (m \<le> ma \<longrightarrow> (\<forall>l. count m (l\<Colon>'a literal) \<le> count ma l)) \<and> ((\<forall>l. count m l \<le> count ma l) \<longrightarrow> m \<le> ma)"  by (simp add: less_eq_multiset.rep_eq) (* 3 ms *)
        then obtain ll :: "'a literal" where
          f3: "count C ll \<le> count (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)) ll \<longrightarrow> C \<le> D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)"
          by blast (* 3 ms *)
        have f4: "count (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)) ll = count (D - replicate_mset (count D L) L) ll - count (replicate_mset (count D (- L)) (- L)) ll"
          by (meson count_diff) (* 3 ms *)
        obtain nn :: nat where
          f5: "count (replicate_mset (count D (- L)) (- L)) ll = nn \<and> (if ll = - L then nn = count D (- L) else 0 = nn)"
          using count_replicate_mset by moura (* 10 ms *)
        have f6: "count (D - replicate_mset (count D L) L) ll = count D ll - count (replicate_mset (count D L) L) ll"
          by (meson count_diff) (* 3 ms *)
        obtain nna :: nat where
          f7: "nna = count (replicate_mset (count D L) L) ll \<and> (if ll = L then nna = count D L else 0 = nna)"
          using count_replicate_mset by moura (* 10 ms *)
        have "count D ll = count D ll - 0"
          using diff_zero by auto (* 0.0 ms *)
        hence "0 = nn \<and> 0 = nna \<longrightarrow> count C ll \<le> count (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)) ll"
          using f7 f6 f5 f4 f2 diff_zero incl by presburger (* 33 ms *)
        thus ?thesis
          using f7 f5 f3 a1 by (metis (no_types) gr0I less_or_eq_imp_le) (* 633 ms *)
      qed
    have "card {Pos v |v. v \<in> atms_of ?D' \<and> v \<notin> atms_of C} < card {Pos v |v. v \<in> atms_of D \<and> v \<notin> atms_of C}"
      proof -
        have D'_incl_D: "{Pos v |v. v \<in> atms_of ?D'} \<subseteq> {Pos v |v. v \<in> atms_of D}"   
          by (smt Collect_mono Multiset.diff_le_self Un_iff atms_of_plus mset_le_exists_conv)
        also have L_D': "Pos (atm_of L) \<notin> {Pos v |v. v \<in> atms_of ?D'}"    
          proof -
            obtain aa :: "'a literal \<Rightarrow> 'a" where
              "\<forall>x0. (\<exists>v1. x0 = Pos v1 \<and> v1 \<in> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))) = (x0 = Pos (aa x0) \<and> aa x0 \<in> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)))"
              by moura (* 10 ms *)
            hence f1: "(\<not> (\<exists>a. Pos (atm_of L) = Pos a \<and> a \<in> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))) \<or> Pos (atm_of L) = Pos (aa (Pos (atm_of L))) \<and> aa (Pos (atm_of L)) \<in> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))) \<and> ((\<exists>a. Pos (atm_of L) = Pos a \<and> a \<in> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))) \<or> (\<forall>a. Pos (atm_of L) \<noteq> Pos a \<or> a \<notin> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))))"
              by blast (* 16 ms *)
            obtain aaa :: "'a literal \<Rightarrow> 'a" where
              f2: "L = Pos (aaa L) \<and> aaa L \<in> atms_of D \<and> aaa L \<notin> atms_of C"
              using L by force (* 6 ms *)
            { assume "Pos (aa (Pos (atm_of L))) \<notin># D - replicate_mset (count D L) L"
              hence "Pos (aa (Pos (atm_of L))) \<notin># D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)"
                by (meson Multiset.diff_le_self mset_leD) (* 6 ms *)
              moreover
              { assume "Neg (aa (Pos (atm_of L))) \<in># D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)"
                hence "Pos (atm_of L) \<noteq> Pos (aa (Pos (atm_of L))) \<or> aa (Pos (atm_of L)) \<notin> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))"
                  using f2 by (metis count_diff count_replicate_mset diff_cancel diff_le_self diff_right_commute less_nat_zero_code literal.sel(1) mset_leD uminus_Pos) (* 786 ms *) }
              ultimately have "(\<exists>a. Pos (atm_of L) = Pos a \<and> a \<in> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))) \<longrightarrow> Pos (atm_of L) \<noteq> Pos (aa (Pos (atm_of L))) \<or> aa (Pos (atm_of L)) \<notin> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))"
                by (meson atm_iff_pos_or_neg_lit) (* 3 ms *) }
            moreover
            { assume "0 \<noteq> count (D - replicate_mset (count D L) L) (Pos (aa (Pos (atm_of L))))"
              hence "Pos (aa (Pos (atm_of L))) \<noteq> Pos (aaa L)"
                using f2 by force (* 23 ms *)
              hence "Pos (atm_of L) \<noteq> Pos (aa (Pos (atm_of L))) \<or> aa (Pos (atm_of L)) \<notin> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L))"
                using f2 by (metis (no_types) literal.sel(1)) (* 13 ms *) }
            ultimately show ?thesis
              using f1 by force (* 616 ms *)
          qed
        moreover have L_D: "Pos (atm_of L) \<in> {Pos v |v. v \<in> atms_of D}" using L by auto
        ultimately have card_D_D': "{Pos v |v. v \<in> atms_of ?D'} \<subset> {Pos v |v. v \<in> atms_of D}" by auto 
        hence "{Pos v |v. v \<in> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)) \<and> v \<notin> atms_of C} \<subseteq> {Pos v |v. v \<in> atms_of D \<and> v \<notin> atms_of C}" by auto
        have f2: "\<forall>L La. (\<not> finite (L\<Colon>'a literal set) \<or> \<not> La \<subset> L) \<or> card La < card L"
          by (meson psubset_card_mono) (* 3 ms *)
        obtain aa :: "'a literal \<Rightarrow> 'a" where
          f3: "L = Pos (aa L) \<and> aa L \<in> atms_of D \<and> aa L \<notin> atms_of C"
          using L by force (* 6 ms *)
        hence "atm_of L = aa L"
          by (metis (no_types) literal.sel(1)) (* 10 ms *)
        hence "{Pos a |a. a \<in> atms_of (D - replicate_mset (count D L) L - replicate_mset (count D (- L)) (- L)) \<and> a \<notin> atms_of C} \<subset> {Pos a |a. a \<in> atms_of D \<and> a \<notin> atms_of C}"
          using f3 card_D_D' L_D' by blast (* 20 ms *)
        thus ?thesis
          using f2 `finite {Pos v |v. v \<in> atms_of D \<and> v \<notin> atms_of C}` by presburger (* 3 ms *)      
      qed
    hence "(\<forall>I. total_over_m I {C} \<longrightarrow> I \<Turnstile> C \<longrightarrow> I \<Turnstile> \<phi>) \<or> tautology \<phi>" using IH C_in_D' H' by auto
  }
  ultimately show "(\<forall>I. total_over_m I {C} \<longrightarrow> I \<Turnstile> C \<longrightarrow> I \<Turnstile> \<phi>) \<or> tautology \<phi>" by blast 
qed
 
end
