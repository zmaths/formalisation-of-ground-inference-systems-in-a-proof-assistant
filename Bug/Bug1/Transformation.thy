theory Transformation
imports Main(* "~~/src/HOL/Library/LaTeXsugar" "~~/src/HOL/Library/OptionalSugar"*) Logic

begin

text \<open>This file is devoted to abstract properties of the transformations, like consistency preservation and lifting from terms to proposition. \<close>


section \<open>Rewrite systems and properties\<close>
subsection \<open>Lifting of rewrite rules\<close>

text \<open>We can lift a rewrite relation r over a full formula: the relation @{text r} works on terms, while @{text propo_rew_step} works on formulas.\<close>

inductive propo_rew_step :: "('v propo \<Rightarrow> 'v propo \<Rightarrow> bool) \<Rightarrow> 'v propo \<Rightarrow> 'v propo \<Rightarrow> bool" for r where
global_rel: "r \<phi> \<psi> \<Longrightarrow> propo_rew_step r \<phi> \<psi>" |
propo_rew_one_step_lift: "propo_rew_step r \<phi> \<phi>'\<Longrightarrow> wf_conn c (\<xi> @ \<phi> # \<xi>') \<Longrightarrow> propo_rew_step r (conn c (\<xi> @ \<phi> # \<xi>')) (conn c (\<xi> @ \<phi>'# \<xi>'))"

text \<open>Here is a more precise link between the lifting and the subformulas: if a rewriting takes place between @{term \<phi>} and @{term \<phi>'}, then there are two subformulas @{term \<psi>} in @{term \<phi>} and @{term \<psi>'} in @{term \<phi>'}, @{term \<psi>'} is the result of the rewriting of @{term r} on  @{term \<psi>}. \<close>


lemma propo_rew_step_subformula_imp:
shows "propo_rew_step r \<phi> \<phi>' \<Longrightarrow> \<exists> \<psi> \<psi>'. \<psi> \<preceq> \<phi> \<and> \<psi>' \<preceq> \<phi>' \<and> r \<psi> \<psi>'"
  apply auto
  apply (induct rule: propo_rew_step.induct)
    using subformula.simps subformula_intros apply blast
  using wf_conn_no_arity_change subformula_intros wf_con_no_arity_change_helper in_set_conv_decomp
  sledgehammer[e, verbose, dont_minimize](wf_conn_no_arity_change subformula_intros wf_con_no_arity_change_helper in_set_conv_decomp in_set_conv_decomp)
  (* 
  TODO Jasmin :
  Symptom: the reconstruction by metis is said to fail, while it does not.
  
  Sledgehammering... 
Including 5 relevant facts: wf_conn_no_arity_change subformula_intros wf_con_no_arity_change_helper in_set_conv_decomp in_set_conv_decomp. 
Sledgehammer minimizer: "e". 
Testing 5 facts: in_set_conv_decomp in_set_conv_decomp subformula_intros wf_con_no_arity_change_helper wf_conn_no_arity_change (timeout: 30.0 s)... 
The generated problem is maybe unprovable. 
Facts in "e" proof (of 5): wf_conn_no_arity_change@1, subformula_intros@2, wf_con_no_arity_change_helper@3, in_set_conv_decomp@4, in_set_conv_decomp@5. 
Generating proof text... 
"e": One-line proof reconstruction failed: by metis.

*) by metis



end
