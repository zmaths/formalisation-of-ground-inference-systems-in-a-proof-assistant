theory Logic

imports Main

begin
section \<open>Logics\<close>
text \<open>In this section we define the syntax of the formula and an abstraction over it to have simpler proofs. After that we define some properties like subformula and rewriting.\<close>
subsection \<open>Definition and abstraction\<close>
text  \<open>The propositional logic is defined inductively. The type parameter is the type of the
  variables. \<close>
datatype 'v propo  =
  FT | FF | FVar "'v" | FNot "'v propo" | FAnd  "'v propo"  "'v propo" | FOr  "'v propo"  "'v propo" | FImp  "'v propo"  "'v propo" | FEq  "'v propo"  "'v propo"

text \<open>We do not define any notation for the formula, to distinguish properly between the formulas and Isabelle's logic.\<close>


text \<open>To ease the proofs, we will write the the formula on a homogeneous manner, namely a connecting argument and a list of arguments.\<close>
datatype 'v connective = CT | CF | CVar "'v" | CNot | CAnd | COr | CImp | CEq


definition "binary_connectives = {CAnd, COr, CImp, CEq}"

text \<open>We define our own induction principal: instead of distinguishing every constructor, we group them by arity.\<close>
(*dmitriy \<rightarrow> naming case*)
lemma propo_induct_arity[case_names nullary unary binary]:
  fixes \<phi> \<psi> :: "'v propo"
  assumes nullary: "(\<And>\<phi> x. \<phi> = FF \<or> \<phi> = FT \<or> \<phi> = FVar x \<Longrightarrow> P \<phi>)"
  and unary: "(\<And>\<psi>. P \<psi> \<Longrightarrow> P (FNot \<psi>))"
  and binary: "(\<And>\<phi> \<psi>1 \<psi>2. P \<psi>1 \<Longrightarrow> P \<psi>2 \<Longrightarrow> \<phi> = FAnd \<psi>1 \<psi>2 \<or> \<phi> = FOr \<psi>1 \<psi>2 \<or> \<phi> = FImp \<psi>1 \<psi>2 \<or> \<phi> = FEq \<psi>1 \<psi>2 \<Longrightarrow> P \<phi>)"
  shows "P \<psi>"
  apply (induct rule: propo.induct)
  using assms by metis+

text \<open>The function @{term conn} is the interpretation of our representation (connective and list of arguments). It is partial since we do not want to give a sense to every property. For example @{term "conn CNot [FF, FT]"} has no sense and we do not give one.\<close>
fun  conn :: "'v connective \<Rightarrow> 'v propo list \<Rightarrow> 'v propo" where
"conn CT [] = FT" |
"conn CF [] = FF" |
"conn (CVar v) [] = FVar v" |
"conn CNot [\<phi>] = FNot \<phi>" |
"conn CAnd (\<phi># [\<psi>]) = FAnd \<phi> \<psi>" |
"conn COr (\<phi># [\<psi>]) = FOr \<phi> \<psi>" |
"conn CImp (\<phi># [\<psi>]) = FImp \<phi> \<psi>" |
"conn CEq (\<phi># [\<psi>]) = FEq \<phi> \<psi>"



text \<open>We will often use case distinction, based on the arity of the @{typ "'v connective"}, thus we define our own splitting principle.\<close>
lemma connective_cases_arity:
  assumes nullary: "(\<And>x. c = CT \<or> c = CF \<or> c = CVar x \<Longrightarrow> P)"
  and binary: "(c \<in> binary_connectives \<Longrightarrow> P)"
  and unary: "(c = CNot  \<Longrightarrow> P)"
  shows "P"
  using assms by (case_tac c, auto simp add: binary_connectives_def)


text \<open>Our previous definition is not necessary correct (connective and list of arguments) , so we define an inductive predicate.\<close>
inductive wf_conn :: "'v connective \<Rightarrow> 'v propo list \<Rightarrow> bool" for c :: "'v connective" where
wf_conn_nullary[simp]: "(c = CT \<or> c = CF \<or> c  = CVar v) \<Longrightarrow> wf_conn c []" |
wf_conn_unary[simp]: "c = CNot \<Longrightarrow> wf_conn c [\<psi>]" |
wf_conn_binary[simp]: "c\<in> binary_connectives \<Longrightarrow> wf_conn c (\<psi>#\<psi>'#[])"


subsection \<open>properties of the abstraction\<close>

text \<open>First we can define simplification rules.\<close>
lemma wf_conn_conn[simp]:
  "wf_conn CT l \<Longrightarrow> conn CT l = FT"
  "wf_conn CF l \<Longrightarrow> conn CF l = FF"
  "wf_conn (CVar x) l \<Longrightarrow> conn (CVar x) l = FVar x"
  apply (simp_all add: wf_conn.simps)
  unfolding binary_connectives_def by simp_all


lemma wf_conn_list_decomp[simp]:
  "wf_conn CT l \<longleftrightarrow> l = []"
  "wf_conn CF l \<longleftrightarrow> l = []"
  "wf_conn (CVar x) l \<longleftrightarrow> l = []"
  "wf_conn CNot (\<xi> @ \<phi> # \<xi>') \<longleftrightarrow> \<xi> = [] \<and> \<xi>' = []"
  apply (simp_all add: wf_conn.simps)
       unfolding binary_connectives_def apply simp_all
  by (metis append_Nil append_is_Nil_conv list.distinct(1) list.sel(3) tl_append2)


lemma wf_conn_list:
  "wf_conn c l \<Longrightarrow> conn c l = FT \<longleftrightarrow> (c = CT \<and> l = [])"
  "wf_conn c l \<Longrightarrow> conn c l = FF  \<longleftrightarrow> (c = CF \<and> l = [])"
  "wf_conn c l \<Longrightarrow> conn c l = FVar x  \<longleftrightarrow> (c = CVar x \<and> l = [])"
  "wf_conn c l \<Longrightarrow> conn c l = FAnd a b  \<longleftrightarrow> (c = CAnd \<and> l = a # b # [])"
  "wf_conn c l \<Longrightarrow> conn c l = FOr a b \<longleftrightarrow> (c = COr \<and> l = a # b # [])"
  "wf_conn c l \<Longrightarrow> conn c l = FEq a b \<longleftrightarrow> (c = CEq \<and> l = a # b # [])"
  "wf_conn c l \<Longrightarrow> conn c l = FImp a b \<longleftrightarrow> (c = CImp \<and> l = a # b # [])"
  apply (induct l rule: wf_conn.induct)
  unfolding binary_connectives_def by auto

text \<open>In the binary connective cases, we will often decompose the list of arguments (of length 2) into two elements.\<close>
lemma list_length2_decomp: "length l = 2 \<Longrightarrow> (\<exists> a b. l = a # b # [])"
  apply (induct l, auto)
  by (case_tac l, auto)

text \<open>@{term wf_conn} for binary operators means that there are two arguments.\<close>
lemma wf_conn_bin_list_length:
  fixes l :: "'v propo list"
  assumes conn: "c \<in> binary_connectives"
  shows "length l = 2 \<longleftrightarrow> wf_conn c l"
proof
  assume "length l = 2"
  thus "wf_conn c l" using wf_conn_binary list_length2_decomp using conn by metis
next
  assume "wf_conn c l"
  thus "length l = 2" (is "?P l")
    proof (cases rule: wf_conn.induct)
      case wf_conn_nullary
      thus "?P []" using conn binary_connectives_def
        using connective.distinct(11) connective.distinct(13) connective.distinct(9) by blast
    next
      fix \<psi> :: "'v propo"
      case wf_conn_unary
      thus "?P [\<psi>]" using conn binary_connectives_def
        using connective.distinct  by blast
    next
      fix \<psi> \<psi>':: "'v propo"
      show "?P [\<psi>, \<psi>']" by auto
    qed
qed

lemma wf_conn_not_list_length:
  fixes l :: "'v propo list"
  shows "length l = 1 \<longleftrightarrow> wf_conn CNot l"
  apply auto
  apply (simp add: length_Suc_conv wf_conn.simps)
  by (metis append_Nil connective.distinct(17) connective.distinct(27) connective.distinct(5) length_Cons list.size(3) wf_conn.simps wf_conn_list_decomp(4))
   

text \<open>Decomposing the Not into an element is also very useful.\<close>
lemma wf_conn_Not_decomp:
  fixes l :: "'v propo list" and a :: "'v"
  assumes corr: "wf_conn CNot l"
  shows "\<exists> a. l = [a]"
  by (metis (no_types, lifting) One_nat_def Suc_length_conv corr length_0_conv wf_conn_not_list_length)


text \<open>The @{term wf_conn} remains correct if the length of list does not change. This lemma is very useful when we do one rewriting step\<close>
lemma wf_conn_no_arity_change:
  "length l = length l' \<Longrightarrow> wf_conn c l \<longleftrightarrow> wf_conn c l'"
proof -
  {
    fix l l'
    have "length l = length l' \<Longrightarrow> wf_conn c l \<Longrightarrow> wf_conn c l'"
      apply (cases c l rule:wf_conn.induct, auto)
      apply (metis wf_conn_not_list_length)
      by (metis wf_conn_bin_list_length)
  }
  thus "length l = length l' \<Longrightarrow> wf_conn c l = wf_conn c l'" by metis
qed  

lemma wf_con_no_arity_change_helper:
  "length (\<xi> @ \<phi> # \<xi>') = length (\<xi> @ \<phi>' # \<xi>') "
  by auto

text \<open>The injectivity of @{term conn} is useful to prove equality of the connectives and the lists.\<close>
lemma conn_inj_not:
 assumes correct: "wf_conn c l"
 and conn: "conn c l = FNot \<psi>"
 shows "c = CNot" and "l = [\<psi>]"
 apply (cases c l rule: wf_conn.cases)
 using correct conn unfolding binary_connectives_def apply auto
 apply (cases c l rule: wf_conn.cases)
 using correct conn unfolding binary_connectives_def by auto


lemma conn_inj:
  fixes c ca :: "'v connective" and l \<psi>s :: "'v propo list"
  assumes corr: "wf_conn ca l"
  and corr': "wf_conn c \<psi>s"
  and eq: "conn ca l = conn c \<psi>s"
  shows "ca = c \<and> \<psi>s = l"
  using corr
proof (cases ca l rule: wf_conn.cases)
  case (wf_conn_nullary v)
  thus "ca = c \<and> \<psi>s = l" using assms
      by (metis conn.simps(1) conn.simps(2) conn.simps(3) wf_conn_list(1) wf_conn_list(2) wf_conn_list(3))
next
  case (wf_conn_unary \<psi>')
  hence *: "FNot \<psi>' = conn c \<psi>s" using conn_inj_not eq assms by auto
  hence "c = ca" by (metis conn_inj_not(1) corr' wf_conn_unary(2))
  also have "\<psi>s = l" using * conn_inj_not(2) corr' wf_conn_unary(1) by force
  ultimately show "ca = c \<and> \<psi>s = l" by auto
next
  case (wf_conn_binary \<psi>' \<psi>'')
  thus "ca = c \<and> \<psi>s = l"
    using eq corr' unfolding binary_connectives_def apply (case_tac ca, auto simp add: wf_conn_list)
    using wf_conn_list(4-7) corr' by metis+
qed



subsection \<open>Subformulas and properties\<close>

text \<open>A characterization using sub-formulas is interesting for rewriting: we will define our relation on the sub-term level, and then lift the rewriting on the term-level. So the rewriting takes place on a subformula.\<close>


inductive subformula :: "'v propo \<Rightarrow> 'v propo \<Rightarrow> bool"  (infix "\<preceq>" 45) for \<phi> where
subformula_refl[simp]: "\<phi> \<preceq> \<phi>" |
subformula_intros: "\<psi> \<in> set l \<Longrightarrow> wf_conn c l \<Longrightarrow> \<phi> \<preceq> \<psi> \<Longrightarrow> \<phi> \<preceq> conn c l"

text \<open>On the @{prop subformula_intros}, we can see why we use our @{term conn} representation: one case is enough to express the subformulas property instead of listing all the cases.\<close>

text \<open>This is an example of a property related to subformulas.\<close>
lemma subformula_in_subformula_not:
shows b: "FNot \<phi> \<preceq> \<psi> \<Longrightarrow> \<phi> \<preceq> \<psi> "
  apply (induct rule: subformula.induct)
    apply(auto intro: subformula_intros)
  using subformula_intros wf_conn_unary using list.set_intros(1) subformula_refl by fastforce 


lemma subformula_in_binary_conn:
  assumes conn: "c \<in> binary_connectives"
  shows "f \<preceq> conn c [f, g]"
  and "g \<preceq> conn c [f, g]"
proof -
  have a: "wf_conn c (f# [g])" using conn wf_conn_binary binary_connectives_def by auto
  also have b: "f \<preceq> f" using subformula_refl by auto
  ultimately show "f \<preceq> conn c [f, g]" by (meson list.set_intros(1) subformula_intros) 
next
  have a: "wf_conn c ([f] @ [g])" using conn wf_conn_binary binary_connectives_def by auto
  also have b: "g \<preceq> g" using subformula_refl by auto
  ultimately  show "g \<preceq> conn c [f, g]" using subformula_intros by force
qed


lemma subformula_trans:
 "\<psi> \<preceq> \<psi>' \<Longrightarrow> \<phi> \<preceq> \<psi> \<Longrightarrow> \<phi> \<preceq> \<psi>'"
  apply (induct \<psi>' rule: subformula.inducts)
  by (auto simp add: subformula_intros)

lemma subformula_leaf:
  fixes \<phi> \<psi> :: "'v propo"
  assumes incl: "\<phi> \<preceq>  \<psi>"
  and simple: "\<psi> = FT \<or> \<psi> = FF \<or> \<psi>  = FVar x"
  shows "\<phi> = \<psi>"
proof -
  {
    fix \<psi>
    text \<open>Writing the lemma into that form allows to prove it with a simple induction.\<close>
    have "\<phi> \<preceq> \<psi> \<Longrightarrow> \<psi> = FT \<or> \<psi> = FF  \<or> \<psi>  = FVar x \<Longrightarrow>  \<phi> = \<psi>"
      apply (induct rule: subformula.induct, auto)
      using wf_conn_list(1,2,3) by fastforce+
  }
  thus ?thesis using assms by auto
qed
 
  

text \<open>The variables inside the formula gives precisely the variables that are needed for the formula.\<close>
primrec vars_of_prop:: "'v propo \<Rightarrow> 'v set" where
"vars_of_prop FT = {}" |
"vars_of_prop FF = {}" |
"vars_of_prop (FVar x) = {x}" |
"vars_of_prop (FNot \<phi>) = vars_of_prop \<phi>" |
"vars_of_prop (FAnd \<phi> \<psi>) = vars_of_prop \<phi> \<union> vars_of_prop \<psi>" |
"vars_of_prop (FOr \<phi> \<psi>) = vars_of_prop \<phi> \<union> vars_of_prop \<psi>" |
"vars_of_prop (FImp \<phi> \<psi>) = vars_of_prop \<phi> \<union> vars_of_prop \<psi>" |
"vars_of_prop (FEq \<phi> \<psi>) = vars_of_prop \<phi> \<union> vars_of_prop \<psi>"



lemma vars_of_prop_incl_conn:
  fixes \<xi> \<xi>' ::"'v propo list" and \<psi> :: "'v propo" and c :: "'v connective"
  assumes corr: "wf_conn c l" and incl: "\<psi> \<in> set l"
  shows "vars_of_prop \<psi> \<subseteq> vars_of_prop  (conn c l)"
proof (case_tac c rule: connective_cases_arity)
  fix x :: "'v"
  assume " c = CT \<or> c = CF \<or> c = CVar x"
  hence False using corr incl  by auto
  thus "vars_of_prop \<psi> \<subseteq> vars_of_prop (conn c l)" by blast
next
  assume c: "c \<in> binary_connectives"
  obtain a b where ab: "l = [a, b]" using c wf_conn_bin_list_length list_length2_decomp corr by metis
  hence "\<psi> = a \<or> \<psi> = b" using incl by auto
  thus "vars_of_prop \<psi> \<subseteq> vars_of_prop (conn c l)" using ab c unfolding binary_connectives_def by auto
next
  fix \<phi> :: "'v propo"
  assume c: "c = CNot"
  have "l = [\<psi>]" using corr c incl split_list by force
  thus "vars_of_prop \<psi> \<subseteq> vars_of_prop (conn c l)" using c by auto
qed

text \<open>The set of variables is compatible with the subformula order.\<close>
lemma subformula_vars_of_prop:
  "\<phi> \<preceq> \<psi> \<Longrightarrow> vars_of_prop \<phi> \<subseteq> vars_of_prop \<psi>"
  apply (induct rule: subformula.induct)
  apply simp
  using vars_of_prop_incl_conn by blast



subsection \<open>Positions\<close>

text \<open>Instead of 1 or 2 we use @{term L} or @{term R}\<close>
datatype sign = L | R

text \<open>We use @{term nil} instead of @{term \<epsilon>}.\<close>
fun pos :: "'v propo \<Rightarrow> sign list set" where
"pos FF = {[]}" |
"pos FT = {[]}" |
"pos (FVar x) = {[]}" |
"pos (FAnd \<phi> \<psi>) = {[]} \<union> { L # p | p. p\<in> pos \<phi>} \<union> { R # p | p. p\<in> pos \<psi>}" |
"pos (FOr \<phi> \<psi>) = {[]} \<union> { L # p | p. p\<in> pos \<phi>} \<union> { R # p | p. p\<in> pos \<psi>}" |
"pos (FEq \<phi> \<psi>) = {[]} \<union> { L # p | p. p\<in> pos \<phi>} \<union> { R # p | p. p\<in> pos \<psi>}" |
"pos (FImp \<phi> \<psi>) = {[]} \<union> { L # p | p. p\<in> pos \<phi>} \<union> { R # p | p. p\<in> pos \<psi>}" |
"pos (FNot \<phi>) = {[]} \<union> { L # p | p. p\<in> pos \<phi>}"

lemma finite_pos: "finite (pos \<phi>)"
  by (induct \<phi>, auto)


lemma finite_inj_comp_set:
  fixes s :: "'v set"
  assumes finite: "finite s"
  and inj: "inj f"
  shows "card ({f p |p. p \<in> s}) = card s"
  using finite
proof (induct s rule: finite_induct)
  show "card {f p |p. p \<in> {}} = card {}" by auto
next
  fix x :: "'v" and s::"'v set"
  assume f: "finite s" and notin: "x \<notin> s"
  and IH: "card {f p |p. p \<in> s} = card s"
  have f': "finite {f p |p. p \<in> insert x s}" using f by auto
  have notin': "f x \<notin> {f p |p. p \<in> s}" using notin inj injD by fastforce
  have "{f p |p. p \<in> insert x s} = insert (f x) {f p |p. p\<in> s}" by auto
  hence "card {f p |p. p \<in> insert x s} = 1 + card {f p |p. p \<in> s}"
    using finite card_insert_disjoint f' notin' by auto
  also have "\<dots> =  card (insert x s)" using notin f IH by auto
  finally show "card {f p |p. p \<in> insert x s} = card (insert x s)" .
qed

lemma cons_inject:
  "inj (op # s)"
  by (meson injI list.inject)


lemma finite_insert_nil_cons:
  "finite s \<Longrightarrow> card (insert [] {L # p |p. p \<in> s}) = 1 + card {L # p |p. p \<in> s}"
using card_insert_disjoint by auto


lemma cord_not[simp]:
  "card (pos (FNot \<phi>)) = 1 + card (pos \<phi>)"
by (simp add: cons_inject finite_inj_comp_set finite_pos)

lemma card_seperate:
  assumes "finite s1" and "finite s2"
  shows "card ({L # p |p. p \<in> s1} \<union> {R # p |p. p \<in> s2}) = card ({L # p |p. p \<in> s1}) + card({R # p |p. p \<in> s2})" (is "card (?L\<union>?R) = card ?L + card ?R")
proof -
  have "finite ?L" using assms by auto
  also have "finite ?R" using assms by auto
  moreover have "?L \<inter> ?R = {}" by blast
  ultimately show "?thesis" using assms card_Un_disjoint by blast
qed


definition prop_size where "prop_size \<phi> = card (pos \<phi>)"


lemma prop_size_vars_of_prop:
  fixes \<phi> :: "'v propo"
  shows "card (vars_of_prop \<phi>) \<le> prop_size \<phi>"
  unfolding prop_size_def apply (induct \<phi>)
                  apply auto
        apply (auto simp add: cons_inject finite_inj_comp_set finite_pos)
proof -
  fix \<phi>1 \<phi>2 :: "'v propo"
  assume IH1: "card (vars_of_prop \<phi>1) \<le> card (pos \<phi>1)"
  and IH2: "card (vars_of_prop \<phi>2) \<le> card (pos \<phi>2)"
  let ?L = "{L # p |p. p \<in> pos \<phi>1}"
  let ?R = "{R # p |p. p \<in> pos \<phi>2}"
  have "card (?L \<union> ?R) =  card ?L +  card ?R"
    using card_seperate finite_pos by blast
  also have "\<dots> = card (pos \<phi>1) + card (pos \<phi>2)"
    by (simp add: cons_inject finite_inj_comp_set finite_pos)
  also have "\<dots> \<ge> card (vars_of_prop \<phi>1) + card (vars_of_prop \<phi>2)" using IH1 IH2 by arith
  hence "\<dots> \<ge> card (vars_of_prop \<phi>1 \<union> vars_of_prop \<phi>2)" using card_Un_le le_trans by blast
  ultimately
    show "card (vars_of_prop \<phi>1 \<union> vars_of_prop \<phi>2) \<le> Suc (card (?L \<union> ?R))"
         "card (vars_of_prop \<phi>1 \<union> vars_of_prop \<phi>2) \<le> Suc (card (?L \<union> ?R))"
         "card (vars_of_prop \<phi>1 \<union> vars_of_prop \<phi>2) \<le> Suc (card (?L \<union> ?R))"
         "card (vars_of_prop \<phi>1 \<union> vars_of_prop \<phi>2) \<le> Suc (card (?L \<union> ?R))"
    by auto
qed


value "pos (FImp (FAnd (FVar P) (FVar Q)) (FOr (FVar P) (FVar Q)))"


inductive path_to :: "sign list \<Rightarrow> 'v propo \<Rightarrow> 'v propo \<Rightarrow> bool" where
path_to_refl[intro]: "path_to [] \<phi> \<phi>" |
path_to_l: "c\<in>binary_connectives \<or> c = CNot \<Longrightarrow> wf_conn c (\<phi>#l) \<Longrightarrow> path_to p \<phi> \<phi>' \<Longrightarrow> path_to (L#p) (conn c (\<phi>#l)) \<phi>'" |
path_to_r: "c\<in>binary_connectives \<Longrightarrow> wf_conn c (\<psi>#\<phi>#[]) \<Longrightarrow> path_to p \<phi> \<phi>' \<Longrightarrow> path_to (R#p) (conn c (\<psi>#\<phi>#[])) \<phi>'"


text \<open>There is a deep link between subformulas and pathes: a (correct) path leads to a subformula and a subformula is associated to a given path.\<close>
lemma path_to_subformula:
  "path_to p \<phi> \<phi>' \<Longrightarrow> \<phi>' \<preceq> \<phi>"
  apply (induct rule:path_to.induct)
  apply simp
  apply (metis list.set_intros(1) subformula_intros)
  using subformula_trans subformula_in_binary_conn(2) by metis

lemma subformula_path_exists:
  fixes \<phi> \<phi>':: "'v propo"
  shows "\<phi>' \<preceq> \<phi> \<Longrightarrow> \<exists>p. path_to p \<phi> \<phi>'"
proof (induct rule: subformula.induct)
  case subformula_refl
  have "path_to [] \<phi>' \<phi>'" by auto
  thus "\<exists>p. path_to p \<phi>' \<phi>'" by metis
next
  case (subformula_intros \<psi> l c)
  note wf = this(2) and IH = this(4) and \<psi> = this(1)
  then obtain p where p: "path_to p \<psi> \<phi>'" by metis
  {
    fix x :: "'v"
    assume "c = CT \<or> c = CF \<or> c = CVar x"
    hence "False" using subformula_intros by auto
    hence "\<exists>p. path_to p (conn c l) \<phi>'" by blast
  }
  also {
    assume c: "c = CNot"
    hence "l = [\<psi>]" using wf \<psi> wf_conn_Not_decomp by fastforce
    hence "path_to (L # p) (conn c l) \<phi>'" by (metis c wf_conn_unary p path_to_l)
   hence "\<exists>p. path_to p (conn c l) \<phi>'" by blast
  }
  moreover {
    assume c: "c\<in> binary_connectives"
    obtain a b where ab: "[a, b] = l" using subformula_intros c wf_conn_bin_list_length list_length2_decomp by metis
    hence "a = \<psi> \<or> b = \<psi>" using \<psi> by auto
    hence "path_to (L # p) (conn c l) \<phi>' \<or> path_to (R # p) (conn c l) \<phi>'" using c  path_to_l path_to_r p ab by (metis wf_conn_binary)
    hence "\<exists>p. path_to p (conn c l) \<phi>'" by blast
  }
  ultimately show "\<exists>p. path_to p (conn c l) \<phi>'" using connective_cases_arity by metis
qed

fun replace_at :: "sign list \<Rightarrow> 'v propo \<Rightarrow> 'v propo \<Rightarrow> 'v propo" where
"replace_at [] _ \<psi> = \<psi>" |
"replace_at (L # l) (FAnd \<phi> \<phi>') \<psi> = FAnd (replace_at l \<phi> \<psi>) \<phi>'"|
"replace_at (R # l) (FAnd \<phi> \<phi>') \<psi> = FAnd \<phi> (replace_at l \<phi>' \<psi>)" |
"replace_at (L # l) (FOr \<phi> \<phi>') \<psi> = FOr (replace_at l \<phi> \<psi>) \<phi>'" |
"replace_at (R # l) (FOr \<phi> \<phi>') \<psi> = FOr \<phi> (replace_at l \<phi>' \<psi>)" |
"replace_at (L # l) (FEq \<phi> \<phi>') \<psi> = FEq (replace_at l \<phi> \<psi>) \<phi>'"|
"replace_at (R # l) (FEq \<phi> \<phi>') \<psi> = FEq \<phi> (replace_at l \<phi>' \<psi>)" |
"replace_at (L # l) (FImp \<phi> \<phi>') \<psi> = FImp (replace_at l \<phi> \<psi>) \<phi>'"|
"replace_at (R # l) (FImp \<phi> \<phi>') \<psi> = FImp \<phi> (replace_at l \<phi>' \<psi>)" |
"replace_at (L # l) (FNot \<phi>) \<psi> = FNot (replace_at l \<phi> \<psi>)"


section \<open>Semantics over the syntax\<close>

text \<open>Given the syntax defined above, we define a semantics, by defining an evaluation function @{term eval}. This function is the bridge between the logic as we define it here and the built-in logic of Isabelle.\<close>
fun eval :: "('v \<Rightarrow> bool) \<Rightarrow> 'v propo \<Rightarrow> bool" (infix "\<Turnstile>" 50) where
"_ \<Turnstile> FT = True" |
"_ \<Turnstile> FF = False" |
"A \<Turnstile> FVar n = (A n)" |
"A \<Turnstile> FNot f = (\<not>(A\<Turnstile> f))" |
"A \<Turnstile> FAnd f1 f2 = (A\<Turnstile>f1 \<and> A\<Turnstile>f2)" |
"A \<Turnstile> FOr f1 f2 = (A\<Turnstile>f1 \<or> A\<Turnstile>f2)" |
"A \<Turnstile> FImp f1 f2 = (A\<Turnstile>f1 \<longrightarrow> A\<Turnstile>f2)"  |
"A \<Turnstile> FEq f1 f2 = (A\<Turnstile>f1 \<longleftrightarrow> A \<Turnstile> f2)"

definition evalf (infix "\<Turnstile>f" 50) where
"evalf \<phi> \<psi> = (\<forall>A. A\<Turnstile> \<phi> \<longrightarrow> A\<Turnstile>\<psi>)"

text \<open>The deduction rule is in the book.\<close>
lemma deduction_rule:
  "(\<phi> \<Turnstile>f \<psi>) = (\<forall>A. (A\<Turnstile> FImp \<phi> \<psi>))"
proof
  assume H: "\<phi> \<Turnstile>f \<psi>"
  {
    fix A
    text \<open>``Suppose that @{term \<phi>} entails @{term \<psi>} (assumption @{thm H}) and let @{term A} be an arbitrary @{typ "'v"}-valuation. We need to show @{term "A\<Turnstile> FImp \<phi> \<psi>"}.  ''\<close>


    {
      text \<open>If @{term "A(\<phi>) = 1"}, then @{term "A(\<phi>) = 1"}, because @{term \<phi>} entails  @{term \<psi>}, and therefore @{term "A \<Turnstile> FImp \<phi> \<psi>"}.\<close>
      assume "A\<Turnstile>\<phi>"
      hence "A\<Turnstile> \<psi>" using H unfolding evalf_def by metis
      hence "A \<Turnstile> FImp \<phi> \<psi>" by auto
    }
    also  {
      text \<open>For otherwise, if @{term "A(\<phi>) = 0"}, then @{term "A\<Turnstile> FImp \<phi> \<psi>"} holds by definition, independently of the value of @{term "A \<Turnstile> \<psi>"}.\<close>
      assume "\<not>A\<Turnstile>\<phi>"
      hence "A \<Turnstile> FImp \<phi> \<psi>" by auto
    }
    text \<open>In both cases @{term "A\<Turnstile> FImp \<phi> \<psi>"}.\<close>
    ultimately have "A\<Turnstile> FImp \<phi> \<psi>" by blast
  }
  thus "\<forall>A. A \<Turnstile> FImp \<phi> \<psi>" by blast
next
  show "\<forall>A. A \<Turnstile> FImp \<phi> \<psi> \<Longrightarrow> \<phi> \<Turnstile>f \<psi> "
    proof (rule ccontr)
      assume "\<not>\<phi>\<Turnstile>f \<psi>"
      then obtain A where "A \<Turnstile> \<phi> \<and> \<not>A\<Turnstile>\<psi>" using evalf_def by metis
      hence "\<not> A \<Turnstile> FImp \<phi> \<psi>" by auto
      also assume "\<forall>A. A \<Turnstile> FImp \<phi> \<psi>"
      ultimately show "False" by blast
    qed
qed


lemma "(\<phi> \<Turnstile>f \<psi>) = (\<forall>A. (A\<Turnstile> FImp \<phi> \<psi>))"
  by (simp add: evalf_def)

definition same_over_set:: "('v \<Rightarrow> bool) \<Rightarrow>('v \<Rightarrow> bool) \<Rightarrow> 'v set \<Rightarrow> bool" where
"same_over_set A B S = (\<forall>c\<in>S. A c = B c)"

text \<open>If two mapping @{term A} and @{term B} have the same value over the variables, then the same formula are satisfiable.\<close>
lemma same_over_set_eval:
  assumes "same_over_set A B (vars_of_prop \<phi>)"
  shows "A \<Turnstile> \<phi> \<longleftrightarrow> B \<Turnstile> \<phi>"
  using assms unfolding same_over_set_def by (induct \<phi>, auto)

end

