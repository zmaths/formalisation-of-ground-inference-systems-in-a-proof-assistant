(*<*)theory Propo
imports Main Propo_Logic Report
begin(*>*)
section \<open>\label{sec:clausal-logic}Clausal Logic\<close> 
text \<open>In this section we define formally the logic, first defining the syntax (Section \ref{sec:prop-syntax}), then the associated semantics (Section \ref{sec:prop-semantics}).\<close>
subsection \<open>\label{sec:prop-syntax}Syntax\<close>

text \<open>We consider a countable and non-empty set @{text \<Sigma>}. Instead of using a set, we use a type (that we will call @{typ "'v"}). The non-emptness is required by the type definition in Isabelle. We do not translate the countability constraint into Isabelle, and will show exactly where this constraint is needed. We then define inductively the set $Prop(\Sigma)$ of propositional formulas over a \emph{signature} $\Sigma$ (usually the alphabet or the words, i.e.\ countable sets, are used):
\<close>
(*<*)experiment begin (*>*)
datatype 'v propo =
  FT  -- \<open>the truth symbol\<close> | 
  FF  -- \<open>the false symbol\<close> | 
  FVar "'v" -- \<open>where $v\in\Sigma$, called atom\<close> | 
  FNot "'v propo" -- \<open>is the negation\<close> | 
  FAnd "'v propo"  "'v propo" -- \<open>is the conjunction symbol\<close> | 
  FOr "'v propo"  "'v propo" -- \<open>is the disjunction symbol\<close> | 
  FImp "'v propo"  "'v propo" -- \<open>is the implication symbol\<close> | 
  FEq "'v propo"  "'v propo" -- \<open>is the equivalence symbol\<close>
  
(*<*)end(*>*)

text \<open>@{term FT}, @{term FF} and @{term "FVar x"} are called \emph{base terms}. @{term FAnd} is a symbol of the logic we are defining, while @{text \<and>} is the conjunction a symbol of the logic in Isabelle. An atom @{term a} or its negation @{term "FNot a"} is called a \emph{literal}. We will omit the parentheses, assuming the negation symbol binds more strongly than the binary operations.\<close>

subsection \<open>\label{sec:prop-semantics}Semantics\<close>
text \<open>

We are working in classical logic, thus there are two truth values: ``true'' (@{term "True"} in Isabelle or @{term "1::nat"} in Weidenbach's presentation) and ``false'' (@{term "False"} in Isabelle  or @{term "0::nat"}). We define a \emph{valuation}: it is a mapping @{text "\<A>: \<Sigma> \<rightarrow> {True, False}"}. A \emph{partial} valuation is a function such that some atoms are not mapped to a value. We extend the valuation over $Prop(\Sigma)$. The standard definition consists in using values @{term "1::nat"} and @{term "0::nat"}, but to ease the formalisation we have translated the logic into Isabelle's logic: 

\begin{center}
\begin{tabular}{l|l|l}
  & Isabelle definition & Weidenbach's definition \\ \hline
@{thm (lhs) eval.simps(1)} & @{thm (rhs) eval.simps(1)} & @{term "1::nat"} \\
@{thm (lhs) eval.simps(2)} & @{thm (rhs) eval.simps(2)} & @{term "0::nat"} \\
@{thm (lhs) eval.simps(3)} & @{thm (rhs) eval.simps(3)} & @{term "\<A> v"} \\
@{thm (lhs) eval.simps(4)} & @{thm (rhs) eval.simps(4)} & @{text "1 - \<A> \<Turnstile> \<phi>"} \\
@{thm (lhs) eval.simps(5)} & @{thm (rhs) eval.simps(5)} & $\max$@{term "(\<A> \<Turnstile>\<phi>\<^sub>1, \<A> \<Turnstile> \<phi>\<^sub>2)"}\\
@{thm (lhs) eval.simps(6)} & @{thm (rhs) eval.simps(6)} & $\min$@{term "(\<A> \<Turnstile>\<phi>\<^sub>1, \<A> \<Turnstile> \<phi>\<^sub>2)"} \\
@{thm (lhs) eval.simps(7)} & @{thm (rhs) eval.simps(7)} & if @{text "\<A> \<Turnstile> \<phi>\<^sub>1"} then @{text "\<A> \<Turnstile> \<phi>\<^sub>2"} else 1 \\
@{thm (lhs) eval.simps(8)} & @{text "(\<A> \<Turnstile> \<phi>\<^sub>1) \<longleftrightarrow> (\<A> \<Turnstile> \<phi>\<^sub>2)"} & @{term "(\<A> \<Turnstile> \<phi>\<^sub>1) = (\<A> \<Turnstile> \<phi>\<^sub>2)"}
\end{tabular}
\end{center}
\<close>

(*<*)definition entails ("\<Turnstile>e" ) where "\<Turnstile>e \<psi> \<longleftrightarrow> (\<forall>\<A>. \<A> \<Turnstile> \<psi>)"
theorem deduction_rule: "\<phi> \<Turnstile>f \<psi> \<longleftrightarrow> \<Turnstile>e (FImp \<phi> \<psi>)"
  by (simp add: evalf_def entails_def)
(*>*)
text \<open>We interpret the logic we are formalising (with the @{term FAnd}) into Isabelle's logic (with the @{text "\<and>"}). This simplifies the proofs (for example we do not have to show that the only possible values are @{term "0::nat"} and @{term "1::nat"}). A formula @{term \<phi>} entails @{term \<psi>}, written @{term "\<phi> \<Turnstile>f \<psi>"} if for all valuation @{term \<A>} such that @{term "\<A> \<Turnstile> \<phi>"}, then @{term "\<A> \<Turnstile> \<psi>"}. A formula @{term \<psi>} is \emph{satisfiable} if there is a model of @{term \<psi>}. It is called \emph{valid}, written @{term "\<Turnstile>e \<phi> "}, if every mapping is a model of @{term \<phi>}: for example @{term "FOr \<phi> (FNot \<phi>)"} is valid. Another important difference between the paper proofs and the Isabelle proofs is the use of overloaded symbols: @{text \<Turnstile>} is used with various types to mean @{text "\<Turnstile>e"}, @{text "\<Turnstile>f"} or the valuation @{text \<Turnstile>} as defined in the previous table. Isabelle has overloading (meaning that we could have used a single symbol), but it is usually better to be explicit in a formal context. Overloading also makes type inference more difficult, meaning that we have to add type annotations.

A partial valuation can also be seen as a set of literals either positive or negative instead of defining the following partial valuation @{term \<A>}: given a set @{term V}, for every atom @{term C},  @{text "\<A>(C) = 1"} if @{term "C \<in> V"}, @{term "0::nat"} if @{term "FNot C \<in> V"}, unspecified otherwise. This set is called the \emph{interpretation}.

Here is a theorem giving a relation between @{text "\<Turnstile>f"} and @{text "\<Turnstile>"}. We give a full Isabelle proof and a full paper proof, showing the parallel in the proofs.
\begin{CWtheorem}[Deduction Theorem]
  @{thm (lhs) deduction_rule} @{text \<longleftrightarrow>} @{thm (rhs) deduction_rule}
\end{CWtheorem}
\begin{proof}
  \begin{description}
  \item[$(\implies)$] Suppose that @{term \<phi>} entails @{term \<psi>} and let @{term "\<A>"} be a $\Sigma$-valuation. We have to show that @{term "\<A> \<Turnstile> FImp \<phi> \<psi>"}. If @{term "\<A>(\<phi>) =(1::nat)"}, then we also have that @{term "\<A>(\<psi>) = (1::nat)"}, thus @{term "(\<A>:: 'v propo \<Rightarrow> nat)(FImp \<phi> \<psi>) = max (1 - \<A>(\<phi>)) (\<A>(\<psi>))"}. Otherwise, @{term "\<A>(\<phi>) = (0::nat)"}, thus @{term "(\<A>:: 'v propo \<Rightarrow> nat)(FImp \<phi> \<psi>) = max (1 - \<A>(\<phi>)) (\<A>(\<psi>))"}@{text "=1"}. We have finally that in both cases @{term "\<A> \<Turnstile> FImp \<phi> \<psi>"}.
  \item[$(\impliedby)$] Let @{term "\<A>"} be an arbitrary valuation: @{term "\<A> \<Turnstile> FImp \<phi> \<psi>"}, i.e.\ @{term "\<A>(FImp \<phi> \<psi>) = (1::nat)"}@{text "= max (1 - \<A>(\<phi>)) (\<A>(\<psi>))"}. If @{term "\<A>(\<phi>) = (0::nat)"}, then @{term "\<A>(\<phi>) = (0::nat)"}, otherwise @{term "\<A>(\<phi>) = (1::nat)"} and necessary @{term "\<A>(\<phi>) = (1::nat)"}. So in both cases  @{term "\<phi> \<Turnstile>f \<psi>"}. \qedhere
  \end{description}
\end{proof}

Here is a full detailed Isabelle proof. This proof is close to previous proof, except it uses the Isabelle definition instead of arithmetic on @{term "0::nat"} and  @{term "1::nat"}. We do only want to ``give a flavour'' of Isabelle proofs: the reader is not expected to make fully sense of it.\<close>
theorem "\<phi> \<Turnstile>f \<psi> \<longleftrightarrow> \<Turnstile>e (FImp \<phi> \<psi>)"
proof
  assume H: "\<phi> \<Turnstile>f \<psi>"
  show "\<Turnstile>e (FImp \<phi> \<psi>)"
  unfolding entails_def
  proof
    fix \<A>
    { assume "\<A> \<Turnstile> \<phi>"
      then have "\<A> \<Turnstile> \<psi>" using H unfolding evalf_def by metis
      then have "\<A> \<Turnstile> FImp \<phi> \<psi>" by auto
    }
    also {
      assume "\<not> \<A> \<Turnstile> \<phi>"
      hence "\<A> \<Turnstile> FImp \<phi> \<psi>" by auto
    }
    ultimately show "\<A>\<Turnstile> FImp \<phi> \<psi>" by blast
  qed
next
  assume H: "\<Turnstile>e (FImp \<phi> \<psi>)"
  show "\<phi> \<Turnstile>f \<psi>"
    proof (rule ccontr)
      assume "\<not>\<phi>\<Turnstile>f \<psi>"
      then obtain A where "A \<Turnstile> \<phi> \<and> \<not>A \<Turnstile> \<psi>" using evalf_def by metis
      hence "\<not> A \<Turnstile> FImp \<phi> \<psi>" by auto
      then show "False" using H entails_def by blast
    qed
qed

text \<open>
There are two blocks between the separated by \textbf{next}: the first block is the implication and the other the converse (as in the paper proof). The keywords of the Isabelle proof are close to the words used in the other version.

We have fully detailed the proof to show that the Isabelle proof can be very close to the paper proof. A shorter is possible using the simplifier and the definition of @{text "\<Turnstile>e"} and @{text "\<Turnstile>f"}:\<close>
theorem "\<phi> \<Turnstile>f \<psi> \<longleftrightarrow> \<Turnstile>e (FImp \<phi> \<psi>)"
  by (simp add: evalf_def entails_def)

text \<open>This is an atypical example: the detailed paper proof is longer than the Isabelle version. Usually, it is the other way around.
 \<close>

(*<*)
end
(*>*)