(*<*)theory ReportPart
imports Multiset Partial_Annotated_Clausal_Logic
begin
(*<*)
(*to allow redefinitions locally*)
declare[[names_short]]

notation (latex output)
 atm_of  ("|_|")
 
(* notation (latex output) 
 atms_of_m  ("|_|")
 *)
 (*remove pairs whenever possible*)
translations
  "M" <= "CONST fst (M, N)" 
  "N" <= "CONST snd (M, N)"   
(*translations
  "_multiset p" <= "{p}"
  "CONST single x" <= "{x}"*)
  
(*notation (latex)
  Multiset.single ("\<lbrace>_\<rbrace>")
  
  notation (latex output)
  relcompp (infix "\<infinity>" 50) 
*)
(*>*)  
end

(*>*)