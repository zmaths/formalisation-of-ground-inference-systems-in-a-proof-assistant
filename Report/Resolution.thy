(*<*)
theory Resolution
imports Main Report Propo_Resolution "~~/src/HOL/Library/LaTeXsugar" "~~/src/HOL/Library/OptionalSugar"
begin

notation (latex output)
  inference (infixl "\<Rightarrow>\<^sub>\<Res>" 50)
  
abbreviation (xsymbols) inference_star (infix "\<Rightarrow>\<^sub>\<Res>\<^sup>*\<^sup>*" 50) where
"inference_star \<equiv> rtranclp inference"  
(*>*)

section \<open>\label{sec:w-split}The Resolution Calculus\<close>

text \<open>
The resolution calculus was invented by Robinson~\cite{Robinson:1965:MLB:321250.321253}: it does transformation on the sets of clauses to find constraints on the values of the literals. These deductions called inferences will be presented in Section \ref{sec:inferences}; then we will describe in more details the inference system we are using in Section \ref{sec:example-1} and finally give an overview of the Isabelle proof (Section \ref{sec:res-proof}). \<close>

subsection \<open>\label{sec:inferences}Inferences\<close>

text \<open>
We try to find a contradiction in $N$, a set of clauses in CNF. This proof of $\bot$ that we are looking for is a trace of application of inference rules. An inference rule deduces a conclusion $\mathcal C$ from a some premises $\mathcal C_1, \dots, \mathcal C_n$, written:
\begin{prooftree}
  \AxiomC{$\mathcal C_1$}
  \AxiomC{$\cdots$}
  \AxiomC{$\mathcal C_n$}
  \TrinaryInfC{$\mathcal C$}
\end{prooftree}
We can also see inferences as transitions: $(N\cup\{C_1,\dotsc,C_n\}) \Rightarrow (N\cup\{C_1,\dotsc,C_n\}\cup \{C\})$ is the transition associated to the inference rule. No clause is removed by an inference rule. An inference is said to be \emph{sound}, when $\mathcal C$ is entailed by its premises: $\mathcal C_1, \dots, \mathcal C_n\vDash \mathcal C$.  

A simple procedure to find a proof of false consists in applying all rules over and over as long as we find new formulas until we find $\bot$. This is a final state for the procedure (we reached false), but it is not necessary a termination state for the rewrite system.  The procedure stops when $\bot$ is found, or when the rules do not provide any new information: at this point the set of all the formulas we have found is called \emph{saturated}.  An inference system is \emph{refutational complete} if we can always find $\bot$ whenever the given set of formulas is inconsistent.

Remark that the given procedure does not always terminate: if new inferences can always be deduced, then it does not stop. If the procedure is refutational complete, then the procedure is a \emph{semi-decision procedure}: it stops if the set is inconsistent (and a proof is found); otherwise, it can stop (in a state where we know that the set of formula is statisfiable) or not.
\<close>

subsection\<open>\label{sec:example-1}The Rules\<close>
text \<open>
We use proof trees in this part instead of a transition system to make the inferences easier to understand and avoid writing the set of known clauses at each step: in a proof tree, this set is the clauses we have started with and the clauses we have deduced by applying the rules.


Let us consider the following rules called \emph{binary resolution with factoring}:

\begin{figure}[!h]
  \centering
  \begin{subfigure}{0.45\linewidth}
    \centering
      \AxiomC{$C\lor P$} 
      \AxiomC{$\neg P\lor C'$}
      \RightLabel{\textsf{Res}}
      \BinaryInfC{$C\lor C'$}
      \DisplayProof 
    \caption{\label{res-rule}{Resolution} on the literal $P$}
  \end{subfigure}%
  \begin{subfigure}{0.45\linewidth}
    \centering
      \AxiomC{$C\lor L\lor L$} 
      \RightLabel{\textsf{Fact}}
      \UnaryInfC{$C\lor L$}
      \DisplayProof 
    \caption{\label{fact-rule}{Factoring} of the literal $L$}
  \end{subfigure}
  \caption{\label{fig:res-calc-rules}The rules of resolution calculus.}
\end{figure}
An important point is that in the two rules, @{term "C"} and @{term C'} can be the empty clause: for example, the \textsf{Factorisation} rule allows deducing @{term L} from @{term "L \<or> L"}. The reason is that the @{text \<or>} based representation is only a representation of the multiset presentation.

The \textsf{Res} rule is a sound inference. It can be easily shown by case distinction on the value of $A$: if $A$ is true in a model, then $\neg A$ is false and thus $D$ must be true, because the premises are valid. Otherwise, $A$ is false: $C$ must be true.

We consider the set of formulas in CNF: $N=\{\neg A\lor B, \neg B\lor C, A\lor \neg C, A\lor B\lor C\lor C, \neg A\lor \neg B\lor \neg C\}$, from which we want to deduce $\bot$. On one hand we can deduce $C$:


\begin{prooftree}
  \AxiomC{$A\lor B\lor C\lor C$}%4
  \RightLabel{\textsf{Fact}}
  \UnaryInfC{$A\lor B\lor C$}%4

  \AxiomC{$A\lor \neg C$}%3
  \RightLabel{\textsf{Res}}
  \BinaryInfC{$A\lor B\lor A$}%6

  \RightLabel{\textsf{Factorisation}}
  \UnaryInfC{$B\lor A$}%6

  \AxiomC{$\neg A\lor B$}%4
  \RightLabel{\textsf{Res}}
  \BinaryInfC{$B \lor B$}%7
  \UnaryInfC{$B$}

  \AxiomC{$\neg B\lor C$}%2
  \RightLabel{\textsf{Res}}
  \BinaryInfC{$C$}%8
\end{prooftree}

On the other hand, but we can also deduce $\neg C$, by using $C$ and $B$ we have previously proven:
\begin{prooftree}
  \AxiomC{$C$}%8
  \AxiomC{$A\lor \neg C$}%3
  \RightLabel{\textsf{Res}}
  \BinaryInfC{$A$}%9

  \AxiomC{$\neg A\lor \neg B\lor \neg C$}%5
  \RightLabel{\textsf{Res}}
  \BinaryInfC{$\neg B\lor \neg C$}%9

  \AxiomC{$B$}
  \RightLabel{\textsf{Res}}
  \BinaryInfC{$\neg C$}%8
\end{prooftree}
Thus:
%\begin{prooftree}
  \AxiomC{$\neg C$}%8
  \AxiomC{$C$}%8
  \RightLabel{\textsf{Res}}
  \BinaryInfC{$\bot$}%8
  \DisplayProof
%\end{prooftree}
. We apply the rule in a given order and change the rule, on which we apply the rules: if the \textsf{Factorisation} rule is applied over-and-over on the \emph{same} clause $A\lor B\lor C\lor C$, then each time we get the same $A\lor B\lor C$. To prove termination and correctness, we would have to ensure that each rule is not always applied on the same clause.
\<close>
subsection \<open>Formalisation in Isabelle\<close>
text \<open>We will first describe the formalisation (Section~\ref{sec:res-proof}). In this version, clauses are only added to our set of clause, while it can useful to remove some to reduce the search space (Section~\ref{sec:reduc-rules}). \<close>
subsubsection \<open>\label{sec:res-proof}The Calculus\<close>
text \<open> 
  \begin{figure}[htbp]
  \centering
  \begin{subfigure}{\linewidth}
      @{thm[mode=Rule] inference_clause.resolution}{\textsf{Res}}
    \caption{\label{res-rule} Resolution on @{term A}}
  \end{subfigure}
  
  
  \begin{subfigure}{\linewidth}
    \centering
      @{thm[mode=Rule] inference_clause.factoring}{\textsf{Fact}}
    \caption{\label{fact-rule}Factoring on @{term L}}
  \end{subfigure}
  \caption{\label{fig:res-calc-rules-isabelle}The rules of resolution calculus}
\end{figure}
 To prove termination, we need to ensure that no rule is applied twice to the same clauses and contrary to a paper proof we can not add the condition after the definition. More precisely removing a duplicate literal from a clause (rule \textsf{Fact}) terminates, but repeated applications of \textsf{Res} does not necessary, thus we maintain a set called @{term already_used} containing the pair of premises that we have already used. This slight addition is only added to the proof of the termination theorem and not to the other proof like \emph{soundness} and \emph{completeness}, as if it does not make any difference, while it does.

The rules of the Isabelle version are in Figure~\ref{fig:res-calc-rules-isabelle}: with these rules, the case @{term "C"} empty is in the rules, since it is simply @{term "C"} being the empty multiset.

The soundness and completeness theorem we wrong in Weidenbach's book~\cite{CWAutomatedReasoning}, but the idea of the proof is correct:
  \begin{CWtheorem}[Wrong Version]The resolution calculus is sound and complete:
    
  @{term N} is unsatisfiable iff @{text "N \<Rightarrow>\<^sub>\<Res>\<^sup>\<star> {\<bottom>}"}.
\end{CWtheorem}

\begin{CWtheorem}[Corrected Version]The resolution calculus is sound and complete:

  @{term N} is unsatisfiable iff there is some @{term N} such that @{text "N \<Rightarrow>\<^sub>\<Res>\<^sup>\<star> N'"} where @{term "{#} \<in> N'"}.
\end{CWtheorem}

The difference between the wrong and the corrected version of the theorem is that in one case @{term "{#} \<in> N'"} and in the other case @{term "N' = {{#}}"} (the set containing only the empty clause). The theorem is correct if we add simplification rules as we do in the next section, but under the current rules it is not. A corrected version verified in Isabelle is the following theorem:

\begin{theorem}[Soundness and Completeness]
  If @{thm (prem 1) resolution_soundness_and_completeness} and  @{thm (prem 2) resolution_soundness_and_completeness}, then the following equivalence holds: @{thm (lhs) resolution_soundness_and_completeness} if and only if @{thm (rhs) resolution_soundness_and_completeness}.
\end{theorem}

There are two differences between both theorems: first (to prove termination later), we use a set of used clauses, which is initially empty, thus the condition @{term "snd \<psi> = {}"}. Moreover, we have to assume that the number of clauses is finite since sets are infinite in Isabelle, thus the @{term "finite (fst \<psi>)"}.

\newenvironment{indenteddescription}%
  {\begin{list}{}{\setlength{\labelwidth}{0pt}
   \setlength{\itemindent}{-\leftmargin}
   \setlength{\listparindent}{\parindent}
   \renewcommand{\makelabel}{\descriptionlabel}}}%
  {\end{list}}
\begin{proof}
  \begin{indenteddescription}
  \item[($\impliedby)$] The converse is a proof by contradiction: assume that @{term N} is satisfiable and we have @{term "rtranclp inference \<psi> \<psi>'"} such that @{term "\<bottom> \<in> fst \<psi>'"} is impossible. Each transition @{term "inference (N, used) (N', used)"} is such that @{term "N \<Turnstile>ps N'"}. As @{term "\<bottom> \<in> \<psi>'"}, we have @{term "N \<Turnstile>p \<bottom>"}. This is false, since @{term N} is satisfiable.

  \item[$(\implies)$] We assume that @{term N} is unsatisfiable. The idea of the proof of the implication in the theorem is to build a semantic tree and to decrease the size by merging sibling leafs. Each of this merging consists in applying some rules of the calculus. 

   A semantic tree is a binary tree, such that each node is labelled with an atom and the leafs are labelled with a formula. Given a node marked with the atom @{term l}, the literal @{term "Pos l"} is true in the left subtree and the literal @{term "Neg l"} is true in the right subtree. At the level of the leafs, all these literals are a (possibly partial) valuation. The formula @{term \<phi>} at a given leaf is such that the interpretation is @{term I} is not a model: $I \nvDash \varphi$. For example in Figure~\ref{fig:semantic-tree-init}, the leftmost leaf is labelled with the formula @{term "{#Neg P, Neg Q#}"}. The interpretation is @{term "{#Pos P, Pos Q#}"}, and we have @{term "{#Neg P, Neg Q#}"}$\nvDash$@{term "{#Neg P, Neg Q#}"}.



  We will now describe the merging of the sibling node on an example, with the following set of clauses: @{term "N={{#Pos P#},{#Pos P, Neg P#}, {#Neg P, Pos Q, Pos Q#}, {#Neg P, Pos Q, Pos Q#}, {#Neg P, Neg Q#}, {#Pos P, Pos Q#}}"}. The first step is to build a semantic tree associated to the set of clauses. To do so we build a semantic tree such that every path from the node to a leaf is a \emph{total} valuation. As the valuation are total, there is always a clause in @{term N} such that the interpretation is not a model. For our set of formulas, a possible semantic tree is in Figure~\ref{fig:semantic-tree-init}. 

  \begin{figure}
    \centering
    \begin{subfigure}{0.9\linewidth}
      \centering
      \begin{tikzpicture}[ state/.style={circle,
          draw=none, %minimum size=2cm,
          fill=white, circular drop shadow, text centered, anchor=north,
          text=black}, 
          leaf/.style={rectangle,
          draw=none, text centered, anchor=north,
          text=black}, level distance=2cm]
        \node[state] (S) {$P$} [->] [sibling distance=8cm] 
          child { [sibling distance=4cm] 
            node[state] (P) {$Q$} [->] {
 		      child  {[sibling distance=3cm] node[leaf] (PQ) {\parbox{1.5cm}{@{term "{#Neg P, Neg Q#}"}}}
                edge from parent node[left] {@{term "Pos Q"}} }
             child  {[sibling distance=2cm] node[leaf] (PQ) {\parbox{1.5cm}{@{term "{#Neg P, Pos Q, Pos Q#}"}}}
               edge from parent node[right] {@{term "Neg Q"}}}
		  	}
            edge from parent node[left] {@{term "Pos P"}} }
          child { [sibling distance=4cm] 
            node[state] (P) {$Q$} [->] {
 		      child  {[sibling distance=2cm] node[leaf] (PQ) {\parbox{1.5cm}{@{term "{#Pos P#}"}}}
                edge from parent node[left] {@{term "Neg Q"}} }
             child  {[sibling distance=2cm] node[leaf] (PQ) {\parbox{1.5cm}{@{term "{#Pos P, Pos Q#}"}}}
               edge from parent node[right] {@{term "Neg Q"}} }
		  	}
           edge from parent node[right] {@{term "Neg P"}} };
      \end{tikzpicture}
      \caption{Initial semantic tree}
      \label{fig:semantic-tree-init}
    \end{subfigure}    
    \par\bigskip
    \begin{subfigure}{0.9\linewidth}
      \centering
      \begin{tikzpicture}[ state/.style={circle,
          draw=none, %minimum size=2cm,
          fill=white, circular drop shadow, text centered, anchor=north,
          text=black}, 
          leaf/.style={rectangle,
          draw=none, text centered, anchor=north,
          text=black}, level distance=2cm]
        \node[state] (S) {$P$} [->] [sibling distance=8cm] 
          child { [sibling distance=4cm] 
            node[state] (P) {$Q$} [->] {
 		      child  {[sibling distance=3cm] node[leaf] (PQ) {\parbox{1.5cm}{@{term "{#Neg P, Neg Q#}"}}}
                edge from parent node[left] {@{term "Pos Q"}} }
             child  {[sibling distance=2cm] node[leaf] (PQ) {\parbox{1.5cm}{@{term "{#Neg P, Pos Q, Pos Q#}"}}}
               edge from parent node[right] {@{term "Neg Q"}}}
		  	}
            edge from parent node[leaf, left] {@{term "Pos P"}} }
          child { [sibling distance=4cm] 
            node[leaf] (P) {@{term "{#Pos P#}"}} [->] {}
           edge from parent node[right] {@{term "Neg P"}} };
      \end{tikzpicture}
      \caption{\label{fig:semantic-tree-rmerge}Initial semantic tree after merging one sibling node}    
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{0.9\linewidth}
      \centering
      \begin{tikzpicture}[ state/.style={circle,
          draw=none, %minimum size=2cm,
          fill=white, circular drop shadow, text centered, anchor=north,
          text=black}, 
          leaf/.style={rectangle,
          draw=none, text centered, anchor=north,
          text=black}, level distance=2cm]
        \node[state] (S) {$P$} [->] [sibling distance=8cm] 
          child { [sibling distance=4cm] 
            node[leaf] (P) {@{term "{#Neg P, Neg P#}"}} [->] {}
            edge from parent node[left] {@{term "Pos P"}} }
          child { [sibling distance=4cm] 
            node[leaf] (P) {@{term "{#Pos P#}"}} [->] {}
           edge from parent node[right] {@{term "Neg P"}} };
      \end{tikzpicture}
      \caption{\label{fig:semantic-tree-rlmerge}Initial semantic tree after merging two sibling nodes}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{0.9\linewidth}
      \centering
      \begin{tikzpicture}[ state/.style={circle,
          draw=none, %minimum size=2cm,
          fill=white, circular drop shadow, text centered, anchor=north,
          text=black}, 
          leaf/.style={rectangle,
          draw=none, text centered, anchor=north,
          text=black}, level distance=2cm]
        \node[leaf] (S) {@{term \<bottom>}} [->] [sibling distance=8cm] {};
      \end{tikzpicture}
      \caption{Initial semantic tree after merging all sibling nodes}
      \label{fig:semantic-tree-end}
    \end{subfigure}

    \caption{Reduction of the size of a semantic tree, on the set of clauses @{term "N={{#Pos P#},{#Pos P, Neg P#}, {#Neg P, Pos Q, Pos Q#}, {#Neg P, Pos Q, Pos Q#}, {#Neg P, Neg Q#}, {#Pos P, Pos Q#}}"}}
    \label{fig:res-semantic-tree-ex}
  \end{figure}

  The Isabelle version consists in taking an element of all the atoms and building the tree recursively:

\textit{
\isamarkuptrue%
\ \ \ {\isachardoublequoteopen}build{\isacharunderscore}sem{\isacharunderscore}tree\ atms\ {\isasympsi}\ {\isacharequal}\isanewline
\ \ \ \ \ \ {\isacharparenleft}if\ atms\ {\isacharequal}\ {\isacharbraceleft}{\isacharbraceright}\ {\isasymor}\ {\isasymnot}\ finite\ then\ Leaf\isanewline
\ \ \ \ \ \ else\ \ \isanewline
\ \ \ \ \ \ \ \ let\ m\ {\isacharequal}\ Min\ atms\ in\isanewline
\ \ \ \ \ \ \ \ let\ t\ {\isacharequal}\ build{\isacharunderscore}sem{\isacharunderscore}tree\ {\isacharparenleft}atms\ {\isacharminus}\ {\isacharbraceleft}m{\isacharbraceright}{\isacharparenright}\ {\isasympsi}\ in\isanewline
\ \ \ \ \ \ \ \ Node\ m\ t\ t{\isacharparenright}{\isachardoublequoteclose}%
\isadelimproof
%
\endisadelimproof
%
\isatagproof
%
\endisatagproof
{\isafoldproof}%
%
\isadelimproof
%
\endisadelimproof
%
}


  The condition @{term "atms = {} \<or> \<not> finite atms"} is necessary to prove termination: if we could define without and apply it on an infinite set of atoms, the tree would be infinite, which is impossible, since we are using a datatype. We have to find a way to take an element of the set: we take the minimum of the set. That is why we have added a type class @{text linorder} on the type of the atoms: it means that there is a linear order of the set. This allows us to take the variables in a given order to build the tree (here taking the minimum of the variables not yet in the tree but in the set for formulas). This is not an issue: as we have only a finite number of literals, we can give each of this literals a number and then use the order given by these numbers. Moreover in practice we use a words over the alphabet and there is a natural order on it (the lexicographic order).


  Now we have built the semantic tree, we can start merging the siblings. First we merge the two rightmost sibling leafs @{term "{#Pos P, Pos Q#}"} and @{term "{#Pos P#}"}: the atom @{term Q} does not appear in one of the two formulas, so we can replace the node @{term Q} by a leaf containing the formula @{term "{#Pos P#}"}. We have not done any use of the rule.

  We can merge the two other sibling leafs with formulas @{term "{#Neg P, Neg Q#}"} and @{term "{#Neg P, Pos Q, Pos Q#}"}: the literal @{term "Q"} appears in both formulas. The first step is to remove the duplicate in the formula to get @{term "{#Neg P, Pos Q#}"}. The associated transition is @{term "inference (N, {}) (N \<union> {{#Neg P, Pos Q#}}, {})"}. Then we can apply the \textsf{Res} rule to @{term "{#Neg P, Pos Q#}"} and @{term "{#Neg P, Pos Q#}"} to get @{term "{#Neg P, Neg P#}"}: @{term "inference (N \<union> {{#Neg P, Pos Q#}}, {}) (N \<union> {{#Neg P, Pos Q#}, {#Neg P, Neg P#}}, {})"} (result in Figure~\ref{fig:semantic-tree-rlmerge}). We did not use any rule here.
  
  We have reduced the number of variables that appear in the leaf. We can now merge the last two sibling leafs @{term "{#Neg P, Neg P#}"} and @{term "{#Pos P#}"}: we first reduce the number of occurrences of @{term "Neg P"} and apply the \textsf{Res} rule: we get @{term \<bottom>} (Figure~\ref{fig:semantic-tree-end}). We have applied two rules of the calculus.

  We have now finished merging the sibling leafs and there is a single remaining leaf containing the formula @{text \<bottom>}: this is what we wanted to have. More generally, at the end we have a formula @{term \<phi>} such that the empty valuation is not a model: $[\ ]\nvDash$@{term \<phi>}. The only solution is that @{term \<phi>} is (as here) @{term \<bottom>}: we have deduced what we wanted to have.


  In the previous example we have skipped the conditions on @{term already_used}. To take care of the conditions, we use the following invariant:
  \begin{definition}\label{def:inv}
    \begin{itemize}
    \item For each pair @{term "(A, B)"} that has been already used,
      there is an atom @{term P} such that @{term "Pos P"} is in
      @{term A} and @{term "Neg P"} is in @{term B}.
    \item Either there is @{term \<chi>} that subsumes the conclusion
      (i.e.\ @{term \<chi>} implies the conclusion of the \textsf{Res}
      rule @{term "(A - {#Pos P#}) + (B -{#Neg P#})"}) or the latter is a tautology).
    \end{itemize}
  \end{definition}

The part of the invariant with the tautology is for the case that we want to apply the \textsf{Res} rule to the same two clauses only changing the literal. For example if we have @{term "{#Pos P, Pos Q#}"} and @{term "{#Neg P, Neg Q#}"}, we can apply the \textsf{Res} rule with @{term "P"} or @{term "Q"}: in each case we have a tautology. In that case every possible conclusion is a tautology (including the previous conclusion). In the semantic tree, we cannot have this case since we are always working with conflicts: $I \nvDash \varphi$ implies that $I$ is not tautology. This shows also that re-applying the \textsf{Res} rule on the same two clauses, even when changing the variables, does not lead to any progress in the proof.

The part with the subsumption is not necessary here, since we do not remove any clause. However, it allows using the conclusion where some duplicate have already been removed: if we want to re-apply the resolution rule on @{term "{#Neg P, Neg Q#}"} and @{term "{#Neg P, Pos Q#}"}, and @{term "{#Neg P#}"} is already in the set of clauses, we can directly take the version without duplicates.
  \end{indenteddescription}
\end{proof}

 
This theorem shows completeness and soundness of the resolution calculus: this proof is one of the proofs of Weidenbach's book, we were able to simplify.
\<close>

subsubsection \<open>\label{sec:reduc-rules}The Calculus with reduction Rules\<close>

text \<open>
The problem with the inference system described above is that new clauses are only added, while it is useful to delete some clauses like the factoring rule: we have both @{term "{#Pos P, Pos P#} + C"} and  @{term "{#Pos P#} + C"}, while the second is enough (since they are equivalent). More precisely the rules described in Section \ref{sec:example-1} are \emph{inference} rules since new clauses are added, while we will describe \emph{reduction} rules that are useful to have fewer clauses (see Figure \ref{fig:reduction-rules}).

\begin{figure}[hbt]
  \centering
  \begin{subfigure}{\linewidth}
    \centering
    @{thm[mode=Rules] subsumption}
    \caption{\emph{Subsumption}: each model of @{term A} is also a model of @{term B}, so the latter can be removed of @{term A}. @{text \<subset>} is the strict inclusion (to ensures that @{term A} and @{term B} are different).}
  \end{subfigure}
  \par\bigskip
  \begin{subfigure}{\linewidth}
    \centering
    @{thm[mode=Rules] tautology_deletion}
    \caption{\emph{Tautology deletion}, we remove a clause.}
  \end{subfigure}
  \par\bigskip  
  \begin{subfigure}{\linewidth}
    \centering
    @{thm[mode=Rules] condensation}
    \caption{\emph{Condensation} is close to the \textsf{Fact} rule but removes the premise.}
  \end{subfigure}
  
  \caption{Reduction rule}
  \label{fig:reduction-rules}
\end{figure}
The rules are applied using a strategy: simplify as much as possible, then apply \textsf{Res} or \textsf{Fact}: this is the transition of the transition system.  This means that (except the first state), all the formulas are always simplified. As we are interested in what happens after the simplification and the rules, we will consider a @{term full_simplifier} that simplifies as much as possible. Then we get the two rules:
\begin{center}
@{thm[mode=Rule] resolution.full_simp}


@{thm[mode=Rule] resolution.inferring}
\end{center}

Using a predicate that fully simplify the formula make thing easier, since we can assume that at each step (except the very first), we can show that we are always in a state where everything is simplified. There are two predicates @{term full_simplifier} and @{term full0_simplifier}: the difference is that @{term full_simplifier} must do one step (in @{term "full_simplifier N N'"} @{term N} is different of @{term N'}), whereas @{term full0_simplifier} do an arbitrary number of steps including zero. We cannot have a rule of the form @{term "full0_simplifier N N' \<Longrightarrow> resolution (N, al) (N', al)"}, otherwise we cannot prove termination: we could remain in the same state while doing steps (@{term "resolution (N, al) (N, al)"}). @{term full0_simplifier} simplifies our state and if nothing can be done, then it does nothing. It is the inference rule that change the state in the rule. 

The proofs are close to the proofs presented in previous version, but we have to show that @{term full0_simplifier} terminates, because we want to use the result \emph{after} the simplification. Now the @{term subsumes} part in the invariant of Definition \ref{def:inv} is important: we remove clauses that are either tautologies or are replaced by a simpler clause. The simplifications does not preserve a semantic tree, because the formulas on the leafs can change; but there are replaced by another equivalent formula.


\begin{theorem}
  The resolution calculus with reduction rules terminates.
\end{theorem}
\begin{proof}
  The idea of the proof is that we cannot apply the \textsf{Fact} rule, since the clauses are always simplified. Then at each step of already used clauses is strictly increasing. As there is only a finite number of simplified clauses containing only the atoms of the set of clauses, we have termination. 
\end{proof}

The proof of this theorem is one of the few that were broken in Weidenbach's book: the given argument were wrong. The proof tried to prove a better bound than ours (our upper bound is $3^n$ \textsf{Res} steps where $n$ is the number of clauses).

The length of the full formalisation of resolution only is $1~200$ lines of Isabelle. The resolution calculus is based on combining clauses. We will now present another algorithm that is based on case distinction of the values of the atoms.

\<close>
 
(*<*)
end
(*>*)