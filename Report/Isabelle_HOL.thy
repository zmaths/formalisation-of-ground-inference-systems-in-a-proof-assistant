(*<*)
theory Isabelle_HOL
imports Main Report
    "~~/src/HOL/Library/LaTeXsugar" 
    "~~/src/HOL/Library/OptionalSugar"
begin

(*>*)

section \<open>\label{sec:isabelle}The Proof Assistant\<close>
text \<open>Isabelle is the prover we use in this formalisation. We describe here the prover (Section~\ref{sec:isa-gene}), then how to do definitions (Section~\ref{isa-defin}), before describing the Isabelle proofs (Section~\ref{sec:proof-lib}).\<close>
subsection \<open>\label{sec:isa-gene}General Presentation\<close>
text \<open>
Isabelle~\cite{paulson1994isabelle} is a generic framework for interactive theorem proving: the use of a meta-logic \emph{Pure} allows a formalisation for different logics and axiomatisations. The built-in meta-logic Pure is an intuistionistic fragment of higher-order logic. Isabelle is based on the ideas of LCF~\cite{milner1973models}: every proof goes through a small trusted kernel. It is written in Standard ML, and has an Isabelle/jEdit interface through the asynchronous PIDE interface~\cite{PIDE}.


HOL, \emph{higher order logic}~\cite{gordon2000lcf}, is the most developed logic in Isabelle: it is based on typed higher-order logic with ML-style rank-1 polymorphism and Haskell-style axiomatic type classes. On the type level, we have either base types (like @{typ nat}), type constructors (for example @{typ "bool list"} to define lists with elements of type @{typ "bool"}) or functions (for example @{typ "nat \<Rightarrow>bool"} is a function going from type @{typ nat} to @{typ bool}). @{typ "'a"} is a base type that can be instantiated later on, for example with a natural number. On the term level, HOL defines axiomatically @{typ bool}, and the constants @{const_typ True} (i.e.\ a constant named @{term True} of type @{typ bool}), @{const_typ False}, @{text "="} @{text "::"} @{typeof "op ="} and the connectives and (@{text \<and>}), or (@{text \<or>}), not (@{text \<not>}). HOL is embedded into the meta-logic Pure using @{const_typ Trueprop}, that translates from @{typ bool} (defined at the HOL level) to @{typ prop}. @{const Trueprop} is omitted when printing, allowing to fully ignore the difference between @{typ bool} and @{typ prop}. There is a slight difference between the HOL level with the @{text \<forall>}, @{text \<longrightarrow>} and @{text "="} and their Pure equivalent with @{text \<And>}, @{text \<Longrightarrow>} and @{text "\<equiv>"}, but the differences do not matter in this report. Other logics than HOL include Isabelle/ZF (based on Zermelo-Fraenkel set theory): it is defined using Pure. 

In HOL, functions can be curried (@{term "f x y"} instead of @{text "f(x, y)"}), and common binary operators can be used with the infix notation, like the plus operator @{term "plus"} in @{term "(2::nat) + 2"}, instead of @{text "plus 2 2"}. Function applications bind stronger that infix operators; thus @{term "f x + g y"} should be read @{text "(f x) + (g y)"} (the addition of two function calls, @{term "f x"} and @{term "g y"}). Types are annotated with \emph{type classes} to add properties about them: for example in an expression @{term "a + (b::'a::{plus})"}, @{term a} is inferred of type @{typ "'a"}, but of type class @{text "plus"} to express the property that there is an addition over @{typ "'a"}. If there is the constraint on a type @{typ "'a"} to have type class @{text plus}, then you can only instantiate @{typ "'a"} with a type that has an plus operator. The polymorphism (i.e.\ working with @{typ "'a"}) allows to prove things generally (eventually by adding type classes constraints) and instantiate the types later.


The main proof method in HOL is the \emph{simplifier}, which uses equation as oriented rewrite rules on the goals, including conditional rewriting (e.g. @{term "ys = [] \<Longrightarrow> rev ys =z"} will be rewritten into @{term "ys = [] \<Longrightarrow> rev [] =z"}; then the simplifier will use the lemma that says @{thm rev.simps(1)} to get @{term "ys = [] \<Longrightarrow> [] =z"}). The list of lemmas that are used can be extended by the user's lemmas.


One very useful tool in Isabelle/HOL is Sledgehammer~\cite{paulson-blanchette-2010}: it translates the theorems of Isabelle and exports them to automated provers like CVC4~\cite{CVC}, E~\cite{eprover}, SPASS~\cite{spass}, Vampire~\cite{vampire} and Z3~\cite{z3prover}. Then these automated provers try to find a proof: if one is found, then the proof can be reconstructed, either in a detailed fashion or using a built-in tactic with the used theorems as arguments. Sledgehammer eases finding proofs, since it avoids looking for easy proofs and remembering the names of hundreds of theorems in the standard library. As of now, none of the formalised theorems have been solved directly by Sledgehammer.

There are a lot of lemmas in either the standard library that is included in Isabelle or the Archive of Formal Proofs~\cite{AFP}. The automation is very developed when working on list and set and is improving when working on multisets. Some related developments in Isabelle include Isabelle Formalisation of Rewriting (IsaFoR)~\cite{IsaFor}: it is a formalisation of term rewrite systems and of various termination proving tools.

We have used the HOL logic in this master's thesis. We will now give more details about how to define function in Isabelle and write proofs. 
\<close>
subsection \<open>\label{isa-defin}Adding Definitions\<close>
text \<open>Definitions introduces a new theorem to context between the defined symbol and the actual definition. This approach does not introduce axioms and so do not introduce inconsistencies. New definitions are defined using already defined types or terms. There are two levels of definition: types (even types with constructors) and terms.\<close>
subsubsection \<open>Types\<close>
text \<open>The primitive way to define a type is to use the \textbf{typedef} command: a type is isomorphic to a non-empty set (defined using a type, that is already defined). For example we can define the type of all natural numbers larger than 2:\<close>
        typedef my_type = "{(n::nat). n > 2}"
        by auto

text \<open>\noindent where \textbf{by} \textit{auto} prove that the type is inhabited.
  
To define types inductively, the command \textbf{datatype} defines inductive and mutually recursive datatypes specified by their constructor

\textbf{datatype} \textit{M} @{text " :: "} $\mathit{\tau} =$ $C_{1}\ \overline{x_{1}} | \cdots|\ C_{\ell}\ \overline{x _{\ell}} $


For example here is a definition of natural numbers:
  \<close>
(*<*)experiment begin (*>*)
        datatype nat =
          Zero | Suc nat
text \<open>There are to constructors, @{term Zero}, which takes no arguments, and @{term Suc} that takes exactly one argument. The command \textbf{datatype} defines the type using \textbf{typedef} and prove that the type is not empty (since every type have to be inhabited in HOL).\<close>
(*<*)end(*>*)  

subsubsection \<open>Terms\<close>
text \<open>A simple definition is of the form {\textbf{definition} \textit{c} :: \textit{type} \textbf{where} \textit{c $\overline{x}$} = \textit{t}}. This introduces a new axiom of the form $c\equiv \lambda \overline{x}.\ t$ where $\overline x$ are the arguments of the function @{term c}. Isabelle ensures that the constant @{term c} is fresh, the variables in $\overline x$ are distinct and that @{term t} does not refer to any undefined variable or undefined types. We can for example have:
\<close>

        definition K :: "'a \<Rightarrow> 'b \<Rightarrow> 'a" where "K x y = x"

text \<open>Definitions are opaque in the sense that they are not unfolded by default when inside a theorem. Definitions are not recursive (i.e.\ you cannot call @{term c} while defining @{term c}). We will use inductive predicates: an inductive predicate is simply a function to @{typ bool} with some assumptions (including calls to itself):


\textbf{inductive} \textit{p} @{text " :: "} $\mathit{\tau}$ \textbf{where}

\begin{tabular}
{c@ {~~}c@ {~~@{text "\<Longrightarrow>"}~~} c @ {~~@{text "\<Longrightarrow>"}~~} c @ {~~@{text "\<Longrightarrow>"}~~}l}
name$_1$:&$Q_{11}$ & @{text "\<cdots>"}  & $Q_{1\ell_1}$ & @{text "p t\<^sub>1"}\\
$\vdots$&$\vdots$      &               &  $\vdots$     & $\vdots$\\
name$_n$:&$Q_{n1}$ & @{text "\<cdots>"} & $Q_{n\ell_n}$ & @{text "p t\<^sub>n"}
\end{tabular}%

\noindent where @{term p} must be fresh. There are some syntactic restrictions on the rules to ensure monotonicity. Internally the Knaster-Tarski theorem is used with a fixed-point equation to show the existence of a least fixpoint. For example, the following is a definition of the predicate \textit{even}:
\<close>

        inductive even :: "nat \<Rightarrow> bool" where
        even0: "even 0" |
        even_SS: "even n \<Longrightarrow> even (Suc (Suc n))"

  
text \<open>where @{const_typ Suc} and @{term "0::nat"} @{text "::"} @{typ "nat"} are the two constructors for natural numbers. The associated fixpoint equation is
\begin{center}  
  @{thm even.simps}
\end{center}

Contrary to {\textbf{inductive}} definitions where no termination is required and no evaluation is possible in general, when defining a function, termination must be proved. Otherwise a non-terminating definition of the form @{term "(f::nat \<Rightarrow> nat) x = (1::nat) + f x"} where @{text "f::nat \<Rightarrow> nat"} is possible, and then you can deduce @{term "(0::nat) = 1"} (by subtracting @{term "f x"}), which is @{term False} and allows to prove anything. When the termination is based on a structural decrease, {\textbf{primrec}} can prove automation automatically:
  \<close>
        primrec plus_nat :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
        "plus_nat 0 n = 0" |
        "plus_nat (Suc m) n = Suc (plus_nat m n)"

text \<open>Here, the number of constructors @{term Suc} is decreasing for each recursive call. This is a structural decreasing and \textbf{primrec} is able to prove termination automatically.\<close>
  


text \<open>It is also possible to use {\textbf{fun}} that tries a few invariants to show that the argument is decreasing (e.g. the @{term size} of the formula). \textbf{fun} is powerful enough to prove the termination of the Ackerman function, while \textbf{primrec} is not: \<close>
        fun ack :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
        "ack 0 n = n + 1" |
        "ack (Suc m) 0 = ack m 1" |
        "ack (Suc m) (Suc n) = ack m (ack (Suc m) n)"

text \<open>If the proof of the termination is more complicated, one can use {\textbf{function}}, which generates three goals: termination, pattern completeness and non-overlapping patterns. An example will be presented in Section \ref{sec:dpll-imp}\<close>


subsection \<open>\label{sec:proof-lib}Isabelle Proofs\<close>
text \<open>
Isabelle proofs are done using some proof methods: they do low level work and go through the (trusted) kernel of Isabelle. Some important tactics are \textit{auto} that uses the simplifier with some knowledge about logic and \textit{blast} that is specialised on logical formulas. \textit{rule th} unifies the conclusion of \textit{th} with the goal: the premises of the goal remains to show.
  
These proof methods can be combined using two proof styles: forward and backward proofs. The backward proof consists in going from the goal, unifying the conclusion of some theorems with the actual goal. After that, the assumption of the theorems are the new goals. This approach is called \textbf{apply}-style. For example, the proof that four is even:\<close>
        lemma "even (Suc (Suc (Suc (Suc 0))))"
          apply (rule even_SS)
          apply (rule even_SS)
          apply (rule even0)
          done

text \<open>
After stating the theorem to prove, the goal is the theorem: @{term "even (Suc (Suc (Suc (Suc 0))))"}. We apply the theorem @{thm even_SS[of "Suc (Suc 0)"]}: using theorem @{thm even_SS}, \texttt{rule} unifies @{term "(n::nat)"} with @{term "Suc (Suc 0)"}. It remains to show that @{term "even (Suc (Suc 0))"}. Then the second \textbf{apply} applies theorem @{thm even_SS[of "0"]}. It remains to show that @{term "even 0"}. This is true by theorem @{thm even0},  i.e.\ the definition of @{term even}: this theorem has no condition, so our proof is finished.

The other style is forward: you go from the assumptions (if there are some) to the conclusion. This approach is used in Isabelle's \emph{Intelligible semi-automated reasoning} (Isar)~\cite{ISAR}. The aim of this language is to be close to the one used by mathematicians. \<close>
        lemma "even (Suc (Suc (Suc (Suc 0))))"
        proof -
          have "even 0" by (rule even0)
          then have "even (Suc (Suc 0))" by (rule even_SS)
          then show "even (Suc (Suc (Suc (Suc 0))))" by (rule even_SS)
        qed   
 
text \<open>\textbf{have} introduces a fact, that has to be proved. The proof after can discharged by for example \textbf{by} followed by a tactic call. The keyword \textbf{then} allows to use previous conclusion, while reasoning on the next one. \textbf{show} introduces a fact that is one of the goals needed to prove the theorem.

 We have introduced the proof assistant we used during this formalisation; we will now speak about the logic we have formalised in Isabelle.\<close> 

(*<*)
end
(*>*)