
(*<*)
theory Abstract_Calc
imports Main 
    Propo_Normalisation Propo_Abstract_Transformation Clausal_Logic
    Report
    "~~/src/HOL/Library/LaTeXsugar"
    "~~/src/HOL/Library/OptionalSugar"
begin

(*>*)
section \<open>\label{sec:normal-forms}Normal Forms\<close>
 text \<open> Before trying to solve the actual problem, we must normalise the set of formulas @{term N} that we are considering. We will formally define two normal forms (Section \ref{sec:prop-cnf-dnf}), then introduce transitions systems (Section \ref{sec:pos-trans}) and show how to define them in Isabelle (Section \ref{sec:nf-isa}).
\<close>

subsection \<open>\label{sec:prop-cnf-dnf}Definition of Two Normal Forms\<close>
text \<open>
We give here the definitions of two normal form as in Weidenbach's book. The idea of this two normal forms is to remove the @{text FImp} and @{text FEq}.
\begin{definition}[CNF, clauses]\label{def:cnf} A formula is in \emph{conjunctive normal form} (CNF) if it is a conjunction of clauses (i.e.\ of disjunction of literals):
$\bigwedge_i\bigvee_jL_{i,j}$ where $L_{i, j}$ are literals and $\bigvee _jL_{i,j}$ is a clause.
\end{definition}


\begin{definition}[DNF]\label{def:dnf} A formula is in \emph{disjunctive normal form} (DNF) if it is a disjunction of conjunction of literals:
$\bigvee_i\bigwedge_jL_{i,j}$ where $L_{i, j}$ are literals.
\end{definition}

An interesting property of the CNF representation is that $C \subset C'$ means that @{term C'} is more general: the satisfiability of @{term C} implies the one of @{term C'}; each valuation satisfying @{term C} satisfies also @{term C} and @{term C'} (written $\{C, C'\}$ seen as a set of clauses). We will write this as @{term "I\<Turnstile>C"} implies @{term "I\<Turnstile>s {C, C'}"}. 

General clausal propositional are related to the normal form by the following theorem:

\begin{theorem}
  Each propositional formula can be transformed into an equivalent CNF form.
\end{theorem}
\begin{proof}
First we recursively transform $\phi\rightarrow\psi$ and $\phi\leftrightarrow\psi$ into  $\neg\phi\lor\psi$ and $(\neg\phi\lor\psi)\land (\neg\psi\lor\phi)$. Then we reorganise the $\land$ and the $\lor$ (see Algorithm \ref{cnf-algo}). This algorithm preserves not only validity (the same models satisfy the formula before and after normalisation), but also it is also an equivalence transformation. It is an algorithm with exponential complexity and faster algorithms exist in practice.
\begin{algorithm}[t]
  \KwData{A formula $\varphi$ without $\rightarrow$ nor $\leftrightarrow$}
  \KwResult{A formula $\varphi'$ equivalent to $\varphi$ in CNF}
 \SetAlgoLined\DontPrintSemicolon
  \SetKwFunction{algo}{cnf}
  \SetKwProg{myalg}{Algorithm}{}{}


  \myalg{\algo{$\varphi$}}{
    \Switch{$\varphi$ }
    {
      \Case{$\top$, $\bot$, $\neg\top$, $\neg\bot$}
      {\KwRet $\varphi$}
      \Case{$\phi\land\psi$}
      { $\phi_1\land \dotsb \land\phi_n :=$ cnf ($\phi$)\;
        $\psi_1\land \dots\land\psi_m :=$ cnf ($\psi$)\;

        \KwRet $\phi_1\land \dotsb \land\phi_n \land \psi_1\land \dots\land\psi_m$}
      \Case{$\phi\lor\psi$}
      { $\phi_1\land \dotsb \land\phi_n :=$ cnf ($\phi$)\;
        $\psi_1\land \dots\land\psi_m :=$ cnf ($\psi$)\;

        \KwRet $(\phi_1\lor\psi_1) \land\dotsb\land (\phi_1\lor\psi_m)\land$
        $ \dotsb \land$
        $(\phi_n\lor\psi_1) \land\dotsb\land (\phi_n\lor\psi_m)$}

      \Case{$\neg(\phi\lor\psi)$}
      {\KwRet cnf$(\neg \phi\land\neg\psi)$}
      \Case{$\neg(\phi\land\psi)$}
      {\KwRet cnf$(\neg \phi\lor\neg\psi)$}
    }}{}
  \caption{\label{cnf-algo}Transformation into CNF form}
\end{algorithm}
\end{proof}

After that, we can do some simplification on the translation into the CNF like transforming $P\lor \neg P$ into $\top$ and $\bot\lor Q$ into $\bot$. These simplifications are not necessary but improve efficiency. 

The presentation as a transition system allows to prove various refinement and implementations that changes the order in which the transformation are done. Changing the order of the transformation lead to better performance for example. The Algorithm~\ref{cnf-algo} is one of the possible orders: the innermost formulas are transformed first, and then the whole formula is transformed. For example in @{term "FAnd \<phi> \<psi>"}, @{term "\<phi>"} and @{term "\<psi>"} are transformed before  @{term "FAnd \<phi> \<psi>"} is transformed.
\<close>

subsection \<open>\label{sec:pos-trans}Positions, Transition Systems\<close>
text \<open>
To refer to a subformula, we define the \emph{position} as list over @{term "{L, R}"} where @{term R} means right and @{term L} means left (@{term L} is also the default value if there is no right formula, e.g. @{term \<psi>} is on the left in @{term "FNot \<psi>"}). The empty list @{term "[]"} means that we are considering the actual level. The set of all positions in a formula is given by (where @{term "Cons L p"} means that @{term L} is added at the beginning of the list @{term p}):
\begin{center}
\begin{tabular}{l@ {~~@{text "="}~~}l}
@{thm (lhs) pos.simps(1)} & @{thm (rhs) pos.simps(1)} \\
@{thm (lhs) pos.simps(2)} & @{thm (rhs) pos.simps(2)} \\
@{thm (lhs) pos.simps(3)} & @{thm (rhs) pos.simps(3)} \\
@{thm (lhs) pos.simps(4)} & @{thm (rhs) pos.simps(4)} \\
@{thm (lhs) pos.simps(5)} & @{thm (rhs) pos.simps(5)} \\
@{thm (lhs) pos.simps(6)} & @{thm (rhs) pos.simps(6)} \\
@{thm (lhs) pos.simps(7)} & @{thm (rhs) pos.simps(7)} \\
@{thm (lhs) pos.simps(8)} & @{thm (rhs) pos.simps(8)}
\end{tabular}
\end{center}

For example @{term \<phi>} is at position @{term "[L, R, L]"} in @{term "FAnd (FEq FT (FNot \<phi>)) FF"}.
Using the position, removing the equivalence symbols can become: transform @{term "FEq \<phi>\<^sub>1 \<phi>\<^sub>2"} into @{term "FAnd (FImp \<phi>\<^sub>1 \<phi>\<^sub>2) (FImp \<phi>\<^sub>2 \<phi>\<^sub>1)"} at any possible position with an @{term FEq}. This is not deterministic anymore (we are not giving an order on the possible positions) and we do not care about the exact chosen path, but only on properties on the result (that do not have to be unique). The heuristics used in practise try to find the shortest path, but we show only properties on the final state (Figure \ref{fig:trans-sys}), whatever the path is.

\begin{figure}[htp]
  \centering
  \definecolor{qqqqff}{rgb}{0.,0.,1.}
  \definecolor{xdxdff}{rgb}{0.49019607843137253,0.49019607843137253,1.}
  \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm, scale=0.8]
  \draw [->] (0,4) -- (2,7);
  \draw [->] (0,4) -- (2,5);
  \draw [->] (0,4) -- (2,3);
  \draw [->] (0,4) -- (2,1);
  \draw [->] (2,7) -- (4,8);
  \draw [->] (2,7) -- (4,6);
  \draw [->] (2,5) -- (4,6);
  \draw [->] (7,8) -- (9,7);
  \draw [->] (7,6) -- (9,7);
  \draw [->] (2,3) -- (4,2);
  \draw [->] (2,1) -- (4,2);
  \draw [->] (7,3) -- (9,3);
  \draw [->] (7,1) -- (9,3);
  \draw [->] (4,8) -- (7,8);
  \draw [->] (4,6) -- (7,6);
  \draw [->] (4,2) -- (7,3);
  \draw [->] (4,2) -- (7,1);
  \begin{scriptsize}
  \draw [fill=xdxdff] (0,4) circle (1.5pt);
  \draw [fill=qqqqff] (2,7) circle (1.5pt);
  \draw [fill=qqqqff] (2,5) circle (1.5pt);
  \draw [fill=qqqqff] (2,3) circle (1.5pt);
  \draw [fill=qqqqff] (2,1) circle (1.5pt);
  \draw [fill=qqqqff] (4,8) circle (1.5pt);
  \draw [fill=qqqqff] (4,6) circle (1.5pt);
  \draw [fill=qqqqff] (7,8) circle (1.5pt);
  \draw [fill=qqqqff] (9,7) circle (1.5pt);
  \draw [fill=qqqqff] (7,6) circle (1.5pt);
  \draw [fill=qqqqff] (4,2) circle (1.5pt);
  \draw [fill=qqqqff] (7,3) circle (1.5pt);
  \draw [fill=qqqqff] (9,3) circle (1.5pt);
  \draw [fill=qqqqff] (7,1) circle (1.5pt);
  \end{scriptsize}
  \end{tikzpicture}
  \caption{\label{fig:trans-sys}Transition system: each arrow represent a possible transition. The points represents states: there are two final states (the two states on the right, without leaving arrow). While an algorithm will chose deterministically one of the different paths, our presentation as a transition system means that we show properties on all possible paths and all the possible final states.}
\end{figure}

The transformation defined as a transition system is defined as @{text "\<chi>[FEq \<phi>\<^sub>1 \<phi>\<^sub>2]\<^sub>p \<Longrightarrow>\<^sub>e\<^sub>l\<^sub>i\<^sub>m\<^sub>_\<^sub>e\<^sub>q \<chi>[FAnd (FImp \<phi>\<^sub>1 \<phi>\<^sub>2) (FImp \<phi>\<^sub>2 \<phi>\<^sub>1)]\<^sub>p"} where @{text "\<chi>[\<phi>]\<^sub>p"} means that @{text \<phi>} is at position @{text p} in @{text \<chi>} and nothing else has changed during the transformation @{text  "\<Longrightarrow>\<^sub>e\<^sub>l\<^sub>i\<^sub>m\<^sub>_\<^sub>e\<^sub>q"}.
 This vision as a transition system is more flexible than an algorithm: whatever order on the rewriting is more convenient, it does not matter for the proof of the transition system.
 
 The definition of a transition system consists in giving all possible transitions: in Figure \ref{fig:trans-sys}, it corresponds to all the possible arrows. The underlying non-deterministic algorithm is simply: do a transition among all the possible ones, as long as possible. In our the example, whatever the order of applying replacing the @{term FEq} symbols, we will get a formula without @{term FEq} symbols. The algorithm terminates when we are in a state where no transition is possible: this is called a \emph{final state}. No unicity is required on the final states.

\<close>

subsection \<open>\label{sec:nf-isa}Formalisation in Isabelle\<close>
text \<open>
We used a presentation as a transition system and not as a term rewrite system: in the book, rewrite systems have been introduced in a previous chapter of the book. We could have formalised rewrite system using IsaFoR~\cite{IsaFor}, but it is an overkill for what we have formalised. We have introduced a transition system that do rewriting, then we lift this rewriting to rewriting one of the subformulas.

\begin{algorithm}
  \KwData{A propositional formula @{term \<phi>}}
  \KwResult{A propositional formula @{term \<phi>'} equivalent to @{term "\<phi>"} in CNF or DNF form)}
 \SetAlgoLined\DontPrintSemicolon
  \SetKwFunction{algo}{satdec}
  \SetKwProg{myalg}{Algorithm}{}{}
  apply rule @{term elim_equiv} as long as possible \\
  apply rule @{term elim_imp} as long as possible \\
  apply rule @{term elimTB} as long as possible \\
  apply rule @{term pushNeg} as long as possible \\
  apply rule @{term pushConj} as long as possible for CNF \\
  apply rule @{term pushDisj} as long as possible for DNF
  \caption{\label{simple-cnd-dnf-algo}Basic CNF/DNF transformation (see rules in Figure \ref{fig:transf-rule})}
\end{algorithm}

\begin{figure}
  \centering
  \begin{description}
  \item[@{term "elim_equiv"}:] @{thm elim_equiv.elim_equiv}
  \item[@{term "elim_imp"}:] @{thm elim_imp.intros}
  \item[@{term "elimTB"}:] remove all the unused @{term FT} and @{term "FF"}:

    @{thm elimTB.intros}
  \item[@{term "pushNeg"}:] push the @{term "FNot"} to the innermost of the formula (to the literal level):

    @{thm pushNeg.intros}
  \item[@{term "pushConj"}:] @{thm pushConj_def}
  \item[@{term "pushDisj"}:] @{thm pushDisj_def}
  \end{description}
  \caption{\label{fig:transf-rule}Transformation rules}
\end{figure}
\<close>

subsubsection \<open>CNF, DNF Definition\<close>
text \<open>
 Contrary to the Weidenbach's definitions of CNF and DNF, our operators are only binary connectives, so we cannot express the definitions exactly as in Definitions~\ref{def:cnf} and~\ref{def:dnf}. More precisely, an implicit generalisation from binary to $n$-ary is used in Weidenbach's book, because there are semantically the same. Isabelle does not allow these changes (since the formulas are synctatically different), so we have only binary operators. For example, @{term "FAnd (FAnd a b) (FAnd c d)"} would be written @{term "a \<and> b \<and> c \<and> d"}, relying on the associativity of @{term FAnd}. 
 
 If we look at the definitions, there are there constraints: firstly in the innermost there are only literals; secondly this literals can be connected by the connective @{term FOr} (respectively @{term FAnd}), finally these groups are connected by @{term FAnd} (respectively by @{term FOr}). In Isabelle we will separate these three constraints: we will relax the constraint on literals and allow base terms  (@{term FT}, @{term FT} or @{term "FVar v"} for an atom @{term v}). These can be grouped using a connective @{term c} that will be instantiate depending on whether we define CNF or DNF:

\begin{itemize}
\item we can have the simple terms directly:

  \begin{center}
    @{thm [mode=Rule] grouped_by.intros(1)}
  \end{center}

\item or we can have the negation of a simple term:

  \begin{center}
    @{thm [mode=Rule] grouped_by.intros(2)}
  \end{center}

\item or we can group two groups using the @{term c} connective. The @{term "wf_conn c l"} verifies that it is a real term.
  \begin{center}
    @{thm [mode=Rule] grouped_by.intros(3)}
  \end{center}

\end{itemize}

After that we make groups withe the other connective that will be instantiated depending on whether we will define CNF or DNF. An inner group is also an outer group and we can combine outer group using the correct connective: 
\begin{itemize}
\item either we have a group:
  \begin{center}
    @{thm[mode=Rule] super_grouped_by.intros(1)}
  \end{center}
\item or we make groups:
  \begin{center}
    @{thm[mode=Rule] super_grouped_by.intros(2)}
  \end{center}

\end{itemize}

It is important to notice is that @{term FF} is a correct CNF and DNF formula, although there is a no single literal in @{term FF}. This case is not explicitly stated in the definition of CNF and DNF, because an empty formula is not very interesting, but the question arises when doing the Isabelle proof. Thus we use a predicate @{term  no_T_F_except_top_level} that checks that there is no @{term FT} nor @{term FF}, except on top-level. We can now combine all these properties:
\begin{itemize}
\item @{thm is_cnf_def} where @{term is_conj_with_TF} is an abbreviation for @{thm (rhs) is_conj_with_TF_def}
\item @{thm is_dnf_def} where @{term is_disj_with_TF} is an abbreviation for  @{thm (rhs) is_disj_with_TF_def}
\end{itemize}
\<close>
subsubsection \<open>Transition System\<close>
text \<open>We have formalised the approach as a transition system without using explicitly the path that leads to the rewritten formula: given a rewrite relation @{term r}, rewriting means that at each level either the actual formula is rewritten or a single one of the arguments has changed.


To make a shorter definition, instead of writing every formula case (@{term FAnd}, @{term FOr},\dots), we made a higher representation: a formula is a connective (@{term CAnd}, @{term COr},\dots) and a list of arguments. This allows to have an shorter representation, but we have to ensures that the list really corresponds to a term, thus the predicate @{term "wf_conn c \<phi>s"} that ensures the number of arguments of @{term c} is compatible with the number of elements in @{term \<phi>s}, e.g. the connective @{term CAnd} corresponding to @{term FAnd} has two arguments exactly.

The predicate @{term "propo_rew_step r \<phi> \<psi>"} means that @{term \<phi>} is rewritten in @{term \<psi>} by the relation @{term "r"}. It is defined inductively as follows:
\begin{itemize}
\item We use a rule representation: on the above you have the assumption to fulfil (here simply @{thm (prem 1) propo_rew_step.global_rel}) to show the term below the line.
\begin{center}
  @{thm[mode=Rule] propo_rew_step.global_rel}
\end{center}


\item Otherwise, rewriting one of the argument is enough:

\begin{center}
  @{thm[mode=Rule] propo_rew_step.propo_rew_one_step_lift}
\end{center}
where @{text @} is the append function. It is a complicated formula (since it is general to enough to mach every possible rewriting), but it means simply that a single one of the arguments has been rewritten. For example if you have @{term "propo_rew_step r (FAnd \<phi> \<psi>) (FAnd \<phi>' \<psi>')"} then either @{term "r (conn CAnd [\<phi>, \<psi>]) (conn CAnd [\<phi>', \<psi>'])"}, or one of the arguments has changed: @{term \<phi>} into @{term \<psi>} and @{term "\<phi>'=\<psi>'"};  or @{term \<phi>'} into @{term \<psi>'} and @{term "\<phi>=\<psi>"}.
\end{itemize}

This abstraction over rewriting is independent of the considered transformation. The link between the path approach and @{term propo_rew_step} is given by:
\begin{theorem}
@{thm[mode=IfThen] propo_rew_step_rewrite}
\end{theorem}

The theorem means that whenever we have some transition from @{term \<phi>} to @{term \<phi>'}, then there is some @{term \<psi>} at a path @{term p} in @{term \<phi>} that is rewritten.

To prove the full transformation of Algorithm~\ref{simple-cnd-dnf-algo}, we iterate the transition relation @{term "propo_rew_step r"} to rewrite as long as possible (i.e.\ until no more step is possible): @{term "(propo_rew_step r)\<^sup>*\<^sup>*"} is the reflexive transitive closure of a curried predicate.
\begin{definition}
  @{thm full_propo_rew_def}
\end{definition}


Each transformation is one call to @{text "\<^sup>\<down>"}. For each of these transformations, we have to show some invariants and  we can then compose our transformations to get the full transformation of the Algorithm \ref{simple-cnd-dnf-algo} by using the relational composition @{term "op OO"}:
\begin{center}
  @{thm (lhs) cnf_rew_def} @{text "="} @{thm (rhs) cnf_rew_def}
\end{center}

The operator @{term "op OO"} is defined by @{thm OO_def}. In the definition of @{term "cnf_rew"}, @{term "cnf_rew \<phi> \<psi>"} means that there are some formulas @{term \<rho>}s such that @{term "full_propo_rew elim_equiv \<phi> \<rho>"}, @{term "full_propo_rew elim_imp \<rho> \<rho>'"}, @{term "full_propo_rew elimTB \<rho>' \<rho>''"}, @{term "full_propo_rew pushNeg \<rho>'' \<rho>'''"} and @{term "full_propo_rew pushDisj \<rho>''' \<psi>"}

\<close>


(*<*)
lemma cnf_rew_consistent': "cnf_rew \<phi> \<psi> \<longrightarrow> (\<forall>\<A>. (\<A> \<Turnstile> \<phi>) \<longleftrightarrow> (\<A> \<Turnstile> \<psi>))"
  using cnf_rew_consistent unfolding preserves_un_sat_def by blast

(*>*)
text \<open> We can then prove the following two theorems:
\begin{theorem}[Equivalence preservation]
@{thm cnf_rew_consistent'}
\end{theorem}
\begin{proof}
The idea of the proof is to show that the equivalence is preserved by each transformation used in @{term cnf_rew}. For @{term elim_equiv} for example, we have to show that @{term "elim_equiv \<phi> \<psi> \<Longrightarrow> (\<forall>\<A>. (\<A> \<Turnstile> \<phi>) \<longleftrightarrow> (\<A> \<Turnstile> \<psi>))"}, hence @{term "full_propo_rew elim_equiv \<phi> \<psi> \<Longrightarrow> (\<forall>\<A>. (\<A> \<Turnstile> \<phi>) \<longleftrightarrow> (\<A> \<Turnstile> \<psi>))"}.
\end{proof}
\begin{theorem}[Correctness]
@{thm cnf_rew_is_cnf}.
\end{theorem}

\<close>

subsubsection \<open>Changing the Order of the Rules\<close>
text \<open>It is better to change the order of the rules and to remove the @{term "FT"} and @{term "FF"} symbols before any other transformation. This leads to shorter formulas, which means that less transitions are needed. The algorithm changes the rules and more elimination are required for @{term FT} and @{term FF}. The definition of this removing @{term elimTBFull} is a bit more complicated: there are more cases, for example replacing @{term "FEq FF \<phi>"} into @{term "FNot \<phi>"}. The associated reordering does not change the ideas behind the proof and except the reordering (namely removing @{term FF} and @{term FT} at the beginning) does not change the other transformations. The new transformation can be defined as:
\begin{definition}
 @{thm cnf_rew'_def}
\end{definition}
@{term elimTBFull} removes all the true and false symbols, except the one at the top-level (for example the formula @{term "FImp FT FT"} can be normalised to @{term FT}), which cannot be removed.
  \<close>

subsubsection \<open>Alternative CNF Representation\<close>
text \<open>All the proof methods we will present in the next sections assumes that the formula are in CNF. To ease the work, instead of assuming each time that the formulas are in CNF, we will use a different representation. A literal is a positive atom or a negative one:
\<close>
(*<*)experiment begin (*>*)
datatype 'a literal =
  Pos "'a"
| Neg "'a"
(*<*)end (*>*)
text \<open>After that a clause is simply a multiset of literals and clauses are set of clauses (of type @{typ "'a clause"}):\<close>
type_synonym 'a clause = "'a literal multiset"
type_synonym 'v clauses = "'v clause set"
text \<open>We use a \textbf{@{text type_synonym}} to be able to write @{typ "'a clause"} instead of the multiset version, but we do not use an opaque type. Multisets are written for example @{term "{#Pos P, Pos P#}"}. All other operators are written the same way as the usual set counterpart (@{text "\<in>"} for inclusion of an element and @{text "\<subset>"} for \emph{strict} inclusion). 
  
In this representation, the equivalent @{term FF} of the empty multiset: we will write it @{term "{#}"}. There is no equivalent of @{term FT}. This is not a real problem: given a set of formulas, we can remove all the true formulas, except if we have only true formulas in our set: in that case, there is no contradiction to find.

We will either write the multiset of literals @{text "\<lbrace>L\<^sub>1, \<dots>, L\<^sub>n\<rbrace>"} (this is the Isabelle representation) or @{text "L\<^sub>1 \<or> \<cdots> \<or> L\<^sub>n"} (this is the more common notation) for the clauses. Multisets are not ordered, contrary to clauses: @{term "{#L, L'#} = {#L', L#}"} while on the syntax level, @{text "(L \<or> L') \<noteq> (L' \<or> L)"}, but thanks to associativity and commutativity of @{text \<or>}, the order in clauses does not matter. \<close>

text \<open>
The length of the development is 500 lines of code to define the logic as presented in the previous section. Then there are 300 lines of code that defines abstract transformation and $1~000$ lines of code to define the transformations, CNF and DNF. Now we have defined how to normalise the representation of a set of formulas; we will describe the algorithms that we have formalised.\<close>
(*<*)
end
(*
%  LocalWords:  satisfiability commutativity

*)
(*>*)