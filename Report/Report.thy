(*<*)theory Report
imports  Multiset Multiset_More
begin
(*<*)
(*to allow redefinitions locally*)
declare[[names_short]]
notation (latex)
  "Mempty" ("\<bottom>")

(*Nicer notation for multisets*)
translations
  "a \<subset> b" <= "a \<subset># b"
  "a \<in> b" <= "a \<in># b"
  "a \<notin> b" <= "a \<notin># b"


syntax (latex output)
 "_multiset" :: "args => 'a multiset"  (*"{_}"*) ("\<lbrace>_\<rbrace>")
 
(*%\newcommand{\relcompp}{\mathbin{\circ\kern-.06em\circ}} %% TYPESETTING
%\newcommand{\relcompp}{\mathbin{\mathit{OO}}}
%\newcommand{\relcompp}{\mathbin{\ocircle}}
%\newcommand{\relcompp}{\mathbin{\fullmoon\fullmoon}}
%\newcommand{\relcompp}{\mathbin{\lower.05ex\hbox{\large${\circ}\mskip-1mu{\circ}$}}} %% TYPESETTING
%\newcommand{\relcompp}{\mathbin{\lower.15ex\hbox{\Large${\circ}$}}} %% TYPESETTING
%\newcommand{\relcompp}{\mathrel{\Circle}} %% TYPESETTING
\newcommand{\relcompp}{\mathbin{\raise.2ex\hbox{\scalebox{0.8}{${\varnodot}\mskip-1mu{\varnodot}$}}}} %% TYPESETTING

*) 
notation (latex output)
  relcompp (infixr "\<odot>" 50) 
    
    
(*translations
  "_multiset p" <= "{p}"
  "CONST single x" <= "{x}"*)
  
(*notation (latex)
  Multiset.single ("\<lbrace>_\<rbrace>")
  
  notation (latex output)
  relcompp (infix "\<infinity>" 50) 
*)
(*>*)  
end

(*>*)