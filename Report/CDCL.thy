(*<*)
theory CDCL
imports Main Propo_CDCL ReportPart   
  Report 
  "~~/src/HOL/Library/LaTeXsugar" 
  "~~/src/HOL/Library/OptionalSugar"
begin

translations
  "L^C" <= "CONST Marked L C"
  "L^C" <= "CONST Propagated L C"
notation (latex output)
  cdcl (infixl "\<Rightarrow>\<^sub>\<CDCL>" 50)

abbreviation (xsymbols) cdcl_plus (infix "\<Rightarrow>\<^sub>\<CDCL>\<^sup>+\<^sup>+" 50) where
"cdcl_plus \<equiv> tranclp cdcl"
  
abbreviation (xsymbols) cdcl_star (infix "\<Rightarrow>\<^sub>\<CDCL>\<^sup>*\<^sup>*" 50) where
"cdcl_star \<equiv> rtranclp cdcl"

lemma full_no_more_rewrite_conflicting':
  fixes S' :: "'v cdcl_state"
  assumes full: "full0 cdcl_s (S0_cdcl N) S'"
  and no_d: "distinct_mset_set N"
  and finite: "finite N"
  shows "(conflicting S' = C_Clause {#} \<and> unsatisfiable (clauses S')) \<or> (conflicting S' = C_True \<and> trail S' \<Turnstile>as clauses S')"
  using assms(3) full_cdcl_s_normal_forms[OF assms(1,2)] by auto
  
(*>*)
section \<open>\label{sec:cdcl}The Conflict-Driven Clause Learning Procedure\<close>

text \<open>Conflict-driven clause learning (CDCL) is an extension of the DPLL procedure, based on the idea that backtracking more than one level is more efficient. We will first describe the rules (Suction \ref{sec:cdcl-rules}), then we will show an example (Section \ref{sec:cdcl-example}) before giving more details about the proof and the strategy (Section \ref{sec:cdcl-isa}). In this section we assume that the clauses are without duplicate literals.\<close> 

subsection \<open>\label{sec:cdcl-rules}Rules\<close> 
text \<open>A state is a tuple $(M; N; U; k; C)$ where $M$ is the model we are working on: it is as before a sequence of marked literals. The marks are not the same as before: decision literals (marked with $+$ in the previous section) are now marked with a natural number; propagation literals are marked with a clause (the number of variables to backtrack on).  $N$ is the set of clauses we are considering. $k$ is an integer representing the level of backtracking. $C$ is a non-empty clause (for backtracking states) or $\top$ or $\bot$ ($\bot$ means that the search of a model has not been succesful so far) and $U$ is a set of clauses to keep the clause that previously caused a contradiction.

The level of an atom $\ell$ in $M$ is $j$ when $Pos \ell \in M_j$ or $Neg \ell \in M_j$ and $M=M_n\cdot L^n\cdots M_j L^j\cdots M_1\cdot L^1\cdot M_0$, or $0$ if $\ell$ is not in $M$. The level of the literal $L$ is the level of the atom of $L$. The level of a clause is the maximum of the levels of the literals. 

We start with $(\varepsilon; N;\emptyset; 0;\top)$ and we want to reach a final state: either $(M; N; U, k; \top)$ where $M\vDash N$, or $(M; N; U, k; \bot)$ meaning that $N$ is unsatisfiable. During the search, an intermediate proof step is characterised by  $M\nvDash N$. Here are the rules:

\begin{description}
\item[Propagate](\textsf{Prop}): as in DPLL, we use our knowledge. If $M\vDash \neg C$ and $C\lor L\in N\cup U$ where $L$ has not yet been defined, then $(M; N; U; k;\top)\cdcl (ML^{C\lor L}; N; U; k;\top)$.
\item[Decide](\textsf{Dec}): we can also determine the value of a literal $L$, if it is undefined in $M$:
\[(M; N; U; k;\top)\cdcl (ML^{k+1}; N; U; k+1;\top)\]
 The level of backtracking is increased, since we have decided a new value to backtrack later on.
\item[Conflict](\textsf{Con}): if there is a conflict in our state $(M; N; U, k; \top)$, i.e.\ $D\in N\cup U$ and $M\vDash \neg D$, then we ``mark'' the conflict: $(M; N; U; k;\top)\cdcl (M; N; U; k;D)$.
\item[Skip](\textsf{Skip}): if we are in a situation where we are backtracking ($D\notin \{\bot, \top\}$) and $\neg L$ does not occur in $D$ (i.e.\ does not participate to the conflict), then we can remove the assignation of the literal, $(ML^{C\lor L}; N; U; k; D)\cdcl (M; N; U; k;D)$.
\item[Resolve](\textsf{Resolve}): if $D$ contains a literal of level $k$ or $k=0$: \[(ML^{C\lor L}; N; U; k; D\lor \neg L)\cdcl (M; N; U; k;D\lor C)\]
The idea of the rule is to go back in to our previous state. Imagine we have the state $(ML^{C\lor L}; N; U; k; \bot\lor \neg L)$: we have chosen $L$, but get a conflict after. We change our decision, but as our decision was necessary (see rule \textsf{Prop}), the conflict must be in $C$: $(M; N; U; k;\bot\lor C)$.

\item[Backtrack](\textsf{Back}): one important rule is the backtracking: \[(M_1 K^{i+1}M_2; N; U; k; D\lor L)\cdcl (M_1L^{D\lor L}; N; U\cup \{D\lor L\}; i; \top) \] given that the literal $L$ is of maximal level $k$ and D is of level $i$ where $i<k$ (i.e.\ each literal in $D$ is of maximum level $i$). The CDCL backtrack rule generalises the DPLL backtrack rule: $i$ is the level we jump back, while in DPLL we can only go back one level.
\item[Restart](\textsf{Restart}): We restart our actual partial valuation @{term M}, but not the learnt clauses. This allows restarting and maybe find a conflict faster thanks to the learnt clauses. This rule is heuristically used in practice and can obviously lead to non-termination:
   \[(M; N; U; k; \top)\cdcl (\varepsilon; N; U; k;\top)\]
provided that @{term "M \<Turnstile>as N"} is false.
\item[Forget](\textsf{Forget}): We remove one of the learnt clause:
\[(M; N; U \cup \{C\}; k; \top)\cdcl (M; N; U; k;\top)\] 
provided that @{term "M \<Turnstile>as N"} is false.
\end{description}


When no rule can be applied anymore, then either @{term "C"} is @{text "\<top>"} and @{term N} is satisfiable and @{term "M \<Turnstile>as R"}, or @{term "C"} is @{term \<bottom>} and @{term N} is unsatisfiable. Notice that in both cases we cannot apply \textsf{Restart} and \textsf{Forget}: when @{term C} is @{text "\<top>"}, then @{term "\<not>(M \<Turnstile>as N)"} does not hold, and when @{term C} is @{text "\<bottom>"}, then @{term C} is not equal to @{text \<top>}. Thus, these states are real termination states. \<close>

subsection \<open>\label{sec:cdcl-example}Example\<close>
text \<open>
We use @{term C_True} instead of @{text \<top>} and @{term "C_Clause D"} instead of @{term D} when @{term D} is different of @{text \<top>}. We apply our procedure on the set @{term "N = {{#Pos P, Pos Q#}, {#Neg P, Pos Q#}, {#Pos P, Neg Q#}, {#Neg P, Neg Q, Pos S#},  {#Neg P, Neg Q, Neg S#}}"}.

\begin{longtable}{c@ {~~$\cdcl$ ~~}p{9cm}}
\endhead
@{term "([], N, \<emptyset>, (0::nat), C_True)"}
  &@{term "([Marked (Pos S) (1::nat)], N, \<emptyset>, (1::nat), C_True)"} (\textsf{Dec})\\
  &@{term "([Marked (Neg P) (2::nat), Marked (Pos S) (1::nat)], N, \<emptyset>, (2::nat), C_True)"} (\textsf{Dec})\\
  &@{term "([Propagated (Pos Q) ({#Pos P, Pos Q#}), Marked (Neg P) (2::nat), Marked (Pos S) 1], N, \<emptyset>, (2::nat), C_True)"} (\textsf{Prop})\\
  &@{term "([Propagated (Pos Q) ({#Pos P, Pos Q#}), Marked (Neg P) (2::nat), Marked (Pos S) 1], N, \<emptyset>, (2::nat), C_Clause {#Pos P, Neg Q#})"} (\textsf{Res})\\
  &@{term "([Marked (Neg P) (2::nat), Marked (Pos S) 1], N, \<emptyset>, (2::nat), C_Clause {#Pos P#})"} (\textsf{Back})\\
  &@{term "([Propagated (Pos P) {#Pos P#}, Marked (Pos S) (1::nat)], N, {#Pos P#}, (0::nat), C_True)"} (\textsf{Back})\\
  \multicolumn{2}{p{\linewidth-2cm}}{The result of \textsf{Resolve} rule is @{term "{#Pos P, Pos P#}"}. We must remove the duplicates; otherwise the conditions to apply the backtrack rule are not verified: @{term "{#Pos P, Pos P#}"} is of the form @{term "D + {#Pos P#}"}, but @{term D} is of level 2 as @{term "Pos P"} and @{term D} must be of level strictly less than @{term P}.}\\
  &@{term "([Propagated (Pos Q) ({#Neg P, Pos Q#}), Propagated (Pos P) {#Pos P#}, Marked (Pos S) (1::nat)], N, {#Pos P#}, (0::nat), C_True)"} (\textsf{Prop})\\
  &@{term "([Propagated (Pos Q) ({#Neg P, Pos Q#}), Propagated (Pos P) {#Pos P#}, Marked (Pos S) (1::nat)], N, {#Pos P#}, (0::nat), C_Clause {#Pos P, Neg Q#})"} (\textsf{Prop})\\
  &@{term "([Propagated (Pos P) {#Pos P#}, Marked (Pos S) (1::nat)], N, {#Pos P#}, (0::nat), C_Clause {#Neg P#})"} (\textsf{Res})\\
  &@{term "([Marked (Pos S) (1::nat)], N, {#Pos P#}, (0::nat), C_Clause {#})"} (\textsf{Res})
\end{longtable}

The last rule simply consists in applying \textsf{Resolution}. At the end we get @{term "C_Clause {#}"}: @{term N} is not satisfiable.

\<close>


subsection \<open>\label{sec:cdcl-isa}Isabelle Formalisation\<close>
text \<open>We first present the rules in Isabelle (Section~\ref{sec:cdcl-isa-rules}), then we show some invariants (Section~\ref{sec:cdcl-wo-strat}). If we do not restrict the rules, there is no termination: to prove it, we use a strategy (Section~\ref{sec:cdcl-strat}).\<close>
subsubsection \<open>\label{sec:cdcl-isa-rules}Rules\<close>
text \<open>CDCL is an extension of DPLL and some types are shared between both approaches:\<close>
(*<*)experiment begin (*>*)
        datatype ('v, 'level, 'mark) marked_lit = 
          is_marked: Marked (lit_of: "'v literal") (level_of: "'level") | 
          Propagated (lit_of: "'v literal") (mark_of: 'mark)
(*<*) end (*>*)
text \<open>Re-using the definition allows a to share some lemmas between DPLL and CDCL. The conflicting clause is either @{text \<top>} or a clause:\<close>
(*<*)experiment begin (*>*)
        datatype 'a conflicting_clause = C_True | C_Clause "'a"
(*<*) end (*>*)
text \<open>The separation between @{term N} and the learnt clauses in @{term U} is only to clarify the presentation: the learnt clauses are consequences of @{term N}, thus propagating can be done based on a clause being either in @{term N} or in @{term U}. 

\begin{center}
@{thm[mode=Rule] propagate[OF propagate.propagate_rule, of S M N U k C L]}\textsf{Prop}
\end{center}

\begin{center}
@{thm[mode=Rule] other[OF decided[OF deciding, of S M N U k L]]}\textsf{Dec}
\end{center}

When a conflict is found we update the state:

\begin{center}
@{thm[mode=Rule] conflict [OF conflict_rule, of S M N U k L]}\textsf{Conf}
\end{center}

\begin{center}
@{thm[mode=Rule] other[OF skip[OF skipping, of S L C M N U k D]]}\textsf{Skip}
\end{center}

We can resolve a conflict: when we have done a case distinction before and found a conflict, we can combine both information:

\begin{center}
@{thm[mode=Rule] other[OF resolve[OF resolving, of S L C M N U k D]]}\textsf{Res}
\end{center}

This is an important invariant of the section: we have to ensure that there is no duplicate in the formulas. To remove them, we use the function @{term remdups_mset}.

The backtrack rule is a generalisation of the backtrack rule of DPLL. To backtrack several levels, we introduced a function @{term get_all_marked_decomposition} that returns a list of all decomposition of the form @{text "M\<^sub>1 \<cdot> L\<^sup>k \<cdot> M\<^sub>2"}. This function is linked to the @{term backtrack_split} we used for DPLL by the following theorem: @{thm get_all_marked_decomposition_backtrack_split}.


\begin{center}
@{thm[mode=Rule] other[OF backtrack[OF backtracking, of S M N U k D L K i M\<^sub>1 M\<^sub>2]]}\textsf{Back}
\end{center}
Remark that contrary to DPLL, we are not taking the negation of literal @{term K} in the new state, but a different literal @{term L}.

We have separated \textsf{Restart} and \textsf{Forget} from the other rules:

\begin{center}
@{thm[mode=Rule] rf[OF forget]}\textsf{Forget}
\end{center}

\begin{center}
@{thm[mode=Rule] rf[OF restart]}\textsf{Restart}
\end{center}
  \<close>

subsubsection \<open>\label{sec:cdcl-wo-strat}Invariants without Strategy\<close>
text \<open>There are a few theorems that can be proved without constraint on the rules, then in the next section we introduce a strategy, where the rules \textsf{Restart} and \textsf{Forget} are not applied.\<close>

text \<open>When we use the previous rule without any strategy, there a few propositions that we can show: firstly that the levels on the literals are sorted from the backtracking level @{term k} to one. This is obvious given the construction, but has to be shown.

The learnt clauses are entailed by the clauses. To prove so we need the three following invariants (collectively called @{term cdcl_learnt_clause}):
\begin{itemize}
\item the learnt clauses are entailed by @{term N}: @{term "N \<Turnstile>ps U"}, but @{term "M \<Turnstile>as CNot C"};
\item whenever the conflicting part is not true, then @{term "N \<Turnstile>p C"};
\item for every mark, it is entailed by the clauses:  @{term "N \<Turnstile>ps set (get_all_mark_of_propagated M)"}. 
\end{itemize}

The proof that the learnt clauses are entailed by the set of clauses is very sketchy in Weidenbach's book and I could not understand the link between some of the arguments of in the proof and the argument. We can show the same property about propagated variables as we did for DPLL (Lemma \ref{th-dpll-var-prop}):
\begin{theorem}
  If:
  \begin{itemize}
  \item one step is done, i.e.\  @{thm (prem 1) cdcl_propagate_is_conclusion};
  \item our property holds in $S$: (@{thm (prem 2) cdcl_propagate_is_conclusion};
  \item our 3 previous invariants are true  @{thm (prem 3) cdcl_propagate_is_conclusion};
  \item some properties about the length and the numbering are correct (i.e.\ the levels are @{text "[k\<dots>1]"});
  \item the defined literal are in @{term N}.
  \end{itemize}
  then  @{thm (concl) cdcl_propagate_is_conclusion}.
\end{theorem}

The proof is significantly more complicated than the proof of DPLL and this needs all the invariants that are given as assumption here. This theorem holds independently of the strategy of application of the rules and is enough to show the counterpart of lemma \hyperref[th-dpll-prop-gene]{2.9.2}.

\begin{lemma}{}
  $([\ ]; N) \cdcl^\star (M, N)$ where $M=M_{m+1}\cdot L_m^+ \cdots L_1^+ \cdots M_1$ and there is no decision literal in the $M_i$. Then $N, L_1^+,\dots, L_i^+ \vDash M_1,\dots, M_{i+1}$.
\end{lemma}

Although these properties are very general, we need some more specific constraints on how to apply the rules to prove termination for example: an infinite application of restarts is possible, but incompatible with termination.
\<close>

subsubsection \<open>\label{sec:cdcl-strat}Invariants with Strategy\<close>

text \<open> Weidenbach's book~\cite{CWAutomatedReasoning} presents the strategy we will use, called \emph{reasonable}: the rule \textsf{Conf} is preferred over \textsf{Prop}, and both are preferred over all other and we do not apply restart and forget. In an inductive predicate, there is no order between the different rules. So we create two inductive definitions: one for \textsf{Prop} and \textsf{Conf} @{term cdcl_cp} and one for the other rule @{term cdcl_o} (\textsf{Back}, \textsf{Dec}, \textsf{Res}). There is a strategy in @{term cdcl_cp}: \textsf{Conf} is preferred over\textsf{Prop}. To do so, the rule is stated such that @{term "propagate S S'"} is applied, only if we cannot do any step with a \textsf{Conf} rule, i.e.\ @{term "no_step conflict S"}. To ease the proof, we apply the rule \textsf{Conf} after applying \textsf{Prop} if possible. This allows to be sure that there is no possible conflict after each application of @{term cdcl_cp} or it has been selected.


@{term cdcl_cp} is defined by:
\begin{itemize}
\item if there is conflict, we apply it:
\begin{center}
@{thm[mode=Rule] cdcl_cp.conflict'}
\end{center}
\item if there is no conflict @{thm (prem 2) propagate_conf'}, then we do a propagation @{thm (prem 1) propagate_conf'} and the conflict @{thm (prem 3) propagate_conf'} after propagation.
\begin{center}
@{thm[mode=Rule] propagate_conf'}
\end{center}
\item  if there is no conflict @{thm (prem 2) propagate_no_conf'}, then we do a propagation @{thm (prem 1) propagate_no_conf'} and there is no conflict @{thm (prem 3) propagate_no_conf'}.
\begin{center}
@{thm[mode=Rule] propagate_no_conf'}
\end{center}

\end{itemize}
This presentation allows proving that after a @{term cdcl_cp} step, then we have no conflict: @{text "(conflicting S = C_True \<longrightarrow> (\<forall>D \<in> clauses S \<union> learned_clauses S. \<not>trail S \<Turnstile>as CNot D)"}.

Then we can define the full strategy:
\begin{itemize}
\item if we can apply @{term cdcl_cp}, we apply it as long as possible (more than once, thus the @{text "\<^sup>+\<^sup>\<down>"}):
\begin{center}
@{thm[mode=Rule] cdcl_s.conflict'}
\end{center}

\item if we cannot apply either propagate (@{term "no_step propagate S"}) nor conflict (@{term "no_step conflict S"}), then we can apply another rule @{term "cdcl_o S S'"}. As before when then apply the @{term cdcl_cp} as long as possible, possibly zero times, using @{text "\<^sup>\<down>"}.
\begin{center}
@{thm[mode=Rule] cdcl_s.other'}
\end{center}
\end{itemize}

One of the important invariants is the following:


\qquad\qquad@{text "\<forall>D. conflicting S' = C_Clause D \<longrightarrow> D \<noteq> \<bottom> \<longrightarrow>"}

\qquad\qquad\qquad@{text "(\<exists>L. L \<in> D \<and> get_level L (trail S') = backtrack_level S')"}


It states that whenever we have a conflict that is not @{term "{#}"}, then there is a literal of level the backtracking level. It explains why the backtracking conditions are not too restrictive: we can remove literals until the marked variable, i.e.\ going from $L_1^{C_1}\cdot L_2^{C_2}\cdot\ldots\cdot L_n^{C_n}\cdot K^{i+1}\cdot M$ to $K^{i+1}\cdot M$ using the \textsf{Skip} and \textsf{Res} rules. Then we can apply the backtrack rule, since there is no duplicate and $K \in C$ we do not get stuck in this case. This important invariant does not appear in the proof in Weidenbach's book.


We call @{term "S0_cdcl N"} the initial state: it is an initial state only if @{term N} is finite (@{term "finite N"}) and has no duplicate (@{term "distinct_mset_set N"}). The termination theorem is the following (the fact that we cannot do any more step is included in the definition of @{text "\<^sup>+\<^sup>\<down>"}).
\begin{theorem}[CDCL final states]
@{thm[mode=IfThen] full_no_more_rewrite_conflicting'}
\end{theorem}

This theorem can be verified with the \textsf{Restart} rule, but to prove termination we have to work without this rule. The termination car be shown with a measure:

\quad@{text "case C of "}


\qquad\qquad@{text "C_True \<Rightarrow> [3 ^|atms_of_m N| - |U|, 1, |atms_of_m N| - length M]"}
    
    
\qquad\quad@{text "|  C_Clause _ \<Rightarrow> [3 ^|atms_of_m N| - card U, 0, length M]"}


We are using a list (and not a pair), since the lexicographic order in Isabelle is defined for lists. The decreasing itself is independent of the strategy, but there is one key property that depends on the strategy and has not been formalised in this work: the learnt clauses have not been learnt before. The proof depends on the formalisation of the superposition calculus (ordered resolution as presented in \ref{sec:w-split}) to show it and on the link between CDCL and superposition. In Weidenbach's book~\cite{CWAutomatedReasoning}, the superposition calculus has been already introduced before CDCL. The idea of superposition is to restrain the possible inference of resolution: the result of the inferences is not redundant with respect to our knowledge. 


The length of the development is $2~500$ lines of code: it is nearly five time longer that the DPLL formalisation (compare the 8 rules of CDCL to the 3 rules of DPLL). There are around five hundred lines of code of shared libraries between CDCL and DPLL to define the marked variables and various lemmas about them. 
\<close>

subsection \<open>\label{sec:cdcl-formalization}Formalisation of Modern SAT Solvers\<close>
text \<open>While the resolution formalisation (Section \ref{sec:w-split}) was not an efficient solver (its purpose was the completeness theorem, not efficiency), Mari\'c~\cite{DBLP:journals/jar/Maric09} has developed a concrete CDCL prover and has verified it in Isabelle/HOL (the code can be found in the Archive of Formal Proofs~\cite{SATSolverVerification-AFP}).

It provides an implementation in Haskell (thanks to Isabelle/HOL's code generator). The algorithm is implemented in a side-effect-free manner, and is less efficient than a C state-of-the-art prover, but it is proven that it terminates and that it finds a proof. There are two ways of trusting a prover:
\begin{enumerate}[(1)]
\item proving the program itself: it can be very hard for state-of-the-art algorithms, but allows a full trust into the prover (e.g. if you have proven completeness, then a result will always be found); checking that a model is correct is very easy. The other direction is the difficult one: if no conflict has been found, is the problem really unsatisfiable?
\item extending a SAT solver so that it outputs a proof of the contradiction. Then an external proven verifier can be used to certify the output. It is often simpler to prove, but this approach has some drawbacks, because producing proofs causes an overhead, but allows implementing state-of-the art algorithm without having to prove it. Notice that this means that we can get \emph{nothing}: if the prover always produces a wrong output, you only know that it is wrong. This approach does not really allow proving completeness, since we have no properties on the SAT solver itself. 
\end{enumerate}

The first approach has been used by Mari\'c, allowing someone to use a proven solver (meaning that if a result can be found, it will be found and if a result is found, it is correct). He has proven even more properties like termination, unlike Oe \textit{et al.} in~\cite{Oe:2012:VVM:2189257.2189281}: they have proven in Guru the correctness of a quite efficient implementation in C (using integer overflow for example, contrary to Mari\'c), but no termination.\<close>

(*<*)
end
(*>*)
