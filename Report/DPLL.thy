(*<*)
theory DPLL
imports Main Propo_DPLL
  Report ReportPart DPLL_Implementation    
    "~~/src/HOL/Library/LaTeXsugar" 
    "~~/src/HOL/Library/OptionalSugar"
begin

(*notation (latex output)
  Proped ("")*)
translations
  "P\<^sup>+" <= "CONST Marked P (CONST Level)"
  "P" <= "CONST Propagated P (CONST Proped)" 
  "P" <= "CONST Pos P"
  "\<not>P" <= "CONST Neg P" 
  "\<not>P" <= "CONST CNot P" 
notation (latex output)
  dpll (infixl "\<Rightarrow>\<^sub>\<DPLL>" 50)
  
abbreviation unmark where
"unmark M \<equiv> (\<lambda>a. {#lit_of a#}) ` set M"

abbreviation (xsymbols) dpll_plus (infix "\<Rightarrow>\<^sub>\<DPLL>\<^sup>+\<^sup>+" 50) where
"dpll_plus \<equiv> tranclp dpll"
  
abbreviation (xsymbols) dpll_star (infix "\<Rightarrow>\<^sub>\<DPLL>\<^sup>*\<^sup>*" 50) where
"dpll_star \<equiv> rtranclp dpll"

lemma dpll_all_inv_in_domain:
  assumes "dpll_all_inv (toS' S)"
  shows "DPLL_part_dom S"
  by (metis (mono_tags, lifting) Product_Type.prod.case_eq_if Product_Type.prod.collapse assms dpll_all_inv_implieS_2_eq3_and_dom)
  

lemma dpll_all_inv_implieS_2_eq3:
  assumes "dpll_all_inv (toS M N)"
  shows "DPLL_part M N = DPLL_ci M N"
  using dpll_all_inv_implieS_2_eq3_and_dom[OF assms] by simp
  
(*notation
  dpll (" _ \<Rightarrow>\<^sub>D\<^sub>P\<^sub>L\<^sub>L _")
*) 
(*>*)
section \<open>\label{sec:dpll} The Davis-Putnam-Logemann-Loveland Calculus\<close>
text \<open>
The idea of the previous calculus was to apply rules on the set of clauses whose unsatisfiability we want to show, until we derive @{text \<bottom>}. The idea now is to ``try'' values for propositional variables and when we have found a contradiction to backtrack on the decision and try the other choice. We will first describe the rules (Section \ref{sec:dpll-rules}), then give an example (Section \ref{sec:dpll-example}). After that we will describe the Isabelle formalisation (Section \ref{sec:dpll-isa}), before describing a simple implementation (Section \ref{sec:dpll-imp}).\<close>

subsection \<open>\label{sec:dpll-rules}The Rules\<close>
text \<open>
The Davis-Putnam-Logemann-Loveland (DPLL) procedure is a procedure developed in 1962~\cites{davis1960comproquathe,DLL:CACM-1962}. The idea is to build a sequence of literals that have been assigned: if we have to ``try'' a value we mark the corresponding literal with $^+$. After our choice, we try to find a contradiction and when we have found one, we backtrack on our last choice and choose the opposite value.

More formally we start with the pair $(\varepsilon; N)$: $N$ is the set of clauses and initially no value has been defined and $\varepsilon$ is the empty list. At the end we have either $(M; N)$ where $ M \vDash N$ (meaning that we have found a model) or $(M; N)$ where $ M \vDash \neg N$ without marked variables in $M$: $N$ is unsatisfiable. By construction, $M$ will not contain a contradiction (no atom appears more than once in $M$ and especially it cannot appear both positively and negatively). Here are the rules:
\begin{itemize}
\item Propagate (\textsf{Prop}): if @{term "C\<or>L \<in> N"} and $M\vDash \neg C$ and the atom $L$ has not yet been defined in $M$, then $(M; N) \dpll (L\cdot M; N)$. We do not have to backtrack on that decision, since we have taken the only possible decision for $L$.

We write @{term "undefined_lit L M"} to mean that @{term L} has not been defined in @{term M}, i.e.\ neither @{text L} nor @{text "-L"} is in @{term M}. Contrary to Weidenbach's presentation, the literal is consed on the left and not on the right: his cons operator appends the element to the list (i.e.\ of type @{typ "'a list \<Rightarrow> 'a \<Rightarrow> 'a list"}). In Isabelle the cons operator adds an element at the beginning  (i.e.\ of type @{typ "'a \<Rightarrow> 'a list \<Rightarrow> 'a list"}). As Isabelle lists are a very developed with a lot of automation, we flipped the direction of the list @{term M} between the presentation in the book and in our files. 

\item Decide (\textsf{Dec}): if a literal $L$ is undefined in $M$, we can define it: $(M; N) \dpll (L^+ M; N)$. In that case we have to backtrack later on the decision because we chose arbitrary whether $L$ or $\neg L$ is put in $M$, thus the mark $^+$.

\item Backtrack (\textsf{Back}): starting from $(M_2L^+ M_1; N)$,  if we have found a contradiction i.e.\ $\exists D\in N$ such that $M\vDash\neg D$, then we take the opposite of our last choice (i.e.\ there is no marked variable in $M_2$,  $\neg(\exists K, K^+ \in M)$): $(M_2L^+ M_1; N) \dpll (\neg LM; N)$. As this is the only other case case for the value on $L$, no mark is needed.
\end{itemize}


\<close>
subsection \<open>\label{sec:dpll-example}Example\<close>
text \<open>
We apply our procedure on the set: @{term "N = {{#Pos P, Pos Q#}, {#Neg P, Pos Q#}, {#Pos P, Neg Q#}, {#Neg P, Neg Q, Pos S#},  {#Neg P, Neg Q, Neg S#}}"} (the example comes from~\cite{CWAutomatedReasoning}). The \textsf{Back} rule stands for application of the backtrack rule, \textsf{Dec} for decide and \textsf{Prop} for Propagate. One possible trace of transitions is

\begin{tabular}{c@ {~~$\dpll$ ~~}l@ {\quad}l}
  @{term "([], N)"}&@{term "([Marked (Pos P) Level], N)"}&\textsf{Dec} since @{term "undefined_lit (Pos P) []"}\\
  &@{term "([Propagated (Pos Q) Proped, Marked (Pos P) Level], N)"}&\textsf{Prop} since @{term "{#Neg P, Pos Q#} \<in> N"}\\
  &@{term "([Marked (Pos S) Level, Propagated (Pos Q) Proped, Marked (Pos P) Level], N)"}&\textsf{Dec} since @{term "undefined_lit (Pos S) [Propagated (Pos Q) Proped, Marked (Pos P) Level]"}\\
  &@{term "([Propagated (Neg S) Proped, Propagated (Pos Q) Proped, Marked (Pos P) Level], N)"}&\textsf{Back} since @{term "[Marked (Pos S) Level, Propagated (Pos Q) Proped, Marked (Pos P) Level] \<Turnstile>as CNot {#Neg P, Neg Q, Neg S#}"}\\
  &@{term "([Propagated (Neg P) Proped], N)"}&\textsf{Back} since @{term "[Propagated (Neg S) Proped, Propagated (Pos Q) Proped, Marked (Pos P) Level] \<Turnstile>as CNot {#Neg P, Neg Q, Pos S#}"}\\
  &@{term "([Propagated (Pos Q) Proped, Propagated (Neg P) Proped], N)"}&\textsf{Prop} since @{term "{#Pos P, Pos Q#} \<in> N"}\\
  \multicolumn{3}{p{\linewidth-1cm}}{Now we have found a conflict: @{term "{#Pos P, Neg Q#} \<in> N"} but is false. We could stop here (we now know that the set of clauses @{term N} is unsatisfiable), but we can still preform some transitions:}\\
  &@{term "([Marked (Neg S) Level, Propagated (Pos Q) Proped, Propagated (Neg P) Proped], N)"}&\textsf{Dec} since @{term "([Propagated (Pos Q) Proped, Propagated (Neg P) Proped], N)"}\\
  &@{term "([Propagated (Pos S) Proped, Propagated (Pos Q) Proped, Propagated (Neg P) Proped], N)"}&\textsf{Back} since @{term "[Marked (Neg S) Level, Propagated (Pos Q) Proped, Propagated (Neg P) Proped] \<Turnstile>as CNot {#Pos P, Neg Q#}"}
\end{tabular}

There is no remaining transition: every atom of the problem has a value and we cannot backtrack, since there is no marked variable. As the found valuation is not a model of @{term N}, @{term N} is unsatisfiable.


We can see here that sometimes we can apply rules but we already know whether the set of clauses is satisfiable. This kind of state is a final state (written @{term "final_dpll_state (M, N)"}), but not a termination state for the transition system. In practice, most implementations do no stop, but as we did here, propagate all the variables: the reason is that @{term "M \<Turnstile> N"} is expensive to test: you have to test that every clause in @{term N} has @{term M} as model, so it is cheaper to verify to finish the propagation. Rule \textsf{Propagate} is not needed to show completeness since we can apply decide on the the opposite and then backtrack on the choice, but ignoring it can lead to exponentially longer solution. The strategy of ignoring \textsf{Propagate} is included in our transition system: we do not give any constraint on which rule to apply.
\<close>


subsection \<open>\label{sec:dpll-isa}Isabelle Formalisation\<close>
text \<open>We have formalised the rules described previously (described in Section~\ref{sec:dpll-isa-rules}). Then we will give some indications of the invariants needed for the proof (Section~\ref{sec:dpll-inv}). When doing transitions, there are states that allows to conclude even if there are more possible transitions; these final states are described in Section~\ref{sec:dpll-final-states}. Then we show the theorem of correctness in Section~\ref{sec:dpll-correctness}\<close>
subsubsection \<open>\label{sec:dpll-isa-rules}The Rules\<close>
(*<*)experiment begin(*>*)
text \<open>We use a generalised version of the marked literals (to share definitions and lemmas between this section and the next section) with annotations on both marked and not marked literals:\<close>
(*<*)qualified(*>*) datatype ('v, 'l, 'm) marked_lit = 
  Marked (lit_of: "'v literal") (level_of: "'l") | Propagated (lit_of: "'v literal") "'m"
(*<*)end (*>*)
text \<open>For DPLL the mark is a simple constant called @{term Level}  for the level and @{term Proped} for the propagation. @{term lit_of} is the function that give the literal that is marked or propagated, while @{term lits_of} returns the literals in a list of marked or propagated literals. We will nevertheless write @{term "Marked L Level"} for @{text "Marked L Level"} and @{term "Propagated L Proped"} for @{text "Propagated L Proped"}.
  
Now we can define a new version of @{text "\<Turnstile>"}: @{thm true_annot_def}. We translate the @{text "\<Turnstile>a"} (``a'' stands for annotated) into @{text \<Turnstile>}, using the function @{term lits_of}.
  \<close>


text \<open>The rule of the calculus are:
\begin{itemize}
\item\textsf{Propagate}:%
\begin{center}
  @{thm[mode=Rule] dpll.propagate[of _ _ "(M, N)"]}
\end{center}

\item\textsf{Decide}:%
  \begin{center}
    @{thm[mode=Rule] dpll.decided[of _ "(M, N)"]}
  \end{center}
Unlike Weidenbach's presentation, we have added the condition @{term "atm_of L \<in> atms_of_m N"}. This (obvious) condition is needed for the soundness of the calculus.


\item\textsf{Backtrack}: The distance between the paper version and the Isabelle version is the largest in this case, because there are implicit information. 
\begin{center}
  @{thm[mode=Rule] dpll.backtrack[of "(M, N)"]}
\end{center}  

@{term "backtrack_split M = (M, L M')"} is a decomposition such that @{term "S = M @ L # M'"}. The marked literal is the first element of @{term M'} when such an element exists, otherwise @{term "M' = []"}. Using @{term "(M', Marked L # M')"} instead of tuple @{term "(M', Marked L, M)"} allows a unified vision if there is no marked literal. Notice that @{term "is_marked L"} is redundant given the definition of @{term backtrack_split}, but we stay closer to the definition when adding the condition.
\end{itemize}  

Every rule implies a progress: there is no explicit backtracking where we would go back to a previous state and change the decision taken there.
 
Now we have stated the definitions, we will give an overview of the proofs.
  \<close>
 

subsubsection \<open>\label{sec:dpll-inv}Invariants\<close>
text \<open>
Most invariants are written assuming that the initial state is @{term "([], N)"}, but it is easier to write the properties as invariants: it avoids doing induction on the possible transition and on the iteration of the transitions.
  
One of the very first properties to show is the following:
\begin{theorem}
@{thm[mode=IfThen] dpll_same_clauses[of "(M, N)" "(M', N')"]}
\end{theorem}
This is correct by the way the rules are stated, but we have nevertheless to write this property down and prove it by induction.

\begin{CWtheorem}\label{theo:dpll-dup-comp-freedom}
  The sequence M will, by construction, neither contain duplicate nor complementary literals.
\end{CWtheorem}
We will define @{term no_dup} being the property that there is no duplicate, in the sense that no atom is defined twice. This property subsumes the duplicate freedom and the complementary freedom of the previous theorem.
\begin{theorem}
@{thm [mode=IfThen] dpll_distinctinv}
\end{theorem}
\begin{proof}
  Isabelle is not fully convinced by the argument ``by construction'', but the proof is simply an induction on the possible transitions. Only the backtrack case needs a little more work in Isabelle, because of the splitting.
\end{proof}

  When applying the rule \textsf{Propagate}, we do not have to backtrack on that decision, since we have taken the only possible decision for $L$. This leads to the following theorem:
\begin{CWlemma} Let @{text"(M; N)"} be a stated reached by the DPLL algorithm from the initial state  $(\varepsilon; N)$. If @{text "M=M\<^sub>m\<^sub>+\<^sub>1L\<^sub>m\<^sup>+\<ldots>L\<^sub>1\<^sup>+M\<^sub>1"} and all @{text M\<^sub>i} have no decision literal then for all @{text "1 \<le> i \<le> m"} it holds: @{text "N, M\<^sub>1,\<ldots>, L\<^sub>i\<^sup>+ \<Turnstile> M\<^sub>i\<^sub>+\<^sub>1"}.
\end{CWlemma}

In Isabelle it is a bit more complicated since writing the decomposition down is not as easy. @{term get_all_marked_decomposition} gives all the possible decomposition of the form @{term "(M\<^sub>i, Cons (Propagated L\<^sub>i Proped) M)"} where no variable is marked in @{term M}. Then we want to show that for each of this decomposition @{text "N, M, L\<^sub>i \<Turnstile> M"}. In Isabelle, the @{text \<Turnstile>} is not overloaded, so it becomes:
\begin{center}
@{term "(\<lambda>a. {#lit_of a#}) ` set (L\<^sub>i # M) \<union> N \<Turnstile>ps (\<lambda>a. {#lit_of a#}) ` set M\<^sub>i"}
\end{center}

@{term "(\<lambda>a. {#lit_of a#}) ` set (L\<^sub>i # M)"} converts all the marked literals from @{term "L\<^sub>i # M"} to normal literals. More precisely they become clauses composed of a single literal, meaning that each literal have to be true. The Isabelle needs one more assumption to prove it: all the atoms in @{term M} have to be in the atoms of @{term N}. This condition is written @{thm (prem 3) dpll_propagate_is_conclusion[of "(M, N)" "(M', N')"]}: the operator @{text "`"} means that 
\begin{lemma}\label{th-dpll-var-prop}
If @{thm (prem 2) dpll_propagate_is_conclusion[of "(M, N)" "(M', N')"]} and one DPLL step @{thm (prem 1) dpll_propagate_is_conclusion[of "(M, N)" "(M', N')"]} is done and the condition on the atoms hold @{thm (prem 3) dpll_propagate_is_conclusion[of "(M, N)" "(M', N')"]}, then @{thm (concl) dpll_propagate_is_conclusion[of "(M, N)" "(M', N')"]}.
\end{lemma}

This conclusion can be generalised.
\begin{CWlemma}\label{th-dpll-prop-gene}
  $(\varepsilon; N) \dpll^\star (M, N)$ where $M=M_{m+1}\cdot L_m^+ \cdots L_1^+ \cdots M_1$ and there is no decision literal in the $M_i$. Then: $N, L_1^+,\dots, L_i^+ \vDash M_1,\dots, M_{i+1}$.
\end{CWlemma}

\begin{lemma}
If one step @{thm (prem 1) all_decomposition_implies_propagated_lits_are_implied[OF dpll_propagate_is_conclusion, of "(M, N)" "(M', N')"]} is done, every atom in $M$ is also in $N$ @{thm (prem 3) all_decomposition_implies_propagated_lits_are_implied[OF dpll_propagate_is_conclusion, of "(M, N)"]} and @{thm (prem 2) all_decomposition_implies_propagated_lits_are_implied[OF dpll_propagate_is_conclusion, of "(M, N)" "(M', N')"]}, then @{thm (concl) all_decomposition_implies_propagated_lits_are_implied[OF dpll_propagate_is_conclusion, of "(M, N)"]}.
\end{lemma}


\begin{lemma}
If @{term M} contains only propagated literals and there is @{term "D \<in> N"} with @{term "M \<Turnstile>as CNot D"} then @{term N} is unsatisfiable
\end{lemma}
  The Isabelle version needs some more assumptions, because of our presentation as invariants: the invariants have to be stated explicitly.
  \<close>
(*<*)
(*TODO: Move*)
lemma [simp]:
  assumes "I \<Turnstile>as N"
  and "D \<in> N"
  shows "I \<Turnstile>a D"
  using assms true_annots_def by blast

lemma [simp]:
  assumes "I \<Turnstile>s N"
  and "D \<in> N"
  shows "I \<Turnstile> D"
  using assms true_clss_def by blast


lemma dpll_not_marked_unsat:
  fixes M :: "'v dpll_annoted_lits" and N :: "'v clauses"
  and D :: "'v clause"
  assumes inv: "all_decomposition_implies N (get_all_marked_decomposition M)" 
  and DN: "D \<in> N" and D: "M \<Turnstile>as CNot D"
  and marked: "\<forall>x \<in> set M. \<not> is_marked x"
  and atm_incl: "atm_of ` lits_of M  \<subseteq> atms_of_m N"
  shows "unsatisfiable N"
proof (rule ccontr) -- \<open>We do a proof by contradiction.\<close>
  assume "\<not> unsatisfiable N"
  then obtain I where I: "I \<Turnstile>s N" and cons: "consistent_interp I" and tot: "total_over_m I N" unfolding satisfiable_def by auto
  have I_D: "I \<Turnstile> D" using I DN by simp

  have no_marked: "{{#lit_of L#} |L. is_marked L \<and> L \<in> set M} = {}" using marked by auto
  have "atms_of_m (N \<union> (\<lambda>a. {#lit_of a#}) ` set M) = atms_of_m N" using atm_incl unfolding atms_of_m_def using lits_of_def by fastforce
  hence "total_over_m I (N \<union> (\<lambda>a. {#lit_of a#}) ` (set M))" using tot unfolding total_over_m_def by auto
  hence "I \<Turnstile>s (\<lambda>a. {#lit_of a#}) ` (set M)" using all_decomposition_implies_propagated_lits_are_implied[OF inv] cons I unfolding true_clss_clss_def no_marked by auto
  hence IM: "I \<Turnstile>s (\<lambda>a. {#lit_of a#}) ` set M" unfolding true_clss_def Ball_def true_cls_def true_lit_def by auto

  have "\<forall>K. K \<in># D \<longrightarrow> -K \<in> I"
    proof (clarify)
     fix K
      assume "K \<in># D"
      hence "-K \<in> lits_of M" using D by (metis in_CNot_implies_uminus(2))
      thus "-K \<in> I" using IM sledgehammer[verbose, e spass z3 cvc4]
        using D DN `\<not> unsatisfiable N` atm_incl inv lits_of_def marked only_propagated_vars_unsat by fastforce
    qed
  hence "\<not> I \<Turnstile> D" using cons unfolding true_cls_def true_lit_def consistent_interp_def by auto
  thus False using I_D by metis
qed
(*>*)  
text \<open>
\begin{theorem}
  @{thm[mode=IfThen] dpll_not_marked_unsat}
\end{theorem}
  
This theorem shows that an important property: if we have found a contradiction while running the DPLL and no variable is marked, then the set of clauses is unsatisfiable. This is part of the proof of correctness.\<close>


subsubsection \<open>\label{sec:dpll-final-states}Final States\<close>
text \<open>Final states are states where can conclude: if @{term M} is a model of @{term N}, we know that @{term N} is satisfiable, even if @{term M} is not total. Otherwise, if we have found a conflict and no variable is marked (i.e.\ we cannot backtrack), then @{term N} is unsatisfiable. The formal Isabelle definition is the following: 
\begin{definition}
@{thm final_dpll_state_def}.
\end{definition}


This definition is linked to the termination of the final states of the rewrite system:
\begin{theorem}\label{th:dpll-termination-final}
If we are in state @{term S} and we cannot do any transition (i.e.\ @{thm (prem 1) dpll_no_more_step_is_a_final_state}), then we are in a final state: @{thm (concl) dpll_no_more_step_is_a_final_state}.
\end{theorem}

As we have seen in the example of Section \ref{sec:dpll-example}, the converse is not true: we can be in a final state, but there might be more rewrite steps to do. In practice, testing the satisfiabilty is too hard to be tested. If any of these conditions are met:
\begin{itemize}
\item we have found a contradiction and cannot backtrack;
\item we have done every possible transition.
\end{itemize}
Theses conditions are cheap to verify in an implementation, contrary to the definition of a final states.
\<close>

subsubsection \<open>\label{sec:dpll-correctness}Correctness Theorems\<close>
text \<open>
There states where we know the satisfiability of the set of clauses before having done all the possible transitions, contrary to the resolution calculus where all inferences have to be done to conclude. However, the termination of the rewrite system is enough to prove that we get in final state (Theorem~\ref{th:dpll-termination-final}): if we are lucky we get a final state early in the trace, but at latest when no more transition is possible, we are in a final state. That is why on one side the completeness theorem is expressed using the @{term final_dpll_state} predicate and the termination is proved one the transition system without the predicate. 

The completeness and soundness theorem is the following: 
\begin{theorem}[Completeness, Soundness]
@{thm[mode=IfThen] dpll_completeness'}
\end{theorem}


The termination (no more rewriting step can be done) can be proven using a well-founded order: the lexicographic order over natural numbers. If @{text "M = L\<^sub>n \<cdots> L\<^sub>2 \<cdot>L\<^sub>1 \<cdot> []"}, then we define the following measure converting the state to a list:
\begin{center}
@{text "\<mu>(M, N) = m\<^sub>n \<cdot> \<ldots> \<cdot>  m\<^sub>2 \<cdot> m\<^sub>1 \<cdot> 3 \<cdot> \<ldots> \<cdot> 3"}
\end{center}
where @{text m\<^sub>i} is 2 when @{term L\<^sub>i} is annotated, @{term "1::nat"} otherwise. and there are as many @{term "1::nat"} as non-assigned variable in @{term M}. In Isabelle this becomes
\begin{center}
@{thm dpll_mes_def}
\end{center}
where @{term "map f l"} applies @{term f} on every element of the list @{term l}.


The termination comes from the decreasing of the measure @{term \<mu>} with respect to the lexicographic order. The lexicographic order with respect to order @{text \<prec>} is @{text "lex \<prec>"}. To prove it, we need some of the invariants:
\begin{lemma}
@{thm [mode=IfThen] dpll_card_decrease'[of "(M, N)" "(M', N')"]}
\end{lemma}
@{term "card (atms_of_m N)"} is the cardinal of set composed of the atoms in @{term N}. Although we consider @{thm (prem 1) dpll_card_decrease'[of "(M, N)" "(M', N')"]}, the conclusion is the other way around with @{term "(M',N')"} appearing before @{term "(M, N)"} @{thm (concl) dpll_card_decrease'[of "(M, N)" "(M', N')"]}.

Using this key lemma, we can show the iteration of the transition is well-founded. To prove it, we have to ensure that the needed invariants (all together named @{term dpll_all_inv}) are correct. Using Isabelle well-foundedness predicate @{term wf}: 
\begin{theorem}[\label{th:dpll-wf}Well-foundedness of DPLL]
@{thm [mode=IfThen] dpll_wf}
\end{theorem}
Given the definition of @{term wf}, we have the next state is the first element of the pair: the relation @{text "{(0, 1), (1, 2), (2, 3), \<dots>}"} is well-founded whereas the relation @{text "{\<dots>, (-3, -2), (-2, -1), (-1, 0)}"} is not.

We can remove the invariant of the condition in the set and show that
\begin{theorem}
@{thm [mode=IfThen] dpll_wf_plus}
\end{theorem}

The length of the formalisation is five hundred lines of Isabelle code, with around the same length of libraries that defines the marked literals.
\<close>


subsection \<open>\label{sec:dpll-imp}Implementation in Isabelle\<close>
text \<open> In this section, we give a simple Isabelle implementation of a verified solver trying to prove the satisfiability of a set of clauses. We will first describe the function implementing the transitions (Section \ref{sec:transition-step}). After this, the details are specific to Isabelle, its \textbf{function} definition (Section \ref{sec:DPLL-imp}). Then we export our verified Isabelle implementation in OCaml using the code generation (Section \ref{sec:code-generation}): the exported code is a verified solver in a real-world language.\<close>
subsubsection \<open>\label{sec:transition-step}Transition Step\<close>
text \<open>We use the following strategy:
\begin{itemize}
\item we first try to find a unit clause, i.e.\ a clause composed of a single literal, using the function @{term find_first_unit_clause};
\item otherwise if there is no unit clause, we try to find a contradiction, using the predicate @{term "\<exists>C \<in> set N. Ms \<Turnstile>as CNot (multiset_of C)"};
\item otherwise if there is no conflict, we select an unused variable (taking the first one when iterating over the formulas, @{term "find_first_unused_var"});
\item otherwise, the arguments are simply returned.
\end{itemize}

In our presentation we used sets, but in our program we will use lists. Moreover, we will use integers @{typ int} instead of type @{typ "'a"}.
\<close>
(*<*)
experiment begin
 (*>*)
text \<open>%
\begin{isamarkuptext}%
\isamarkuptrue%
\quad\quad\isacommand{definition}\isamarkupfalse%
\ DPLL{\isacharunderscore}step\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}int\ dpll{\isacharunderscore}annoted{\isacharunderscore}lits\ {\isasymtimes}\ int\ literal\ list\ list\ {\isasymRightarrow}\ int\ dpll{\isacharunderscore}annoted{\isacharunderscore}lits\ {\isasymtimes}\ int\ literal\ list\ list{\isachardoublequoteclose}\ \ \isakeyword{where}\isanewline
\quad\quad\isa{DPLL{\isacharunderscore}step\ {\isacharequal}\ {\isacharparenleft}{\isasymlambda}{\isacharparenleft}Ms{\isacharcomma} N{\isacharparenright}{\isachardot}\isanewline
\quad\quad\quad\textsf{case}\ find{\isacharunderscore}first{\isacharunderscore}unit{\isacharunderscore}clause\ N\ Ms\ \textsf{of}\isanewline
\quad\quad\qquad Some\ L\ {\isasymRightarrow}\ {\isacharparenleft}L\ {\isasymcdot}\ Ms{\isacharcomma}\ N{\isacharparenright}{\isacharparenright}\isanewline
\quad\quad\quad\ {\isacharbar}\ None\ {\isasymRightarrow}\isanewline
\quad\quad\quad\quad\ \textsf{if}\ {\isasymexists}C{\isasymin}N{\isachardot}\ Ms\ {\isasymTurnstile}as\ {\isacharparenleft}{\isasymnot}\ multiset{\isacharunderscore}of\ C{\isacharparenright}\isanewline
\quad\quad\quad\quad\ \textsf{then}\isanewline
\quad\quad\quad\quad\quad\textsf{case}\ backtrack{\isacharunderscore}split\ Ms\ \textsf{of}\isanewline
\quad\quad\quad\quad\qquad{\isacharparenleft}-{\isacharcomma}\ {\isacharbrackleft}{\isacharbrackright}{\isacharparenright}\ {\isasymRightarrow}\ {\isacharparenleft}Ms{\isacharcomma}\ N{\isacharparenright}\ {\isacharbar}\isanewline
\quad\quad\quad\quad\quad\ {\isacharbar}\  {\isacharparenleft}-{\isacharcomma}\ L\ {\isasymcdot}\ M{\isacharparenright}\ {\isasymRightarrow}\ {\isacharparenleft}{\isacharminus}\ lit{\isacharunderscore}of\ L\ {\isasymcdot}\ M{\isacharcomma}\ N{\isacharparenright}\isanewline
\quad\quad\quad\quad\ \textsf{else}\isanewline
\quad\quad\quad\quad\quad\textsf{case}\ find{\isacharunderscore}first{\isacharunderscore}unused{\isacharunderscore}var\ N\ {\isacharparenleft}lits{\isacharunderscore}of\ Ms{\isacharparenright}\ \textsf{of}\isanewline
\quad\quad\quad\quad\qquad None\ {\isasymRightarrow}\ {\isacharparenleft}Ms{\isacharcomma}\ N{\isacharparenright}\isanewline
\quad\quad\quad\quad\quad\ {\isacharbar}\ Some\ a\ {\isasymRightarrow}\ {\isacharparenleft}a\isactrlsup {\isacharplus}\ {\isasymcdot}\ Ms{\isacharcomma}\ N{\isacharparenright}}
\end{isamarkuptext}%
\<close>
(*<*)


end
(*>*)

text \<open>We do not have used any special data structure nor good strategy, since the implementation is only for presentation purpose. 

When no step is possible, then the function @{term DPLL_step} returns its arguments. As we are using different types than the one described in our previous section, we define
@{term "toS Ms N"} that converts the state @{term "(Ms, N)"} from the type @{typ "int dpll_annoted_lits \<times> int literal list list"} to the type presentation we used in the previous section with sets. The correctness theorem associated with this definition is the following:

\begin{theorem}
@{thm[mode=IfThen] DPLL_step_is_a_dpll_step}
\end{theorem}

This lemma shows that @{term DPLL_step} does some subset of the possible transitions, but this is not enough: we can prove the same lemma for the identity function, since the condition @{thm (prem 2)DPLL_step_is_a_dpll_step} is always false. We have to show that @{term DPLL_step} is doing a step until a final state is reached.
 \begin{theorem}
@{thm DPLL_step_stuck_final_state}.
\end{theorem}
\<close>

subsubsection \<open>\label{sec:DPLL-imp}Combining and Termination\<close>
text \<open>

 Using the previously defined @{term DPLL_step}, we would like to combine to write something like: \<close>
(*<*) experiment begin (*>*)
        function DPLL_nt:: "int dpll_annoted_lits \<Rightarrow> int literal list list
            \<Rightarrow> int dpll_annoted_lits \<times> int literal list list" where
        "DPLL_nt Ms N = 
          (let (Ms', N') = DPLL_step (Ms, N) in
           if (Ms', N') = (Ms, N) then (Ms, N) else DPLL_nt Ms' N)"
(*<*)  by fast+ (*>*)
text \<open>But we cannot prove the termination, since the function @{term "DPLL_nt"} is not terminating (\textit{nt} stands for non terminating): we have no control on the arguments @{term "(Ms, N)"} so they can be in an a state where our invariants needed to prove termination do not hold. \<close>
(*<*)termination oops (*>*)
text \<open>A first way to define it, where we still have a total function, is to stop the execution whenever the invariant is false:
\<close>
        function DPLL_ci:: "int dpll_annoted_lits \<Rightarrow> int literal list list 
            \<Rightarrow> int dpll_annoted_lits \<times> int literal list list" where
        "DPLL_ci Ms N = 
          (if \<not>dpll_all_inv (toS Ms N)
          then (Ms, N)
          else
           let (Ms', N') = DPLL_step (Ms, N) in
           if (Ms', N') = (Ms, N) then (Ms, N) else DPLL_ci Ms' N)"
(*<*)        by fast+(*>*)

text \<open>Whenever the invariant @{term "dpll_all_inv (toS Ms N)"} is false, we stop the execution: \textit{ci} in the function name stands for \emph{check invariant}. Otherwise we use the order given by Theorem \ref{th:dpll-wf}, but with the state conversion. Here is the beginning of the proof. We give the well-founded relation to use:\<close>
        termination  
          apply (relation "{(S', S).  (toS' S', toS' S) \<in> {(S', S). dpll_all_inv S \<and> dpll S S'}}")   (*<*) 
        oops (*>*)
text \<open>Then we can show that @{term DPLL_ci} is really doing some steps of the transition systems:
\begin{theorem}
@{thm DPLL_ci_dpll_rtranclp}.
\end{theorem}
\begin{proof}
  The proof is easy. If the invariant is false, then we do not do any step (@{term "Ms = Ms'"} and @{term "N = N'"}) and we use the reflexivity of the relation. Otherwise, we have done a single step.
\end{proof}
We do not need any assumption about the invariant: whenever it is false, the conclusion comes from the fact that @{term "dpll\<^sup>*\<^sup>*"} is reflexive.

This version is not adapted to an implementation: we do not want to verify that the invariant holds at each step. A solution is to use \textbf{function}~\cite{Kraussdefiningrecursive} such that termination is not required everywhere: then the function is only specified on its domain and theorems (like simplification rule) can only be applied if we are in the domain.\<close>
        function (domintros) DPLL_part:: "int dpll_annoted_lits \<Rightarrow> int literal list list 
            \<Rightarrow> int dpll_annoted_lits \<times> int literal list list" where
        "DPLL_part Ms N = 
          (let (Ms', N') = DPLL_step (Ms, N) in
           if (Ms', N') = (Ms, N) then (Ms, N) else DPLL_part Ms' N)"
(*<*)  by fast+ 
end
(*>*)

text\<open> The simplification are generated automatically by the \textbf{function} package. The @{term "DPLL_part"} encodes non termination: the domain is defined as all the points where the procedure terminates. The aim is to find conditions such that where are in the domain, as for example:
@{thm dpll_all_inv_in_domain}  
  
If the function would not terminate, then the domain would be empty. For terminating functions, the condition on the domain is always true. The function @{term DPLL_part} is deeply linked to @{term DPLL_ci} when the invariant is verified:
\begin{theorem}
@{thm[mode=IfThen] dpll_all_inv_implieS_2_eq3}
\end{theorem}

This function is better suited for a code exportation since the invariant is not checked at each step (and the code exportation to verify is not invariant).
\<close>
subsubsection \<open>\label{sec:code-generation}Code Generation\<close>
text \<open>
\begin{figure}
  \centering
  \definecolor{qqqqff}{rgb}{0,0,0.}
  \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
  \draw [rotate around={90.:(-2.,2.5)}] (-2.,2.5) ellipse (2.081138830084191cm and 1.442615274452684cm);
  \draw [rotate around={90.:(4.,3.)}] (4.,3.) ellipse (1.414213562373095cm and 1.cm);
  \draw [->] (-2.,4.) -- (4.,4.);
  \draw [->] (4.,2.) -- (-2.,1.);
  \draw (-4.2,6.5) node[anchor=north west] {\parbox{4cm}{Concrete type @{typ "int dpll_annoted_lits \<times> int literal list list"}}};
  \draw (2.5,6) node[anchor=north west] {\parbox{4cm}{Abstract type @{typ "dpll_state"}}};
  \draw (0.6,4.88) node[anchor=north west] {@{term state_of}};
  \draw (0.74,0.94) node[anchor=north west] {@{term rough_state_of}};
  \draw [rotate around={90.:(-2.,2.)}] (-2.,2.) ellipse (3.cm and 2.23606797749979cm);
  \draw (-2.72,3.5) node[anchor=north west] {\parbox{2cm}{Invariant @{term dpll_all_inv}}};
  \begin{scriptsize}
  \draw [fill=qqqqff] (-2.,4.) circle (1.5pt);
  \draw [fill=qqqqff] (-2.,1.) circle (1.5pt);
  \draw [fill=qqqqff] (4.,4.) circle (1.5pt);
  \draw [fill=qqqqff] (4.,2.) circle (1.5pt);
  \end{scriptsize}
  \end{tikzpicture}
  \caption{\label{fig:dpll-morphism}Morphisms between the abstract type and the concrete type with the invariant: the invariant folds only on a subset of the concrete type, and only this part can be linked to the abstract type.}
\end{figure}
In the previous subsection, we implemented a concrete solver based on DPLL in Isabelle. To export the code in OCaml an obtain a verified solver, we use the \textbf{@{text code_generation}} tool~\cite{IsaCodeGene}. It cannot handle partial function so we introduce a type that embeds the invariant. This follows the classical idiom when using the code generator. Isabelle has a mechanism to define new types as isomorphic to a subtype of an already defined type. Here our new type @{typ dpll_state} is isomorphic to the :\<close>
(*<*) experiment begin (*>*)
        typedef dpll_state =  "{(M::(int, dpll_marked_level, dpll_mark) marked_lit list, N::int literal list list). dpll_all_inv (toS M N)}"
          morphisms rough_state_of state_of(*<*)
        proof
            show "([],[]) \<in> {(M:: (int, dpll_marked_level, dpll_mark) marked_lit list, N::int literal list list). dpll_all_inv (toS M N)}" by (auto simp add: dpll_all_inv_def)
        qed  
        
        end(*>*)
text \<open>Here our new type @{typ dpll_state} is isomorphic to the elements of type @{typ "(int, dpll_marked_level, dpll_mark) marked_lit list \<times> int literal list list"} such that the invariant @{term dpll_all_inv} is true.
  

Now we have defined the type, we can use it and define @{term DPLL_tot}:
\begin{centering}
  @{thm DPLL_tot.simps}
\end{centering}
\noindent where @{const_typ DPLL_step'} is @{term DPLL_step}, but converts its arguments from the abstract type forth to use @{term DPLL_step} and back after the step. This definition of @{term DPLL_tot} allows proving termination.

The definition allows proving the same properties than the other definition, especially the correctness of the transformation.
\begin{theorem}[Correctness of @{term DPLL_tot}]
  If we have evaluated our function, i.e.\ @{thm (prem 1) DPLL_tot_correct[symmetric]} and @{thm (prem 2) DPLL_tot_correct}, then the following equivalence holds: @{thm (concl, rhs) DPLL_tot_correct}@{text "\<longleftrightarrow>"} @{thm (concl, lhs) DPLL_tot_correct}.
\end{theorem}
  \<close>

text \<open>We can now use Isabelle @{text export_code}, that allows generating code from our @{term DPLL_tot} definition. We have to define a constructor @{term Con} going from the concrete type to the abstract type: @{thm Con_def}. Given the conditions, we have proven the termination (since @{term DPLL_tot} terminates) and the correctness. 
  
  
Here is an extract of the generated code in OCaml:

\begin{verbatim}
    let rec rough_state_of (Con x) = x;;
    let rec dPLL_stepa s = Con (dPLL_step (rough_state_of s));;
    let rec dPLL4 s = 
       let sa = dPLL_stepa s in
          (if equal_dpll_state sa s then s else dPLL4 sa);;
\end{verbatim}


There is no invariant check, thanks to our type definition. The result is a proven implementation, if the code generation, the Isabelle kernel and the OCaml compiler are trusted. There is not invariant check.



We have presented the DPLL procedure and a simple implementation. We will now present an evolution of this procedure, that is faster in practice.
  \<close>



(*<*)

end
(*>*)
