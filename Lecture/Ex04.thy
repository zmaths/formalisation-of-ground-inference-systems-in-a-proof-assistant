theory Ex04
imports Main "~~/src/HOL/IMP/AExp" "~~/src/HOL/IMP/BExp" Fun

begin 
section \<open>Ex 4.1\<close>
inductive star :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
refl[simp, intro]: "star r a a" |
step[simp, intro]: "r a\<^sub>1 a\<^sub>2 \<Longrightarrow> star r a\<^sub>2 a\<^sub>3 \<Longrightarrow> star r a\<^sub>1 a\<^sub>3"

lemma star_prepend:
  "r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"
  using star.step .

lemma star_append:
  "star r x y \<Longrightarrow> r y z \<Longrightarrow> star r x z"
  by (induct rule: star.induct) auto

inductive star' :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
refl[simp, intro]: "star' r a a" |
step[simp, intro]: "star' r a\<^sub>1 a\<^sub>2 \<Longrightarrow> r a\<^sub>2 a\<^sub>3 \<Longrightarrow> star' r a\<^sub>1 a\<^sub>3"

lemma star'_append:
  "star' r x y \<Longrightarrow> r y z \<Longrightarrow> star' r x z"
  by auto

lemma star'_prepend:
  "star' r y z \<Longrightarrow> r x y \<Longrightarrow> star' r x z"
  by (induct rule: star'.induct) auto

lemma "star r x y \<longleftrightarrow> star' r x y"
  apply (rule iffI)
   apply (induct rule: star.induct, auto intro: star'_prepend)
  by (induct rule: star'.induct, auto intro: star_append)

lemma "star r x y \<longleftrightarrow> star' r x y" (is "?S \<longleftrightarrow> ?S'")
proof
  assume ?S
  thus ?S' by (induct rule: star.induct) (auto intro: star'_prepend)
next
  assume ?S'
  thus ?S by (induct rule: star'.induct) (auto intro: star_append)
qed

section \<open>Ex 4.2\<close>
fun elems :: "'a list \<Rightarrow> 'a set" where
"elems [] = {}" |
"elems (a # l) = insert a (elems l)"


lemma 
  assumes "x \<in> elems xs"
  shows "\<exists>ys zs. xs = ys @ x # zs \<and> x \<notin> elems ys"
  using assms 
proof (induct xs)
  case Nil
  thus ?case by simp
next
  case (Cons a xs) note IH = this(1) and x_xs = this(2)
  show ?case
    proof (cases "x = a")
      case True
      hence "a # xs = [] @ x # xs \<and> x \<notin> elems []" by auto
      thus ?thesis by metis
    next
      case False
      hence "x \<in> elems xs" using x_xs by auto
      then obtain ys zs where "xs = ys @ x # zs \<and> x \<notin> elems ys" using IH by metis
      hence "a # xs = (a # ys) @ x # zs \<and> x \<notin> elems (a # ys)" using False by auto
      thus ?thesis by metis
    qed
qed

section \<open>Ex 4.3\<close>
inductive ev :: "nat \<Rightarrow> bool" where
ev0: "ev 0" |
evSS: "ev n \<Longrightarrow> ev (Suc (Suc n))"

thm ev.cases

lemma "ev (Suc (Suc n)) \<Longrightarrow> ev n" 
proof -
  assume "ev (Suc (Suc n))" then show "ev n" by cases
qed

inductive_cases evSS_elim: "ev (Suc (Suc n))" (*<*)
thm evSS_elim
lemma "ev (Suc (Suc n)) \<Longrightarrow> ev n"
by (rule evSS_elim)

lemma "\<not> ev (Suc (Suc (Suc 0)))" (*<*)
proof
  assume "ev (Suc (Suc (Suc 0)))" then show "False"
  proof cases
    assume "ev (Suc 0)" then show "False"
    by cases
  qed
qed

inductive_cases evS0_elim: "ev (Suc 0)"

lemma "\<not> ev (Suc (Suc (Suc 0)))"
by (auto elim: evSS_elim evS0_elim)

lemma "\<not> ev (Suc (Suc (Suc 0)))"
by (auto elim: ev.cases)

lemma 
  assumes "ev (Suc (Suc n))"
  shows "ev n"
  using assms
proof cases
qed

lemma 
  assumes "ev (Suc (Suc n))"
  shows "ev n"
  using assms by cases


lemma 
  assumes "ev (Suc (Suc n))"
  shows "ev n"
  using assms
by (auto elim: ev.cases)

lemma "\<not>ev (Suc (Suc (Suc 0)))"
by (auto elim: ev.cases)













section \<open>Homework 1\<close>
datatype 'a tree =
  Leaf 'a | Node "'a tree" "'a tree"

primrec alphabet :: "'a tree \<Rightarrow> 'a set" where
"alphabet (Leaf a) = {a}" |
"alphabet (Node t u) = alphabet t \<union> alphabet u"

primrec depth :: "'a tree \<Rightarrow> 'a \<Rightarrow> nat" where
"depth (Leaf _) a = 0" |
"depth (Node t u) a = 
  max (if a \<in> alphabet t then depth t a + 1 else 0)
      (if a \<in> alphabet u then depth u a + 1 else 0)"  

fun height :: "'a tree \<Rightarrow> nat" where
"height (Leaf _) = 0" |
"height (Node u v) = 1 + max (height u) (height v)"

lemma [simp]: "a \<notin> alphabet t \<Longrightarrow> depth t a = 0"
  by (induct t, auto)
lemma [simp, intro]: "finite (alphabet t)"
  by (induct t) auto

lemma depth_le_height[simp]:
  "depth t a \<le> height t"
  by (induct t) auto

notepad
begin
  fix t :: "'a tree"
  assume "\<exists>a \<in> alphabet t. depth t a = height t"
  then obtain a where "a \<in> alphabet t" and "depth t a = height t" by blast

  assume "\<exists>a \<in> alphabet t. depth t a = height t"
  then obtain a where "a \<in> alphabet t \<and> depth t a = height t" by metis

  assume "a \<in> alphabet t \<and> depth t a = height t" 
  hence "\<exists>a \<in> alphabet t. depth t a = height t" by blast

  assume "a \<in> alphabet t" and "depth t a = height t" 
  hence "\<exists>a \<in> alphabet t. depth t a = height t" by blast
end


lemma
 "\<exists>a\<in>alphabet t. depth t a = height t" (is "\<exists>a\<in> ?\<alpha>. ?d a = ?h")
(*proof (induct t)
   case Leaf
   thus ?case by simp
next
   case (Node u v) note IH\<^sub>u = this(1) and IH\<^sub>v = this(2)
   let ?t = "Node u v"
   { assume uv: "height u \<ge> height v"
     hence "height ?t = 1 + height u" by auto
     also obtain a where au: "a \<in> alphabet u" and "depth u a = height u"  using IH\<^sub>u by metis
     moreover have d: "depth (Node u v) a = 1 + depth u a" apply (simp add: au) sorry
     ultimately have "a\<in>alphabet (Node u v) \<and> depth (Node u v) a = height (Node u v)" by auto
   }
  *) 
sorry
   

section \<open>homework 4.3\<close>
datatype com =
  SKIP
| Assign vname aexp
| Seq com com
| If bexp com com
| Repeat nat com

value "f ^^ 2"
fun eval :: "com \<Rightarrow> state \<Rightarrow> state" where
"eval SKIP s = s" |
"eval (Seq c\<^sub>1 c\<^sub>2) s = eval c\<^sub>2 (eval c\<^sub>1 s)" |
"eval (If cond c\<^sub>1 c\<^sub>2) s = (if bval cond s then eval c\<^sub>1 s else eval c\<^sub>2 s)" |
"eval (Assign v exp) s = (s(v := aval exp s))"|
"eval (Repeat n c) s =  (eval c ^^ n) s"
value "eval (Repeat 2 (Assign x (Plus (V x) (N 1)))) <>"

inductive ival :: "com \<Rightarrow> state \<Rightarrow> state \<Rightarrow> bool" where
skip[simp, intro]: "ival SKIP s s" |
Seq[simp, intro]: "ival c\<^sub>1 s s' \<Longrightarrow> ival c\<^sub>2 s' s'' \<Longrightarrow> ival (Seq c\<^sub>1 c\<^sub>2) s s''" |
If_true[simp, intro]: "bval cond s \<Longrightarrow> ival c\<^sub>1 s s' \<Longrightarrow> ival (If cond c\<^sub>1 c\<^sub>2) s s'" |
If_false[simp, intro]: "\<not>bval cond s \<Longrightarrow> ival c\<^sub>2 s s' \<Longrightarrow> ival (If cond c\<^sub>1 c\<^sub>2) s s'" |
Assign[simp, intro]: "ival (Assign v exp) s (s(v := aval exp s))" |
Repeat0[simp, intro]: "ival (Repeat 0 c) s s" |
Repeat0_Suc[simp, intro]: "ival (Repeat n c) s s' \<Longrightarrow> ival c s' s'' \<Longrightarrow> ival (Repeat (Suc n) c) s s''"


lemma eval_sound:
  "ival c s (eval c s)"
proof (induction c s rule: eval.induct[case_names SKIP Seq If Assign Repeat])
  case (Repeat n c s)
  show ?case 
    apply (induct n arbitrary: s) by simp_all (metis Repeat.IH Repeat0_Suc)
next
  case (Assign v exp s)
  thus ?case by (auto simp only: eval.simps)
qed auto

lemma eval_complete: "ival c s s' \<Longrightarrow> s' = eval c s"
  by (induct rule: ival.induct) auto

lemma ival_total:
  "\<exists>s'. ival c s s'"
  using eval_sound by blast

lemma ival_determ:
  assumes "ival c s s'" and "ival c s s''"
  shows "s' = s''"
  using assms by (blast dest: eval_complete)

section \<open>Homework 4.3\<close>
inductive_set rst_close :: "'a rel \<Rightarrow> 'a rel" for r :: "'a rel" where
refl[intro, simp]: "(s, s) \<in> rst_close r" |
r[intro, simp]: "(s, s') \<in> r \<Longrightarrow> (s, s') \<in> rst_close r" |
sym: "(s, s') \<in> rst_close r \<Longrightarrow> (s', s) \<in> rst_close r" |
trans[intro, simp]: "(s, s') \<in> rst_close r \<Longrightarrow> (s', s'') \<in> rst_close r \<Longrightarrow> (s, s'') \<in> rst_close r"

value "r\<inverse>"
thm rst_close.induct
lemma "rst_close r = rtrancl (r \<union> r\<inverse>)" (is "?A = ?B")
proof
  { fix x y
    have "(x, y) \<in> ?A \<Longrightarrow> (x, y) \<in> ?B"
      proof (induction rule: rst_close.induct)
        case (refl s)
        thus ?case by auto
      next
        case (r s s')
        thus ?case by auto
      next
        case (sym s s')
        thus ?case by (metis (no_types, lifting) Un_commute converse_Un converse_converse rtrancl_converseI)
      next
        case (trans s s' s'')
        thus ?case by (metis rtrancl_trans) 
      qed
  }
  thus "?A \<subseteq> ?B" by fast
next
  { fix x y
    have "(x, y) \<in> ?B \<Longrightarrow> (x, y) \<in> ?A"
      proof (induction rule: rtrancl.induct)
        print_cases
        case (rtrancl_refl s)
        thus ?case by auto
      next
        case (rtrancl_into_rtrancl s s' s'')
        thus ?case by (metis UnE converse_iff rst_close.simps)
      qed
  }
  thus "?B \<subseteq> ?A" by fast
qed

thm rst_close.induct

section \<open>hw 4.4\<close>
lemma
  fixes f :: "bool \<Rightarrow> bool"
  shows "f (f (f b)) = f b"
proof -
  {
    assume b: "b = True" and fT: "f True = True"
    hence ?thesis unfolding b unfolding fT by blast
  }
  also {
    assume b: "b = True" and fT[simp]: "f True = False"
    hence ?thesis unfolding b unfolding fT by (case_tac "f False") auto
  }
  moreover {
    assume b: "b = False" and fT[simp]: "f True = True"
    hence ?thesis unfolding b unfolding fT
      proof -
        have "(\<not> f (f (f False))) \<noteq> f False" by fastforce
        thus "f (f (f False)) = f False" by meson
      qed
  }
  moreover {
    assume b: "b = False" and fT[simp]: "f True = False"
    have "(\<not> f (f (f False))) \<noteq> f False" by fastforce
    hence ?thesis unfolding b unfolding fT by fastforce
  }
  ultimately show ?thesis by blast
qed

end
