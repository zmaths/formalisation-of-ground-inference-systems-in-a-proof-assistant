theory Ex03
imports Main "~~/src/HOL/IMP/ASM" "~~/src/HOL/IMP/BExp"
begin

inductive is_aval :: "aexp \<Rightarrow> state \<Rightarrow> val \<Rightarrow> bool" where
[simp, intro]: "is_aval (N n) s n" |
[simp, intro]: "is_aval (V v) s (s v)" |
[simp, intro]: "is_aval a\<^sub>1 s n\<^sub>1 \<Longrightarrow> is_aval a\<^sub>2 s n\<^sub>2 \<Longrightarrow> is_aval (Plus a\<^sub>1 a\<^sub>2) s (n\<^sub>1 + n\<^sub>2)"

lemma "is_aval (Plus (N 2) (Plus (V x) (N 3))) s (2 + (s x + 3))"  by blast

lemma aval1:
  "is_aval a s v \<Longrightarrow> aval a s = v"
  by (induct rule: is_aval.induct) auto

lemma aval2:
  "aval a s = v \<Longrightarrow> is_aval a s v"
  by (induct a arbitrary: v) auto

theorem
  "is_aval a s v \<longleftrightarrow> aval a s = v" (is "?A \<longleftrightarrow> ?B")
proof
  assume ?A 
  thus ?B using aval1 by blast
next
  assume ?B 
  thus ?A using aval2 by blast
qed

fun exec1 :: "instr \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack option" where
"exec1 (LOADI n) _ stk  =  Some (n # stk)" |
"exec1 (LOAD x) s stk  =  Some (s x # stk)" |
"exec1  ADD _ stk  = 
  (case stk of
    a # b # stk' \<Rightarrow> Some ((a + b) # stk')
  | _ \<Rightarrow> None)"

fun exec :: "instr list \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack option" where
"exec [] _ stk = Some stk" |
"exec (i#is) s stk = 
  (case exec1 i s stk of
    Some stk' \<Rightarrow> exec is s stk'
  | _ \<Rightarrow> None)"

theorem exec_comp:
  "exec (comp a) s stk = Some (aval a s # stk)"
  apply (induct a arbitrary: stk)
  apply auto
  oops

lemma exec_append[simp]:
  "exec (is1 @ is2) s stk =
     (case exec is1 s stk of
       Some stk \<Rightarrow> exec is2 s stk
     | None \<Rightarrow> None)"
  by (induction is1 arbitrary: stk) (auto split: option.split)

theorem exec_comp[simp]:
  "exec (comp a) s stk = Some (aval a s # stk)"
  by (induct a arbitrary: stk) auto

section \<open>Ex 3.3\<close>
datatype ifexp = Bc' bool | If ifexp ifexp ifexp | Less' aexp aexp

fun ifval :: "ifexp \<Rightarrow> state \<Rightarrow> bool" where
"ifval (Bc' v) _ = v" |
"ifval (Less' a\<^sub>1 a\<^sub>2) s = (aval a\<^sub>1 s < aval a\<^sub>2 s)" |
"ifval (If cond th el) s = (if ifval cond s then ifval th s else ifval el s)"

fun or :: "bexp \<Rightarrow> bexp \<Rightarrow> bexp" where
"or b1 b2 = Not (And (Not b1) (Not b2))"

fun If_bexp :: "bexp \<Rightarrow> bexp \<Rightarrow> bexp \<Rightarrow> bexp" where
"If_bexp b1 b2 b3 = or (And b1 b2) (And (Not b1) b3)"

fun translate :: "ifexp \<Rightarrow> bexp" where
"translate (Bc' v) = Bc v" |
"translate (Less' a\<^sub>1 a\<^sub>2) = Less a\<^sub>1 a\<^sub>2" |
"translate (If cond th el) = If_bexp (translate cond) (translate th) (translate el)"

lemma "bval (translate a) = ifval a"
  by (induct a) auto

section \<open>3.4\<close>
fun sum_n :: "nat \<Rightarrow> nat" where
"sum_n 0 = 0" |
"sum_n (Suc n) = Suc n + sum_n n"
(*
Induction over n:
n = 0

  have sum_n 0 = 0
  also have (0::nat) * (0 + 1) div 2 = 0
  so (0::nat) * (0 + 1) div 2 = 0

*)
lemma "sum_n n = n * (n + 1) div 2" (*<*)
proof (induction n)
print_cases
  case 0
  show ?case
  proof -
    have 1: "sum_n 0 = 0" using sum_n.simps(1) .
    have 2: "(0::nat) * (0 + 1) div 2 = 0" by simp
    show ?thesis by (subst 1, subst 2) (rule refl)
  qed
next
  case (Suc n) note IH = this
  have "sum_n (Suc n) = Suc n + sum_n n" using sum_n.simps(2) .
  also have "\<dots> = Suc n + n * (n + 1) div 2" apply (subst IH) by (rule refl)
  also have "\<dots> = (2 * Suc n + n * (n + 1)) div 2" using div_mult_self4 eq_numeral_simps(4) by metis
  also have "\<dots> = (Suc n * (2 + n)) div 2" using algebra_simps(3) Suc_eq_plus1 mult.commute by metis
  also have "\<dots> = (Suc n * (n + 2)) div 2" unfolding add.commute by (rule refl)
  finally show ?case using Suc_eq_plus1 Suc_eq_plus1_left add.left_commute one_add_one by metis
qed
end
