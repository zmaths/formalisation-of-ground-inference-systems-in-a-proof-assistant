(*<*)
theory Patronymikos_Eponymikos
imports
  "~~/src/HOL/IMP/Denotational"
  "~~/src/HOL/IMP/Hoare"
begin
(*>*)


(* Exercise 11.1 *)

(*
      | Repeat com bexp         ("(REPEAT _/ UNTIL _)"  [0, 61] 61)
*)


(* Exercise 11.2 *)

definition MAX :: com where
  "MAX \<equiv> undefined"

lemmas Seq_bwd = Seq[rotated]

lemmas hoare_rule[intro?] = Seq_bwd Assign Assign' If

lemma [simp]: "(a :: int) < b \<Longrightarrow> max a b = b"
  oops

lemma [simp]: "\<not> (a :: int) < b \<Longrightarrow> max a b = a"
  oops

lemma "\<turnstile> {\<lambda>s. True} MAX {\<lambda>s. s ''c'' = max (s ''a'') (s ''b'')}"
  oops

definition MAX_wrong :: com where
  "MAX_wrong =
   ''a'' ::= N 0 ;;
   ''b'' ::= N 0 ;;
   ''c'' ::= N 0"

lemma "\<turnstile> {\<lambda>s. True} MAX_wrong {\<lambda>s. s ''c'' = max (s ''a'') (s ''b'')}"
  oops

lemma "\<turnstile> {\<lambda>s. a = s ''a'' \<and> b = s ''b''}
  MAX
  {\<lambda>s. s ''c'' = max a b \<and> a = s ''a'' \<and> b = s ''b''}"
oops


(* Homework 11.1 *)

theorem lfp_dual_gfp: "lfp f = - gfp (\<lambda>A :: 'a set. - f (- A))"
apply rule
by (simp_all add: compl_le_swap1 gfp_least lfp_lowerbound compl_le_swap2 gfp_upperbound lfp_greatest)


thm lfp_def lfp_lowerbound gfp_upperbound gfp_least lfp_greatest
theorem gfp_dual_lfp: "gfp f = - lfp (\<lambda>A :: 'a set. - f (- A))"
by (simp add: lfp_dual_gfp)



(* Homework 11.2 *)

definition dual_chain :: "(nat \<Rightarrow> 'a set) \<Rightarrow> bool" where
"dual_chain S = (\<forall>i. S (Suc i) \<subseteq> S i)"

definition dual_cont :: "('a set \<Rightarrow> 'b set) \<Rightarrow> bool" where
"dual_cont f = (\<forall>S. dual_chain S \<longrightarrow> f (\<Inter>n. S n) = (\<Inter>n. f (S n)))"

lemma dual_cont_inter2:
  fixes f :: "'a set \<Rightarrow>'b set"
  assumes dc: "dual_cont f"
  and "c \<subseteq> d"
  shows "f (c\<inter>d) = f c \<inter> f d"
proof -
  let ?S = "\<lambda>n. case n of 0 => d | Suc 0 => c | _ => c"
  have "dual_chain ?S" using assms unfolding dual_chain_def by (auto split: nat.split)
  hence "f (\<Inter>n. ?S n) = (\<Inter>n. f (?S n))" using dc unfolding dual_cont_def by blast
  also have "(\<Inter>n. ?S n) = c \<inter> d" using assms(2) 
    apply (auto split: nat.split) 
    by (metis (full_types) old.nat.simps(4,5))+
  moreover have "(\<Inter>n. f (?S n)) = f c \<inter> f d" 
    using assms(2) apply auto 
    apply (metis old.nat.simps(5))
    apply (metis old.nat.simps(4))
    using Int_absorb2 calculation(1) calculation(2) by fastforce
  ultimately show ?thesis by auto
qed


lemma dual_cont_inter_less_than:
  fixes f :: "'a set \<Rightarrow>'b set"
  assumes dc: "dual_cont f"
  shows "f (a \<inter> b) \<subseteq> f a \<inter> f b" (is "?A \<subseteq> ?B")
proof -
  have "f (a \<inter> b) = f (a \<inter> b \<inter> b)" by auto
  also have "\<dots> = f (a\<inter>b) \<inter> f b"  (is ?G)
    apply (rule dual_cont_inter2)
    using assms by auto
  moreover have "f (a \<inter> b \<inter> a) = f (a\<inter>b) \<inter> f a"
    apply (rule dual_cont_inter2)
    using assms by auto
  moreover have " f (a \<inter> b) = f (a \<inter> b \<inter> a)" by (simp add: inf.commute)
  ultimately have "f (a \<inter> b) = f (a \<inter> b) \<inter> f a \<inter> f b" by blast
  thus "?A \<subseteq> ?B" by blast
qed

lemma dual_count_mono:
  fixes f :: "'a set \<Rightarrow>'b set"
  assumes dc: "dual_cont f"
  shows "mono f"
proof
  fix x y:: "'a set"
  assume xy: "x \<subseteq> y"
  hence "(x \<inter> y) = x" by auto
  hence "f x = f (x \<inter> y)" by auto
  also have "\<dots> = f ((x \<inter> y) \<inter> y)" by auto
  also have "\<dots> = f x \<inter> f y" 
    using `f (x \<inter> y) = f (x \<inter> y \<inter> y)` dc dual_cont_inter2 xy by blast
  finally show "f x \<le> f y" by blast
qed


lemma
  assumes dc: "dual_cont f"
  shows "f x \<le> x"
oops

lemma chain_dual_iterates: fixes f :: "'a set \<Rightarrow> 'a set"
  assumes "mono f" shows "dual_chain(\<lambda>n. (f^^n) UNIV)"
proof-
  { fix n have "(f ^^ Suc n) UNIV \<subseteq> (f ^^ n) UNIV" using assms
    by (induction n)  (auto simp: mono_def)
 }
  thus ?thesis by(auto simp: dual_chain_def)
qed

theorem gfp_if_cont:
  assumes "dual_cont f" shows "gfp f = (\<Inter>n. (f ^^ n) UNIV)" (is "_ = ?U")
proof
  show "?U \<subseteq> gfp f"
  proof (rule gfp_upperbound)
    have "f (INTER UNIV  (\<lambda>n. (f ^^ n) UNIV)) = (\<Inter>n. f ( (\<lambda>n. (f ^^ n) UNIV) n))"
      using chain_dual_iterates[OF dual_count_mono[OF assms]] assms unfolding dual_cont_def by auto
    then have "f ?U = (\<Inter>n \<in> {n. n>0}. (f ^^ n) UNIV)"
      apply auto
      apply (metis (mono_tags, lifting) Suc_diff_1 comp_apply funpow_Suc_right funpow_swap1)
      by auto
    also have "\<dots> = (f^^0) UNIV \<inter> \<dots>" by simp
    also have "\<dots> = ?U"
      apply (auto simp del: funpow.simps)
      using neq0_conv by blast
    finally show " ?U \<subseteq> f ?U" by simp
  qed
next
  { fix n p assume "p \<subseteq> f p"
    have "p \<subseteq> (f^^n) UNIV"
    proof(induction n)
      case 0 show ?case by simp
    next
      case Suc
      from monoD[OF dual_count_mono[OF assms] Suc] `p \<subseteq> f p`
      show ?case by simp
    qed
  }
  thus "gfp f \<subseteq> ?U" by(auto simp: gfp_def)
qed


end

