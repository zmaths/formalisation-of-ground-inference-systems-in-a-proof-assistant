theory Ex05

imports Main "~~/src/HOL/IMP/AExp" "~~/src/HOL/IMP/BExp" "~~/src/HOL/IMP/Big_Step" 

begin

section \<open>Exercise 5.1\<close>















lemma "equiv_c (IF And b\<^sub>1 b\<^sub>2 THEN c\<^sub>1 ELSE c\<^sub>2) (IF b\<^sub>1 THEN (IF b\<^sub>2 THEN c\<^sub>1 ELSE c\<^sub>2) ELSE c\<^sub>2)"
  by (fastforce split: split_if_asm)

(*
{s = 0} while s < 2 && s < 3 do s:= s + 1 {s = 2}
{s = 0} while s < 2 do while s < 3 do s:= s + 1 {s = 3}
*)  
lemma ex1_CE: 
  shows "~ equiv_c
      (WHILE And (Less (V ''1'') (N 2)) (Less (V ''1'') (N 3)) DO ''1'' ::=  Plus (V ''1'') (N 1)) 
      (WHILE (Less (V ''1'') (N 2)) DO (WHILE Less (V ''1'') (N 3) DO ''1'' ::=  Plus (V ''1'') (N 1)))"
   (is "~ equiv_c ?p\<^sub>1 ?p\<^sub>2")   
proof -
  let ?s = " (\<lambda>x. 0)(''1'' := 0)"
  let  ?t = "(\<lambda>x. 0)( ''1'' := 2)"
  let  ?t' = "(\<lambda>x. 0)( ''1'' := 3)"  
  have "(?p\<^sub>1, ?s) \<Rightarrow> ?t"
    by fastforce
  also have "(?p\<^sub>2, ?s) \<Rightarrow> ?t'"
    by fastforce
  ultimately show ?thesis by (smt big_step_determ fun_upd_same) 
qed    
    
    
lemma 
  "equiv_c
      (WHILE And b\<^sub>1 b\<^sub>2 DO c) 
      (WHILE b\<^sub>1 DO (WHILE b\<^sub>2 DO c))"
(*    
    b\<^sub>1 = And (Less (V s\<^sub>1) (V s\<^sub>2)) (Less (V s\<^sub>1) (V s\<^sub>2))
    b\<^sub>2 = Less (V s\<^sub>3) (V s\<^sub>1)
    c = s\<^sub>3 ::= V s\<^sub>1
  Skolem constants:
    s = (\<lambda>x. _)(s\<^sub>1 := - 1, s\<^sub>2 := 0, s\<^sub>3 := 2, s\<^sub>4 := 1)
    t = (\<lambda>x. _)(s\<^sub>1 := - 1, s\<^sub>2 := 0, s\<^sub>3 := 2, s\<^sub>4 := 1)
    *)
oops

lemma 
  assumes "\<forall>b\<^sub>1 b\<^sub>2 c.  equiv_c
      (WHILE And b\<^sub>1 b\<^sub>2 DO c) 
      (WHILE b\<^sub>1 DO (WHILE b\<^sub>2 DO c))"
   shows False   
  using ex1_CE assms by simp
(*
{} while True && False do SKIP {}
{s = 0} while True do SKIP;; while True && False do SKIP {?}
*)  
lemma 
  shows "~ equiv_c
      (WHILE And (Bc True) (Bc False) DO SKIP)
      (WHILE Bc True DO SKIP;; (WHILE And (Bc True) (Bc False) DO SKIP))"
   (is "~ equiv_c ?p\<^sub>1 ?p\<^sub>2")   
proof (rule ccontr)
  assume H: "~ ?thesis"
  let ?s = "\<lambda>x. 0"
  have "((WHILE And (Bc True) (Bc False) DO SKIP), ?s) \<Rightarrow> ?s" by fastforce
  moreover 
  {
    fix s t
    assume "(WHILE Bc True DO SKIP, s) \<Rightarrow> t"
    hence False
       by (induction "WHILE Bc True DO SKIP" s t  rule: big_step_induct) auto
  }
  ultimately show False using H by auto
qed 

(*{a=1}      while (a < 2 & a < 1) do a := 4*)
lemma ex1_CE2:
  shows "~ equiv_c
      (WHILE And (Less (V a) (N 2)) (Less (V a) (N 1)) DO a ::= N 12)
      (WHILE (Less (V a) (N 2)) DO a ::= N 12;; 
         (WHILE And (Less (V a) (N 2)) (Less (V a) (N 1)) DO a ::= N 12))"
   (is "~ equiv_c ?p\<^sub>1 ?p\<^sub>2")   
proof (clarsimp, intro exI)
  let ?s = "(\<lambda>s. 1)"
  let ?t = "(\<lambda>s. 1)(a := 12)"
  have "(?p\<^sub>1, ?s) \<Rightarrow> ?s"
    by auto
  also have "(?p\<^sub>2, ?s) \<Rightarrow> ?t" 
    by fastforce
  ultimately show " (?p\<^sub>1, ?s) \<Rightarrow> ?t = (\<not> (?p\<^sub>2, ?s) \<Rightarrow> ?t)" 
    by (metis big_step_determ fun_upd_same numeral_One numeral_eq_iff semiring_norm(85))
qed    
    
lemma 
  "equiv_c
      (WHILE And b\<^sub>1 b\<^sub>2 DO c) 
      (WHILE b\<^sub>1 DO c;; WHILE And b\<^sub>1 b\<^sub>2 DO c)"
(*    
Nitpick found a potentially spurious counterexample:

  Free variables:
    b\<^sub>1 = Less (N (- 1)) (V s\<^sub>1)
    b\<^sub>2 = Bc True
    c = SKIP
  Skolem constants:
    s = (\<lambda>x. _)(s\<^sub>1 := - 1, s\<^sub>2 := 1, s\<^sub>3 := 0, s\<^sub>4 := - 1)
    t = (\<lambda>x. _)(s\<^sub>1 := - 1, s\<^sub>2 := 1, s\<^sub>3 := 0, s\<^sub>4 := - 1)
    *)
oops

lemma 
  assumes "\<forall>b\<^sub>1 b\<^sub>2 c. equiv_c
      (WHILE And b\<^sub>1 b\<^sub>2 DO c) 
      (WHILE b\<^sub>1 DO c;; WHILE And b\<^sub>1 b\<^sub>2 DO c)"
  shows False
  using ex1_CE2 assms by simp 
  

definition Or :: "bexp \<Rightarrow> bexp \<Rightarrow> bexp" where
"Or a b = Not (And (Not a) (Not b))"


lemma While_condition_false_end: "(WHILE b DO c, s) \<Rightarrow> t \<Longrightarrow> ~bval b t"
proof(induction "WHILE b DO c" s t rule: big_step_induct)
   case WhileFalse thus ?case .
next
   case WhileTrue show ?case by fact
qed

lemma 
  "(WHILE Or b\<^sub>1 b\<^sub>2 DO c) \<sim> (WHILE Or b\<^sub>1 b\<^sub>2 DO c;; WHILE b\<^sub>1 DO c)"
   (is "equiv_c ?p\<^sub>1 ?p\<^sub>2")
proof (clarify, rule iffI)
  fix s t
  assume a: "(?p\<^sub>1, s) \<Rightarrow> t"
  have b: "~bval b\<^sub>1 t" using While_condition_false_end[OF a] unfolding Or_def by auto 
  show "(?p\<^sub>2, s) \<Rightarrow> t"  
    by (auto intro!: Seq simp add: a b)
next
  fix s t
  assume a: "(?p\<^sub>2, s) \<Rightarrow> t"
  then obtain s' where s: "(?p\<^sub>1, s) \<Rightarrow> s'" and s': "(WHILE b\<^sub>1 DO c, s') \<Rightarrow> t"  by auto
  have b: "~bval b\<^sub>1 s'" using While_condition_false_end[OF s] unfolding Or_def by auto 
  have "s' = t" using big_step_determ[OF WhileFalse[OF b]  s'] by auto
  show "(?p\<^sub>1, s) \<Rightarrow> t" using s unfolding `s' = t` .
qed

datatype
  com' = SKIP' 
      | Assign' vname aexp       ("_ :::= _" [1000, 61] 61)
      | Seq'    com'  com'         ("_;;;/ _"  [60, 61] 60)
      | If'     bexp com' com'     ("(IF' _/ THEN' _/ ELSE' _)"  [0, 0, 61] 61)
      | While'  bexp com'         ("(WHILE' _/ DO' _)"  [0, 61] 61)
      | Ass' bexp                 ("ASS' _")
      | Or' com' com'             ("_ \<parallel> _" 61)
      
inductive
  big_step' :: "com' \<times> state \<Rightarrow> state \<Rightarrow> bool" (infix "\<Rightarrow>g" 55)
where
Skip: "(SKIP',s) \<Rightarrow>g s" |
Assign: "(x :::= a,s) \<Rightarrow>g s(x := aval a s)" |
Seq: "\<lbrakk> (c\<^sub>1,s\<^sub>1) \<Rightarrow>g s\<^sub>2;  (c\<^sub>2,s\<^sub>2) \<Rightarrow>g s\<^sub>3 \<rbrakk> \<Longrightarrow> (c\<^sub>1;;;c\<^sub>2, s\<^sub>1) \<Rightarrow>g s\<^sub>3" |
IfTrue: "\<lbrakk> bval b s;  (c\<^sub>1,s) \<Rightarrow>g t \<rbrakk> \<Longrightarrow> (IF' b THEN' c\<^sub>1 ELSE' c\<^sub>2, s) \<Rightarrow>g t" |
IfFalse: "\<lbrakk> \<not>bval b s;  (c\<^sub>2,s) \<Rightarrow>g t \<rbrakk> \<Longrightarrow> (IF' b THEN' c\<^sub>1 ELSE' c\<^sub>2, s) \<Rightarrow>g t" |
WhileFalse: "\<not>bval b s \<Longrightarrow> (WHILE' b DO' c,s) \<Rightarrow>g s" |
WhileTrue: "\<lbrakk> bval b s\<^sub>1;  (c,s\<^sub>1) \<Rightarrow>g s\<^sub>2;  (WHILE' b DO' c, s\<^sub>2) \<Rightarrow>g s\<^sub>3 \<rbrakk> 
\<Longrightarrow> (WHILE' b DO' c, s\<^sub>1) \<Rightarrow>g s\<^sub>3" |
Ass: "bval b s \<Longrightarrow> (Ass' b, s) \<Rightarrow>g s" |
Or_l: "(c\<^sub>1, s) \<Rightarrow>g s' \<Longrightarrow> (c\<^sub>1 \<parallel> c\<^sub>2, s) \<Rightarrow>g s'" |
Or_r: "(c\<^sub>2, s) \<Rightarrow>g s' \<Longrightarrow> (c\<^sub>1 \<parallel> c\<^sub>2, s) \<Rightarrow>g s'"

declare big_step'.intros[intro]
abbreviation
  equiv_c :: "com' \<Rightarrow> com' \<Rightarrow> bool" (infix "\<sim>g" 50) where
  "c \<sim>g c' \<equiv> (\<forall>s t. (c,s) \<Rightarrow>g t  =  (c',s) \<Rightarrow>g t)"
  
lemma "c\<^sub>1 \<parallel> c\<^sub>2 \<sim>g c\<^sub>2 \<parallel> c\<^sub>1" (is "?p\<^sub>1 \<sim>g ?p\<^sub>2")
proof (clarify, rule iffI)
  fix s t
  assume "(?p\<^sub>1, s) \<Rightarrow>g t"
  thus "(?p\<^sub>2, s) \<Rightarrow>g t" by (cases rule: big_step'.cases) auto
next
  fix s t
  assume "(?p\<^sub>2, s) \<Rightarrow>g t"
  thus "(?p\<^sub>1, s) \<Rightarrow>g t" by (cases rule: big_step'.cases) auto
qed

thm big_step.induct
thm big_step'.induct[of "(ASS' b, s)"]
thm big_step'.induct
inductive_cases ass'E:"(ASS' b, s) \<Rightarrow>g t" 
inductive_cases or'E:"(c\<^sub>1 \<parallel> c\<^sub>2, s) \<Rightarrow>g t" 
inductive_cases seq'E:"(c\<^sub>1;;; c\<^sub>2, s) \<Rightarrow>g t" 
thm ass'E
lemma assume_condition_true[elim!]:
  "(ASS' b, s) \<Rightarrow>g t  \<Longrightarrow> bval b s \<and> s = t"
  by (auto elim: ass'E)


lemma
  "(IF' b THEN' c\<^sub>1 ELSE' c\<^sub>2) \<sim>g (ASS' b;;; c\<^sub>1) \<parallel> (ASS' (Not b);;; c\<^sub>2)"  (is "?p\<^sub>1 \<sim>g ?p\<^sub>2")
proof (clarify, rule iffI)  
  fix s t
  assume "(?p\<^sub>1, s) \<Rightarrow>g t"
  thus "(?p\<^sub>2, s) \<Rightarrow>g t" by (cases rule: big_step'.cases) fastforce+  
next
  fix s t
  assume a: "(?p\<^sub>2, s) \<Rightarrow>g t"
  thus "(?p\<^sub>1, s) \<Rightarrow>g t" 
    by (auto elim!: or'E seq'E dest!: assume_condition_true)
qed
  
  
  
  


section \<open>Homework 5.1\<close>
datatype gcom =
  Skip
| Ass vname aexp
| Sq gcom gcom
| IfBlock "(bexp \<times> gcom) list"

inductive
  big_stepg:: "gcom \<times> state \<Rightarrow> state \<Rightarrow> bool" (infix "\<Rightarrow>gg" 55) where
Skip[simp, intro]: "(Skip, s) \<Rightarrow>gg s" |
Ass[simp, intro]: "(Ass v exp, s) \<Rightarrow>gg (s(v:= aval exp s))" |
Sq[simp, intro]: "(\<sigma>\<^sub>1, s) \<Rightarrow>gg s' \<Longrightarrow> (\<sigma>\<^sub>2, s') \<Rightarrow>gg s'' \<Longrightarrow> (Sq \<sigma>\<^sub>1 \<sigma>\<^sub>2, s) \<Rightarrow>gg s''" |
IfBlock[simp, intro]: "bval b s \<Longrightarrow> (b, c) \<in> set G \<Longrightarrow> (c, s) \<Rightarrow>gg s' \<Longrightarrow> (IfBlock G, s) \<Rightarrow>gg s'" 

lemmas big_stepg_induct = big_stepg.induct[split_format(complete)]

inductive_cases SqE: "(Sq c\<^sub>1 c\<^sub>2, s) \<Rightarrow>gg t"
inductive_cases IfBlockE: "(IfBlock G\<^sub>s, s) \<Rightarrow>gg s'"
thm SqE IfBlockE[of G\<^sub>s s s']


lemma IfBlock_subset_big_step:
  assumes
    G\<^sub>s: "(IfBlock G\<^sub>s, s) \<Rightarrow>gg s'" and
    G\<^sub>s': "set G\<^sub>s \<subseteq> set G\<^sub>s'"
  shows "(IfBlock G\<^sub>s', s) \<Rightarrow>gg s'"
proof -
  obtain b prog where "bval b s" and "(b, prog) \<in> set G\<^sub>s" and "(prog, s) \<Rightarrow>gg s'"
    using G\<^sub>s by (rule IfBlockE)
  thus ?thesis using G\<^sub>s' by auto
qed

thm big_stepg_induct[of "IfBlock G\<^sub>s" s s' ]


schematic_lemma ex1: "(Sq (Ass ''x'' (N 5)) (Ass ''y'' (V ''x'')), s) \<Rightarrow>gg ?s'" (*<*)
apply (rule Sq)
 apply (rule Ass)
apply (rule Ass)
done
(*>*)text {* $\qquad\ldots$ *}
thm ex1[simplified]

schematic_lemma ex2: "(IfBlock [(Less (N 4) (N 5), Ass ''x'' (N 2))], s) \<Rightarrow>gg ?s'" (*<*)
apply (rule IfBlock)
  prefer 2 apply simp
 apply simp
apply (rule Ass)
done

fun big_stepgf:: "gcom \<Rightarrow> state \<Rightarrow> state option" where
"big_stepgf (Ass v exp) s = Some (s(v := aval exp s))" |
"big_stepgf Skip s = Some s" |
"big_stepgf (IfBlock []) s = None"|
"big_stepgf (IfBlock ((b, prog) # G)) s =
  big_stepgf (if bval b s then prog else (IfBlock G)) s" |
"big_stepgf (Sq c c') s =
  (case (big_stepgf c s) of
    None \<Rightarrow> None
  | Some s' \<Rightarrow> big_stepgf c' s')"

lemma big_stepgf_IfBlock_no_pair[simp]:
  "big_stepgf (IfBlock (b # G)) s =
  big_stepgf (if bval (fst b) s then snd b else (IfBlock G)) s"
 by (cases b) auto

lemmas big_stepf_induct = big_stepgf.induct[case_names Ass Skip IfNil IfCons Sq]

lemma big_stepgf_Some_imp_ex_inter:
  "big_stepgf (Sq c c') s = Some s'' \<Longrightarrow> 
    \<exists>s'. big_stepgf c s = Some s' \<and> big_stepgf c' s' = Some s''"
  by (auto split: option.splits)


lemma big_stepgf_sound:
  "big_stepgf c s = Some s' \<Longrightarrow> (c, s) \<Rightarrow>gg s'"
proof (induction c s arbitrary: s' rule: big_stepf_induct)
  case Ass thus ?case by auto
next
  case Skip thus ?case by auto
next
  case (Sq c c' s)
  then obtain sa where sa: "big_stepgf c s = Some sa" and s': "big_stepgf c' sa = Some s'"
        using big_stepgf_Some_imp_ex_inter[OF Sq(3)] by fast
  thus ?case using Sq(1,2)[OF sa] by blast
next
  case (IfNil) thus ?case by auto
next
  case (IfCons b c G s)
  thus ?case by (auto split: split_if_asm) (metis IfBlock_subset_big_step set_subset_Cons) 
qed


section \<open>Homework 5.2\<close>

abbreviation equiv_cg :: "gcom \<Rightarrow> gcom \<Rightarrow> bool" (infix "~g" 50) where
"p ~g p' \<equiv> (\<forall>s s'. ((p, s) \<Rightarrow>gg s' \<longleftrightarrow> (p', s) \<Rightarrow>gg s'))"

lemma reflp_equiv_cf: "reflp (op ~g)"
 unfolding reflp_def by auto

lemma sym_equiv_cf: "symp (op ~g)"
 unfolding symp_def by auto

lemma transp_equiv_cf: "transp (op ~g)"
 unfolding transp_def by auto

lemma Sq_cong:
  assumes 
    c1: "c1 ~g c1'" and
    c2: "c2 ~g c2'"
  shows "Sq c1 c2 ~g Sq c1' c2'"
proof (clarify, rule iffI)
  fix s s'
  assume "(Sq c1 c2, s) \<Rightarrow>gg s'"
  then obtain t where "(c1, s) \<Rightarrow>gg t" and "(c2, t) \<Rightarrow>gg s'" by (metis (poly_guards_query) SqE)
  hence "(c1', s) \<Rightarrow>gg t" and "(c2', t) \<Rightarrow>gg s'" using c1 c2 by auto
  thus "(Sq c1' c2', s) \<Rightarrow>gg s'" by auto
next
  fix s s'
  assume "(Sq c1' c2', s) \<Rightarrow>gg s'"
  then obtain t where "(c1', s) \<Rightarrow>gg t" and "(c2', t) \<Rightarrow>gg s'" by (metis (poly_guards_query) SqE)
  hence "(c1, s) \<Rightarrow>gg t" and "(c2, t) \<Rightarrow>gg s'" using c1 c2 by auto
  thus "(Sq c1 c2, s) \<Rightarrow>gg s'" by auto
qed



section \<open>5.3\<close>

inductive_cases ifBlockE: "(IfBlock (zip bs cs), s) \<Rightarrow>gg s'"
thm ifBlockE

lemma IfBlock_cong_imp:
  assumes length: "length bs = n" "length cs = n" "length cs' = n"
  and alli: "\<And>i. i < n \<Longrightarrow> cs ! i ~g cs' ! i"
  and IfB: "(IfBlock (zip bs cs), s) \<Rightarrow>gg s'"
  shows "(IfBlock (zip bs cs'), s) \<Rightarrow>gg s'"
proof -
  obtain b c where b: "bval b s" and bc: "(b, c) \<in> set (zip bs cs)" and c: "(c, s) \<Rightarrow>gg s'" using IfB by cases
  obtain i where i: "cs ! i = c" and b: "bs ! i = b" and  "i < n" by (metis bc in_set_zip length(2) prod.sel(1) snd_conv)
  have "(cs' ! i, s) \<Rightarrow>gg s'" using c alli[OF `i < n`] unfolding i[symmetric] by auto

  have bcs': "(b, cs' ! i) \<in> set (zip bs cs')"
    using  `i < n` b length  in_set_zip by force
  show ?thesis using IfBlock[OF `bval b s` bcs' `(cs' ! i, s) \<Rightarrow>gg s'`] . 
qed

  
lemma
  fixes cs :: "gcom list"
  assumes "length bs = n" "length cs = n" "length cs' = n"
  and alli: "\<And>i. i < n \<Longrightarrow> cs ! i ~g cs' ! i"
  shows "IfBlock (zip bs cs) ~g IfBlock (zip bs cs')"
  using IfBlock_cong_imp assms by meson

lemma IfBlock_set_eq_cong:
  assumes set: "set Gs = set Gs'"
  assumes IfB: "(IfBlock Gs, s) \<Rightarrow>gg s'"
  shows "(IfBlock Gs) ~g (IfBlock Gs')"
  using IfBlock_subset_big_step set by blast

lemma IfBlock_set_eq_cong_imp:
  assumes set: "set Gs = set Gs'"
  assumes IfB: "(IfBlock Gs, s) \<Rightarrow>gg s'"
  shows "(IfBlock Gs', s) \<Rightarrow>gg s'"
proof -
  obtain b c where b: "bval b s" and bc: "(b, c) \<in> set Gs" and c: "(c, s) \<Rightarrow>gg s'" using IfB by cases  
  have bc': "(b, c) \<in> set Gs'" using bc set by blast
  show ?thesis  using IfBlock[OF b bc' c] .
qed  



lemma IfBlock_set_eq_cong':
  assumes set: "set Gs = set Gs'"
  shows "(IfBlock Gs) ~g (IfBlock Gs')" 
  using assms IfBlock_set_eq_cong_imp by metis

lemma IfBlock_cong: (* v1 *)
  assumes allc: "\<forall> (b, (c, c')) \<in> set GGs. c ~g c'"
  shows "IfBlock (map (\<lambda> (b, (c, _)). (b, c)) GGs) ~g IfBlock (map (\<lambda> (b, (_, c')). (b, c')) GGs)"
proof (rule ; rule ; rule)
  fix s s'
  assume "(IfBlock (map (\<lambda>(b, c, _). (b, c)) GGs), s) \<Rightarrow>gg s'"
  from this obtain b and c where in1: "(b, c) \<in> set (map (\<lambda>(b, c, _). (b, c)) GGs)" and btrue: "bval b s" and comp1: "(c, s) \<Rightarrow>gg s'" using IfBlockE by blast
  from in1 allc obtain c' where cong: "c ~g c'" and in2: "(b, c') \<in> set (map (\<lambda> (b, (_, c')). (b, c')) GGs)" by force
  from cong comp1 have comp2: "(c', s) \<Rightarrow>gg s'" by auto
  from in2 btrue comp2 show "(IfBlock (map (\<lambda>(b, _, c'). (b, c')) GGs), s) \<Rightarrow>gg s'" by blast
next
  fix s s'
  assume "(IfBlock (map (\<lambda> (b, (_, c')). (b, c')) GGs), s) \<Rightarrow>gg s'"
  from this obtain b and c' where in2: "(b, c') \<in> set (map (\<lambda> (b, (_, c')). (b, c')) GGs)" and btrue: "bval b s" and comp2: "(c', s) \<Rightarrow>gg s'" using IfBlockE by blast
  have "\<forall> (b, (c, c')) \<in> set GGs. c' ~g c" using allc by auto
  with in2 obtain c where cong: "c ~g c'" and in1: "(b, c) \<in> set (map (\<lambda>(b, c, _). (b, c)) GGs)"
   using image_iff by fastforce

  from cong comp2 have comp1: "(c, s) \<Rightarrow>gg s'" by auto
  from in1 btrue comp1 show "(IfBlock (map (\<lambda>(b, c, _). (b, c)) GGs), s) \<Rightarrow>gg s'" by blast
qed

end
