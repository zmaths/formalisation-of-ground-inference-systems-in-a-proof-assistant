theory Ex07
imports Main (*"~~/src/HOL/IMP/Types"  "~~/src/HOL/IMP/Compiler" *) "~~/src/HOL/IMP/Star" "~~/src/HOL/IMP/AExp"
begin

declare [[syntax_ambiguity_warning = false]]

(*
fun atype :: "tyenv \<Rightarrow> Types.aexp \<Rightarrow> ty option" where
"atype _ (Ic _) = Some Ity" |
"atype _ (Rc _) = Some Rty" |
"atype \<Gamma> (Types.V x) = Some (\<Gamma> x)" |
"atype \<Gamma> (Types.Plus a b) = 
  (case (atype \<Gamma> a, atype \<Gamma> b) of
    (Some c, Some d) => (if c = d then Some c else None)
  | _ => None)"

lemma atyping_atype:
  "\<Gamma> \<turnstile> a : \<tau> \<longleftrightarrow> atype \<Gamma> a = Some \<tau>"
by (induction a) (auto split: option.split)


fun btype :: "tyenv \<Rightarrow> Types.bexp \<Rightarrow> bool" where
"btype _ (Types.Bc _) \<longleftrightarrow> True" |
"btype \<Gamma> (Types.Not b) \<longleftrightarrow> btype \<Gamma> b" |
"btype \<Gamma> (Types.And b c) = (btype \<Gamma> b \<and> btype \<Gamma> c)" |
"btype \<Gamma> (Types.Less a b) = 
  (case (atype \<Gamma> a, atype \<Gamma> b) of
   (Some c, Some d) => c = d
  | _ => False)"


lemma btyping_bok:
  "\<Gamma> \<turnstile> b \<longleftrightarrow> btype \<Gamma> b"
by (induction b)  (auto simp add: atyping_atype split: option.split)

fun cok :: "tyenv \<Rightarrow> Types.com \<Rightarrow> bool" where
"cok _ (Types.SKIP) \<longleftrightarrow> True" |
"cok \<Gamma> (x ::= a) \<longleftrightarrow> atype \<Gamma> a = Some (\<Gamma> x)" |
"cok \<Gamma> (c\<^sub>1 ;; c\<^sub>2) \<longleftrightarrow> cok \<Gamma> c\<^sub>1 \<and> cok \<Gamma> c\<^sub>2"  |
"cok \<Gamma> (WHILE b DO c) \<longleftrightarrow> btype \<Gamma> b \<and> cok \<Gamma> c"  |
"cok \<Gamma> (IF b THEN c ELSE d) \<longleftrightarrow> btype \<Gamma> b \<and> cok \<Gamma> c \<and> cok \<Gamma> d"


lemma ctyping_cok:
  "\<Gamma> \<turnstile> c \<longleftrightarrow> cok \<Gamma> c"
by (induction c)  (auto simp add: btyping_bok atyping_atype split: option.split)


value "ccomp (IF Less (V ''x'') (N 3) THEN ''y'' ::= N 3 ELSE SKIP)"
(*"[LOAD ''x'', LOADI 3, JMPGE 3, LOADI 3, STORE ''y'', JMP 0]"*)


fun ccomp :: "com \<Rightarrow> instr list" where
"ccomp SKIP = []" |
"ccomp (x ::= a) = acomp a @ [STORE x]" |
"ccomp (c\<^sub>1;;c\<^sub>2) = ccomp c\<^sub>1 @ ccomp c\<^sub>2" |
"ccomp (IF b THEN c\<^sub>1 ELSE SKIP) =
  (let cc\<^sub>1 = ccomp c\<^sub>1; cb = bcomp b False (size cc\<^sub>1 )
   in cb @ cc\<^sub>1)" |
"ccomp (IF b THEN c\<^sub>1 ELSE c\<^sub>2) =
  (let cc\<^sub>1 = ccomp c\<^sub>1; cc\<^sub>2 = ccomp c\<^sub>2; cb = bcomp b False (size cc\<^sub>1 + (if c\<^sub>2 = SKIP then 0 else 1))
   in cb @ cc\<^sub>1 @ (if c\<^sub>2 \<noteq> SKIP then [JMP (size cc\<^sub>2)] else []) @ cc\<^sub>2)" |
"ccomp (WHILE b DO c) =
 (let cc = ccomp c; cb = bcomp b False (size cc + 1)
  in cb @ cc @ [JMP (-(size cb + size cc + 1))])"

value "ccomp (IF Less (V ''x'') (N 3) THEN ''y'' ::= N 3 ELSE SKIP)"

lemma ccomp_bigstep:
  "(c,s) \<Rightarrow> t \<Longrightarrow> ccomp c \<turnstile> (0,s,stk) \<rightarrow>* (size(ccomp c),t,stk)"
proof(induction arbitrary: stk rule: big_step_induct)
  case (Assign x a s)
  show ?case by (fastforce simp:fun_upd_def)
next
  case (Seq c1 s1 s2 c2 s3)
  let ?cc1 = "ccomp c1"  let ?cc2 = "ccomp c2"
  have "?cc1 @ ?cc2 \<turnstile> (0,s1,stk) \<rightarrow>* (size ?cc1,s2,stk)"
    using Seq.IH(1) by fastforce
  moreover
  have "?cc1 @ ?cc2 \<turnstile> (size ?cc1,s2,stk) \<rightarrow>* (size(?cc1 @ ?cc2),s3,stk)"
    using Seq.IH(2) by fastforce
  ultimately show ?case by simp (blast intro: star_trans)
next
  case (WhileTrue b s1 c s2 s3)
  let ?cc = "ccomp c"
  let ?cb = "bcomp b False (size ?cc + 1)"
  let ?cw = "ccomp(WHILE b DO c)"
  have "?cw \<turnstile> (0,s1,stk) \<rightarrow>* (size ?cb,s1,stk)"
    using `bval b s1` by fastforce
  moreover
  have "?cw \<turnstile> (size ?cb,s1,stk) \<rightarrow>* (size ?cb + size ?cc,s2,stk)"
    using WhileTrue.IH(1) by fastforce
  moreover
  have "?cw \<turnstile> (size ?cb + size ?cc,s2,stk) \<rightarrow>* (0,s2,stk)"
    by fastforce
  moreover
  have "?cw \<turnstile> (0,s2,stk) \<rightarrow>* (size ?cw,s3,stk)" by (rule WhileTrue.IH(2))
  ultimately show ?case by(blast intro: star_trans)
next
  case (IfTrue b s c\<^sub>1 t c\<^sub>2)
  thus ?case by (cases c\<^sub>2) fastforce+
next
  case (IfFalse b s c\<^sub>1 t c\<^sub>2)
  thus ?case 
    apply (cases c\<^sub>1) 
         prefer 5 apply force 
       apply fastforce+ 
    done
qed (fastforce)+


*)
declare [[syntax_ambiguity_warning = false]]

datatype pcom =
  Skp ("SKIP")
| Asn vname aexp (infix "::=" 70)
| Atm_Asn vname aexp (infix "::=!" 70)
| Seq pcom pcom (infixr";" 20)
| Par pcom pcom (infix "\<parallel>" 30)


inductive small_step_aexp :: "state \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>a _" 10) where
V: "s \<turnstile> V x \<rightarrow>a N (s x) " |
Plus_l: "s \<turnstile> a\<^sub>1 \<rightarrow>a a\<^sub>1'  \<Longrightarrow> s \<turnstile> Plus a\<^sub>1 a\<^sub>2 \<rightarrow>a Plus a\<^sub>1' a\<^sub>2 " |
Plus_r: "s \<turnstile> a\<^sub>2 \<rightarrow>a a\<^sub>2'  \<Longrightarrow> s \<turnstile> Plus a\<^sub>1 a\<^sub>2 \<rightarrow>a Plus a\<^sub>1 a\<^sub>2'" |
PlusN: "s \<turnstile> Plus (N i) (N j) \<rightarrow>a N (i+j)"

declare small_step_aexp.intros[intro]
abbreviation small_steps_aexp:: "state \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>*a _" 10) where
"s \<turnstile> a \<rightarrow>*a b \<equiv> star (small_step_aexp s) a b"

inductive small_step :: "pcom \<times> state \<Rightarrow> pcom \<times> state \<Rightarrow> bool" ("_ \<rightarrow>p _" 10) where
AsnCont:"s \<turnstile> a \<rightarrow>a b \<Longrightarrow> (x ::= a, s) \<rightarrow>p (x ::= b, s)" |
AsnN: "(x ::= N i, s) \<rightarrow>p (SKIP, s(x := i))" |
Atm_Asn: "s \<turnstile> a \<rightarrow>*a N i \<Longrightarrow>(x ::=! a, s) \<rightarrow>p (SKIP, s(x := i))" |
SeqCont: "(a,s)\<rightarrow>p (a', s') \<Longrightarrow> (a;b, s) \<rightarrow>p (a'; b, s')" |
SeqS: "(SKIP;b, s) \<rightarrow>p (b, s)" |
Par_l: "(a,s)\<rightarrow>p (a', s') \<Longrightarrow> (a\<parallel>b, s) \<rightarrow>p (a'\<parallel> b, s')" |
Par_r: "(b,s)\<rightarrow>p (b', s') \<Longrightarrow> (a\<parallel>b, s) \<rightarrow>p (a\<parallel> b', s')" |
ParS: "(SKIP\<parallel>b, s) \<rightarrow>p (b', s')"

declare small_step.intros[intro]
lemmas small_step_induct = small_step.induct[split_format(complete)]

abbreviation small_steps:: "pcom\<times>state \<Rightarrow> pcom \<times> state \<Rightarrow>bool" (" _ \<rightarrow>*p _" 40) where
"a \<rightarrow>*p b \<equiv> star small_step a b"


value " (''x'' ::=! Plus (V ''x'') (V ''y''))"
value "A; (C; D)"
abbreviation
" p1 ==
  ''x'' ::= N 0;
  ''y''::= N 1;
  (''x'' ::=! Plus (V ''x'') (V ''y''); ''x'' ::=! Plus (V ''x'') (V ''y''))
  \<parallel>
    (''x'' ::= N 5; ''y'' ::= N 100)
   "
   
   
lemma ex1: "(p1, s) \<rightarrow>*p (SKIP, s(''y'' := 1, ''x'' := 2, ''x'' := 5, ''y'' := 100))"
apply (rule star.intros)
 apply (rule SeqCont)
 apply (rule AsnN)
 
apply (rule star.intros)
 apply (rule SeqS) 
 
apply (rule star.intros)
 apply (rule SeqCont)
 apply (rule AsnN)
 
apply (rule star.intros)
 apply (rule SeqS) 
 
apply (rule star.intros)
 apply (rule Par_l)
 apply (rule SeqCont)
 
 apply (rule Atm_Asn)
  apply (rule star.intros)
  apply (rule Plus_l, blast)

  apply (rule star.intros) 
  apply (rule Plus_r, blast)
  
 apply (rule star_step1, blast)

apply (rule star.intros)
 apply (rule Par_l)
 apply (rule SeqS)

apply (rule star.intros)
 apply (rule Par_l)
 apply (rule Atm_Asn)
  apply (rule star.intros)
  apply (rule Plus_l, blast)

  apply (rule star.intros) 
  apply (rule Plus_r, blast)
  
 apply (rule star_step1, blast)
 apply simp
by blast 

thm ex1[simplified]

value "(''x'' ::= N 0; ''y'' ::= N 0; ''x'' ::= Plus (V ''x'') (N 1) \<parallel> ''y'' ::= Plus (V ''y'') (N 1), <>)"


value "(''x'' ::= N 0; ''y'' ::= N 0; ''x'' ::= Plus (V ''y'') (N 1) \<parallel> ''y'' ::= Plus (V ''y'') (N 1), s)"


value "(''x'' ::= N 0; ''y'' ::= N 0; ''x'' ::= Plus (V ''y'') (N 1) \<parallel> ''y'' ::= Plus (V ''x'') (N 1), s)"

fun all_assigned_vars :: "pcom \<Rightarrow> vname set" where
"all_assigned_vars Skp = {}" |
"all_assigned_vars (v ::=! _) = {v}" |
"all_assigned_vars (v ::= _) = {v}" |
"all_assigned_vars (a; b) = all_assigned_vars a \<union> all_assigned_vars b" |
"all_assigned_vars (a \<parallel> b) = all_assigned_vars a \<union> all_assigned_vars b"

fun avars :: "aexp \<Rightarrow> vname set" where
"avars (N _) = {}" |
"avars (V v) = {v}"|
"avars (Plus a b) = avars a \<union> avars b"


fun all_read_vars:: "pcom \<Rightarrow> vname set" where
"all_read_vars SKIP = {}" |
"all_read_vars (a ; b) = all_read_vars a \<union> all_read_vars b" |
"all_read_vars (a ::= b) = {a} \<union> avars b" |
"all_read_vars (a ::=! b) = {a} \<union> avars b" |
"all_read_vars (a \<parallel> b) = all_read_vars a \<union> all_read_vars b"

fun can_be_replaced where
"can_be_replaced x vars read_vars assigned_vars \<longleftrightarrow> (card (vars \<inter> assigned_vars) \<le> 1 \<and> x \<notin> read_vars) \<or> card (vars \<inter> assigned_vars) = 0"



primrec unatom_par:: "pcom \<Rightarrow> vname set \<Rightarrow> vname set \<Rightarrow> pcom" where
"unatom_par SKIP _ _ = SKIP" |
"unatom_par (a ; b) assigned read = (unatom_par a assigned read; unatom_par b assigned read)" |
"unatom_par (x ::=! b) assigned read = 
  (if can_be_replaced x (avars b) read assigned then x ::= b else x ::=! b)" |
"unatom_par (x ::= b) _ _ = (x ::= b)" |
"unatom_par (a \<parallel> b) assigned read = 
  (unatom_par a (assigned \<union> all_assigned_vars b) (read \<union> all_read_vars b) 
    \<parallel>
  unatom_par b (assigned \<union> all_assigned_vars a) (read \<union> all_read_vars a))"
  
abbreviation "unatom p \<equiv> unatom_par p {} {}"   

inductive_cases Atm_AsnE: "(x1 ::=! x2, s) \<rightarrow>*p (SKIP, s')"
thm Atm_AsnE

inductive_cases starE: "z \<rightarrow>*p y"
thm starE

inductive_cases Atm_AsnE': "(x ::=! a, s) \<rightarrow>p cs'"
thm Atm_AsnE'

inductive_cases AsnE: "(x ::= a, s) \<rightarrow>p cs'"
thm AsnE

inductive_cases SkpE: "(SKIP, s) \<rightarrow>p (a, b)"

lemma SkpEs:"(SKIP, s) \<rightarrow>*p (SKIP, s') \<Longrightarrow> s' = s"
  by (induction "(SKIP, s)" "(SKIP, s')" rule: star.induct) (auto elim: SkpE)

lemma Atm_Asn_star[simp]: "((x ::=! a, s) \<rightarrow>*p (SKIP, s')) \<longleftrightarrow> ((x ::=! a, s) \<rightarrow>p (SKIP, s')) " (is "?A \<longleftrightarrow> ?B")
proof
  assume "?B"
  thus ?A by auto
next
  assume ?A
  thus ?B
    apply (cases)
    apply (auto elim!: Atm_AsnE' SkpEs)
    using SkpEs by blast
qed
inductive_cases SeqE: "(p\<^sub>1 ; p\<^sub>2, b) \<rightarrow>p (aa, ba)"
thm SeqE

lemma Seq_star_decomp: 
  "(p\<^sub>1; p\<^sub>2, s) \<rightarrow>*p (SKIP, s'') \<Longrightarrow> (\<exists>s'. (p\<^sub>1, s) \<rightarrow>*p (SKIP, s') \<and> (p\<^sub>2, s') \<rightarrow>*p (SKIP, s''))"
proof (induction "p\<^sub>1; p\<^sub>2" s SKIP s'' arbitrary: p\<^sub>1 p\<^sub>2 rule: star_induct)
  case (step s p' s' s'')
  show ?case using step(1,2)
    by (auto elim!: SeqE)  (meson star.step step.hyps(3))
qed

lemma Asn_Atm_Asn:
  "(x ::= a, s) \<rightarrow>*p (SKIP, s') \<Longrightarrow> (x ::=! a, s) \<rightarrow>p (SKIP, s')"
proof (induction "x ::= a" s SKIP s' arbitrary: x a rule: star_induct)
  case (step s p' s' s'')
  show ?case using step
    apply (auto elim!: AsnE)
     apply (metis Atm_Asn Atm_AsnE' star.step)
    using SkpEs by blast
qed

lemma Seq_trans_comp:
  assumes "(p\<^sub>1, s) \<rightarrow>*p (SKIP, t)" and "(p\<^sub>2, t) \<rightarrow>*p (SKIP, s')"
  shows "(p\<^sub>1; p\<^sub>2, s) \<rightarrow>*p (SKIP, s')"
  using assms by (induction p\<^sub>1 s SKIP t rule: star_induct) (meson SeqS SeqCont star.step)+

lemma Atm_Asn_Atm:
  "s \<turnstile> a \<rightarrow>*a a' \<Longrightarrow> a' = N i \<Longrightarrow> (x ::= a, s) \<rightarrow>*p (SKIP, s(x := i))"
 by (induction rule: star.induct, auto) (meson AsnCont star_step1 star_trans)

theorem "(p, s) \<rightarrow>*p (SKIP, s') \<Longrightarrow> (unatom_par p ass read, s) \<rightarrow>*p (SKIP, s') "
proof (induction p arbitrary: s s' ass read)
  case Skp
  thus ?case by simp
next
  case (Atm_Asn x a)
  thus ?case by (auto elim: Atm_AsnE' intro: Atm_Asn_Atm)
next
  case (Asn x a)
  thus ?case by simp
next
  case (Seq p1 p2)
  obtain t where "(p1, s) \<rightarrow>*p (SKIP, t)" 
  and "(p2, t) \<rightarrow>*p (SKIP, s')"
    using Seq_star_decomp[OF Seq(3)[simplified]] by auto
  hence "(unatom_par p1 ass read, s) \<rightarrow>*p (SKIP, t)" 
  and "(unatom_par p2 ass read, t) \<rightarrow>*p (SKIP, s')"
    using Seq.IH by blast+
  thus ?case 
    using Seq_trans_comp by auto
next
  case (Par p1 p2)
  show ?case using Par(3) apply auto sorry
  
qed    

theorem "(unatom_par p ass read, s) \<rightarrow>*p (SKIP, s') \<Longrightarrow> (p, s) \<rightarrow>*p (SKIP, s')"
proof (induction p arbitrary: s s' ass read)
  case Skp
  thus ?case by simp
next
  case (Atm_Asn x a)
  thus ?case by (auto simp add: Asn_Atm_Asn split: split_if_asm)
next
  case (Asn x a)
  thus ?case by simp
next
  case (Seq p1 p2)
  obtain t where "(unatom_par p1 ass read, s) \<rightarrow>*p (SKIP, t)" 
  and "(unatom_par p2 ass read, t) \<rightarrow>*p (SKIP, s')"
    using Seq_star_decomp[OF Seq(3)[simplified]] by auto
  hence "(p1, s) \<rightarrow>*p (SKIP, t)" and "(p2, t) \<rightarrow>*p (SKIP, s')" using Seq.IH by blast+
  thus ?case by (fast intro: Seq_trans_comp)
next
  case (Par p1 p2)
  show ?case using Par(3) apply auto sorry
  
qed    
*)
  end