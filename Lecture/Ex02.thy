theory Ex02
imports "~~/src/HOL/IMP/AExp"
begin

section \<open>Ex 2.1\<close>
fun subst :: "vname \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
"subst x a' (N n) = N n" |
"subst x a' (V y) = (if x = y then a' else V y)" |
"subst x a' (Plus a b) = Plus (subst x a' a) (subst x a' b)"

lemma subst_lemma:
  "aval (subst x a' a) s = aval a (s (x := aval a' s))"
  by (induct a, auto)

lemma comp:
  "aval a1 s = aval a2 s \<Longrightarrow> aval (subst x a1 a) s = aval (subst x a2 a) s"
 by (simp add: subst_lemma)

section \<open>Ex 2.2\<close>
datatype aexp' = N' int | V' vname | Plus' aexp' aexp' | Div' aexp' aexp' | Incr vname

fun aval' :: "aexp' \<Rightarrow> state \<Rightarrow> (val \<times> state) option" where
"aval' (N' n) s = Some (n, s)" |
"aval' (V' x) s = Some (s x, s)" |
"aval' (Incr x) s = Some (s x, s(x := s x + 1))" |
"aval' (Plus' a\<^sub>1 a\<^sub>2) s = 
  (case aval' a\<^sub>1 s of
    Some (v\<^sub>1, s\<^sub>1) \<Rightarrow>
      (case aval' a\<^sub>2 s\<^sub>1 of
        Some (v\<^sub>2, s\<^sub>2) \<Rightarrow> Some (v\<^sub>1 + v\<^sub>2, s\<^sub>2)
      | _ \<Rightarrow> None)
  | None \<Rightarrow> None)" |
"aval' (Div' a\<^sub>1 a\<^sub>2) s = 
  (case aval' a\<^sub>1 s of
    Some (v\<^sub>1, s\<^sub>1) \<Rightarrow>
      (case aval' a\<^sub>2 s\<^sub>1 of
        Some (v\<^sub>2, s\<^sub>2) \<Rightarrow> if v\<^sub>2 = 0 then None else Some (v\<^sub>1 div v\<^sub>2, s\<^sub>2)
      | None \<Rightarrow> None)
  | None \<Rightarrow> None)"

lemma "aval' a s = Some (v, s') \<Longrightarrow> s x \<le> s' x"
   apply (induct a arbitrary: s s' v) apply auto
   by (fastforce split: option.splits split_if_asm)+

section \<open>Ex 2.3\<close>
fun vars :: "aexp \<Rightarrow> vname set" where
"vars (N _) = {}" |
"vars (V x) = {x}" |
"vars (Plus a b) = vars a \<union> vars b"

lemma ndep: "x \<notin> vars e \<Longrightarrow> aval e (s(x := v)) = aval e s"
  by (induct e, auto)

end