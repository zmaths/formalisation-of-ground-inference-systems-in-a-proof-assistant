theory Ex01
imports Main
begin

section {*Ex 1.1*}
value "2 + (2::nat)"
value "(2::nat) * (5 + 3)"
value "(3::nat) * 4 - 2 * (7 + 1)"

section {*Ex 1.2*}
primrec plus :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
"plus 0 n = n" |
"plus (Suc m) n = Suc (plus m n)"

lemma [simp]: "plus m 0 = m" by (induct m, auto)
lemma [simp]: "plus m (Suc n) = Suc (plus m n)" by (induct m, auto)

lemma "plus m n = plus n m"
  apply (induct n) by auto

lemma "plus a (plus b c) = plus (plus a b) c"
  apply (induct a) by auto

primrec count :: "'a list \<Rightarrow> 'a \<Rightarrow> nat" where
"count [] _ = 0" |
"count (a # l) b = (if a = b then 1 else 0) + count l b"

lemma "count xs a \<le> length xs"
  apply (induct xs) by auto

section {*Ex 1.4*}
fun snoc where
"snoc [] m = m # []" |
"snoc (a # b) m = a # snoc b m"

fun reverse where
"reverse [] = []" |
"reverse (a # l) = snoc (reverse l) a"

lemma reverse_snoc:  "reverse (snoc l a) = a # reverse l"
  by (induct l, auto)

lemma "reverse (reverse l) = l"
apply (induct l, auto simp add: reverse_snoc)
done

section {*Homework 1*}
fun pow2 :: "nat \<Rightarrow> nat" where
"pow2 0 = 1" |
"pow2 (Suc n) = 2 * pow2 n"

lemma "pow2 1 = 2"
  by auto

text {* We will prove @{term "pow2 (n + m) = pow2 n * pow2 m"} by induction on n.

First we want to prove that @{term "pow2 (0 + m) = pow2 0 * pow2 m"} then as @{term "0 + m = m"} and @{term "pow2 0 = 1"}, we have the result.

Now IH: @{term "pow2 (n + m) = pow2 n * pow2 m"}
We want to show @{term "pow2 (Suc n + m) = pow2 (Suc n) * pow2 m"} 
We have @{term "pow2 (Suc n + m) = pow2 (Suc (n + m))"}, thus by definition of  @{term "pow2"}, @{term "pow2 (Suc n + m) = 2 * pow2 n * pow2 m"}. As @{term "pow2 (Suc n) = 2 * pow2 n"}, we get the result.
*}
lemma "pow2 (n + m) = pow2 n * pow2 m" by (induct m, auto)

section {*Homework 2*}
primrec double :: "'a list \<Rightarrow> 'a list" where
"double [] = []" |
"double (a # l) = a # a # double l"

lemma [simp]: "double (a @ b) = double a @ double b"
   by (induct a, auto)

lemma "rev (double xs) = double (rev xs)"
   by (induct xs, auto)

section {*Homework 1'*}

fun sum where 
"sum 0 = 0" |
"sum (Suc m) = Suc m + sum m"

lemma "sum n = n * (n+1) div 2"
apply (induct n, auto)
done

fun sum_sq :: "nat \<Rightarrow> nat" where 
"sum_sq 0 = 0" |
"sum_sq (Suc m) = Suc m * Suc m + sum_sq m"

lemma "a * b + a * c = a * (b + (c::nat))"
by (simp add: add_mult_distrib2)
  
lemma "sum_sq n = n * (n+1) * (2*n + 1) div 6"
proof (induct n)
  case 0
  thus ?case by simp
next
  case (Suc n)
  let ?m = "Suc n"
  have "sum_sq ?m = ?m * ?m + sum_sq n" by auto 
  also have "\<dots> = ?m * ?m +  n * (n + 1) * (2 * n + 1) div 6" unfolding Suc.hyps by simp
  also have 1: "(?m * ?m) = 6 * ((n + 1) * (n + 1)) div 6" by auto
  have "?m * ?m +  n * (n + 1) * (2 * n + 1) div 6 = (6 * (n + 1) * (n + 1)) div 6 + (n + 1)* n * (2 * n + 1) div 6" unfolding 1 by (metis (mono_tags, lifting) mult.commute mult.left_commute)
  also have "\<dots> = (6 * (n + 1) * (n + 1) + (n + 1)* n * (2 * n + 1)) div 6" by (metis (no_types, lifting) "1" Suc_eq_plus1 ab_semigroup_mult_class.mult_ac(1) div_mult_self4 zero_neq_numeral)
  also have "\<dots> = ((n + 1) * ((n + 1) * 6) + (n + 1)* (n * (2 * n + 1))) div 6" by linarith
  also have "\<dots> = ((n + 1) * ((n + 1) * 6 + (n * (2 * n + 1)))) div 6" using add_mult_distrib2[of "n+1"] by presburger
  also 
    have 1: "(n + 1) * 6 + (n * (2 * n + 1)) = (Suc n + 1) * (2 * Suc n + 1)" by auto
    have "((n + 1) * ((n + 1) * 6 + (n * (2 * n + 1)))) div 6 = ((n + 1) * (Suc n + 1) * (2 * Suc n + 1)) div 6" unfolding 1 by linarith
  finally show ?case by simp
qed

lemma "6 * sum_sq n = n * (n+1) * (2*n + 1)"
  apply (induct n, auto)
  using add_mult_distrib by auto

datatype tree= Leaf | Node tree nat tree
fun mirror where
"mirror Leaf = Leaf" |
"mirror (Node ag n ad) = Node (mirror ag) n (mirror ad)"

lemma "mirror (mirror a) = a"
  apply (induct a)
  apply auto
  done

fun doublei where
"doublei (Leaf) = Leaf" |
"doublei (Node Leaf a Leaf) = Leaf"

fun pal where
"pal 0 = []" |
"pal (Suc n) = Suc n # pal n @ [Suc n]"

lemma "rev (pal n) = pal n"
  by (induct n, auto)

end