theory tmpl02_aexp
  imports Main
begin
 section \<open>Homework 2.2\<close>
type_synonym vname = string
type_synonym state = "string \<Rightarrow> int"
type_synonym val = "int"

declare algebra_simps[simp]

datatype aexp = N int | V vname | Plus aexp aexp | Mult int aexp

fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val" where
"aval (N n) s = n" |
"aval (V x) s = s x" |
"aval (Plus a\<^sub>1 a\<^sub>2) s = aval a\<^sub>1 s + aval a\<^sub>2 s" |
"aval (Mult i a) s = i * aval a s"

text \<open>
  \textbf{Step A} Implement the function @{text normal} which returns @{const True} only when the
  arithmetic expression is normalized.
\<close>

fun normal :: "aexp \<Rightarrow> bool" where
  "normal (Mult _ (N _)) = False" |
  "normal (Mult _ (Plus _ _)) = False" |
  "normal (Mult _ (Mult _ _)) = False" |
  "normal (Plus a b) = (normal a \<and> normal b)" |
  "normal _ = True"

text \<open>
  \textbf{Step B} Implement the function @{text normallize} which translates an arbitrary arithmetic
  expression intro a normalized arithmetic expression.
\<close>

fun normalize :: "aexp \<Rightarrow> aexp" where
  "normalize (Mult i (N n)) = N (i * n)" |
  "normalize (Mult i (V a)) = Mult i (V a)" |
  "normalize (Mult i (Mult n s)) = normalize (Mult (i * n) s)" |
  "normalize (Mult i (Plus a b)) = Plus (normalize (Mult i a)) (normalize (Mult i b))" |
  "normalize (Plus a b) = Plus (normalize a) (normalize b)" |
  "normalize (V a) = V a" |
  "normalize (N a) = N a" 

text \<open>
  \textbf{Step C} Prove that @{const normalize} does not change the result of the arithmetic
  expression.
\<close>

lemma "aval (normalize a) s = aval a s"
  by (induct a rule: normalize.induct) auto

text \<open>
  \textbf{Step D} Prove that @{const normalize} does indeed return a normalized arithmetic
  expression.
\<close>

lemma "normal (normalize a)"
  by (induct a rule: normalize.induct, auto)

section \<open>Homework 2.1\<close>
datatype bst = Leaf | Node int bst bst

fun \<alpha> where 
"\<alpha> Leaf = {}" |
"\<alpha> (Node n a b) = insert n (\<alpha> a \<union> \<alpha> b)"

fun invar where
"invar Leaf = True" |
"invar (Node n a b) = (invar a \<and> invar b \<and> (\<forall>x \<in> \<alpha> a. x < n) \<and> (\<forall>x \<in> \<alpha> b. x > n))"

fun lookup where
"lookup x Leaf = False" |
"lookup x (Node n a b) = (x = n \<or> (if x > n then lookup x b else lookup x a))"

value "lookup (-1) (Node (- 2) (Node (- 1) Leaf Leaf) Leaf)"
lemma "invar t \<Longrightarrow> lookup x t \<longleftrightarrow> x \<in> \<alpha> t"
  by (induct t, auto)

fun ins where
"ins x Leaf = Node x Leaf Leaf" | 
"ins x (Node n a b) = 
  (if x = n then (Node n a b) 
   else if x < n then Node n (ins x a) b 
        else Node n a (ins x b))"
lemma [simp]: "\<alpha> (ins x t) = insert x (\<alpha> t)"
  by (induct t, auto)

lemma "invar t \<Longrightarrow> \<alpha> (ins x t) = insert x (\<alpha> t)"
  by (induct t, auto)
lemma "invar t \<Longrightarrow> invar (ins x t)"
  by (induct t, simp_all)

fun dell where
"dell m Leaf = Leaf" |
"dell m (Node n a b) = (if m = n then Node n Leaf b else if m > n then dell m b else Node n (dell m a) b)"

lemma dell_correct1:
  "invar t \<Longrightarrow> \<alpha> (dell m t) = {x \<in> \<alpha> t. x \<ge> m}"
  by (induct t, auto)

lemma
  "invar t \<Longrightarrow> invar (dell m t)"
  by (induct t, auto simp add: dell_correct1)


