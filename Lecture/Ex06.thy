theory Ex06
imports Main  "~~/src/HOL/IMP/AExp" "~~/src/HOL/IMP/BExp"  "~~/src/HOL/IMP/Big_Step" "~~/src/HOL/IMP/Star"  "~~/src/HOL/IMP/Small_Step"
begin


section \<open>6.1\<close>
fun deskip :: "com \<Rightarrow> com" where
"deskip (SKIP;; a) = a" |
"deskip (a;; SKIP) = a" |
"deskip (a;; b) = deskip a;; deskip b" |
"deskip (IF b THEN c\<^sub>1 ELSE c\<^sub>2) = IF b THEN deskip c\<^sub>1 ELSE deskip c\<^sub>2" |
"deskip (WHILE b DO c) = WHILE b DO deskip c" |
"deskip p = p"


lemma "deskip c \<sim> c"
  apply (induction c rule: deskip.induct)
  apply ( simp_all add: )
  prefer 22 using sim_while_cong apply (blast )
apply blast+
  done
  
section \<open>6.2\<close>
datatype
  com' = SKIP' 
      | Assign' vname aexp       ("_ :::= _" [1000, 61] 61)
      | Seq'    com'  com'         ("_;;;/ _"  [60, 61] 60)
      | If'     bexp com' com'     ("(IF' _/ THEN' _/ ELSE' _)"  [0, 0, 61] 61)
      | While'  bexp com'         ("(WHILE' _/ DO' _)"  [0, 61] 61)
      | Ass' bexp                 ("ASS' _")
      | Or' com' com'             ("_ \<parallel> _" 61)
      
inductive
  big_step' :: "com' \<times> state \<Rightarrow> state \<Rightarrow> bool" (infix "\<Rightarrow>g" 55)
where
Skip: "(SKIP',s) \<Rightarrow>g s" |
Assign: "(x :::= a,s) \<Rightarrow>g s(x := aval a s)" |
Seq: "\<lbrakk> (c\<^sub>1,s\<^sub>1) \<Rightarrow>g s\<^sub>2;  (c\<^sub>2,s\<^sub>2) \<Rightarrow>g s\<^sub>3 \<rbrakk> \<Longrightarrow> (c\<^sub>1;;;c\<^sub>2, s\<^sub>1) \<Rightarrow>g s\<^sub>3" |
IfTrue: "\<lbrakk> bval b s;  (c\<^sub>1,s) \<Rightarrow>g t \<rbrakk> \<Longrightarrow> (IF' b THEN' c\<^sub>1 ELSE' c\<^sub>2, s) \<Rightarrow>g t" |
IfFalse: "\<lbrakk> \<not>bval b s;  (c\<^sub>2,s) \<Rightarrow>g t \<rbrakk> \<Longrightarrow> (IF' b THEN' c\<^sub>1 ELSE' c\<^sub>2, s) \<Rightarrow>g t" |
WhileFalse: "\<not>bval b s \<Longrightarrow> (WHILE' b DO' c,s) \<Rightarrow>g s" |
WhileTrue: "\<lbrakk> bval b s\<^sub>1;  (c,s\<^sub>1) \<Rightarrow>g s\<^sub>2;  (WHILE' b DO' c, s\<^sub>2) \<Rightarrow>g s\<^sub>3 \<rbrakk> 
\<Longrightarrow> (WHILE' b DO' c, s\<^sub>1) \<Rightarrow>g s\<^sub>3" |
Ass: "bval b s \<Longrightarrow> (Ass' b, s) \<Rightarrow>g s" |
Or_l: "(c\<^sub>1, s) \<Rightarrow>g s' \<Longrightarrow> (c\<^sub>1 \<parallel> c\<^sub>2, s) \<Rightarrow>g s'" |
Or_r: "(c\<^sub>2, s) \<Rightarrow>g s' \<Longrightarrow> (c\<^sub>1 \<parallel> c\<^sub>2, s) \<Rightarrow>g s'"

declare big_step'.intros[intro]
abbreviation
  equiv_c :: "com' \<Rightarrow> com' \<Rightarrow> bool" (infix "\<sim>g" 50) where
  "c \<sim>g c' \<equiv> (\<forall>s t. (c,s) \<Rightarrow>g t  =  (c',s) \<Rightarrow>g t)"
  
lemma "c\<^sub>1 \<parallel> c\<^sub>2 \<sim>g c\<^sub>2 \<parallel> c\<^sub>1" (is "?p\<^sub>1 \<sim>g ?p\<^sub>2")
proof (clarify, rule iffI)
  fix s t
  assume "(?p\<^sub>1, s) \<Rightarrow>g t"
  thus "(?p\<^sub>2, s) \<Rightarrow>g t" by (cases rule: big_step'.cases) auto
next
  fix s t
  assume "(?p\<^sub>2, s) \<Rightarrow>g t"
  thus "(?p\<^sub>1, s) \<Rightarrow>g t" by (cases rule: big_step'.cases) auto
qed

thm big_step.induct
thm big_step'.induct[of "(ASS' b, s)"]
thm big_step'.induct
inductive_cases ass'E:"(ASS' b, s) \<Rightarrow>g t" 
inductive_cases or'E:"(c\<^sub>1 \<parallel> c\<^sub>2, s) \<Rightarrow>g t" 
inductive_cases seq'E:"(c\<^sub>1;;; c\<^sub>2, s) \<Rightarrow>g t" 
inductive_cases skip'E:"(SKIP', s) \<Rightarrow>g t" 
thm ass'E
lemma assume_condition_true[elim!]:
  "(ASS' b, s) \<Rightarrow>g t  \<Longrightarrow> bval b s \<and> s = t"
  by (auto elim: ass'E)


lemma
  "(IF' b THEN' c\<^sub>1 ELSE' c\<^sub>2) \<sim>g (ASS' b;;; c\<^sub>1) \<parallel> (ASS' (Not b);;; c\<^sub>2)"  (is "?p\<^sub>1 \<sim>g ?p\<^sub>2")
proof (clarify, rule iffI)  
  fix s t
  assume "(?p\<^sub>1, s) \<Rightarrow>g t"
  thus "(?p\<^sub>2, s) \<Rightarrow>g t" by (cases rule: big_step'.cases) fastforce+  
next
  fix s t
  assume a: "(?p\<^sub>2, s) \<Rightarrow>g t"
  thus "(?p\<^sub>1, s) \<Rightarrow>g t" 
    by (auto elim!: or'E seq'E dest!: assume_condition_true)
qed
  
inductive
  small_step' :: "com' * state \<Rightarrow> com' * state \<Rightarrow> bool" (infix "\<rightarrow>g" 55)
where
Assign:  "(x :::= a, s) \<rightarrow>g (SKIP', s(x := aval a s))" |

Seq1:    "(SKIP';;;c\<^sub>2,s) \<rightarrow>g (c\<^sub>2,s)" |
Seq2:    "(c\<^sub>1,s) \<rightarrow>g (c\<^sub>1',s') \<Longrightarrow> (c\<^sub>1;;;c\<^sub>2,s) \<rightarrow>g (c\<^sub>1';;;c\<^sub>2,s')" |

IfTrue:  "bval b s \<Longrightarrow> (IF' b THEN' c\<^sub>1 ELSE' c\<^sub>2,s) \<rightarrow>g (c\<^sub>1,s)" |
IfFalse: "\<not>bval b s \<Longrightarrow> (IF' b THEN' c\<^sub>1 ELSE' c\<^sub>2,s) \<rightarrow>g (c\<^sub>2,s)" |

While:   "(WHILE' b DO' c,s) \<rightarrow>g
            (IF' b THEN' c;;; WHILE' b DO' c ELSE' SKIP',s)" |

Ass: "bval b s \<Longrightarrow> (Ass' b, s) \<rightarrow>g (SKIP', s)" |

Or_l: "(c\<^sub>1 \<parallel> c\<^sub>2, s) \<rightarrow>g (c\<^sub>1, s)" |

Or_r: "(c\<^sub>1 \<parallel> c\<^sub>2, s) \<rightarrow>g (c\<^sub>2, s)"

declare small_step'.intros[intro]
lemmas small_step_induct = small_step.induct[split_format(complete)]

inductive_cases Skip'E[elim!]: "(SKIP',s) \<rightarrow>g ct"
thm Skip'E
inductive_cases Assign'E[elim!]: "(x:::=a,s) \<rightarrow>g ct"
thm Assign'E
inductive_cases Seq'E[elim]: "(c1;;;c2,s) \<rightarrow>g ct"
thm SeqE
inductive_cases If'E[elim!]: "(IF' b THEN' c1 ELSE' c2,s) \<rightarrow>g ct"
inductive_cases While'E[elim]: "(WHILE' b DO' c, s) \<rightarrow>g ct"

abbreviation
  small_steps' :: "com' * state \<Rightarrow> com' * state \<Rightarrow> bool" (infix "\<rightarrow>g*" 55)
where "x \<rightarrow>g* y == star small_step' x y"

lemma star_seq2:
  "(c\<^sub>1, s) \<rightarrow>g* (c\<^sub>1', s') \<Longrightarrow>(c\<^sub>1;;; c\<^sub>2, s) \<rightarrow>g* (c\<^sub>1';;;c\<^sub>2, s')"
proof (induction rule: star_induct)
  case (refl c\<^sub>1 s)
  show ?case by auto
next
  case (step c\<^sub>1 s c\<^sub>1' s' c\<^sub>1'' s'') 
  thus ?case by (meson small_step'.Seq2 star.step)
qed

lemma seq_comp:
  "\<lbrakk> (c1,s1) \<rightarrow>g* (SKIP',s2); (c2,s2) \<rightarrow>g* (SKIP',s3) \<rbrakk>
   \<Longrightarrow> (c1;;;c2, s1) \<rightarrow>g* (SKIP',s3)"
by(blast intro: star.step star_seq2 star_trans)

lemma sos_implies_sn:
  assumes "(c, s) \<Rightarrow>g  s'"
  shows "(c, s) \<rightarrow>g* (SKIP', s')"
  using assms
  by (induction) (blast intro: Ex06.seq_comp star.step)+

inductive_cases while'E:"(WHILE' b DO' c, s) \<Rightarrow>g t" 
thm while'E

lemma small1_big_continue:
  "cs \<rightarrow>g cs' \<Longrightarrow> cs' \<Rightarrow>g t \<Longrightarrow> cs \<Rightarrow>g t"
proof (induction arbitrary: t rule: small_step'.induct)
  case (While b c s)
  thus ?case by (cases) (blast intro: seq'E skip'E)+
qed (fastforce elim: skip'E seq'E ass'E)+

thm small_to_big
lemma small_to_big:
  "cs \<rightarrow>g* (SKIP',t) \<Longrightarrow> cs \<Rightarrow>g t"
apply (induction cs "(SKIP', t)" rule: star.induct)
apply (auto intro: small1_big_continue)
done

lemma sos_iff_sm:
  "cs \<rightarrow>g* (SKIP',t) = cs \<Rightarrow>g t"
  by (metis Ex06.small_to_big sos_implies_sn surj_pair)

section \<open>hw 6.1\<close>
subsection "List setup"

text {* 
  In the following, we use the length of lists as integers 
  instead of natural numbers. Instead of converting @{typ nat}
  to @{typ int} explicitly, we tell Isabelle to coerce @{typ nat}
  automatically when necessary.
*}
declare [[coercion_enabled]] 
declare [[coercion "int :: nat \<Rightarrow> int"]]

text {* 
  Similarly, we will want to access the ith element of a list, 
  where @{term i} is an @{typ int}.
*}
fun inth :: "'a list \<Rightarrow> int \<Rightarrow> 'a" (infixl "!!" 100) where
"(x # xs) !! i = (if i = 0 then x else xs !! (i - 1))"

text {*
  The only additional lemma we need about this function 
  is indexing over append:
*}
lemma inth_append [simp]:
  "0 \<le> i \<Longrightarrow>
  (xs @ ys) !! i = (if i < size xs then xs !! i else ys !! (i - size xs))"
by (induction xs arbitrary: i) (auto simp: algebra_simps)

text{* We hide coercion @{const int} applied to @{const length}: *}

abbreviation (output)
  "isize xs == int (length xs)"

notation isize ("size")

declare [[syntax_ambiguity_warning = false]]
type_synonym reg = nat
datatype instr =
  LOADI int reg
| LOAD vname reg
| ADD reg reg reg
| STORE reg vname
| JMP int
| JMPLESS reg reg int
| JMPGE reg reg int


type_synonym reg_state = "reg \<Rightarrow> val"
type_synonym config = "int \<times> state \<times> reg_state"

fun iexec :: "instr \<Rightarrow> config \<Rightarrow> config" where
"iexec (LOADI n r) (i,s,regs) = (i+1,s, regs(r:=n))" |
"iexec (LOAD x r) (i,s,regs) = (i+1,s, regs(r:=s x))" |
"iexec (ADD r\<^sub>1 r\<^sub>2 r\<^sub>3) (i,s,regs) = (i+1,s, regs(r\<^sub>3:= regs r\<^sub>1 + regs r\<^sub>2))" |
"iexec (STORE r x) (i,s,regs) = (i+1,s(x := regs r), regs)"  |
"iexec (JMP n) (i,s,regs) = (i+n,s, regs)"  |
"iexec (JMPLESS r\<^sub>1 r\<^sub>2 n) (i,s,regs) = (i+(if regs r\<^sub>1 < regs r\<^sub>2 then n else 0),s, regs)"   |
"iexec (JMPGE r\<^sub>1 r\<^sub>2 n) (i,s,regs) = (i+(if regs r\<^sub>1 \<ge> regs r\<^sub>2 then n else 0),s, regs)" 


definition
  exec1 :: "instr list \<Rightarrow> config \<Rightarrow> config \<Rightarrow> bool"
     ("(_/ \<turnstile> (_ \<rightarrow>/ _))" [59,0,59] 60) 
where
  "P \<turnstile> c \<rightarrow> c' = 
  (\<exists>i s stk. c = (i,s,stk) \<and> c' = iexec(P!!i) (i,s,stk) \<and> 0 \<le> i \<and> i < size P)"

declare star.step[intro]

lemmas exec_induct = star.induct [of "exec1 P", split_format(complete)]

lemma exec1I [intro, code_pred_intro]:
  "c' = iexec (P!!i) (i,s,stk) \<Longrightarrow> 0 \<le> i \<Longrightarrow> i < size P
  \<Longrightarrow> P \<turnstile> (i,s,stk) \<rightarrow> c'"
by (simp add: exec1_def)

abbreviation 
  exec :: "instr list \<Rightarrow> config \<Rightarrow> config \<Rightarrow> bool" ("(_/ \<turnstile> (_ \<rightarrow>*/ _))" 50)
where
  "exec P \<equiv> star (exec1 P)"

lemma iexec_shift [simp]: 
  "((n+i',s',stk') = iexec x (n+i,s,stk)) = ((i',s',stk') = iexec x (i,s,stk))"
  by (cases x) auto

lemma exec1_appendR: "P \<turnstile> c \<rightarrow> c' \<Longrightarrow> P@P' \<turnstile> c \<rightarrow> c'"
by (auto simp: exec1_def)

lemma exec_appendR: "P \<turnstile> c \<rightarrow>* c' \<Longrightarrow> P@P' \<turnstile> c \<rightarrow>* c'"
by (induction rule: star.induct)  (fastforce intro: exec1_appendR)+

lemma exec1_appendL:
  fixes i i' :: int 
  shows
  "P \<turnstile> (i,s,stk) \<rightarrow> (i',s',stk') \<Longrightarrow>
   P' @ P \<turnstile> (size(P')+i,s,stk) \<rightarrow> (size(P')+i',s',stk')"
  unfolding exec1_def
  by (auto simp del: iexec.simps)


lemma exec_appendL:
  fixes i i' :: int 
  shows
 "P \<turnstile> (i,s,stk) \<rightarrow>* (i',s',stk')  \<Longrightarrow>
  P' @ P \<turnstile> (size(P')+i,s,stk) \<rightarrow>* (size(P')+i',s',stk')"
  by (induction rule: exec_induct) (blast intro!: exec1_appendL)+


lemma exec_Cons_1 [intro]:
  "P \<turnstile> (0,s,stk) \<rightarrow>* (j,t,stk') \<Longrightarrow>
  instr#P \<turnstile> (1,s,stk) \<rightarrow>* (1+j,t,stk')"
by (drule exec_appendL[where P'="[instr]"]) simp

lemma exec_appendL_if[intro]:
  fixes i i' j :: int
  shows
  "size P' <= i
   \<Longrightarrow> P \<turnstile> (i - size P',s,stk) \<rightarrow>* (j,s',stk')
   \<Longrightarrow> i' = size P' + j
   \<Longrightarrow> P' @ P \<turnstile> (i,s,stk) \<rightarrow>* (i',s',stk')"
by (drule exec_appendL[where P'=P']) simp


lemma exec_append_trans[intro]:
  fixes i' i'' j'' :: int
  shows
"P \<turnstile> (0,s,stk) \<rightarrow>* (i',s',stk') \<Longrightarrow>
 size P \<le> i' \<Longrightarrow>
 P' \<turnstile>  (i' - size P,s',stk') \<rightarrow>* (i'',s'',stk'') \<Longrightarrow>
 j'' = size P + i''
 \<Longrightarrow>
 P @ P' \<turnstile> (0,s,stk) \<rightarrow>* (j'',s'',stk'')"
by(metis star_trans[OF exec_appendR exec_appendL_if])


subsection \<open>STEP C\<close>
fun acomp :: "reg \<Rightarrow> aexp \<Rightarrow> instr list" where
"acomp r (N n) = [LOADI n r]" |
"acomp r (V x) = [LOAD x r]" |
"acomp r (Plus a1 a2) = acomp r a1 @ acomp (Suc r) a2 @ [ADD r (Suc r) r]"

(*whatever*)


section \<open>hw 6.2\<close>
datatype gcom =
  Skip
| Ass vname aexp
| Sq gcom gcom
| IfBlock "(bexp \<times> gcom) list"

inductive small_stepg :: "gcom \<times> state \<Rightarrow> gcom \<times> state \<Rightarrow> bool" (infix "\<rightarrow>gg" 55) where
sSeqSk: "(Sq Skip c\<^sub>2, s) \<rightarrow>gg (c\<^sub>2, s)" | 
sAss: "(Ass v a, s) \<rightarrow>gg (Skip, s(v:= aval a s))" |
sSeq: "(c\<^sub>1, s)  \<rightarrow>gg (c\<^sub>1', s') \<Longrightarrow> (Sq c\<^sub>1 c\<^sub>2, s) \<rightarrow>gg (Sq c\<^sub>1' c\<^sub>2, s')" |
"(b, c) \<in> set G\<^sub>s \<Longrightarrow> bval b s \<Longrightarrow> (IfBlock G\<^sub>s, s) \<rightarrow>gg (c, s)"
abbreviation
small_stepgs :: "gcom \<times> state \<Rightarrow> gcom \<times> state \<Rightarrow> bool" (infix "\<rightarrow>gg*" 55 )
where "x \<rightarrow>gg* y \<equiv> star small_stepg x y"

lemmas small_stepg_induct = small_stepg.induct[split_format(complete)]

declare small_stepg.intros [intro, simp]

inductive_cases sSkipE[elim!]: "(Skip, s) \<rightarrow>gg ct"
inductive_cases sAssE[elim!]: "(Ass x a, s) \<rightarrow>gg ct"
inductive_cases sSqE[elim!]: "(Sq c1 c2, s) \<rightarrow>gg ct"
inductive_cases sIfBlockE[elim!]: "(IfBlock Gs, s) \<rightarrow>gg ct"

inductive
big_stepg :: "gcom \<times> state \<Rightarrow> state \<Rightarrow> bool" (infix "\<Rightarrow>g" 55 )
where
Skip: "(Skip, s) \<Rightarrow>g s"
| Ass: "(Ass x a, s) \<Rightarrow>g s(x := aval a s)"
| Sq: "(c, s) \<Rightarrow>g s' \<Longrightarrow> (c', s' ) \<Rightarrow>g s'' \<Longrightarrow> (Sq c c', s) \<Rightarrow>g s''"
| IfBlock : " (b, c) \<in> set Gs \<Longrightarrow> bval b s \<Longrightarrow> (c, s) \<Rightarrow>g t \<Longrightarrow> (IfBlock Gs, s) \<Rightarrow>g t"
declare big_stepg.intros [intro, simp]
lemmas big_stepg_induct = big_stepg.induct[split_format(complete)]
inductive_cases SkipE [elim!]: " (Skip, s) \<Rightarrow>g t"
inductive_cases AssE [elim!]: " (Ass x a, s) \<Rightarrow>g t"
inductive_cases SqE [elim!]: " (Sq c1 c2 , s) \<Rightarrow>g t"
inductive_cases IfBlockE [elim!]: " (IfBlock Gs, s) \<Rightarrow>g t"

thm star_induct[of small_stepg c s Skip s']
lemma star_seqg2:
  assumes "(c, s) \<rightarrow>gg* (c', s')"
  shows "(Sq c d, s) \<rightarrow>gg* (Sq c' d, s')"
  using assms by (induction arbitrary: rule: star_induct, auto)

lemma star_stepg2:
  assumes "(c, s) \<rightarrow>gg* (Skip, s')"
  and  "(c', s') \<rightarrow>gg* (c'', s'')"
  shows "(Sq c c', s) \<rightarrow>gg* (c'', s'')"
  by (meson assms sSeqSk star.step star_seqg2 star_trans)


lemma 
  big_iff_small_imp: "cs \<Rightarrow>g t \<Longrightarrow> cs \<rightarrow>gg* (Skip, t)"
proof (induction arbitrary:  rule: big_stepg.induct)
  case (Skip)
  thus ?case by auto
next
  case (Ass v exp s)
  show ?case
    by (rule star_step1) blast
next
  case (IfBlock b c Gs s)
  thus ?case by auto
next
  case (Sq c s s' c' s'') note H = this(3) and H' = this(4)
  show ?case using H H' star_stepg2 by auto
qed


lemma 
  big_iff_small_rec: "cs \<rightarrow>gg* (Skip, t) \<Longrightarrow> cs \<Rightarrow>g t"
proof (induction cs "(Skip, t)" arbitrary:  rule: star.induct)
  case (refl)
  thus ?case by auto
next
  case (step cs ct)
  also have "cs \<rightarrow>gg ct \<Longrightarrow> ct \<Rightarrow>g t \<Longrightarrow> cs \<Rightarrow>g t"
    by (induction arbitrary: t rule: small_stepg.induct)  auto
  ultimately show ?case by blast
qed
lemma "cs \<rightarrow>gg* (Skip, t) \<longleftrightarrow> cs \<Rightarrow>g t"
  using big_iff_small_imp big_iff_small_rec by blast

lemma small_stepg_Sq_trans:
  assumes HL: "(c, s) \<rightarrow>gg* (Skip, s')"
      and HR: "(c', s') \<rightarrow>gg* (Skip, s'')" 
  shows "(Sq c c', s) \<rightarrow>gg* (Skip, s'')"
using assms star.induct[of "op \<rightarrow>gg*" "(c, s)" "(Skip, s')" 
  "\<lambda>(c, s) (c', s'). ((c, s) \<rightarrow>gg* (Skip, s') \<longrightarrow> (c', s')  \<rightarrow>gg* (Sq c c', s) \<longrightarrow> (Skip, s'') \<rightarrow>gg* (Skip, s''))"]
end