theory Cognome_Prenome
imports
(*  "~~/src/HOL/IMP/Big_Step"
  "~~/src/HOL/IMP/Vars"
  "~~/src/HOL/IMP/Def_Init"
  "~~/src/HOL/IMP/Def_Init_Exp"
  *)
        "~~/src/HOL/IMP/Big_Step"
      "~~/src/HOL/IMP/Vars"
begin


(* Exercise 8.1 *)
(*
inductive
  small_step :: "com \<times> state \<Rightarrow> com \<times> state \<Rightarrow> bool" (infix "\<rightarrow>" 55)
where
Assign:  "Def_Init_Exp.aval a s = Some i \<Longrightarrow> (x ::= a, s) \<rightarrow> (SKIP, s(x := Some i))" |

Semi1:   "(SKIP ;; c, s) \<rightarrow> (c, s)" |
Semi2:   "(c\<^sub>1, s) \<rightarrow> (c\<^sub>1', s') \<Longrightarrow> (c\<^sub>1 ;; c\<^sub>2, s) \<rightarrow> (c\<^sub>1' ;; c\<^sub>2, s')" |

IfTrue:  "Def_Init_Exp.bval b s = Some True \<Longrightarrow> (IF b THEN c\<^sub>1 ELSE c\<^sub>2, s) \<rightarrow> (c\<^sub>1, s)" |
IfFalse: "Def_Init_Exp.bval b s = Some False \<Longrightarrow> (IF b THEN c\<^sub>1 ELSE c\<^sub>2, s) \<rightarrow> (c\<^sub>2, s)" |

While:   "(WHILE b DO c, s) \<rightarrow> (IF b THEN c ;; WHILE b DO c ELSE SKIP, s)"

lemmas small_step_induct = small_step.induct[split_format(complete)]

inductive
  small_steps :: "com * state \<Rightarrow> com * state \<Rightarrow> bool"  (infix "\<rightarrow>*" 55) where
refl:  "cs \<rightarrow>* cs" |
step:  "cs \<rightarrow> cs' \<Longrightarrow> cs' \<rightarrow>* cs'' \<Longrightarrow> cs \<rightarrow>* cs''"

lemmas small_steps_induct = small_steps.induct[split_format(complete)]

fun AA :: "com \<Rightarrow> vname set" where
"AA c = undefined"

fun D :: "vname set \<Rightarrow> com \<Rightarrow> bool" where
"D A c = True"

theorem progress: "D (dom s) c \<Longrightarrow> c \<noteq> SKIP \<Longrightarrow> \<exists>cs'. (c, s) \<rightarrow> cs'"
  oops

lemma D_incr: "(c, s) \<rightarrow> (c', s') \<Longrightarrow> dom s \<union> AA c \<subseteq> dom s' \<union> AA c'"
  oops

lemma D_mono: "A \<subseteq> A' \<Longrightarrow> D A c \<Longrightarrow> D A' c"
  oops

theorem D_preservation: "(c, s) \<rightarrow> (c', s') \<Longrightarrow> D (dom s) c \<Longrightarrow> D (dom s') c'"
  oops

theorem D_sound:
  "(c, s) \<rightarrow>* (c', s') \<Longrightarrow> D (dom s) c \<Longrightarrow> c' \<noteq> SKIP \<Longrightarrow>
  \<exists>cs''. (c', s') \<rightarrow> cs''"
  oops

lemma DD: "Def_Init.D A c A' \<Longrightarrow> A' = A \<union> AA c"
  oops

lemma D_equiv: "D A c \<longleftrightarrow> Def_Init.D A c (A \<union> AA c)"
  oops


(* Exercise 8.2 *)

fun
  gen_ad :: "com \<Rightarrow> (vname * aexp) set" and
  kill_ad :: "com \<Rightarrow> (vname * aexp) set"
where
"gen_ad c = undefined" |
"kill_ad c = undefined"

definition AD :: "(vname * aexp) set \<Rightarrow> com \<Rightarrow> (vname * aexp) set" where
"AD A c = undefined"

theorem "(c, s) \<Rightarrow> s' \<Longrightarrow> \<forall>(x, a) \<in> A. s x = AExp.aval a s \<Longrightarrow>
  \<forall>(x, a) \<in> AD A c. s' x = AExp.aval a s'"
oops
*)

(* Homework 8.1 *)

(* WARNING: Change the imports to

    imports
      "~~/src/HOL/IMP/Big_Step"
      "~~/src/HOL/IMP/Vars"

   to avoid loading the alternative initialization-aware semantics. *)

lemma eq_on_antimono[intro]: "A \<subseteq> B \<Longrightarrow> f = g on B \<Longrightarrow> f = g on A"
by auto
lemma big_step_changes_only_vars: "(c, s) \<Rightarrow> t \<Longrightarrow> t = s on - vars c"
by (induction rule: big_step_induct) auto

lemma change_notin_vars:
  assumes "s = sa on vars c" and "(c, s) \<Rightarrow> t"
  shows "(c, sa) \<Rightarrow> (\<lambda>x. (if x \<in> vars c then t else sa) x)"
using assms(2,1)
proof (induction arbitrary: sa rule: big_step_induct)
  case Skip
  thus ?case by auto
next
  case (Assign v a s)
  have "(\<lambda>x. (if x \<in> vars (v ::= a) then s(v := aval a s) else sa) x) = sa(v := aval a sa)" 
    apply rule
    using Assign by auto
  thus ?case by auto
next 
  case (Seq c s1 s2 d s3) note c = this(1) and d = this(2) and IH = this(3,4) and s1sa = this(5)
  let ?sa' = "(\<lambda>x. (if x \<in> vars c then s2 else sa) x)"
  let ?sa'' = "(\<lambda>x. (if x \<in> vars (c;; d) then s3 else sa) x)"
  have ssa: "s1 = sa on vars c" using Seq(5) by auto
  have csa: "(c, sa) \<Rightarrow> ?sa'" using Seq.IH(1)[OF ssa ] .

  have s'sa: "s2 = ?sa' on vars d" 
    using big_step_changes_only_vars[OF c] by (simp add: s1sa)
  have " (\<lambda>x. (if x \<in> vars d then s3 else (\<lambda>a. (if a \<in> vars c then s2 else sa) a)) x) = ?sa''"
    using big_step_changes_only_vars d by force
  hence "(d, ?sa') \<Rightarrow> ?sa''" using Seq.IH(2)[OF s'sa] by presburger
  thus ?case using csa by blast
next
  case (IfTrue b s c t d) note b = this(1) and c = this(2)
  have "(\<lambda>x. (if x \<in> vars c then t else sa) x) = (\<lambda>x. (if x \<in> vars (IF b THEN c ELSE d) then t else sa) x)"
     apply (rule ext)
     using IfTrue(4) apply auto
     using big_step_changes_only_vars c by fastforce+
   also have csa: "(c, sa) \<Rightarrow> (\<lambda>x. (if x \<in> vars c then t else sa) x)"
     using IfTrue.IH[of sa] IfTrue by auto
   moreover have bsa: "bval b sa" 
     using b IfTrue(4) by (metis Un_iff bval_eq_if_eq_on_vars vars_com_simps(4))
   ultimately show ?case by auto
next
  case (IfFalse b s d t c) note b = this(1) and d = this(2)
  have "(\<lambda>x. (if x \<in> vars d then t else sa) x) = (\<lambda>x. (if x \<in> vars (IF b THEN c ELSE d) then t else sa) x)"
    using IfFalse(4) big_step_changes_only_vars d by (fastforce simp add: ext)+
  also have dsa: "(d, sa) \<Rightarrow> (\<lambda>x. (if x \<in> vars d then t else sa) x)"
    using IfFalse.IH[of sa] IfFalse by auto
  moreover have bsa: "\<not>bval b sa" using b IfFalse(4)
     bval_eq_if_eq_on_vars[of b s sa] by force
  ultimately show ?case by auto   
next
  case (WhileTrue b s c s' t)
  have "bval b sa"
    by (metis UnI1 WhileTrue.hyps(1) WhileTrue.prems bval_eq_if_eq_on_vars vars_com_simps(5))

  let ?sa' = "(\<lambda>x. (if x \<in> vars c then s' else sa) x)"
  have "(c, sa) \<Rightarrow> ?sa'" 
    by (simp add: WhileTrue.IH(1) WhileTrue.prems)
    thm WhileTrue.IH(2)[of ?sa']
  have "(WHILE b DO c, ?sa') \<Rightarrow> (\<lambda>x. (if x \<in> vars (WHILE b DO c) then t else ?sa') x)"
    using WhileTrue.IH(2)[of ?sa'] WhileTrue.hyps(2) WhileTrue.prems big_step_changes_only_vars by fastforce
  also have "\<dots> = (\<lambda>x. (if x \<in> vars (WHILE b DO c) then t else sa) x)"
    by (auto simp add: ext)
  finally show ?case
    using `(c, sa) \<Rightarrow> (\<lambda>x. (if x \<in> vars c then s' else sa) x)` `bval b sa` by blast
next
  case (WhileFalse b s c)
  hence lem: "(\<lambda>x. (if x \<in> vars (WHILE b DO c) then s else sa) x) = sa" by auto
  have "\<not> bval b sa"
   by (metis Un_iff WhileFalse.hyps WhileFalse.prems bval_eq_if_eq_on_vars rvars.simps(5) vars_com_def)
  then show ?case
   unfolding lem using big_step.WhileFalse by blast
qed


subsection \<open>Step C\<close>
theorem disjoint_imp_Seq_commute:
  assumes
    vars: "vars c1 \<inter> vars c2 = {}" and
    step: "(Seq c1 c2, s) \<Rightarrow> t"
  shows "(Seq c2 c1, s) \<Rightarrow> t" (*<*)
proof -
  obtain s' where c1: "(c1, s) \<Rightarrow> s'" and c2: "(c2, s') \<Rightarrow> t" using step by blast
  have s's: "s' = s on vars c2" using c1 vars using big_step_changes_only_vars by blast
  have "(c2, s) \<Rightarrow> (\<lambda>x. (if x \<in> vars c2 then t else s) x)" 
    apply (rule change_notin_vars)
    using s's c2 by auto

  also have "(c1, (\<lambda>x. (if x \<in> vars c2 then t else s) x)) \<Rightarrow> 
    (\<lambda>x. (if x \<in> vars c1 then s' else (\<lambda>x. (if x \<in> vars c2 then t else s) x)) x)" 
    apply (rule change_notin_vars)
    using vars c1 by auto

  moreover have "(\<lambda>x. (if x \<in> vars c1 then s' else (\<lambda>x. (if x \<in> vars c2 then t else s) x)) x) = t"
    using vars big_step_changes_only_vars[OF c1] big_step_changes_only_vars[OF c2] by fastforce

  ultimately show ?thesis by auto
qed

end

