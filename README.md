# Formalisation of Ground Inference Systems in a Proof Assistant #


### Abstract ###

The theory files are in the directory thys (theories)

### How do I get things running (Master Thesis)? ###

* Install [Isabelle 2015](http://isabelle.in.tum.de/)
* Download the files or clone the repository `git clone --branch masterthesis https://bitbucket.org/zmaths/formalisation-of-ground-inference-systems-in-a-proof-assistant.git
`
* Then go to the directory `cd formalisation-of-ground-inference-systems-in-a-proof-assistant`
* To build the theory files: `/path/to/Isabelle2015/bin/isabelle build -D thys`
* To build the associated master's thesis, run: `/path/to/Isabelle2015/bin/isabelle build -d thys -D Report Inferences`. It can be found after compiling it in `Report/document/document.pdf`

### Actual Version ###
* The actual version is using the latest Isabelle repository version (there were some changes in the Multiset library since the release), but the development moved to [Isafol](https://bitbucket.org/jasmin_blanchette/isafol).