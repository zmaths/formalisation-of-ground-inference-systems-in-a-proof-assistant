theory Propo_Resolution

imports Partial_Clausal_Logic

begin
section \<open>Resolution\<close>
subsection \<open>Simplification Rules\<close>

inductive simplify  :: "'v clauses \<Rightarrow> 'v clauses \<Rightarrow> bool" for N :: "'v clause set" where
tautology_deletion: "(A + {#Pos P#} + {#Neg P#})\<in> N \<Longrightarrow> simplify  N (N - {A + {#Pos P#} + {#Neg P#}})"|
condensation: "(A + {#L#} + {#L#}) \<in> N \<Longrightarrow> simplify N ( N- {A + {#L#} + {#L#}} \<union> {A + {#L#}})" |
subsumption: "A \<in> N \<Longrightarrow> A <# B \<Longrightarrow> B \<in> N \<Longrightarrow> simplify N (N - {B})"

lemma simplifier_preserves_un_sat':
  fixes N N' :: "'v clauses"
  assumes "simplify N N'"
  and "total_over_m I N"
  shows "I \<Turnstile>s N' \<longrightarrow> I \<Turnstile>s N"
  using assms
proof (induct rule: simplify.induct)
  case (tautology_deletion A P)
  hence "I \<Turnstile> A + {#Pos P#} + {#Neg P#}"
    by (metis total_over_m_def total_over_set_literal_defined true_cls_singleton true_cls_union true_lit_def uminus_Neg union_commute)
  thus ?case by (metis Un_Diff_cancel2 true_clss_singleton true_clss_union)
next
  case (condensation A P)
  thus ?case by (metis Diff_insert_absorb Set.set_insert insertE true_cls_union true_clss_def true_clss_singleton true_clss_union)
next
  case (subsumption A B)
  have "A \<noteq> B" using subsumption.hyps(2) by auto
  hence "I \<Turnstile>s N - {B} \<Longrightarrow> I \<Turnstile> A" using `A \<in> N` by (simp add: true_clss_def)
  also have "I \<Turnstile> A \<Longrightarrow> I \<Turnstile> B" using `A <# B` by (meson mset_lessD true_cls_def)
  ultimately show ?case by (metis insert_Diff_single true_clss_insert)
qed

lemma simplifier_preserves_un_sat:
  fixes N N' :: "'v clauses"
  assumes "simplify N N'"
  and "total_over_m I N"
  shows "I \<Turnstile>s N \<longrightarrow> I \<Turnstile>s N'"
  using assms apply (induct rule: simplify.induct)
  prefer 2 apply (metis Un_insert_right insert_Diff sup_bot_right true_cls_union true_clss_insert)
  by simp_all

lemma simplifier_preserves_un_sat'':
  fixes N N' :: "'v clauses"
  assumes "simplify N N'"
  and "total_over_m I N'"
  shows "I \<Turnstile>s N \<longrightarrow> I \<Turnstile>s N'"
  using assms apply (induct rule: simplify.induct)
  prefer 2 apply (metis Un_insert_right insert_Diff sup_bot_right true_cls_union true_clss_insert)
  by simp_all

lemma simplifier_preserves_un_sat_eq:
  fixes N N' :: "'v clauses"
  assumes "simplify N N'"
  and "total_over_m I N"
  shows "I \<Turnstile>s N \<longleftrightarrow> I \<Turnstile>s N'"
  using simplifier_preserves_un_sat simplifier_preserves_un_sat' assms by blast

lemma simplifier_preserves_finite:
 assumes "simplify \<psi> \<psi>'"
 shows "finite \<psi> \<longleftrightarrow> finite \<psi>'"
 using assms by (induct rule: simplify.induct, auto simp add: remove_def)

lemma rtranclp_simplifier_preserves_finite:
 assumes "rtranclp simplify \<psi> \<psi>'"
 shows "finite \<psi> \<longleftrightarrow> finite \<psi>'"
 using assms by (induct rule: rtranclp.induct) (auto simp add: simplifier_preserves_finite)

lemma simplifier_atms_of_m:
  assumes "simplify \<psi> \<psi>'"
  shows "atms_of_m \<psi>' \<subseteq> atms_of_m \<psi>"
  using assms unfolding atms_of_m_def
proof (induct rule: simplify.induct)
  case (tautology_deletion A P)
  thus ?case by auto
next
  case (condensation A P)
  also have "A + {#P#} + {#P#} \<in> \<psi> \<Longrightarrow> \<exists>x\<in>\<psi>. atm_of P \<in> atm_of ` set_mset x" by (metis Un_iff atms_of_def atms_of_plus atms_of_singleton insert_iff)
  ultimately show ?case by (auto simp add: atms_of_def)
next
  case (subsumption A P)
  thus ?case by auto
qed


lemma rtranclp_simplifier_atms_of_m:
  assumes "rtranclp simplify \<psi> \<psi>'"
  shows "atms_of_m \<psi>' \<subseteq> atms_of_m \<psi>"
  using assms apply (induct rule: rtranclp.induct)
   apply (fastforce intro: simplifier_atms_of_m)
  using simplifier_atms_of_m by blast

lemma factoring_imp_simplifier:
  assumes "{#L#} + {#L#} + C \<in> N"
  shows "\<exists>N'. simplify N N'"
proof -
  have "C + {#L#} + {#L#} \<in> N" using assms by (simp add: add.commute union_lcomm)
  from condensation[OF this] show ?thesis by blast
qed

subsection \<open>Unconstrained Resolution\<close>
type_synonym 'v uncon_state = "'v clauses"
inductive uncon_res :: "'v uncon_state \<Rightarrow> 'v uncon_state \<Rightarrow> bool" where
resolution: "{#Pos p#} + C \<in> N \<Longrightarrow> {#Neg p#} + D \<in> N \<Longrightarrow> ({#Pos p#} + C, {#Neg p#} + D) \<notin> already_used \<Longrightarrow> uncon_res (N) (N \<union> {C + D})" |
factoring: "{#L#} + {#L#} + C \<in> N \<Longrightarrow> uncon_res N (N \<union>{C + {#L#}})"

lemma uncon_res_increasing:
  assumes "uncon_res S S'" and "\<psi> \<in> S"
  shows "\<psi> \<in> S'"
  using assms by (induct rule: uncon_res.induct) auto

lemma rtranclp_uncon_inference_increasing:
  assumes "rtranclp uncon_res S S'" and "\<psi> \<in> S"
  shows "\<psi> \<in> S'"
  using assms by (induct rule: rtranclp.induct) (auto simp add: uncon_res_increasing)

subsubsection \<open>Subsumption\<close>

definition "subsumes \<chi> \<chi>' \<equiv> (\<forall>I. total_over_m I {\<chi>'} \<longrightarrow> total_over_m I {\<chi>}) \<and> (\<forall>I. total_over_m I {\<chi>} \<longrightarrow> I \<Turnstile> \<chi> \<longrightarrow> I \<Turnstile> \<chi>')"

lemma subsumes_refl[simp]:
  "subsumes \<chi> \<chi>"
  unfolding subsumes_def by auto


lemma subsumes_subsumption:
  assumes "subsumes D \<chi>"
  and "C \<subset># D" and "\<not>tautology \<chi>"
  shows "subsumes C \<chi>" unfolding subsumes_def
proof
  show "\<forall>I. total_over_m I {\<chi>} \<longrightarrow> total_over_m I {C}"
    using assms subsumption_total_over_m unfolding subsumes_def by (blast intro: subset_mset.less_imp_le)
next
  show "\<forall>I. total_over_m I {C} \<longrightarrow> I \<Turnstile> C \<longrightarrow> I \<Turnstile> \<chi>"
     using assms subsumption_chained unfolding subsumes_def by (blast intro: subset_mset.less_imp_le)
qed

lemma subsumes_tautology:
  assumes "subsumes (C + {#Pos P#} + {#Neg P#}) \<chi>"
  shows "tautology \<chi>"
  using assms unfolding subsumes_def by (simp add: tautology_def)


subsection \<open>Inference Rule\<close>
type_synonym 'v state = "'v clauses \<times> ('v clause \<times> 'v clause) set"
inductive inference_clause :: "'v state \<Rightarrow> 'v clause \<times> ('v clause \<times> 'v clause) set \<Rightarrow> bool" (infix "\<Rightarrow>\<^sub>\<Res>" 100) where
resolution: "{#Pos p#} + C \<in> N \<Longrightarrow> {#Neg p#} + D \<in> N \<Longrightarrow> ({#Pos p#} + C, {#Neg p#} + D) \<notin> already_used \<Longrightarrow> inference_clause (N, already_used) (C + D, already_used \<union> {({#Pos p#} + C, {#Neg p#} + D)})" |
factoring: "{#L#} + {#L#} + C \<in> N \<Longrightarrow> inference_clause (N, already_used) (C + {#L#}, already_used)"

inductive inference :: "'v state \<Rightarrow> 'v state \<Rightarrow> bool" where
inference_step: "inference_clause S (clause, already_used) \<Longrightarrow> inference S (fst S \<union> {clause}, already_used)"


abbreviation "already_used_inv state \<equiv> (\<forall>(A, B) \<in> snd state. \<exists>p. Pos p \<in># A \<and> Neg p \<in># B \<and> ((\<exists>\<chi> \<in> fst state. subsumes \<chi> ((A - {#Pos p#}) + (B - {#Neg p#}))) \<or> tautology ((A - {#Pos p#}) + (B - {#Neg p#}))))"

lemma inference_clause_preserves_already_used_inv:
  assumes "inference_clause S S'"
  and "already_used_inv S"
  shows "already_used_inv (fst S \<union> {fst S'}, snd S')"
  using assms apply (induct rule: inference_clause.induct)
  by fastforce+

lemma inference_preserves_already_used_inv:
  assumes "inference S S'"
  and "already_used_inv S"
  shows "already_used_inv S'"
  using assms
proof (induct rule: inference.induct)
  case (inference_step S clause already_used)
  thus ?case using inference_clause_preserves_already_used_inv[of S "(clause, already_used)"] by simp
qed

lemma rtranclp_inference_preserves_already_used_inv:
  assumes "rtranclp inference S S'"
  and "already_used_inv S"
  shows "already_used_inv S'"
  using assms apply (induct rule: rtranclp.induct, simp)
  using inference_preserves_already_used_inv unfolding tautology_def by fast

lemma subsumes_condensation:
  assumes "subsumes (C + {#L#} + {#L#}) D"
  shows "subsumes (C + {#L#}) D"
  using assms unfolding subsumes_def by simp

lemma simplify_preserves_already_used_inv:
  assumes "simplify N N'"
  and "already_used_inv (N, already_used)"
  shows "already_used_inv (N', already_used)"
  using assms
proof (induct rule: simplify.induct)
  case (condensation C L)
  thus ?case
    using subsumes_condensation by simp fast
next
  {
     fix a:: 'a and A :: "'a set" and P
     have "(\<exists>x \<in> Set.remove a A. P x) \<longleftrightarrow> (\<exists>x \<in> A. x \<noteq> a \<and> P x)" by auto
  } note ex_member_remove = this
  {
    fix a a0 :: "'v clause" and A :: "'v clauses" and  y
    have tt2: "a \<in> A \<Longrightarrow> a0 <# a\<Longrightarrow> (\<exists>x \<in> A. subsumes x y) \<longleftrightarrow> (subsumes a y  \<or> (\<exists>x \<in> A. x \<noteq> a \<and> subsumes x y))" by auto
  } note tt2 = this
  case (subsumption A B)
  thus ?case
    apply (simp only:fst_conv snd_conv tt2 ex_member_remove) apply clarify
    using subset_mset.less_imp_le subsumes_subsumption subsumption.hyps(2) Un_insert_right insert_iff by fast
next
  case (tautology_deletion C P)
  thus ?case apply clarify
  proof -
    fix a b
    assume "C + {#Pos P#} + {#Neg P#} \<in> N"
    assume "already_used_inv (N, already_used)"
    and "(a, b) \<in> snd (N - {C + {#Pos P#} + {#Neg P#}}, already_used)"
    then obtain p where "Pos p \<in># a \<and> Neg p \<in># b \<and> ((\<exists>\<chi>\<in>fst (N \<union> {C + {#Pos P#} + {#Neg P#}}, already_used). subsumes \<chi> (a - {#Pos p#} + (b - {#Neg p#}))) \<or> tautology (a - {#Pos p#} + (b - {#Neg p#})))"
      by fastforce
    also have "tautology (C + {#Pos P#} + {#Neg P#})" by auto
    ultimately show " \<exists>p. Pos p \<in># a \<and> Neg p \<in># b \<and> ((\<exists>\<chi>\<in>fst (N - {C + {#Pos P#} + {#Neg P#}}, already_used). subsumes \<chi> (a - {#Pos p#} + (b - {#Neg p#}))) \<or> tautology (a - {#Pos p#} + (b - {#Neg p#})))"
       by (metis (no_types) Diff_iff Un_insert_right empty_iff fst_conv insertE subsumes_tautology sup_bot.right_neutral)
  qed
qed


lemma factoring_satisfiable: "I \<Turnstile> {#L#} + {#L#} + C \<longleftrightarrow> I \<Turnstile> {#L#} + C"
  and resolution_satisfiable: "consistent_interp I \<Longrightarrow> I \<Turnstile> {#Pos p#} + C \<Longrightarrow> I \<Turnstile> {#Neg p#} + D \<Longrightarrow> I \<Turnstile> C + D"
  and factoring_same_vars: "atms_of ({#L#} + {#L#} + C) = atms_of ({#L#} + C)"
  unfolding true_cls_def consistent_interp_def by (auto split: split_if_asm)

lemma inference_increasing:
  assumes "inference S S'" and "\<psi> \<in> fst S"
  shows "\<psi> \<in> fst S'"
  using assms by (induct rule: inference.induct, auto)

lemma rtranclp_inference_increasing:
  assumes "rtranclp inference S S'" and "\<psi> \<in> fst S"
  shows "\<psi> \<in> fst S'"
  using assms by (induct rule: rtranclp.induct, auto simp add: inference_increasing)

lemma inference_clause_already_used_increasing:
  assumes "inference_clause S S'"
  shows "snd S \<subseteq> snd S'"
  using assms by (induct rule:inference_clause.induct, auto)


lemma inference_already_used_increasing:
  assumes "inference S S'"
  shows "snd S \<subseteq> snd S'"
  using assms apply (induct rule:inference.induct)
  using inference_clause_already_used_increasing by fastforce

lemma inference_clause_preserves_un_sat:
  fixes N N' :: "'v clauses"
  assumes "inference_clause T T'"
  and  "total_over_m I (fst T)"
  and consistent: "consistent_interp I"
  shows "I \<Turnstile>s fst T \<longleftrightarrow> I \<Turnstile>s fst T \<union> {fst T'}"
  using assms apply (induct rule: inference_clause.induct)
  unfolding consistent_interp_def true_clss_def by auto force+


lemma inference_preserves_un_sat:
  fixes N N' :: "'v clauses"
  assumes "inference T T'"
  and  "total_over_m I (fst T)"
  and consistent: "consistent_interp I"
  shows "I \<Turnstile>s fst T \<longleftrightarrow> I \<Turnstile>s fst T'"
  using assms apply (induct rule: inference.induct)
  using inference_clause_preserves_un_sat by fastforce

lemma inference_clause_preserves_atms_of_m:
  assumes "inference_clause S S'"
  shows "atms_of_m (fst (fst S \<union> {fst S'}, snd S')) \<subseteq> atms_of_m (fst S)"
  using assms apply (induct rule: inference_clause.induct)
   apply auto
     apply (metis Set.set_insert UnCI atms_of_m_insert atms_of_plus)
    apply (metis Set.set_insert UnCI atms_of_m_insert atms_of_plus)
   apply (simp add: in_m_in_literals union_assoc)
  unfolding atms_of_m_def using assms by fastforce

lemma inference_preserves_atms_of_m:
  fixes N N' :: "'v clauses"
  assumes "inference T T'"
  shows "atms_of_m (fst T') \<subseteq> atms_of_m (fst T)"
  using assms apply (induct rule: inference.induct)
  using inference_clause_preserves_atms_of_m by fastforce

lemma inference_preserves_total:
  fixes N N' :: "'v clauses"
  assumes "inference (N, already_used) (N', already_used')"
  shows "total_over_m I N \<Longrightarrow> total_over_m I N'"
  using assms inference_preserves_atms_of_m unfolding total_over_m_def total_over_set_def by fastforce


lemma rtranclp_inference_preserves_total:
  assumes "rtranclp inference T T'"
  shows "total_over_m I (fst T) \<Longrightarrow> total_over_m I (fst T')"
  using assms by (induct rule: rtranclp.induct, auto simp add: inference_preserves_total)

lemma rtranclp_inference_preserves_un_sat:
  assumes "rtranclp inference N N'"
  and  "total_over_m I (fst N)"
  and consistent: "consistent_interp I"
  shows "I \<Turnstile>s fst N \<longleftrightarrow> I \<Turnstile>s fst N'"
  using assms apply (induct rule: rtranclp.induct)
  apply (simp add: inference_preserves_un_sat)
  using inference_preserves_un_sat rtranclp_inference_preserves_total by blast

lemma inference_preserves_finite:
  assumes "inference \<psi> \<psi>'" and "finite (fst \<psi>)"
  shows "finite (fst \<psi>')"
  using assms by (induct rule: inference.induct, auto simp add: simplifier_preserves_finite)


lemma inference_clause_preserves_finite_snd:
  assumes "inference_clause \<psi> \<psi>'" and "finite (snd \<psi>)"
  shows "finite (snd \<psi>')"
  using assms by (induct rule: inference_clause.induct, auto)


lemma inference_preserves_finite_snd:
  assumes "inference \<psi> \<psi>'" and "finite (snd \<psi>)"
  shows "finite (snd \<psi>')"
  using assms inference_clause_preserves_finite_snd by (induct rule: inference.induct, fastforce)


lemma rtranclp_inference_preserves_finite:
  assumes "rtranclp inference \<psi> \<psi>'" and "finite (fst \<psi>)"
  shows "finite (fst \<psi>')"
  using assms by (induct rule: rtranclp.induct, auto simp add: simplifier_preserves_finite inference_preserves_finite)

lemma consistent_interp_insert:
  assumes "consistent_interp I"
  and "atm_of P \<notin> atm_of ` I"
  shows "consistent_interp (insert P I)"
proof -
  have P: "insert P I = I \<union> {P}" by auto
  show ?thesis unfolding P
  apply (rule consistent_interp_disjoint)
  using assms by (auto simp add: atms_of_s_def)
qed

lemma simplify_clause_preserves_sat:
  assumes simp: "simplify \<psi> \<psi>'"
  and "satisfiable \<psi>'"
  shows "satisfiable \<psi>"
  using assms
proof (induction)
  case (tautology_deletion A P) note AP = this(1) and sat = this(2)
  let ?A' = "A + {#Pos P#} + {#Neg P#}"
  let ?\<psi>' = "\<psi> - {?A'}"
  obtain I where I: "I \<Turnstile>s ?\<psi>'" and cons: "consistent_interp I"  and tot: "total_over_m I ?\<psi>'" using sat unfolding satisfiable_def by auto
  { assume "Pos P \<in> I \<or> Neg P \<in> I"
    hence "I \<Turnstile> ?A'" by auto
    hence "I \<Turnstile>s \<psi>" using I by (metis insert_Diff tautology_deletion.hyps true_clss_insert)
    hence "?case" using cons tot by auto
  }
  also {
    assume Pos: "Pos P \<notin> I" and Neg: "Neg P \<notin> I"
    hence "consistent_interp (I \<union> {Pos P})" using cons by simp
    also have I'A: "I \<union> {Pos P} \<Turnstile> ?A'" by auto
    have "{Pos P} \<union> I \<Turnstile>s \<psi> - {A + {#Pos P#} + {#Neg P#}}"
      using `I \<Turnstile>s \<psi> - {A + {#Pos P#} + {#Neg P#}}` true_clss_union_increase' by blast
    hence "I \<union> {Pos P} \<Turnstile>s \<psi>"
      by (metis (no_types) Un_empty_right Un_insert_left Un_insert_right I'A insert_Diff sup_bot.left_neutral tautology_deletion.hyps true_clss_insert) (* 60 ms *)
    ultimately have ?case using satisfiable_carac' by blast
  }
  ultimately show ?case by blast
next
  case (condensation A L) note AL = this(1) and sat = this(2)
  have f3: "simplify \<psi> (\<psi> - {A + {#L#} + {#L#}} \<union> {A + {#L#}})"
    using AL simplify.condensation by blast
  obtain LL :: "'a literal multiset set \<Rightarrow> 'a literal set" where
    f4: "LL (\<psi> - {A + {#L#} + {#L#}} \<union> {A + {#L#}}) \<Turnstile>s \<psi> - {A + {#L#} + {#L#}} \<union> {A + {#L#}} \<and> consistent_interp (LL (\<psi> - {A + {#L#} + {#L#}} \<union> {A + {#L#}})) \<and> total_over_m (LL (\<psi> - {A + {#L#} + {#L#}} \<union> {A + {#L#}})) (\<psi> - {A + {#L#} + {#L#}} \<union> {A + {#L#}})"
    using sat by (meson satisfiable_def)
  have f5: "insert (A + {#L#} + {#L#}) (\<psi> - {A + {#L#} + {#L#}}) = \<psi>"
    using AL by fastforce
  have "atms_of (A + {#L#} + {#L#}) = atms_of ({#L#} + A)"
    by simp
  thus ?case
    using f5 f4 f3 by (metis (no_types) add.commute satisfiable_def simplifier_preserves_un_sat' total_over_m_insert total_over_m_union)
next
  case (subsumption A B) note A = this(1) and AB = this(2) and B = this(3) and sat = this(4)
  let ?\<psi>' = "\<psi> - {B}"
  obtain I where I: "I \<Turnstile>s ?\<psi>'" and cons: "consistent_interp I"  and tot: "total_over_m I ?\<psi>'" using sat unfolding satisfiable_def by auto
  have "I \<Turnstile> A" using A I by (metis AB Diff_iff subset_mset.less_irrefl singletonD true_clss_def)
  hence "I \<Turnstile> B" using AB subset_mset.less_imp_le subsumption_imp_eval by blast
  hence "I \<Turnstile>s \<psi>" using I by (metis insert_Diff_single true_clss_insert)
  thus ?case using cons satisfiable_carac' by blast
qed

lemma simplifier_preserves_unsat:
  assumes "inference \<psi> \<psi>'"
  shows "satisfiable (fst \<psi>') \<longrightarrow> satisfiable (fst \<psi>)"
  using assms apply (induct rule: inference.induct)
  using satisfiable_decreasing by (metis fst_conv)+

lemma inference_preserves_unsat:
  assumes "inference\<^sup>*\<^sup>* S S'"
  shows "satisfiable (fst S') \<longrightarrow>  satisfiable (fst S)"
  using assms apply (induct rule: rtranclp.induct)
  apply simp_all
  using simplifier_preserves_unsat by blast

datatype 'v sem_tree = Node "'v" "'v sem_tree" "'v sem_tree" | Leaf

fun sem_tree_size :: "'v sem_tree \<Rightarrow> nat" where
"sem_tree_size (Leaf) = 0" |
"sem_tree_size (Node _ ag ad) = 1 + sem_tree_size ag + sem_tree_size ad"

lemma sem_tree_size[case_names bigger]:
  "(\<And>xs:: 'v sem_tree. (\<And>ys:: 'v sem_tree. sem_tree_size ys < sem_tree_size xs \<Longrightarrow> P ys) \<Longrightarrow> P xs) \<Longrightarrow> P xs"
  by (fact Nat.measure_induct_rule)


fun partial_interps :: "'v sem_tree \<Rightarrow> 'v interp \<Rightarrow> 'v clauses \<Rightarrow> bool" where
"partial_interps Leaf I \<psi> = (\<exists>\<chi>. \<not> I \<Turnstile> \<chi> \<and> \<chi> \<in> \<psi> \<and> total_over_m I {\<chi>})" |
"partial_interps (Node v ag ad) I \<psi> = (partial_interps ag (I \<union> {Pos v}) \<psi> \<and> partial_interps ad (I\<union> {Neg v}) \<psi>)"


lemma simplifier_preserve_partial_leaf:
  "simplify N N' \<Longrightarrow> partial_interps Leaf I N \<Longrightarrow> partial_interps Leaf I N'"
  apply (induct rule: simplify.induct)
    using union_lcomm apply auto[1]
   apply (simp, metis atms_of_plus total_over_set_union true_cls_union)
  apply simp
  by (metis atms_of_m_singleton mset_le_exists_conv subset_mset_def subsumption_imp_eval total_over_m_def total_over_m_sum)


lemma simplifier_preserve_partial_tree:
  assumes "simplify N N'"
  and "partial_interps t I N"
  shows "partial_interps t I N'"
  using assms apply (induct t arbitrary: I, simp)
  using  simplifier_preserve_partial_leaf by metis


lemma inference_preserve_partial_tree:
  assumes "inference S S'"
  and "partial_interps t I (fst S)"
  shows "partial_interps t I (fst S')"
  using assms apply (induct t arbitrary: I, simp_all)
  by (meson inference_increasing)


lemma rtranclp_inference_preserve_partial_tree:
  assumes "rtranclp inference N N'"
  and "partial_interps t I (fst N)"
  shows "partial_interps t I (fst N')"
  using assms apply (induct rule: rtranclp.induct, auto)
  using inference_preserve_partial_tree by force


function build_sem_tree :: "'v :: linorder set  \<Rightarrow> 'v clauses \<Rightarrow> 'v sem_tree" where
"build_sem_tree atms \<psi> =
  (if atms = {} \<or> \<not> finite atms
  then Leaf
  else Node (Min atms) (build_sem_tree (Set.remove (Min atms) atms) \<psi>)
     (build_sem_tree (Set.remove (Min atms) atms) \<psi>))"
by auto
termination
  apply (relation "measure (\<lambda>(A, _).  card A)", simp_all)
  apply (metis Min_in card_Diff1_less remove_def)+
done
declare build_sem_tree.induct[case_names tree]

lemma unsatisfiable_empty[simp]:
  "\<not>unsatisfiable {}"
   unfolding satisfiable_def apply auto
  using consistent_interp_def unfolding total_over_m_def total_over_set_def atms_of_m_def by blast

lemma partial_interps_build_sem_tree_atms_general:
  fixes \<psi> :: "'v :: linorder clauses" and p :: "'v literal list"
  assumes unsat: "unsatisfiable \<psi>" and "finite \<psi>" and "consistent_interp I"
  and "finite atms"
  and "atms_of_m \<psi> = atms \<union> atms_of_s I" and "atms \<inter> atms_of_s I = {}"
  shows "partial_interps (build_sem_tree atms \<psi>) I \<psi>"
  using assms
proof (induct arbitrary: I rule: build_sem_tree.induct)
  case (1 atms \<psi> Ia) note IH1 = this(1) and IH2 = this(2) and unsat = this(3) and finite = this(4) and cons = this(5)  and f = this(6) and un = this(7) and disj = this(8)
  {
    assume atms: "atms = {}"
    hence atmsIa: "atms_of_m \<psi> = atms_of_s Ia" using un by auto
    hence "total_over_m Ia \<psi>" unfolding total_over_m_def atmsIa by auto
    hence \<chi>: "\<exists>\<chi> \<in> \<psi>. \<not> Ia \<Turnstile> \<chi>" using unsat cons unfolding true_clss_def satisfiable_def by auto
    hence "build_sem_tree atms \<psi> = Leaf" using atms by auto
    also
      have tot: "\<And>\<chi>. \<chi> \<in> \<psi> \<Longrightarrow> total_over_m Ia {\<chi>}"
      unfolding total_over_m_def total_over_set_def atms_of_m_def atms_of_s_def using atmsIa atms_of_m_def by fastforce
    have "partial_interps Leaf Ia \<psi>"
      using \<chi> tot by (auto simp add: total_over_m_def total_over_set_def atms_of_m_def)

      ultimately have ?case by metis
  }
  also {
    assume atms: "atms \<noteq> {}"
    have "build_sem_tree atms \<psi> = Node (Min atms) (build_sem_tree (Set.remove (Min atms) atms) \<psi>) (build_sem_tree (Set.remove (Min atms) atms) \<psi>)"
      using build_sem_tree.simps[of "atms" \<psi>] f atms by metis

    have "consistent_interp (Ia \<union> {Pos (Min atms)})" unfolding consistent_interp_def
      by (metis Int_iff Min_in Un_iff atm_of_uminus atms cons consistent_interp_def disj empty_iff f in_atms_of_s_decomp insert_iff literal.distinct(1) literal.exhaust_sel literal.sel(2) uminus_Neg uminus_Pos)
    also have "atms_of_m \<psi> = Set.remove (Min atms) atms \<union> atms_of_s (Ia \<union> {Pos (Min atms)})"
      using Min_in atms f un by fastforce
    moreover have disj': "Set.remove (Min atms) atms \<inter> atms_of_s (Ia \<union> {Pos (Min atms)}) = {}"
      by simp (metis disj disjoint_iff_not_equal member_remove)
    moreover have "finite (Set.remove (Min atms) atms)" using f by (simp add: remove_def)
    ultimately have subtree1: "partial_interps (build_sem_tree (Set.remove (Min atms) atms) \<psi>) (Ia \<union> {Pos (Min atms)}) \<psi>"  using IH1[of "Ia \<union> {Pos (Min (atms))}"]  atms f unsat finite by metis

    have "consistent_interp (Ia \<union> {Neg (Min atms)})" unfolding consistent_interp_def
      by (metis Int_iff Min_in Un_iff atm_of_uminus atms cons consistent_interp_def disj empty_iff f in_atms_of_s_decomp insert_iff literal.distinct(1) literal.exhaust_sel literal.sel(2) uminus_Neg)
    also have "atms_of_m \<psi> = Set.remove (Min atms) atms \<union> atms_of_s (Ia \<union> {Neg (Min atms)})"
      using `atms_of_m \<psi> = Set.remove (Min atms) atms \<union> atms_of_s (Ia \<union> {Pos (Min atms)})` by blast

    moreover have disj': "Set.remove (Min atms) atms \<inter> atms_of_s (Ia \<union> {Neg (Min atms)}) = {}"
      using disj by auto
    moreover have "finite (Set.remove (Min atms) atms)" using f by (simp add: remove_def)
    ultimately have subtree2: "partial_interps (build_sem_tree (Set.remove (Min atms) atms) \<psi>) (Ia \<union> {Neg (Min atms)}) \<psi>"  using IH2[of "Ia \<union> {Neg (Min (atms))}"]  atms f unsat finite by metis

    hence ?case
      using IH1 subtree1 subtree2 f local.finite unsat atms by simp
  }
  ultimately show ?case by metis
qed


lemma partial_interps_build_sem_tree_atms:
  fixes \<psi> :: "'v :: linorder clauses" and p :: "'v literal list"
  assumes unsat: "unsatisfiable \<psi>" and finite: "finite \<psi>"
  shows "partial_interps (build_sem_tree (atms_of_m \<psi>) \<psi>) {} \<psi>"
proof -
  have "consistent_interp {}" unfolding consistent_interp_def by auto
  also have "atms_of_m \<psi> = atms_of_m \<psi> \<union> atms_of_s {}" unfolding atms_of_s_def by auto
  moreover have "atms_of_m \<psi> \<inter> atms_of_s {} = {} " unfolding atms_of_s_def by auto
  moreover have "finite (atms_of_m \<psi>)" unfolding atms_of_m_def using finite by simp
  ultimately show "partial_interps (build_sem_tree (atms_of_m \<psi>) \<psi>) {} \<psi>"  using partial_interps_build_sem_tree_atms_general[of "\<psi>" "{}" "atms_of_m \<psi>"] assms by metis
qed

lemma can_decrease_count:
  fixes \<psi>'' :: "'v clauses \<times> ('v clause \<times> 'v clause \<times> 'v) set"
  assumes "count \<chi> L = n"
  and "L \<in># \<chi>" and "\<chi> \<in> fst \<psi>"
  shows "\<exists>\<psi>' \<chi>'. inference\<^sup>*\<^sup>* \<psi> \<psi>' \<and> \<chi>' \<in> fst \<psi>' \<and> (\<forall>L. L :# \<chi> \<longleftrightarrow> L :# \<chi>') \<and> count \<chi>' L = 1 \<and> (\<forall>\<phi>. \<phi> \<in> fst \<psi> \<longrightarrow> \<phi> \<in> fst \<psi>') \<and> (I \<Turnstile> \<chi> \<longleftrightarrow> I \<Turnstile> \<chi>') \<and> (\<forall>I'. total_over_m I' {\<chi>} \<longrightarrow> total_over_m I' {\<chi>'})"
  using assms
proof (induct n arbitrary: \<chi> \<psi>)
  case 0
  thus ?case by simp
next
   case (Suc n \<chi>)
   note IH = this(1) and count = this(2) and L = this(3) and \<chi> = this(4)
   {
     assume "n = 0"
     hence "inference\<^sup>*\<^sup>* \<psi> \<psi>"
     and "\<chi> \<in> fst \<psi>"
     and "\<forall>L. (L \<in># \<chi>) \<longleftrightarrow> (L \<in># \<chi>)"
     and "count \<chi> L = (1::nat)"
     and "\<forall>\<phi>. \<phi> \<in> fst \<psi> \<longrightarrow> \<phi> \<in> fst \<psi>"
       by (auto simp add: count L \<chi>)
     hence ?case by metis
   }
   also {
     assume "n > 0"
     hence "\<exists>C. \<chi> = C + {#L, L#}" by (metis L One_nat_def add_diff_cancel_right' count_diff count_single diff_Suc_Suc diff_zero local.count multi_member_split union_assoc)
     then obtain C where C: "\<chi> = C + {#L, L#}" by metis
     let ?\<chi>' = "C +{#L#}"
     let ?\<psi>' = "(fst \<psi> \<union> {?\<chi>'}, snd \<psi>)"
     have \<phi>: "\<forall>\<phi> \<in> fst \<psi>. (\<phi> \<in> fst \<psi> \<or> \<phi> \<noteq> ?\<chi>') \<longleftrightarrow> \<phi> \<in> fst ?\<psi>'" unfolding C by auto
     have inf: "inference \<psi> ?\<psi>'" using C factoring \<chi> prod.collapse union_commute inference_step by metis
     also have count': "count ?\<chi>' L = n" using C count by auto
     moreover have L\<chi>': "L :# ?\<chi>'" by auto
     moreover have \<chi>'\<psi>': "?\<chi>' \<in> fst ?\<psi>'" by auto
     ultimately obtain \<psi>''  and \<chi>''
     where "inference\<^sup>*\<^sup>* ?\<psi>' \<psi>''" and \<alpha>: "\<chi>'' \<in> fst \<psi>''" and "\<forall>La. (La \<in># ?\<chi>') \<longleftrightarrow> (La \<in># \<chi>'')" and \<beta>: "count \<chi>'' L = (1::nat)" and \<phi>': "\<forall>\<phi>. \<phi> \<in> fst ?\<psi>' \<longrightarrow> \<phi> \<in> fst \<psi>''"
     and I\<chi>: "I \<Turnstile> ?\<chi>' \<longleftrightarrow> I \<Turnstile> \<chi>''" and tot: "\<forall>I'. total_over_m I' {?\<chi>'} \<longrightarrow> total_over_m I' {\<chi>''}" using IH[of ?\<chi>' ?\<psi>'] count' L\<chi>' \<chi>'\<psi>' by blast

     hence "inference\<^sup>*\<^sup>* \<psi> \<psi>''"
     and "\<forall>La. (La \<in># \<chi>) \<longleftrightarrow> (La \<in># \<chi>'')"
     using inf unfolding C by auto
     also have "\<forall>\<phi>. \<phi> \<in> fst \<psi> \<longrightarrow> \<phi> \<in> fst \<psi>''" using \<phi> \<phi>' by metis
     moreover have "I \<Turnstile> \<chi> \<longleftrightarrow> I \<Turnstile> \<chi>''" using I\<chi> unfolding true_cls_def C by auto
     moreover have "\<forall>I'. total_over_m I' {\<chi>} \<longrightarrow> total_over_m I' {\<chi>''}" using tot unfolding C total_over_m_def by auto
     ultimately have ?case using \<phi> \<phi>' \<alpha> \<beta>  by metis
  }
  ultimately show ?case by auto
qed
(*TODO Move*)
lemma true_cls_not_in_remove:
  assumes "L \<notin># \<chi>"
  and " I \<union> {L} \<Turnstile> \<chi>"
  shows "I \<Turnstile> \<chi>"
  using assms unfolding true_cls_def true_lit_def by auto

lemma can_decrease_tree_size:
  fixes \<psi> :: "'v state" and tree :: "'v sem_tree"
  assumes "finite (fst \<psi>)"  and "already_used_inv \<psi>"
  and "partial_interps tree I (fst \<psi>)"
  shows "\<exists>(tree':: 'v sem_tree) \<psi>'. inference\<^sup>*\<^sup>* \<psi> \<psi>' \<and> partial_interps tree' I (fst \<psi>') \<and> (sem_tree_size tree' < sem_tree_size tree \<or> sem_tree_size tree = 0)"
  using assms
proof (induct arbitrary: I rule: sem_tree_size)
  case (bigger xs I) note IH = this(1) and finite = this(2) and a_u_i = this(3) and part = this(4)

  {
    assume "sem_tree_size xs = 0"
    hence "?case" using part by blast
  }

  also {
    assume sn0: "sem_tree_size xs > 0"
    obtain ag ad v where xs: "xs = Node v ag ad" using sn0 by (case_tac xs, auto)
    {
       assume "sem_tree_size ag = 0" and "sem_tree_size ad = 0"
       hence ag: "ag = Leaf" and ad: "ad = Leaf" by (case_tac ag,  auto) (case_tac ad,  auto)

       then obtain \<chi> \<chi>' where
         \<chi>: "\<not> I \<union> {Pos v} \<Turnstile> \<chi>" and
         tot\<chi>: "total_over_m (I \<union> {Pos v}) {\<chi>}" and
         \<chi>\<psi>: "\<chi> \<in> fst \<psi>" and
         \<chi>': "\<not> I \<union> {Neg v} \<Turnstile> \<chi>'"  and
         tot\<chi>': "total_over_m (I \<union> {Neg v}) {\<chi>'}" and
         \<chi>'\<psi>: "\<chi>' \<in> fst \<psi>"
         using part unfolding xs by auto
       have Posv: "\<not>Pos v \<in># \<chi>" using \<chi> unfolding true_cls_def true_lit_def by auto
       have Negv: "\<not>Neg v \<in># \<chi>'" using \<chi>' unfolding true_cls_def true_lit_def by auto
       {
         assume Neg\<chi>: "\<not>Neg v \<in># \<chi>"
         hence "\<not> I \<Turnstile> \<chi>" using \<chi> Posv unfolding true_cls_def true_lit_def by blast
         also have "total_over_m I {\<chi>}" using Posv Neg\<chi> atm_imp_pos_or_neg_lit tot\<chi> unfolding total_over_m_def total_over_set_def by fastforce
         ultimately have "partial_interps Leaf I (fst \<psi>)"
         and "sem_tree_size Leaf < sem_tree_size xs"
         and "inference\<^sup>*\<^sup>* \<psi> \<psi>"
           unfolding xs by (auto simp add: \<chi>\<psi>)
       }
       also {
          assume Pos\<chi>: "\<not>Pos v \<in># \<chi>'"
          hence I\<chi>: "\<not> I \<Turnstile> \<chi>'" using \<chi>' Posv unfolding true_cls_def true_lit_def by blast
          also have "total_over_m I {\<chi>'}" using Negv Pos\<chi> atm_imp_pos_or_neg_lit tot\<chi>' unfolding total_over_m_def total_over_set_def by fastforce
          ultimately have  "partial_interps Leaf I (fst \<psi>)"
          and "sem_tree_size Leaf < sem_tree_size xs"
          and "inference\<^sup>*\<^sup>* \<psi> \<psi>" using \<chi>'\<psi> I\<chi> unfolding xs by auto
       }
       moreover {
          assume neg: "Neg v \<in># \<chi>" and pos: "Pos v \<in># \<chi>'"
          then obtain \<psi>' \<chi>2 where inf: "rtranclp inference \<psi> \<psi>'" and \<chi>2incl: "\<chi>2 \<in> fst \<psi>'"
            and \<chi>\<chi>2_incl: "\<forall>L. L :# \<chi> \<longleftrightarrow> L :# \<chi>2"
            and count\<chi>2: "count \<chi>2 (Neg v) = 1"
            and \<phi>: "\<forall>\<phi>::'v literal multiset. \<phi> \<in> fst \<psi> \<longrightarrow> \<phi> \<in> fst \<psi>'"
            and I\<chi>: "I \<Turnstile> \<chi> \<longleftrightarrow> I \<Turnstile> \<chi>2"
            and tot_imp\<chi>: "\<forall>I'. total_over_m I' {\<chi>} \<longrightarrow> total_over_m I' {\<chi>2}"
            using can_decrease_count[of \<chi> "Neg v" "count \<chi> (Neg v)" \<psi> I] \<chi>\<psi> \<chi>'\<psi> by auto

          have "\<chi>' \<in> fst \<psi>'" by (simp add: \<chi>'\<psi> \<phi>)
          with pos
          obtain \<psi>'' \<chi>2' where
          inf': "inference\<^sup>*\<^sup>* \<psi>' \<psi>''"
          and \<chi>2'_incl: "\<chi>2' \<in> fst \<psi>''"
          and \<chi>'\<chi>2_incl: "\<forall>L::'v literal. (L \<in># \<chi>') = (L \<in># \<chi>2')"
          and count\<chi>2': "count \<chi>2' (Pos v) = (1::nat)"
          and \<phi>': "\<forall>\<phi>::'v literal multiset. \<phi> \<in> fst \<psi>' \<longrightarrow> \<phi> \<in> fst \<psi>''"
          and I\<chi>': "I \<Turnstile> \<chi>' \<longleftrightarrow> I \<Turnstile> \<chi>2'"
          and tot_imp\<chi>': "\<forall>I'. total_over_m I' {\<chi>'} \<longrightarrow> total_over_m I' {\<chi>2'}"
          using can_decrease_count[of \<chi>' "Pos v" " count \<chi>' (Pos v)" \<psi>' I] by auto

          obtain C where \<chi>2: "\<chi>2 = C + {#Neg v#}" and negC: "Neg v \<notin># C" and posC: "Pos v \<notin># C"
            by (metis (no_types, lifting) One_nat_def Posv Suc_inject Suc_pred \<chi>\<chi>2_incl count\<chi>2 count_diff count_single gr0I insert_DiffM insert_DiffM2 multi_member_skip old.nat.distinct(2))

          obtain C' where \<chi>2': "\<chi>2' = C' + {#Pos v#}" and posC': "Pos v \<notin># C'"  and negC': "Neg v \<notin># C'"
            proof -
              assume a1: "\<And>C'. \<lbrakk>\<chi>2' = C' + {#Pos v#}; Pos v \<notin># C'; Neg v \<notin># C'\<rbrakk> \<Longrightarrow> thesis"
              have f2: "\<And>n. (n::nat) - n = 0"
                by simp (* 0.2 ms *)
              have "Neg v \<notin># \<chi>2' - {#Pos v#}"
                using Negv \<chi>'\<chi>2_incl by auto (* 14 ms *)
              thus ?thesis
                using f2 a1 by (metis add.commute count\<chi>2' count_diff count_single insert_DiffM less_nat_zero_code zero_less_one) (* 41 ms *)
            qed

          have "already_used_inv \<psi>'"
            using rtranclp_inference_preserves_already_used_inv[of \<psi> \<psi>'] a_u_i inf by blast
          hence a_u_i_\<psi>'': "already_used_inv \<psi>''"
            using rtranclp_inference_preserves_already_used_inv a_u_i inf' unfolding tautology_def
            by simp

          have totC: "total_over_m I {C}" using tot_imp\<chi> tot\<chi> tot_over_m_remove[of I "Pos v" C] negC posC unfolding \<chi>2 by (metis total_over_m_sum uminus_Neg uminus_of_uminus_id)
          have totC': "total_over_m I {C'}" using tot_imp\<chi>' tot\<chi>' total_over_m_sum tot_over_m_remove[of I "Neg v" C'] negC' posC' unfolding \<chi>2' by (metis total_over_m_sum uminus_Neg)
          have "\<not> I \<Turnstile> C + C'" using \<chi> I\<chi> \<chi>' I\<chi>' unfolding \<chi>2 \<chi>2' true_cls_def unfolding true_cls_def by (metis add_gr_0 count_union true_cls_singleton true_cls_union_increase)
          hence part_I_\<psi>''': "partial_interps Leaf I (fst \<psi>'' \<union> {C + C'})"
            using totC totC' by simp (metis `\<not> I \<Turnstile> C + C'` atms_of_m_singleton total_over_m_def total_over_m_sum)
          {
            assume "({#Pos v#} + C', {#Neg v#} + C) \<notin> snd \<psi>''"
            hence inf'': " inference \<psi>'' (fst \<psi>'' \<union> {C + C'}, snd \<psi>'' \<union> {(\<chi>2', \<chi>2)})" using  add.commute \<phi>' \<chi>2incl `\<chi>2' \<in> fst \<psi>''`  unfolding \<chi>2 \<chi>2' by (metis prod.collapse inference_step resolution)
            have "inference\<^sup>*\<^sup>* \<psi> (fst \<psi>'' \<union> {C + C'}, snd \<psi>'' \<union> {(\<chi>2', \<chi>2)})" using inf inf' inf''  rtranclp_trans by auto
            moreover have "sem_tree_size Leaf < sem_tree_size xs" unfolding xs by auto
            ultimately have "?case" using part_I_\<psi>''' by (metis fst_conv)
          }
          also {
            assume a: "({#Pos v#} + C', {#Neg v#} + C) \<in> snd \<psi>''"
            hence "(\<exists>\<chi> \<in> fst \<psi>''. (\<forall>I. total_over_m I {C+C'} \<longrightarrow> total_over_m I {\<chi>} ) \<and> (\<forall>I. total_over_m I {\<chi>} \<longrightarrow> I \<Turnstile> \<chi> \<longrightarrow> I \<Turnstile> C' + C)) \<or> tautology (C' + C)"
              proof -
                obtain p where p: "Pos p \<in># ({#Pos v#} + C')" and
                n: "Neg p \<in># ({#Neg v#} + C)" and
                decomp: "((\<exists>\<chi>\<in>fst \<psi>''. (\<forall>I. total_over_m I {({#Pos v#} + C') - {#Pos p#} + (({#Neg v#} + C) - {#Neg p#})} \<longrightarrow> total_over_m I {\<chi>}) \<and> (\<forall>I. total_over_m I {\<chi>} \<longrightarrow> I \<Turnstile> \<chi> \<longrightarrow> I \<Turnstile> ({#Pos v#} + C') - {#Pos p#} + (({#Neg v#} + C) - {#Neg p#}))) \<or> tautology (({#Pos v#} + C') - {#Pos p#} + (({#Neg v#} + C) - {#Neg p#})))"
                  using a by (blast intro: allE[OF a_u_i_\<psi>''[unfolded subsumes_def Ball_def], of "({#Pos v#} + C', {#Neg v#} + C)"])
                { assume "p \<noteq> v"
                  hence "Pos p \<in># C' \<and> Neg p \<in># C" using p n by force
                  hence ?thesis by (metis add_gr_0 count_union tautology_Pos_Neg)
                }
                also {
                  assume "p = v"
                 hence "?thesis" using decomp by (metis add.commute add_diff_cancel_left')
                }
                ultimately show ?thesis by auto
              qed
            also {
              assume "\<exists>\<chi> \<in> fst \<psi>''. (\<forall>I. total_over_m I {C+C'} \<longrightarrow> total_over_m I {\<chi>}) \<and>(\<forall>I. total_over_m I {\<chi>} \<longrightarrow> I \<Turnstile> \<chi> \<longrightarrow> I \<Turnstile> C' + C)"
              then obtain \<theta> where \<theta>: "\<theta> \<in> fst \<psi>''" and tot_\<theta>_CC': "\<forall>I. total_over_m I {C+C'} \<longrightarrow> total_over_m I {\<theta>}" and \<theta>_inv: "\<forall>I. total_over_m I {\<theta>} \<longrightarrow> I \<Turnstile> \<theta> \<longrightarrow> I \<Turnstile> C' + C" by blast
              have "partial_interps Leaf I (fst \<psi>'')"using tot_\<theta>_CC' \<theta> \<theta>_inv totC totC'  `\<not> I \<Turnstile> C + C'` total_over_m_sum by fastforce
              moreover have "sem_tree_size Leaf < sem_tree_size xs" unfolding xs by auto
              ultimately have ?case by (metis inf inf' rtranclp_trans)
            }
            moreover {
              assume tautCC': "tautology (C' + C)"
              have "total_over_m I {C'+C}" using totC totC' total_over_m_sum by auto
              hence "\<not>tautology (C' + C)" using `\<not> I \<Turnstile> C + C'` unfolding add.commute[of C C'] total_over_m_def  unfolding tautology_def by auto
              hence False using tautCC'  unfolding tautology_def by auto
            }
            ultimately have ?case by auto
          }
          ultimately have ?case by auto
       }
       ultimately have ?case using part by (metis (no_types) sem_tree_size.simps(1))
     }
     also {
       assume size_ag: "sem_tree_size ag > 0"
       have "sem_tree_size ag < sem_tree_size xs" unfolding xs by auto
       also have "partial_interps ag (I \<union> {Pos v}) (fst \<psi>)"
       and partad: "partial_interps ad (I \<union> {Neg v}) (fst \<psi>)" using part partial_interps.simps(2) unfolding xs by metis+
       moreover have "sem_tree_size ag < sem_tree_size xs \<longrightarrow> finite (fst \<psi>) \<longrightarrow> already_used_inv \<psi> \<longrightarrow> ( partial_interps ag (I \<union> {Pos v}) (fst \<psi>) \<longrightarrow> (\<exists>tree' \<psi>'. inference\<^sup>*\<^sup>* \<psi> \<psi>' \<and> partial_interps tree' (I \<union> {Pos v}) (fst \<psi>') \<and> (sem_tree_size tree' < sem_tree_size ag \<or> sem_tree_size ag = 0)))" using IH by auto
       ultimately obtain \<psi>' :: "'v state" and  tree' :: "'v sem_tree"  where
         inf: "inference\<^sup>*\<^sup>* \<psi> \<psi>'"
         and part: "partial_interps tree' (I \<union> {Pos v}) (fst \<psi>')"
         and size: "sem_tree_size tree' < sem_tree_size ag \<or> sem_tree_size ag = 0" using finite part rtranclp.rtrancl_refl a_u_i by blast

       have "partial_interps ad (I \<union> {Neg v}) (fst \<psi>')" using rtranclp_inference_preserve_partial_tree inf partad by metis
       hence "partial_interps (Node v tree' ad) I (fst \<psi>')" using part by auto
       hence ?case using inf size size_ag part unfolding xs by fastforce
     }
     moreover {
       assume size_ad: "sem_tree_size ad > 0"
       have "sem_tree_size ad < sem_tree_size xs" unfolding xs by auto
       also have partag: "partial_interps ag (I \<union> {Pos v}) (fst \<psi>)"
       and "partial_interps ad (I \<union> {Neg v}) (fst \<psi>)" using part partial_interps.simps(2) unfolding xs by metis+
       moreover have "sem_tree_size ad < sem_tree_size xs \<longrightarrow> finite (fst \<psi>) \<longrightarrow> already_used_inv \<psi> \<longrightarrow> ( partial_interps ad (I \<union> {Neg v}) (fst \<psi>) \<longrightarrow> (\<exists>tree' \<psi>'. inference\<^sup>*\<^sup>* \<psi> \<psi>' \<and> partial_interps tree' (I \<union> {Neg v}) (fst \<psi>') \<and> (sem_tree_size tree' < sem_tree_size ad \<or> sem_tree_size ad = 0)))" using IH by auto
       ultimately obtain \<psi>' :: "'v state" and  tree' :: "'v sem_tree"  where
         inf: "inference\<^sup>*\<^sup>* \<psi> \<psi>'"
         and part: "partial_interps tree' (I \<union> {Neg v}) (fst \<psi>')"
         and size: "sem_tree_size tree' < sem_tree_size ad \<or> sem_tree_size ad = 0" using finite part  rtranclp.rtrancl_refl a_u_i by blast

       have "partial_interps ag (I \<union> {Pos v}) (fst \<psi>')" using rtranclp_inference_preserve_partial_tree inf partag by metis
       hence "partial_interps (Node v ag tree') I (fst \<psi>')" using part by auto
       hence "?case" using inf size size_ad unfolding xs by fastforce
     }
     ultimately have ?case by auto
  }
  ultimately show ?case by auto
qed

lemma inference_completeness_inv:
  fixes \<psi> :: "'v ::linorder state"
  assumes unsat: "\<not>satisfiable (fst \<psi>)" and finite: "finite (fst \<psi>)" and a_u_v: "already_used_inv \<psi>"
  shows "\<exists>\<psi>'. (inference\<^sup>*\<^sup>* \<psi> \<psi>' \<and> {#} \<in> fst \<psi>')"
proof -
  obtain tree where  "partial_interps tree {} (fst \<psi>)" using partial_interps_build_sem_tree_atms assms by metis
  thus ?thesis
    using unsat finite a_u_v
    proof (induct tree arbitrary: \<psi> rule: sem_tree_size)
      case (bigger tree \<psi>) note H = this
      {
        fix \<chi>
        assume tree: "tree = Leaf"
        obtain \<chi> where \<chi>: "\<not> {} \<Turnstile> \<chi>" and tot\<chi>: "total_over_m {} {\<chi>}" and \<chi>\<psi>: "\<chi> \<in> fst \<psi>" using H unfolding tree by auto
        also have "{#} = \<chi>"
          using H unfolding \<chi> total_over_m_def apply auto
          using atms_empty_iff_empty tot\<chi> unfolding true_cls_def total_over_m_def total_over_set_def by fastforce
        moreover have "inference\<^sup>*\<^sup>* \<psi> \<psi>" by auto
        ultimately have ?case by metis
      }
      also {
        fix v tree1 tree2
        assume tree: "tree = Node v tree1 tree2"
        obtain tree' \<psi>' where inf: "inference\<^sup>*\<^sup>* \<psi> \<psi>'"
        and part': "partial_interps tree' {} (fst \<psi>')"
        and decrease: "sem_tree_size tree' < sem_tree_size tree \<or> sem_tree_size tree = 0" using can_decrease_tree_size[of \<psi>] H(2,4,5)  unfolding tautology_def by meson
        have "sem_tree_size tree' < sem_tree_size tree" using decrease unfolding tree by auto
        also have "finite (fst \<psi>')" using rtranclp_inference_preserves_finite inf H(4) by metis
        moreover have "unsatisfiable (fst \<psi>')" using inference_preserves_unsat inf bigger.prems(2) by blast
        moreover have "already_used_inv \<psi>'" using H(5) inf rtranclp_inference_preserves_already_used_inv[of \<psi> \<psi>'] by auto
        ultimately have ?case using inf rtranclp_trans part' H(1) by fastforce
      }
      ultimately show ?case by (case_tac tree, auto)
   qed
qed

lemma inference_completeness:
  fixes \<psi> :: "'v ::linorder state"
  assumes unsat: "\<not>satisfiable (fst \<psi>)"
  and finite: "finite (fst \<psi>)"
  and "snd \<psi> = {}"
  shows "\<exists>\<psi>'. (rtranclp inference \<psi> \<psi>' \<and> {#} \<in> fst \<psi>')"
proof -
  have "already_used_inv \<psi>" unfolding assms by auto
  thus ?thesis using assms inference_completeness_inv by blast
qed

lemma inference_soundness:
  fixes \<psi> :: "'v ::linorder state"
  assumes "rtranclp inference \<psi> \<psi>'" and "{#} \<in> fst \<psi>'"
  shows "unsatisfiable (fst \<psi>)"
  using assms by (meson rtranclp_inference_preserves_un_sat satisfiable_def true_cls_empty true_clss_def)

lemma inference_soundness_and_completeness:
fixes \<psi> :: "'v ::linorder state"
assumes finite: "finite (fst \<psi>)"
and "snd \<psi> = {}"
shows "(\<exists>\<psi>'. (inference\<^sup>*\<^sup>* \<psi> \<psi>' \<and> {#} \<in> fst \<psi>')) \<longleftrightarrow> unsatisfiable (fst \<psi>)"
  using assms inference_completeness inference_soundness by metis

subsection \<open>Lemma about the simplified state\<close>

abbreviation "simplified \<psi> \<equiv> (\<forall>\<psi>'. \<not>simplify \<psi> \<psi>')"

lemma simplified_count:
  assumes simp: "simplified \<psi>" and \<chi>: "\<chi> \<in> \<psi>"
  shows "count \<chi> L \<le> 1"
proof -
  {
    let ?\<chi>' = "\<chi> - {#L, L#}"
    assume "count \<chi> L \<ge> 2"
    hence f1: "count (\<chi> - {#L, L#} + {#L, L#}) L = count \<chi> L"
      by simp (* 66 ms *)
    hence "L \<in># \<chi> - {#L#}"
      by simp (* 60 ms *)
    hence \<chi>': "?\<chi>' + {#L#} + {#L#} = \<chi>"
      using f1 by (metis (no_types) diff_diff_add diff_single_eq_union union_assoc union_single_eq_member)
    have "\<exists>\<psi>'. simplify \<psi> \<psi>'" by (metis (no_types, hide_lams) \<chi> \<chi>' add.commute factoring_imp_simplifier union_assoc)
    hence False using simp by auto
  }
  thus ?thesis by arith
qed

lemma simplified_no_both:
  assumes  simp: "simplified \<psi>" and \<chi>: "\<chi> \<in> \<psi>"
  shows "\<not> (L \<in># \<chi> \<and> -L \<in># \<chi>)"
proof (rule ccontr)
  assume "\<not> \<not> (L \<in># \<chi> \<and> - L \<in># \<chi>)"
  hence "L \<in># \<chi> \<and> - L \<in># \<chi>" by metis
  then obtain  \<chi>' where "\<chi> = \<chi>' + {#Pos (atm_of L)#}+ {#Neg (atm_of L)#}"
    by (metis Neg_atm_of_iff Pos_atm_of_iff diff_union_swap insert_DiffM2 uminus_Neg uminus_Pos)
  thus False using \<chi> simp tautology_deletion by fastforce
qed

lemma simplified_not_tautology:
  assumes "simplified {\<psi>}"
  shows "~tautology \<psi>"
proof (rule ccontr)
  assume "~ ?thesis"
  then obtain p where "Pos p \<in># \<psi> \<and> Neg p \<in># \<psi>" using tautology_decomp by metis
  then obtain \<chi> where "\<psi> = \<chi> + {#Pos p#} + {#Neg p#}" by (metis insert_noteq_member literal.distinct(1) multi_member_split)
  hence "~ simplified {\<psi>}" by (auto intro: tautology_deletion)
  thus False using assms by auto
qed

lemma simplified_remove:
  assumes "simplified {\<psi>}"
  shows "simplified {\<psi> - {#l#}}"
proof (rule ccontr)
  assume ns: "\<not> simplified {\<psi> - {#l#}}"
  {
    assume "\<not> l\<in># \<psi> "
    hence "\<psi> - {#l#} = \<psi>" by simp
    hence False using ns assms by auto
  }
  also {
    assume l\<psi>: "l\<in># \<psi>"
    have A: "\<And>A. A \<in> {\<psi> - {#l#}} \<longleftrightarrow> A + {#l#} \<in> {\<psi>} " by (auto simp add: l\<psi>)
    obtain l' where l': "simplify {\<psi> - {#l#}} l'" using ns by metis
    hence "\<exists>l'. simplify {\<psi>} l'"
      proof (induction rule: simplify.induct)
        case (tautology_deletion A P)
        have "{#Neg P#} + ({#Pos P#} + (A + {#l#})) \<in> {\<psi>}"
          by (metis (no_types) A add.commute tautology_deletion.hyps union_lcomm) (* 116 ms *)
        thus ?thesis
           by (metis simplify.tautology_deletion[of "A+{#l#}" P "{\<psi>}"] add.commute) (* 19 ms *)
       next
         case (condensation A L)
         have "A + {#L#} + {#L#} + {#l#} \<in> {\<psi>}"
           using A condensation.hyps by blast
         hence "{#L, L#} + (A + {#l#}) \<in> {\<psi>}"
           by (metis (no_types) union_assoc union_commute)
         thus ?case
           using factoring_imp_simplifier by blast
       next
         case (subsumption A B)
         thus ?case by blast
       qed
    hence False using assms(1) by blast
  }
  ultimately show False by auto
qed


lemma in_simplified_simplified:
  assumes simp: "simplified \<psi>" and incl: "\<psi>' \<subseteq> \<psi>"
  shows "simplified \<psi>'"
proof (rule ccontr)
  assume "\<not> ?thesis"
  then obtain \<psi>'' where "simplify \<psi>' \<psi>''"  by metis
    hence "\<exists>l'. simplify \<psi> l'"
      proof (induction rule: simplify.induct)
        case (tautology_deletion A P)
        thus ?thesis using simplify.tautology_deletion[of "A" P "\<psi>"] incl by blast  (* 19 ms *)
       next
         case (condensation A L)
         thus ?case using simplify.condensation[of A L "\<psi>"] incl by blast
       next
         case (subsumption A B)
         thus ?case using simplify.subsumption[of A "\<psi>" B] incl by blast
       qed
  thus False using assms(1) by blast
qed

lemma simplified_in:
  assumes "simplified \<psi>"
  and "N \<in> \<psi>"
  shows "simplified {N}"
  using assms by (metis Set.set_insert empty_subsetI in_simplified_simplified insert_mono)

lemma subsumes_imp_formula:
  assumes "\<psi> \<le># \<phi>"
  shows "{\<psi>} \<Turnstile>p \<phi>"
  unfolding true_clss_cls_def apply auto
  using assms subsumption_imp_eval by blast

definition full_simplifier ::  "'a clauses \<Rightarrow> 'a clauses \<Rightarrow> bool" ("simp\<^sup>+\<^sup>\<down>")
where "full_simplifier \<psi> \<psi>' = (simplify\<^sup>+\<^sup>+ \<psi> \<psi>' \<and> simplified \<psi>')"
definition full0_simplifier ::  "'a clauses \<Rightarrow> 'a clauses \<Rightarrow> bool" ("simp\<^sup>\<down>")
where "full0_simplifier \<psi> \<psi>' = (simplify\<^sup>*\<^sup>* \<psi> \<psi>' \<and> simplified \<psi>')"

lemma simplified_imp_distinct_mset_tauto:
  assumes simp: "simplified \<psi>'"
  shows "distinct_mset_set \<psi>'" and  "\<forall>\<chi> \<in> \<psi>'. \<not>tautology \<chi>"
proof -
  show "\<forall>\<chi> \<in> \<psi>'. \<not>tautology \<chi>" using simp by (auto simp add: simplified_in simplified_not_tautology)

  show "distinct_mset_set \<psi>'"
    proof (rule ccontr)
      assume "\<not>?thesis"
      then obtain \<chi> where "\<chi> \<in> \<psi>'" and "\<not>distinct_mset \<chi>" unfolding distinct_mset_set_def by auto
      then obtain L where "count \<chi> L \<ge> 2" unfolding distinct_mset_def by (metis gr_implies_not0 le_antisym less_one not_le simp simplified_count)
      thus False by (metis Suc_1 `\<chi> \<in> \<psi>'` not_less_eq_eq simp simplified_count)
    qed
qed

lemma simplified_no_more_full_simplified:
  assumes "simplified \<psi>"
  shows "\<not>full_simplifier \<psi> \<psi>'"
  using assms unfolding full_simplifier_def by (meson tranclpD)


subsection \<open>Resolution and Invariants\<close>

inductive resolution :: "'v state \<Rightarrow> 'v state \<Rightarrow> bool" where
full_simp: "full_simplifier N N' \<Longrightarrow> resolution (N, already_used) (N', already_used)" |
inferring: "inference (N, already_used) (N', already_used') \<Longrightarrow> simplified N \<Longrightarrow> full0_simplifier N' N'' \<Longrightarrow> resolution (N, already_used) (N'', already_used')"

subsubsection \<open>Invariants\<close>

lemma resolution_finite:
  assumes "resolution \<psi> \<psi>'" and "finite (fst \<psi>)"
  shows "finite (fst \<psi>')"
  using assms apply (induct rule: resolution.induct, auto simp add: full_simplifier_def full0_simplifier_def)
   apply (meson rtranclp_simplifier_preserves_finite tranclp_into_rtranclp )
  by (metis fst_conv inference_preserves_finite rtranclp_simplifier_preserves_finite)

lemma rtranclp_resolution_finite:
  assumes "resolution\<^sup>*\<^sup>* \<psi> \<psi>'" and "finite (fst \<psi>)"
  shows "finite (fst \<psi>')"
  using assms by (induct rule: rtranclp.induct, auto simp add: resolution_finite)

lemma resolution_finite_snd:
  assumes "resolution \<psi> \<psi>'" and "finite (snd \<psi>)"
  shows "finite (snd \<psi>')"
  using assms apply (induct rule: resolution.induct, auto simp add: inference_preserves_finite_snd)
  using inference_preserves_finite_snd snd_conv by metis

lemma rtranclp_resolution_finite_snd:
  assumes "resolution\<^sup>*\<^sup>* \<psi> \<psi>'" and "finite (snd \<psi>)"
  shows "finite (snd \<psi>')"
  using assms by (induct rule: rtranclp.induct, auto simp add: resolution_finite_snd)

lemma resolution_always_simplified:
 assumes "resolution \<psi> \<psi>'"
 shows "simplified (fst \<psi>')"
 using assms by (induct rule: resolution.induct, auto simp add: full_simplifier_def full0_simplifier_def)

lemma tranclp_resolution_always_simplified:
  assumes "tranclp resolution \<psi> \<psi>'"
  shows "simplified (fst \<psi>')"
  using assms by (induct rule: tranclp.induct, auto simp add: resolution_always_simplified)

lemma resolution_atms_of:
  assumes "resolution \<psi> \<psi>'" and "finite (fst \<psi>)"
  shows "atms_of_m (fst \<psi>') \<subseteq> atms_of_m (fst \<psi>)"
  using assms apply (induct rule: resolution.induct, auto simp add: inference_preserves_atms_of_m rtranclp_simplifier_atms_of_m tranclp_into_rtranclp full_simplifier_def full0_simplifier_def)
  by (metis (no_types, lifting) fst_conv inference_preserves_atms_of_m rtranclp_simplifier_atms_of_m set_rev_mp)

lemma rtranclp_resolution_atms_of:
  assumes "resolution\<^sup>*\<^sup>* \<psi> \<psi>'" and "finite (fst \<psi>)"
  shows "atms_of_m (fst \<psi>') \<subseteq> atms_of_m (fst \<psi>)"
  using assms apply (induct rule: rtranclp.induct, auto simp add: resolution_atms_of rtranclp_resolution_finite resolution_finite)
  using resolution_atms_of rtranclp_resolution_finite by (metis (no_types, hide_lams) fst_conv subsetCE)

lemma resolution_include:
  assumes res: "resolution \<psi> \<psi>'" and finite: "finite (fst \<psi>)"
  shows "fst \<psi>' \<subseteq> build_all_simple_clss (atms_of_m (fst \<psi>))"
proof -
  have finite': "finite (fst \<psi>')" using local.finite res resolution_finite by blast
  have "simplified (fst \<psi>')" using res finite' resolution_always_simplified by blast
  hence "fst \<psi>' \<subseteq> build_all_simple_clss (atms_of_m (fst \<psi>'))" using simplified_in_build_all finite' simplified_imp_distinct_mset_tauto[of "fst \<psi>'"] by auto
  also have "atms_of_m (fst \<psi>') \<subseteq> atms_of_m (fst \<psi>)" using res finite resolution_atms_of[of \<psi> \<psi>'] by auto
  ultimately show ?thesis by (meson atms_of_m_finite local.finite order.trans rev_finite_subset build_all_simple_clss_mono)
qed

lemma rtranclp_resolution_include:
  assumes res: "tranclp resolution \<psi> \<psi>'" and finite: "finite (fst \<psi>)"
  shows "fst \<psi>' \<subseteq> build_all_simple_clss (atms_of_m (fst \<psi>))"
  using assms apply (induct rule: tranclp.induct)
  apply (auto simp add: resolution_include)
  by (metis (no_types, lifting) atms_of_m_finite build_all_simple_clss_finite build_all_simple_clss_mono finite_subset fst_conv resolution_include set_rev_mp rtranclp_resolution_atms_of tranclp_into_rtranclp)

abbreviation "already_used_all_simple already_used vars \<equiv> (\<forall> (A, B) \<in> already_used. simplified {A} \<and> simplified {B} \<and> atms_of A \<subseteq> vars \<and> atms_of B \<subseteq> vars)"

lemma already_used_all_simple_vars_incl:
  assumes "vars \<subseteq> vars'"
  shows "already_used_all_simple a vars \<Longrightarrow> already_used_all_simple a vars'"
  using assms by fast

lemma inference_clause_preserves_already_used_all_simple:
  assumes "inference_clause S S'"
  and "already_used_all_simple (snd S) vars"
  and "simplified (fst S)"
  and "atms_of_m (fst S) \<subseteq> vars"
  shows "already_used_all_simple (snd (fst S \<union> {fst S'}, snd S')) vars"
  using assms
proof (induct rule: inference_clause.induct)
  case (factoring L C N already_used)
  thus ?case by (simp add: simplified_in factoring_imp_simplifier)
next
  case (resolution P C N D already_used) note H = this
  show ?case apply clarify
    proof -
      fix A B v
      assume "(A, B) \<in>  snd (fst (N, already_used) \<union> {fst (C + D, already_used \<union> {({#Pos P#} + C, {#Neg P#} + D)})}, snd (C + D, already_used \<union> {({#Pos P#} + C, {#Neg P#} + D)}))"
      hence "(A, B) \<in> already_used \<or> (A, B) = ({#Pos P#} + C, {#Neg P#} + D)" by auto
      also {
        assume "(A, B) \<in> already_used"
        hence "simplified {A} \<and> simplified {B} \<and> atms_of A \<subseteq> vars \<and> atms_of B \<subseteq> vars" using H(4) by auto
      }
      moreover {
        assume eq: "(A, B) = ({#Pos P#} + C, {#Neg P#} + D)"
        hence "simplified {A}" using simplified_in H(1,5) by auto
        also have "simplified {B}" using eq simplified_in H(2,5) by auto
        moreover have "atms_of A \<subseteq> atms_of_m N" using eq H(1) atms_of_atms_of_m_mono[of A N] by auto
        moreover have "atms_of B \<subseteq> atms_of_m N" using eq H(2) atms_of_atms_of_m_mono[of B N] by auto
        ultimately have "simplified {A} \<and> simplified {B} \<and> atms_of A \<subseteq> vars \<and> atms_of B \<subseteq> vars" using H(6) by auto
      }
      ultimately show "simplified {A} \<and> simplified {B} \<and> atms_of A \<subseteq> vars \<and> atms_of B \<subseteq> vars" by auto
    qed
qed

lemma inference_preserves_already_used_all_simple:
  assumes "inference S S'"
  and "already_used_all_simple (snd S) vars"
  and "simplified (fst S)"
  and "atms_of_m (fst S) \<subseteq> vars"
  shows "already_used_all_simple (snd S') vars"
  using assms
proof (induct rule: inference.induct)
  case (inference_step S clause already_used)
  thus ?case using inference_clause_preserves_already_used_all_simple[of S "(clause, already_used)" vars] by auto
qed

lemma already_used_all_simple_inv:
  assumes "resolution S S'"
  and "already_used_all_simple (snd S) vars"
  and "atms_of_m (fst S) \<subseteq> vars"
  shows "already_used_all_simple (snd S') vars"
  using assms
proof (induct rule: resolution.induct)
  case (full_simp N N')
  thus ?case by simp
next
  case (inferring N already_used N' already_used' N'')
  thus "already_used_all_simple (snd (N'', already_used')) vars" using inference_preserves_already_used_all_simple[of "(N, already_used)"] by simp
qed

lemma rtranclp_already_used_all_simple_inv:
  assumes "resolution\<^sup>*\<^sup>* S S'"
  and "already_used_all_simple (snd S) vars"
  and "atms_of_m (fst S) \<subseteq> vars"
  and "finite (fst S)"
  shows "already_used_all_simple (snd S') vars"
  using assms
proof (induct rule: rtranclp.induct)
  case (rtrancl_refl)
  thus ?case by simp
next
  case (rtrancl_into_rtrancl \<psi> \<psi>' \<psi>'') note infstar = this(1) and IH = this and res = this(3) and already = this(4) and atms = this(5) and finite = this(6)
  have "already_used_all_simple (snd \<psi>') vars" using IH already atms finite by simp
  also have "atms_of_m (fst \<psi>') \<subseteq> atms_of_m (fst \<psi>)" by (simp add: infstar local.finite rtranclp_resolution_atms_of)
  hence "atms_of_m (fst \<psi>') \<subseteq> vars" using atms by auto
  ultimately show ?case
  using already_used_all_simple_inv[OF res] by simp
qed

lemma inference_clause_simplified_already_used_subset:
  assumes "inference_clause S S'"
  and "simplified (fst S)"
  shows "snd S \<subset> snd S'"
  using assms apply (induct rule: inference_clause.induct, auto)
  using factoring_imp_simplifier by blast

lemma inference_simplified_already_used_subset:
  assumes "inference S S'"
  and "simplified (fst S)"
  shows "snd S \<subset> snd S'"
  using assms apply (induct rule: inference.induct)
  by (metis inference_clause_simplified_already_used_subset snd_conv)

lemma resolution_simplified_already_used_subset:
  assumes "resolution S S'"
  and "simplified (fst S)"
  shows "snd S \<subset> snd S'"
  using assms apply (induct rule: resolution.induct, simp_all add: full_simplifier_def)
  apply (meson tranclpD)
  by (metis inference_simplified_already_used_subset fst_conv snd_conv)

lemma tranclp_resolution_simplified_already_used_subset:
  assumes "tranclp resolution S S'"
  and "simplified (fst S)"
  shows "snd S \<subset> snd S'"
  using assms apply (induct rule: tranclp.induct)
  using resolution_simplified_already_used_subset apply metis
  by (meson tranclp_resolution_always_simplified resolution_simplified_already_used_subset less_trans)

abbreviation "already_used_top vars \<equiv> build_all_simple_clss vars \<times> build_all_simple_clss vars "

lemma already_used_all_simple_in_already_used_top:
  assumes "already_used_all_simple s vars" and "finite vars"
  shows "s \<subseteq> already_used_top vars"
proof
  fix x
  assume x_s: "x \<in> s"
  obtain A B where x: "x = (A, B)" by (case_tac x, auto)
  hence "simplified {A}" and "atms_of A \<subseteq> vars" using assms(1) x_s by fastforce+
  hence A: "A \<in> build_all_simple_clss vars" using build_all_simple_clss_mono[of vars "atms_of A"] x assms(2) simplified_imp_distinct_mset_tauto[of "{A}"] distinct_mset_not_tautology_implies_in_build_all_simple_clss by fast
  also have "simplified {B}" and "atms_of B \<subseteq> vars" using assms(1) x_s x by fast+
  hence B: "B \<in> build_all_simple_clss vars" using simplified_imp_distinct_mset_tauto[of "{B}"] distinct_mset_not_tautology_implies_in_build_all_simple_clss  build_all_simple_clss_mono[of vars "atms_of B"] x assms(2) by fast
  ultimately show "x \<in> build_all_simple_clss vars \<times> build_all_simple_clss vars" unfolding x by auto
qed

lemma already_used_top_finite:
  assumes "finite vars"
  shows "finite (already_used_top vars)"
  using build_all_simple_clss_finite assms by auto

lemma already_used_top_increasing:
  assumes "var \<subseteq> var'" and "finite var'"
  shows "already_used_top var \<subseteq> already_used_top var'"
  using assms build_all_simple_clss_mono by auto

lemma already_used_all_simple_finite:
  fixes s :: "('a::linorder literal multiset \<times> 'a literal multiset) set" and vars :: "'a set"
  assumes "already_used_all_simple s vars" and "finite vars"
  shows "finite s"
  using assms  already_used_all_simple_in_already_used_top[OF assms(1)] rev_finite_subset[OF already_used_top_finite[of vars]] by auto

abbreviation "card_simple vars \<psi> \<equiv> card (already_used_top vars - \<psi>)"

lemma resolution_card_simple_decreasing:
  assumes res: "resolution \<psi> \<psi>'"
  and a_u_s: "already_used_all_simple (snd \<psi>) vars"
  and finite_v: "finite vars"
  and finite_fst: "finite (fst \<psi>)"
  and finite_snd: "finite (snd \<psi>)"
  and simp: "simplified (fst \<psi>)"
  and "atms_of_m (fst \<psi>) \<subseteq> vars"
  shows "card_simple vars (snd \<psi>') < card_simple vars (snd \<psi>)"
proof -
  let ?vars = "vars"
  let ?top = "build_all_simple_clss ?vars \<times> build_all_simple_clss ?vars"
  have 1: "card_simple vars (snd \<psi>) = card ?top - card (snd \<psi>)"
    using card_Diff_subset finite_snd  already_used_all_simple_in_already_used_top[OF a_u_s] finite_v by metis
  have a_u_s': "already_used_all_simple (snd \<psi>') vars" using already_used_all_simple_inv res a_u_s assms(7) by blast
  have f: "finite (snd \<psi>')" using already_used_all_simple_finite a_u_s' finite_v by auto
  have 2: "card_simple vars (snd \<psi>') = card ?top - card (snd \<psi>')" using card_Diff_subset[OF f] already_used_all_simple_in_already_used_top[OF a_u_s' finite_v] by auto
  have "card (already_used_top vars) \<ge> card (snd \<psi>')" using already_used_all_simple_in_already_used_top[OF a_u_s' finite_v] card_mono[of "already_used_top vars" "snd \<psi>'"] already_used_top_finite[OF finite_v] by metis
  thus ?thesis using  psubset_card_mono[OF f resolution_simplified_already_used_subset[OF res simp]] unfolding 1 2 by linarith
qed


lemma tranclp_resolution_card_simple_decreasing:
  assumes "tranclp resolution \<psi> \<psi>'" and finite_fst: "finite (fst \<psi>)"
  and "already_used_all_simple (snd \<psi>) vars"
  and "atms_of_m (fst \<psi>) \<subseteq> vars"
  and finite_v: "finite vars"
  and finite_snd: "finite (snd \<psi>)"
  and "simplified (fst \<psi>)"
  shows "card_simple vars (snd \<psi>') < card_simple vars (snd \<psi>)"
  using assms
proof (induct rule: tranclp.induct)
  case (r_into_trancl \<psi> \<psi>')
  thus ?case by (simp add: resolution_card_simple_decreasing)
next
  case (trancl_into_trancl \<psi> \<psi>' \<psi>'') note res = this(1) and res' = this(3) and a_u_s = this(5) and atms = this(6) and f_v = this(7) and f_fst = this(4) and H = this
  hence "card_simple vars (snd \<psi>') < card_simple vars (snd \<psi>)" by auto
  also have a_u_s': "already_used_all_simple (snd \<psi>') vars" using rtranclp_already_used_all_simple_inv[OF tranclp_into_rtranclp[OF res] a_u_s atms f_fst] .
  have "finite (fst \<psi>')" by (meson build_all_simple_clss_finite rev_finite_subset rtranclp_resolution_include trancl_into_trancl.hyps(1) trancl_into_trancl.prems(1))
  moreover have "finite (snd \<psi>')" using already_used_all_simple_finite[OF a_u_s' f_v] .
  moreover have "simplified (fst \<psi>')" using res tranclp_resolution_always_simplified by blast
  moreover have "atms_of_m (fst \<psi>') \<subseteq> vars" by (meson atms f_fst order.trans res rtranclp_resolution_atms_of tranclp_into_rtranclp)
  ultimately show ?case using resolution_card_simple_decreasing[OF res' a_u_s' f_v] f_v less_trans[of "card_simple vars (snd \<psi>'')" "card_simple vars (snd \<psi>')" "card_simple vars (snd \<psi>)"] by blast
qed


lemma tranclp_resolution_card_simple_decreasing_2:
  assumes "tranclp resolution \<psi> \<psi>'"
  and finite_fst: "finite (fst \<psi>)"
  and empty_snd: "snd \<psi> = {}"
  and "simplified (fst \<psi>)"
  shows "card_simple (atms_of_m (fst \<psi>)) (snd \<psi>') < card_simple (atms_of_m (fst \<psi>)) (snd \<psi>)"
proof -
  let ?vars = "(atms_of_m (fst \<psi>))"
  have "already_used_all_simple (snd \<psi>) ?vars" unfolding empty_snd by auto
  also have "atms_of_m (fst \<psi>) \<subseteq> ?vars" by auto
  moreover have finite_v: "finite ?vars" using finite_fst by auto
  moreover have finite_snd: "finite (snd \<psi>)" unfolding empty_snd by auto
  ultimately show ?thesis using assms(1,2,4) tranclp_resolution_card_simple_decreasing[of \<psi> \<psi>'] by presburger
qed

subsubsection \<open>well-foundness if the relation\<close>


lemma wf_simplified_resolution:
  assumes f_vars: "finite vars"
  shows "wf {(y:: 'v:: linorder state, x). (atms_of_m (fst x) \<subseteq> vars \<and> simplified (fst x) \<and> finite (snd x) \<and> finite (fst x) \<and> already_used_all_simple (snd x) vars) \<and> resolution x y}"
proof -
  {
    fix a b :: "'v::linorder state"
    assume " (b, a) \<in> {(y, x). (atms_of_m (fst x) \<subseteq> vars \<and> simplified (fst x) \<and> finite (snd x) \<and> finite (fst x) \<and> already_used_all_simple (snd x) vars) \<and> resolution x y}"
    hence "atms_of_m (fst a) \<subseteq> vars" and simp: "simplified (fst a)" and "finite (snd a)" and "finite (fst a)" and a_u_v: "already_used_all_simple (snd a) vars" and res: "resolution a b" by auto
    have "finite (already_used_top vars)" using f_vars already_used_top_finite by blast
    also have "already_used_top vars \<subseteq> already_used_top vars" by auto
    moreover have "snd b \<subseteq> already_used_top vars" using already_used_all_simple_in_already_used_top[of "snd b" vars] a_u_v already_used_all_simple_inv[OF res] `finite (fst a)` `atms_of_m (fst a) \<subseteq> vars` f_vars by presburger
    moreover have "snd a \<subset> snd b" using resolution_simplified_already_used_subset[OF res simp] .
    ultimately have "finite (already_used_top vars) \<and> already_used_top vars \<subseteq> already_used_top vars \<and> snd b \<subseteq> already_used_top vars \<and> snd a \<subset> snd b" by metis
  }
  thus ?thesis using wf_bounded_set[of "{(y:: 'v:: linorder state, x). (atms_of_m (fst x) \<subseteq> vars \<and> simplified (fst x) \<and> finite (snd x) \<and> finite (fst x)\<and> already_used_all_simple (snd x) vars) \<and> resolution x y}" "\<lambda>_. already_used_top vars" "snd"] by auto
qed

lemma wf_simplified_resolution':
  assumes f_vars: "finite vars"
  shows "wf {(y:: 'v:: linorder state, x). (atms_of_m (fst x) \<subseteq> vars \<and> \<not>simplified (fst x) \<and> finite (snd x) \<and> finite (fst x) \<and> already_used_all_simple (snd x) vars) \<and> resolution x y}"
  unfolding wf_def
   apply simp
  by (metis (mono_tags, hide_lams) fst_conv resolution_always_simplified)

lemma wf_resolution:
  assumes f_vars: "finite vars"
  shows "wf ({(y:: 'v:: linorder state, x). (atms_of_m (fst x) \<subseteq> vars \<and> simplified (fst x) \<and> finite (snd x) \<and> finite (fst x) \<and> already_used_all_simple (snd x) vars) \<and> resolution x y} \<union>
        {(y, x). (atms_of_m (fst x) \<subseteq> vars \<and> \<not> simplified (fst x) \<and> finite (snd x) \<and> finite (fst x) \<and> already_used_all_simple (snd x) vars) \<and> resolution x y})"
proof -
  let ?R = "{(y:: 'v:: linorder state, x). (atms_of_m (fst x) \<subseteq> vars \<and> simplified (fst x) \<and> finite (snd x) \<and> finite (fst x) \<and> already_used_all_simple (snd x) vars) \<and> resolution x y}"
  let ?S = "{(y:: 'v:: linorder state, x). (atms_of_m (fst x) \<subseteq> vars \<and> \<not>simplified (fst x) \<and> finite (snd x) \<and> finite (fst x) \<and> already_used_all_simple (snd x) vars) \<and> resolution x y}"
  have "Domain ?R Int Range ?S = {}" using resolution_always_simplified by auto blast
  thus "wf (?R \<union> ?S)" using wf_simplified_resolution[OF f_vars] wf_simplified_resolution'[OF f_vars] wf_Un[of ?R ?S] by fast
qed

lemma rtrancp_simplify_already_used_inv:
  assumes "simplify\<^sup>*\<^sup>* S S'"
  and "already_used_inv (S, N)"
  shows "already_used_inv (S', N)"
  using assms apply (induction)
  using simplify_preserves_already_used_inv by fast+

lemma full_simplify_already_used_inv:
  assumes "full_simplifier S S'"
  and "already_used_inv (S, N)"
  shows "already_used_inv (S', N)"
  using assms tranclp_into_rtranclp[of simplify S S'] rtrancp_simplify_already_used_inv unfolding full_simplifier_def by fast

lemma full0_simplify_already_used_inv:
  assumes "full0_simplifier S S'"
  and "already_used_inv (S, N)"
  shows "already_used_inv (S', N)"
  using assms  rtrancp_simplify_already_used_inv unfolding full0_simplifier_def by fast
lemma resolution_already_used_inv:
  assumes "resolution S S'"
  and "already_used_inv S"
  shows "already_used_inv S'"
  using assms
proof (induction)
  case (full_simp N N' already_used)
  thus ?case using full_simplify_already_used_inv by fast
next
  case (inferring N already_used N' already_used' N''') note inf = this(1) and full = this(3) and a_u_v = this(4)
  thus ?case
    using inference_preserves_already_used_inv[OF inf a_u_v] full0_simplify_already_used_inv full by fast
qed

lemma rtranclp_resolution_already_used_inv:
  assumes "resolution\<^sup>*\<^sup>* S S'"
  and "already_used_inv S"
  shows "already_used_inv S'"
  using assms apply (induction)
  using resolution_already_used_inv by fast+

lemma rtanclp_simplifier_preserves_unsat:
  assumes "simplify\<^sup>*\<^sup>* \<psi> \<psi>'"
  shows "satisfiable \<psi>' \<longrightarrow> satisfiable \<psi>"
  using assms apply (induction)
  using simplify_clause_preserves_sat by blast+

lemma full_simplifier_preserves_unsat:
  assumes "full_simplifier \<psi> \<psi>'"
  shows "satisfiable \<psi>' \<longrightarrow> satisfiable \<psi>"
  using assms rtanclp_simplifier_preserves_unsat[of \<psi> \<psi>'] tranclp_into_rtranclp unfolding full_simplifier_def by metis

lemma full0_simplifier_preserves_unsat:
  assumes "full0_simplifier \<psi> \<psi>'"
  shows "satisfiable \<psi>' \<longrightarrow> satisfiable \<psi>"
  using assms rtanclp_simplifier_preserves_unsat[of \<psi> \<psi>'] unfolding full0_simplifier_def by metis

lemma resolution_preserves_unsat:
  assumes "resolution \<psi> \<psi>'"
  shows "satisfiable (fst \<psi>') \<longrightarrow> satisfiable (fst \<psi>)"
  using assms apply (induct rule: resolution.induct)
  using full_simplifier_preserves_unsat apply (metis fst_conv)
  using full0_simplifier_preserves_unsat simplifier_preserves_unsat by fastforce

lemma rtranclp_resolution_preserves_unsat:
  assumes "resolution\<^sup>*\<^sup>* \<psi> \<psi>'"
  shows "satisfiable (fst \<psi>') \<longrightarrow> satisfiable (fst \<psi>)"
  using assms apply induction
  using resolution_preserves_unsat by fast+

lemma rtranclp_simplifier_preserve_partial_tree:
  assumes "simplify\<^sup>*\<^sup>* N N'"
  and "partial_interps t I N"
  shows "partial_interps t I N'"
  using assms apply (induction, simp)
  using simplifier_preserve_partial_tree by metis

lemma full_simplifier_preserve_partial_tree:
  assumes "full_simplifier N N'"
  and "partial_interps t I N"
  shows "partial_interps t I N'"
  using assms  rtranclp_simplifier_preserve_partial_tree[of N N' t I] tranclp_into_rtranclp unfolding full_simplifier_def by fast

lemma full0_simplifier_preserve_partial_tree:
  assumes "full0_simplifier N N'"
  and "partial_interps t I N"
  shows "partial_interps t I N'"
  using assms rtranclp_simplifier_preserve_partial_tree[of N N' t I] tranclp_into_rtranclp unfolding full0_simplifier_def by fast

lemma resolution_preserve_partial_tree:
  assumes "resolution S S'"
  and "partial_interps t I (fst S)"
  shows "partial_interps t I (fst S')"
  using assms apply (induction)
    using full_simplifier_preserve_partial_tree fst_conv apply metis
  using full0_simplifier_preserve_partial_tree inference_preserve_partial_tree by fastforce

lemma rtranclp_resolution_preserve_partial_tree:
  assumes "resolution\<^sup>*\<^sup>* S S'"
  and "partial_interps t I (fst S)"
  shows "partial_interps t I (fst S')"
  using assms apply (induction)
  using resolution_preserve_partial_tree by fast+
  thm nat_less_induct nat.induct

lemma nat_ge_induct[case_names 0 Suc]:
  assumes "P 0"
  and "(\<And>n. (\<And>m. m<Suc n \<Longrightarrow> P m) \<Longrightarrow> P (Suc n))"
  shows "P n"
  using assms apply (induct rule: nat_less_induct)
  by (case_tac n) auto

lemma wf_always_more_step_False:
  assumes "wf R"
  shows "(\<forall>x. \<exists>z. (z, x)\<in>R) \<Longrightarrow> False"
 using assms unfolding wf_def by (meson Domain.DomainI assms wfE_min)

lemma wfP_if_measure: fixes f :: "'a \<Rightarrow> nat"
shows "(\<And>x y. P x \<Longrightarrow> g x y  \<Longrightarrow> f y < f x) \<Longrightarrow> wf {(y,x). P x \<and> g x y}"
  apply(insert wf_measure[of f])
  apply(simp only: measure_def inv_image_def less_than_def less_eq)
  apply(erule wf_subset)
  apply auto
  done
lemma finite_finite_mset_element_of_mset[simp]:
  assumes "finite N"
  shows "finite {f \<phi> L |\<phi> L. \<phi> \<in> N \<and> L \<in># \<phi> \<and> P \<phi> L}"
  using assms
proof (induction N rule: finite_induct)
  case empty
  show ?case by auto
next
  case (insert x N) note finite = this(1) and IH = this(3)
  have "{f \<phi> L |\<phi> L. (\<phi> = x \<or> \<phi> \<in> N) \<and> L \<in># \<phi> \<and> P \<phi> L} \<subseteq> {f x L | L. L \<in># x \<and> P x L} \<union> {f \<phi> L |\<phi> L. \<phi> \<in> N \<and> L \<in># \<phi> \<and> P \<phi> L}" by auto
  also have "finite {f x L | L. L \<in># x}" by auto
  ultimately show ?case using IH finite_subset by fastforce
qed


 value card
 value filter_mset
value "{#count \<phi> L |L \<in># \<phi>. 2 \<le> count \<phi> L#}"
value "(\<lambda>\<phi>. msetsum {#count \<phi> L |L \<in># \<phi>. 2 \<le> count \<phi> L#})"

syntax
  "_comprehension1'_mset" :: "'a \<Rightarrow> 'b \<Rightarrow> 'b multiset \<Rightarrow> 'a multiset"
      ("({#_/. _ : setof _#})")
translations
  "{#e. x: setof M#}" == "CONST set_mset (CONST image_mset (%x. e) M)"
value "{# a. a : setof {#1,1,2::int#}#} = {1,2}"

definition sum_count_ge_2 :: "'a multiset set \<Rightarrow> nat" ("\<Xi>") where
"sum_count_ge_2 \<equiv>folding.F (\<lambda>\<phi>. op +(msetsum {#count \<phi> L |L \<in># \<phi>. 2 \<le> count \<phi> L#})) 0"


interpretation sum_count_ge_2!: folding "(\<lambda>\<phi>. op +(msetsum {#count \<phi> L |L \<in># \<phi>. 2 \<le> count \<phi> L#}))" 0
where
  "folding.F (\<lambda>\<phi>. op +(msetsum {#count \<phi> L |L \<in># \<phi>. 2 \<le> count \<phi> L#})) 0 = sum_count_ge_2"
proof -
  show "folding (\<lambda>\<phi>. op + (msetsum (image_mset (count \<phi>) {# L :# \<phi>. 2 \<le> count \<phi> L#})))"
    by standard auto
  then interpret sum_count_ge_2!: folding "(\<lambda>\<phi>. op +(msetsum {#count \<phi> L |L \<in># \<phi>. 2 \<le> count \<phi> L#}))" 0 .
  show "folding.F (\<lambda>\<phi>. op + (msetsum (image_mset (count \<phi>) {# L :# \<phi>. 2 \<le> count \<phi> L#}))) 0 = sum_count_ge_2" by (auto simp add: sum_count_ge_2_def)
qed





lemma finite_incl_le_setsum:
 "finite (B::'a multiset set) \<Longrightarrow> A \<subseteq> B \<Longrightarrow> \<Xi> A \<le> \<Xi> B"
proof (induction arbitrary:A rule: finite_induct)
  case empty
  thus ?case by simp
next
  case (insert a F) note finite = this(1) and aF = this(2) and IH = this(3) and AF = this(4)
  show ?case
    proof (cases "a \<in> A")
      assume "a \<notin> A"
      hence "A \<subseteq> F" using AF by auto
      thus ?case using IH[of A] by (simp add: aF local.finite)
    next
      assume aA: "a \<in> A"
      hence "A - {a} \<subseteq> F" using AF by auto
      hence "\<Xi> (A - {a}) \<le> \<Xi> F" using IH by blast
      thus ?case
         proof -
           obtain nn :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
             "\<forall>x0 x1. (\<exists>v2. x0 = x1 + v2) = (x0 = x1 + nn x0 x1)"
             by moura (* 18 ms *)
           hence "\<Xi> F = \<Xi> (A - {a}) + nn (\<Xi> F) (\<Xi> (A - {a}))"
             using Nat.le_iff_add `\<Xi> (A - {a}) \<le> \<Xi> F` by presburger (* 3 ms *)
           thus ?thesis
             by (metis (no_types) Nat.le_iff_add aA aF add.assoc finite.insertI finite_subset insert.prems local.finite sum_count_ge_2.insert sum_count_ge_2.remove) (* 139 ms *)
         qed
    qed
qed

lemma mset_condensation1:
  "{# La :# A + {#L#}. 2 \<le> count (A +  {#L#}) La#} = {# La :# A. La \<noteq> L \<and>  2 \<le> count A La#} #\<union> (if count A L \<ge> 1 then replicate_mset (count A L + 1) L else {#})"
   by (auto intro: multiset_eqI)
lemma mset_condensation2:
  "{# La :# A + {#L#}+  {#L#}. 2 \<le> count (A +  {#L#}+  {#L#}) La#} = {# La :# A. La \<noteq> L \<and>  2 \<le> count A La#} #\<union> (replicate_mset (count A L + 2) L)"
   by (auto intro: multiset_eqI)

lemma msetsum_disjoint:
  assumes "A #\<inter> B = {#}"
  shows "(\<Sum>La\<in>#A #\<union> B. f La) =
    (\<Sum>La\<in>#A. f La) + (\<Sum>La\<in>#B. f La)"
  by (metis assms diff_zero empty_sup image_mset_union  msetsum.union multiset_inter_commute multiset_union_diff_commute sup_subset_mset_def zero_diff)

(*Move to multiset more*)
lemma msetsum_replicate_mset[simp]:
  "msetsum (replicate_mset n m) = n * m"
    by (induct n) auto

lemma simplify_finite_measure_decrease:
  "simplify N N' \<Longrightarrow> finite N \<Longrightarrow> card N' + \<Xi> N' < card N + \<Xi> N"
proof (induction rule: simplify.induct)
  case (tautology_deletion A P) note an = this(1) and fin = this(2)
  let ?N' = "N - {A + {#Pos P#} + {#Neg P#}}"
  have "card ?N' < card N"
    by (meson card_Diff1_less tautology_deletion.hyps tautology_deletion.prems)
  also have "?N' \<subseteq> N" by auto
  hence "sum_count_ge_2 ?N' \<le> sum_count_ge_2 N" using finite_incl_le_setsum[OF fin] by blast
  ultimately show ?case by linarith
next
  case (condensation A L) note AN = this(1) and fin = this(2)
  let ?C' = "A + {#L#}"
  let ?C = "A + {#L#} + {#L#}"
  let ?N' = "N - {?C} \<union> {?C'}"
  have "card ?N' \<le> card N"
    using AN by (metis (no_types, lifting) Diff_subset Un_empty_right Un_insert_right card.remove card_insert_if card_mono fin finite_Diff order_refl)
  also have "\<Xi> {?C'} < \<Xi> {?C}"
   proof -
     have 1: "(\<Sum>La\<in>#{# La :# A. La \<noteq> L \<and> 2 \<le> count A La#} #\<union> (if 1 \<le> count A L then replicate_mset (count A L + 1) L else {#}). count A La + (if L = La then 1 else 0)) = (\<Sum>La\<in>#{# La :# A. La \<noteq> L \<and> 2 \<le> count A La#}. count A La + (if L = La then 1 else 0)) + (\<Sum>La\<in>#(if 1 \<le> count A L then replicate_mset (count A L + 1) L else {#}). count A La + (if L = La then 1 else 0))"
       (is "_ = (\<Sum> La \<in># ?A. ?f La) + ( \<Sum> La \<in># ?B. _)")
       apply (rule msetsum_disjoint[of ?A ?B ?f])
       by (auto intro: multiset_eqI)
     also have 2: "(\<Sum>La\<in>#(if 1 \<le> count A L then replicate_mset (count A L + 1) L else {#}). count A La + (if L = La then 1 else 0)) = (if 1 \<le> count A L then count A L * (count A L + 2)+1 else 0)"
       by auto
     moreover have "2'": "(\<Sum>La\<in>#{# La :# A. La \<noteq> L \<and> 2 \<le> count A La#}. count A La + (if L = La then 1 else 0)) = (\<Sum>La\<in>#{# La :# A. La \<noteq> L \<and> 2 \<le> count A La#}. count A La)"
       proof -
         obtain ll :: "('a literal \<Rightarrow> nat) \<Rightarrow> ('a literal \<Rightarrow> nat) \<Rightarrow> 'a literal multiset \<Rightarrow> 'a literal" where
           "\<forall>x0 x1 x2. (\<exists>v3. v3 \<in># x2 \<and> x1 v3 \<noteq> x0 v3) = (ll x0 x1 x2 \<in># x2 \<and> x1 (ll x0 x1 x2) \<noteq> x0 (ll x0 x1 x2))"
           by moura (* 23 ms *)
         hence f1: "\<forall>m f fa. ll fa f m \<in># m \<and> f (ll fa f m) \<noteq> fa (ll fa f m) \<or> image_mset f m = image_mset fa m"
           by (meson image_mset_cong) (* 103 ms *)
         have "ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#} \<notin># {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#} \<or> count A (ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#}) + (if L = ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#} then 1 else 0) = count A (ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#})"
           by (simp add:) (* 46 ms *)
         thus ?thesis
           using f1 by (metis (no_types, lifting))
       qed
     moreover have 3: "(\<Sum>La\<in>#{# La :# A. La \<noteq> L \<and> 2 \<le> count A La#} #\<union> replicate_mset (count A L + 2) L. count A La + (if L = La then 1 else 0) + (if L = La then 1 else 0)) = (\<Sum>La\<in>#{# La :# A. La \<noteq> L \<and> 2 \<le> count A La#}. count A La + (if L = La then 1 else 0) + (if L = La then 1 else 0)) + (\<Sum>La\<in>#replicate_mset (count A L + 2) L. count A La + (if L = La then 1 else 0) + (if L = La then 1 else 0))"  (is "_ = (\<Sum> La \<in># ?A. ?f La) + (\<Sum> La \<in># ?B. _)")
       apply (rule msetsum_disjoint[of ?A ?B ?f])
       by (auto intro: multiset_eqI)
     moreover have 4: "(\<Sum>La\<in>#replicate_mset (count A L + 2) L. count A La + (if L = La then 1 else 0) + (if L = La then 1 else 0)) = 4 + count A L *(count A L + 4)" by (simp add: algebra_simps)
     moreover have "4'": "(\<Sum>La\<in>#{# La :# A. La \<noteq> L \<and> 2 \<le> count A La#}. count A La + (if L = La then 1 else 0) + (if L = La then 1 else 0)) = (\<Sum>La\<in>#{# La :# A. La \<noteq> L \<and> 2 \<le> count A La#}. count A La)"
       proof -
         obtain ll :: "('a literal \<Rightarrow> nat) \<Rightarrow> ('a literal \<Rightarrow> nat) \<Rightarrow> 'a literal multiset \<Rightarrow> 'a literal" where
           "\<forall>x0 x1 x2. (\<exists>v3. v3 \<in># x2 \<and> x1 v3 \<noteq> x0 v3) = (ll x0 x1 x2 \<in># x2 \<and> x1 (ll x0 x1 x2) \<noteq> x0 (ll x0 x1 x2))"
           by moura (* 3 ms *)
         hence f1: "\<forall>m f fa. ll fa f m \<in># m \<and> f (ll fa f m) \<noteq> fa (ll fa f m) \<or> image_mset f m = image_mset fa m"
           by (meson image_mset_cong) (* 40 ms *)
         have "ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0) + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#} \<notin># {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#} \<or> count A (ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0) + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#}) + (if L = ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0) + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#} then 1 else 0) + (if L = ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0) + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#} then 1 else 0) = count A (ll (count A) (\<lambda>l. count A l + (if L = l then 1 else 0) + (if L = l then 1 else 0)) {# l :# A. l \<noteq> L \<and> 2 \<le> count A l#})"
           by simp (* 30 ms *)
         thus ?thesis
           using f1 by (metis (no_types, lifting)) (* > 1.0 s, timed out *)
       qed
    show ?thesis
      apply (simp)
      unfolding mset_condensation2 unfolding mset_condensation1
      unfolding 1 2 "2'" 3 4 "4'"
      by (auto simp add: algebra_simps)
   qed
  have "\<Xi> ?N' < \<Xi> N"
    proof (cases)
      assume a1: "?C' \<in> N"
      thus ?thesis
        proof -
          have f2: "\<And>m M. insert (m::'a literal multiset) (M - {m}) = M \<union> {} \<or> m \<notin> M"
            using Un_empty_right insert_Diff by blast (* 6 ms *)
          have f3: "\<And>m M Ma. insert (m::'a literal multiset) M - insert m Ma = M - insert m Ma"
            by simp (* 3 ms *)
          hence f4: "\<And>M m. M - {m::'a literal multiset} = M \<union> {} \<or> m \<in> M"
            using Diff_insert_absorb Un_empty_right by fastforce (* 6 ms *)
          have f5: "insert (A + {#L#} + {#L#}) N = N"
            using f3 f2 Un_empty_right condensation.hyps insert_iff by fastforce (* 3 ms *)
          have "\<And>m M. insert (m::'a literal multiset) M = M \<union> {} \<or> m \<notin> M"
            using f3 f2 Un_empty_right add.right_neutral insert_iff by fastforce (* 3 ms *)
          hence "\<Xi> (N - {A + {#L#} + {#L#}}) < \<Xi> N"
            using f5 f4 by (metis Un_empty_right `\<Xi> {A + {#L#}} < \<Xi> {A + {#L#} + {#L#}}` add.right_neutral add_diff_cancel_left' add_gr_0 diff_less fin finite.emptyI not_le sum_count_ge_2.empty sum_count_ge_2.insert_remove trans_le_add2) (* 1.0 s *)
          thus ?thesis
            using f3 f2 a1 by (metis (no_types) Un_empty_right Un_insert_right condensation.hyps insert_iff multi_self_add_other_not_self) (* 180 ms *)
        qed
    next
      assume "?C' \<notin> N"
      thus ?thesis using `\<Xi> {A + {#L#}} < \<Xi> {A + {#L#} + {#L#}}` condensation.hyps fin sum_count_ge_2.remove by fastforce
    qed
  ultimately show ?case by linarith
next
  case (subsumption A B) note AN = this(1) and AB = this(2) and BN = this(3) and fin = this(4)
  have "card (N - {B}) < card N" using BN by (meson card_Diff1_less subsumption.prems)
  also have "\<Xi> (N - {B}) \<le> \<Xi> N" by (simp add: Diff_subset finite_incl_le_setsum subsumption.prems)
  ultimately show ?case by linarith
qed

lemma simplify_terminates:
  "wf {(N', N). finite N \<and> simplify N N'}"
  using assms apply (rule wfP_if_measure[of finite simplify "\<lambda>N. card N + \<Xi> N"])
  using simplify_finite_measure_decrease by blast



lemma wf_terminates:
  assumes "wf r"
  shows "\<exists>N'.(N', N)\<in> r\<^sup>*  \<and> (\<forall>N''. (N'', N')\<notin> r)"
proof -
  let ?P = "\<lambda>N. (\<exists>N'.(N', N)\<in> r\<^sup>*  \<and> (\<forall>N''. (N'', N')\<notin> r))"
  have "(\<forall>x. (\<forall>y. (y, x) \<in> r \<longrightarrow> ?P y) \<longrightarrow> ?P x)"
    proof clarify
      fix x
      assume H: "\<forall>y. (y, x) \<in> r \<longrightarrow> ?P y"
      { assume "\<exists>y. (y, x) \<in> r"
        then obtain y where y: "(y, x) \<in> r" by blast
        hence "?P y" using H by blast
        hence "?P x" using y by (meson rtrancl.rtrancl_into_rtrancl)
      }
      also {
        assume "\<not>(\<exists>y. (y, x) \<in> r)"
        hence "?P x" by auto
      }
      ultimately show "?P x" by blast
    qed
  also have "(\<forall>x. (\<forall>y. (y, x) \<in> r \<longrightarrow> ?P y) \<longrightarrow> ?P x) \<longrightarrow> All ?P"
    using assms unfolding wf_def by (rule allE)
  ultimately have "All ?P" by blast
  thus "?P N" by blast
qed

lemmas wfP_terminates = wf_terminates [to_pred]
lemma wf_terminates':
  assumes wf: "wf {(b, a). (a, b) \<in> r \<and> P a}"
  shows "\<exists>N'.(N', N)\<in> {(b, a). (a, b) \<in> r \<and> P a}\<^sup>*  \<and> (\<forall>N''. (N'', N')\<notin> {(b, a). (a, b) \<in> r \<and> P a})"
  using assms by (blast intro: Propo_Resolution.wf_terminates)

lemma wfP_terminates':
  assumes wf: "wf {(b, a). r a b  \<and> P a}"
  shows "\<exists>N'.(N', N) \<in> {(b, a). r a b \<and> P a}\<^sup>*  \<and> (\<forall>N''. (N'', N')\<notin> {(b, a). r a b \<and> P a})"
  using assms by (blast intro: Propo_Resolution.wf_terminates)

lemma rtranclp_simplify_terminates:
  assumes fin: "finite N"
  shows "\<exists>N'. simplify\<^sup>*\<^sup>* N N' \<and> simplified N'"
proof -
  have H: "{(N', N). finite N \<and> simplify N N'} = {(N', N). simplify N N' \<and> finite N}" by auto
  hence wf: "wf {(N', N). simplify N N' \<and> finite N}"
    using simplify_terminates by (simp add: H)
  obtain N' where N': "(N', N)\<in> {(b, a). simplify a b \<and> finite a}\<^sup>*" and
    more: "(\<forall>N''. (N'', N')\<notin> {(b, a). simplify a b \<and> finite a})"
    using wfP_terminates'[OF wf, of N] by blast
  have 1: "simplify\<^sup>*\<^sup>* N N'"
    using N' by (induction rule: rtrancl.induct) auto
  hence "finite N'" using fin rtranclp_simplifier_preserves_finite by blast
  hence 2: "\<forall>N''. \<not>simplify N' N''" using more by auto

  show ?thesis using 1 2 by blast
qed

lemma finite_simplified_full_simp:
  assumes "finite N"
  shows "simplified N \<or> (\<exists>N'. full_simplifier N N')"
  using rtranclp_simplify_terminates[OF assms] unfolding full_simplifier_def by (metis Nitpick.rtranclp_unfold)

lemma finite_simplified_full0_simp:
  assumes "finite N"
  shows "\<exists>N'. full0_simplifier N N'"
  using rtranclp_simplify_terminates[OF assms] unfolding full0_simplifier_def by metis


lemma can_decrease_tree_size_resolution:
  fixes \<psi> :: "'v state" and tree :: "'v sem_tree"
  assumes "finite (fst \<psi>)" and "already_used_inv \<psi>"
  and "partial_interps tree I (fst \<psi>)"
  and "simplified (fst \<psi>)"
  shows "\<exists>(tree':: 'v sem_tree) \<psi>'. resolution\<^sup>*\<^sup>* \<psi> \<psi>' \<and> partial_interps tree' I (fst \<psi>') \<and> (sem_tree_size tree' < sem_tree_size tree \<or> sem_tree_size tree = 0)"
  using assms
proof (induct arbitrary: I rule: sem_tree_size)
  case (bigger xs I) note IH = this(1) and finite = this(2) and a_u_i = this(3) and part = this(4) and simp = this(5)

  { assume "sem_tree_size xs = 0"
    hence ?case using part by blast
  }

  also {
    assume sn0: "sem_tree_size xs > 0"
    obtain ag ad v where xs: "xs = Node v ag ad" using sn0 by (case_tac xs, auto)
    {
       assume "sem_tree_size ag = 0 \<and> sem_tree_size ad = 0"
       hence ag: "ag = Leaf" and ad: "ad = Leaf" by (case_tac ag,  auto, case_tac ad,  auto)

       then obtain \<chi> \<chi>' where \<chi>: "\<not> I \<union> {Pos v} \<Turnstile> \<chi>" and tot\<chi>: "total_over_m (I \<union> {Pos v}) {\<chi>}" and \<chi>\<psi>: "\<chi> \<in> fst \<psi>" and \<chi>': "\<not> I \<union> {Neg v} \<Turnstile> \<chi>'"  and tot\<chi>': "total_over_m (I \<union> {Neg v}) {\<chi>'}" and \<chi>'\<psi>: "\<chi>' \<in> fst \<psi>"
         using part unfolding xs by auto
       have Posv: "Pos v \<notin># \<chi>" using \<chi> unfolding true_cls_def true_lit_def by auto
       have Negv: "Neg v \<notin># \<chi>'" using \<chi>' unfolding true_cls_def true_lit_def by auto
       {
         assume Neg\<chi>: "\<not>Neg v \<in># \<chi>"
         hence "\<not> I \<Turnstile> \<chi>" using \<chi> Posv unfolding true_cls_def true_lit_def by blast
         also have "total_over_m I {\<chi>}" using Posv Neg\<chi> atm_imp_pos_or_neg_lit tot\<chi> unfolding total_over_m_def total_over_set_def by fastforce
         ultimately have "partial_interps Leaf I (fst \<psi>)"
         and "sem_tree_size Leaf < sem_tree_size xs"
         and "resolution\<^sup>*\<^sup>* \<psi> \<psi>"
           unfolding xs by (auto simp add: \<chi>\<psi>)
       }
       also {
          assume Pos\<chi>: "\<not>Pos v \<in># \<chi>'"
          hence I\<chi>: "\<not> I \<Turnstile> \<chi>'" using \<chi>' Posv unfolding true_cls_def true_lit_def by blast
          also have "total_over_m I {\<chi>'}" using Negv Pos\<chi> atm_imp_pos_or_neg_lit tot\<chi>' unfolding total_over_m_def total_over_set_def by fastforce
          ultimately have  "partial_interps Leaf I (fst \<psi>)"
          and "sem_tree_size Leaf < sem_tree_size xs"
          and "resolution\<^sup>*\<^sup>* \<psi> \<psi>" using \<chi>'\<psi> I\<chi> unfolding xs by auto
       }
       moreover {
          assume neg: "Neg v \<in># \<chi>" and pos: "Pos v \<in># \<chi>'"
          have "count \<chi> (Neg v) = 1" using simplified_count[OF simp \<chi>\<psi>] neg by (metis One_nat_def Suc_le_mono Suc_pred eq_iff le0)
          have "count \<chi>' (Pos v) = 1" using simplified_count[OF simp \<chi>'\<psi>] pos by (metis One_nat_def Suc_le_mono Suc_pred eq_iff le0)
          obtain C where \<chi>C: "\<chi> = C + {#Neg v#}" and negC: "Neg v \<notin># C" and posC: "Pos v \<notin># C"
            proof -
              assume a1: "\<And>C. \<lbrakk>\<chi> = C + {#Neg v#}; Neg v \<notin># C; Pos v \<notin># C\<rbrakk> \<Longrightarrow> thesis"
              have f2: "\<And>n. (0::nat) + n = n"
                by simp (* 0.0 ms *)
              obtain mm :: "'v literal multiset \<Rightarrow> 'v literal \<Rightarrow> 'v literal multiset" where
                f3: "{#Neg v#} + mm \<chi> (Neg v) = \<chi>"
                by (metis (no_types) `count \<chi> (Neg v) = 1` add.commute multi_member_split zero_less_one) (* 33 ms *)
              hence "Pos v \<notin># mm \<chi> (Neg v)"
                using f2 by (metis (no_types) Posv `count \<chi> (Neg v) = 1` add.right_neutral add_left_cancel count_single count_union less_nat_zero_code) (* 106 ms *)
              thus ?thesis
                using f3 a1 by (metis (no_types) `count \<chi> (Neg v) = 1` add.commute add.right_neutral add_left_cancel count_single count_union less_nat_zero_code) (* 160 ms *)
            qed
          obtain C' where \<chi>C': "\<chi>' = C' + {#Pos v#}" and posC': "Pos v \<notin># C'"  and negC': "Neg v \<notin># C'" by (metis (no_types, hide_lams) Negv `count \<chi>' (Pos v) = 1` add_diff_cancel_right' cancel_comm_monoid_add_class.diff_cancel count_diff count_single less_nat_zero_code mset_leD mset_le_add_left multi_member_split zero_less_one)

          have totC: "total_over_m I {C}" using tot\<chi> tot_over_m_remove[of I "Pos v" C] negC posC unfolding \<chi>C by (metis total_over_m_sum uminus_Neg uminus_of_uminus_id)
          have totC': "total_over_m I {C'}" using tot\<chi>' total_over_m_sum tot_over_m_remove[of I "Neg v" C'] negC' posC' unfolding \<chi>C' by (metis total_over_m_sum uminus_Neg)
          have "\<not> I \<Turnstile> C + C'" using \<chi> \<chi>' \<chi>C \<chi>C' unfolding \<chi> \<chi>' true_cls_def unfolding true_cls_def true_cls_union_increase by auto
          hence part_I_\<psi>''': "partial_interps Leaf I (fst \<psi> \<union> {C + C'})"
            using totC totC' by simp (metis `\<not> I \<Turnstile> C + C'` atms_of_m_singleton total_over_m_def total_over_m_sum)
          {
            assume "({#Pos v#} + C', {#Neg v#} + C) \<notin> snd \<psi>"
            hence inf'': "inference \<psi> (fst \<psi> \<union> {C + C'}, snd \<psi> \<union> {(\<chi>', \<chi>)})" by (metis \<chi>'\<psi> \<chi>C \<chi>C' \<chi>\<psi> add.commute inference_step prod.collapse resolution)
            obtain N' where full: "full0_simplifier (fst \<psi> \<union> {C + C'}) N'"
              by (metis finite_simplified_full0_simp fst_conv inf'' inference_preserves_finite local.finite)
            have "resolution \<psi> (N', snd \<psi> \<union> {(\<chi>', \<chi>)})" using resolution.intros(2)[OF _ simp full, of "snd \<psi>" "snd \<psi> \<union> {(\<chi>', \<chi>)}"] inf'' by (metis surjective_pairing)
            moreover have "partial_interps Leaf I N'" using full0_simplifier_preserve_partial_tree[OF full part_I_\<psi>'''] .
            moreover have "sem_tree_size Leaf < sem_tree_size xs" unfolding xs by auto
            ultimately have ?case by (metis (no_types) prod.sel(1) rtranclp.rtrancl_into_rtrancl rtranclp.rtrancl_refl)
          }
          also {
            assume a: "({#Pos v#} + C', {#Neg v#} + C) \<in> snd \<psi>"
            hence "(\<exists>\<chi> \<in> fst \<psi>. (\<forall>I. total_over_m I {C+C'} \<longrightarrow> total_over_m I {\<chi>} ) \<and> (\<forall>I. total_over_m I {\<chi>} \<longrightarrow> I \<Turnstile> \<chi> \<longrightarrow> I \<Turnstile> C' + C)) \<or> tautology (C' + C)"
              proof -
                obtain p where p: "Pos p \<in># ({#Pos v#} + C') \<and> Neg p \<in># ({#Neg v#} + C) \<and> ((\<exists>\<chi>\<in>fst \<psi>. (\<forall>I. total_over_m I {({#Pos v#} + C') - {#Pos p#} + (({#Neg v#} + C) - {#Neg p#})} \<longrightarrow> total_over_m I {\<chi>}) \<and> (\<forall>I. total_over_m I {\<chi>} \<longrightarrow> I \<Turnstile> \<chi> \<longrightarrow> I \<Turnstile> ({#Pos v#} + C') - {#Pos p#} + (({#Neg v#} + C) - {#Neg p#}))) \<or> tautology (({#Pos v#} + C') - {#Pos p#} + (({#Neg v#} + C) - {#Neg p#})))"
                  using a by (blast intro: allE[OF a_u_i[unfolded subsumes_def Ball_def], of " ({#Pos v#} + C', {#Neg v#} + C)"])
                { assume "p \<noteq> v"
                  hence "Pos p \<in># C' \<and> Neg p \<in># C" using p by force
                  hence ?thesis by (metis add_gr_0 count_union tautology_Pos_Neg)
                }
                also {
                  assume "p = v"
                 hence "?thesis" using p by (metis add.commute add_diff_cancel_left')
                }
                ultimately show ?thesis by auto
              qed
            also {
              assume "\<exists>\<chi> \<in> fst \<psi>. (\<forall>I. total_over_m I {C+C'} \<longrightarrow> total_over_m I {\<chi>}) \<and>(\<forall>I. total_over_m I {\<chi>} \<longrightarrow> I \<Turnstile> \<chi> \<longrightarrow> I \<Turnstile> C' + C)"
              then obtain \<theta> where \<theta>: "\<theta> \<in> fst \<psi>" and tot_\<theta>_CC': "\<forall>I. total_over_m I {C+C'} \<longrightarrow> total_over_m I {\<theta>}" and \<theta>_inv: "\<forall>I. total_over_m I {\<theta>} \<longrightarrow> I \<Turnstile> \<theta> \<longrightarrow> I \<Turnstile> C' + C" by blast
              have "partial_interps Leaf I (fst \<psi>)"using tot_\<theta>_CC' \<theta> \<theta>_inv totC totC'  `\<not> I \<Turnstile> C + C'` total_over_m_sum by fastforce
              moreover have "sem_tree_size Leaf < sem_tree_size xs" unfolding xs by auto
              ultimately have ?case by blast
            }
            moreover {
              assume tautCC': "tautology (C' + C)"
              have "total_over_m I {C'+C}" using totC totC' total_over_m_sum by auto
              hence "\<not>tautology (C' + C)" using `\<not> I \<Turnstile> C + C'` unfolding add.commute[of C C'] total_over_m_def  unfolding tautology_def by auto
              hence False using tautCC'  unfolding tautology_def by auto
            }
            ultimately have ?case by auto
          }
          ultimately have ?case by auto
       }
       ultimately have ?case using part by (metis (no_types) sem_tree_size.simps(1))
     }
     also {
       assume size_ag: "sem_tree_size ag > 0"
       have "sem_tree_size ag < sem_tree_size xs" unfolding xs by auto
       also have "partial_interps ag (I \<union> {Pos v}) (fst \<psi>)"
       and partad: "partial_interps ad (I \<union> {Neg v}) (fst \<psi>)" using part partial_interps.simps(2) unfolding xs by metis+
       moreover have "sem_tree_size ag < sem_tree_size xs \<Longrightarrow>
        finite (fst \<psi>) \<Longrightarrow> already_used_inv \<psi> \<Longrightarrow> partial_interps ag (I \<union> {Pos v}) (fst \<psi>) \<Longrightarrow> simplified (fst \<psi>) \<Longrightarrow> \<exists>tree' \<psi>'. resolution\<^sup>*\<^sup>* \<psi> \<psi>' \<and> partial_interps tree' (I \<union> {Pos v}) (fst \<psi>') \<and> (sem_tree_size tree' < sem_tree_size ag \<or> sem_tree_size ag = 0)" using IH[of ag "I \<union> {Pos v}"]  by auto
       ultimately obtain \<psi>' :: "'v state" and  tree' :: "'v sem_tree"  where
         inf: "resolution\<^sup>*\<^sup>* \<psi> \<psi>'"
         and part: "partial_interps tree' (I \<union> {Pos v}) (fst \<psi>')"
         and size: "sem_tree_size tree' < sem_tree_size ag \<or> sem_tree_size ag = 0" using finite part rtranclp.rtrancl_refl a_u_i simp by blast

       have "partial_interps ad (I \<union> {Neg v}) (fst \<psi>')" using rtranclp_resolution_preserve_partial_tree inf partad by fast
       hence "partial_interps (Node v tree' ad) I (fst \<psi>')" using part by auto
       hence ?case using inf size size_ag part unfolding xs by fastforce
     }
     moreover {
       assume size_ad: "sem_tree_size ad > 0"
       have "sem_tree_size ad < sem_tree_size xs" unfolding xs by auto
       also have partag: "partial_interps ag (I \<union> {Pos v}) (fst \<psi>)"
       and "partial_interps ad (I \<union> {Neg v}) (fst \<psi>)" using part partial_interps.simps(2) unfolding xs by metis+
       moreover have "sem_tree_size ad < sem_tree_size xs \<longrightarrow> finite (fst \<psi>) \<longrightarrow> already_used_inv \<psi> \<longrightarrow> ( partial_interps ad (I \<union> {Neg v}) (fst \<psi>) \<longrightarrow> simplified (fst \<psi>) \<longrightarrow> (\<exists>tree' \<psi>'. resolution\<^sup>*\<^sup>* \<psi> \<psi>' \<and> partial_interps tree' (I \<union> {Neg v}) (fst \<psi>') \<and> (sem_tree_size tree' < sem_tree_size ad \<or> sem_tree_size ad = 0)))" using IH by blast
       ultimately obtain \<psi>' :: "'v state" and  tree' :: "'v sem_tree"  where
         inf: "resolution\<^sup>*\<^sup>* \<psi> \<psi>'"
         and part: "partial_interps tree' (I \<union> {Neg v}) (fst \<psi>')"
         and size: "sem_tree_size tree' < sem_tree_size ad \<or> sem_tree_size ad = 0" using finite part  rtranclp.rtrancl_refl a_u_i simp by blast

       have "partial_interps ag (I \<union> {Pos v}) (fst \<psi>')" using rtranclp_resolution_preserve_partial_tree inf partag by fast
       hence "partial_interps (Node v ag tree') I (fst \<psi>')" using part by auto
       hence "?case" using inf size size_ad unfolding xs by fastforce
     }
     ultimately have ?case by auto
  }
  ultimately show ?case by auto
qed

lemma resolution_completeness_inv:
  fixes \<psi> :: "'v ::linorder state"
  assumes unsat: "\<not>satisfiable (fst \<psi>)" and finite: "finite (fst \<psi>)" and a_u_v: "already_used_inv \<psi>"
  shows "\<exists>\<psi>'. (resolution\<^sup>*\<^sup>* \<psi> \<psi>' \<and> {#} \<in> fst \<psi>')"
proof -
  obtain tree where  "partial_interps tree {} (fst \<psi>)" using partial_interps_build_sem_tree_atms assms by metis
  thus ?thesis
    using unsat finite a_u_v
    proof (induct tree arbitrary: \<psi> rule: sem_tree_size)
      case (bigger tree \<psi>) note H = this
      {
        fix \<chi>
        assume tree: "tree = Leaf"
        obtain \<chi> where \<chi>: "\<not> {} \<Turnstile> \<chi>" and tot\<chi>: "total_over_m {} {\<chi>}" and \<chi>\<psi>: "\<chi> \<in> fst \<psi>" using H unfolding tree by auto
        also have "{#} = \<chi>"
          using H unfolding \<chi> total_over_m_def apply auto
          using atms_empty_iff_empty tot\<chi> unfolding true_cls_def total_over_m_def total_over_set_def by fastforce
        moreover have "resolution\<^sup>*\<^sup>* \<psi> \<psi>" by auto
        ultimately have ?case by metis
      }
      also {
        fix v tree1 tree2
        assume tree: "tree = Node v tree1 tree2"
        obtain \<psi>\<^sub>0 where \<psi>\<^sub>0: "resolution\<^sup>*\<^sup>* \<psi> \<psi>\<^sub>0" and simp: "simplified (fst \<psi>\<^sub>0)"
          proof -
            { assume "simplified (fst \<psi>)"
              also have "resolution\<^sup>*\<^sup>* \<psi> \<psi>" by auto
              ultimately have "thesis" using that by blast
            }
            also {
              assume "\<not>simplified (fst \<psi>)"
              hence "\<exists>\<psi>'.  full_simplifier (fst \<psi>) \<psi>'" by (metis Nitpick.rtranclp_unfold bigger.prems(3) full_simplifier_def rtranclp_simplify_terminates)
              then obtain N where "full_simplifier (fst \<psi>) N" by metis
              hence "resolution \<psi> (N, snd \<psi>)" using resolution.intros(1)[of "fst \<psi>" N "snd \<psi>"] by auto
              also have "simplified N" using `full_simplifier (fst \<psi>) N` full_simplifier_def by blast
              ultimately have ?thesis using that by force
            }
            ultimately show ?thesis by auto
          qed


        have p: "partial_interps tree {} (fst \<psi>\<^sub>0)"
        and uns: "unsatisfiable (fst \<psi>\<^sub>0)"
        and f: "finite (fst \<psi>\<^sub>0)"
        and a_u_v: "already_used_inv \<psi>\<^sub>0"
             using \<psi>\<^sub>0 bigger.prems(1) rtranclp_resolution_preserve_partial_tree apply blast
            using \<psi>\<^sub>0 bigger.prems(2) rtranclp_resolution_preserves_unsat apply blast
           using \<psi>\<^sub>0 bigger.prems(3) rtranclp_resolution_finite apply blast
          using rtranclp_resolution_already_used_inv[OF \<psi>\<^sub>0 bigger.prems(4)] by blast
        obtain tree' \<psi>' where inf: "resolution\<^sup>*\<^sup>* \<psi>\<^sub>0 \<psi>'"
        and part': "partial_interps tree' {} (fst \<psi>')"
        and decrease: "sem_tree_size tree' < sem_tree_size tree \<or> sem_tree_size tree = 0" using can_decrease_tree_size_resolution[OF f a_u_v p simp] unfolding tautology_def by meson
        have s: "sem_tree_size tree' < sem_tree_size tree" using decrease unfolding tree by auto
        have fin: "finite (fst \<psi>')"
          using f inf rtranclp_resolution_finite by blast
        have unsat: "unsatisfiable (fst \<psi>')"
          using rtranclp_resolution_preserves_unsat inf uns by metis
        have a_u_i': "already_used_inv \<psi>'" using a_u_v inf rtranclp_resolution_already_used_inv[of \<psi>\<^sub>0 \<psi>'] by auto
        have ?case using inf rtranclp_trans[of resolution] H(1)[OF s part' unsat fin a_u_i'] \<psi>\<^sub>0 by blast
      }
      ultimately show ?case by (case_tac tree, auto)
   qed
qed

lemma resolution_preserves_already_used_inv:
  assumes "resolution S S'"
  and "already_used_inv S"
  shows "already_used_inv S'"
  using assms
  apply (induct rule: resolution.induct)
   apply (rule full_simplify_already_used_inv; simp)
  apply (rule full0_simplify_already_used_inv, simp)
  apply (rule inference_preserves_already_used_inv, simp)
  apply blast
  done

lemma rtranclp_resolution_preserves_already_used_inv:
  assumes "resolution\<^sup>*\<^sup>* S S'"
  and "already_used_inv S"
  shows "already_used_inv S'"
  using assms
  apply (induct rule: rtranclp.induct)
   apply simp
  using resolution_preserves_already_used_inv by fast

lemma resolution_completeness:
  fixes \<psi> :: "'v ::linorder state"
  assumes unsat: "\<not>satisfiable (fst \<psi>)"
  and finite: "finite (fst \<psi>)"
  and "snd \<psi> = {}"
  shows "\<exists>\<psi>'. (resolution\<^sup>*\<^sup>* \<psi> \<psi>' \<and> {#} \<in> fst \<psi>')"
proof -
  have "already_used_inv \<psi>" unfolding assms by auto
  thus ?thesis using assms resolution_completeness_inv by blast
qed

lemma rtranclp_preserves_sat:
  assumes "simplify\<^sup>*\<^sup>* S S'"
  and "satisfiable S"
  shows "satisfiable S'"
  using assms apply (induction)
   apply simp
  by (meson satisfiable_carac satisfiable_def simplifier_preserves_un_sat_eq)

lemma resolution_preserves_sat:
  assumes "resolution S S'"
  and "satisfiable (fst S)"
  shows "satisfiable (fst S')"
  using assms apply (induction rule: resolution.induct)
   using full_simplifier_def rtranclp_preserves_sat tranclp_into_rtranclp apply (fastforce simp add:)
  by (metis fst_conv full0_simplifier_def inference_preserves_un_sat rtranclp_preserves_sat satisfiable_carac' satisfiable_def)


lemma rtranclp_resolution_preserves_sat:
  assumes "resolution\<^sup>*\<^sup>* S S'"
  and "satisfiable (fst S)"
  shows "satisfiable (fst S')"
  using assms apply (induction rule: rtranclp.induct)
   apply simp
  using resolution_preserves_sat by blast

lemma resolution_soundness:
  fixes \<psi> :: "'v ::linorder state"
  assumes "resolution\<^sup>*\<^sup>* \<psi> \<psi>'" and "{#} \<in> fst \<psi>'"
  shows "unsatisfiable (fst \<psi>)"
  using assms by (meson rtranclp_resolution_preserves_sat satisfiable_def true_cls_empty true_clss_def)

lemma resolution_soundness_and_completeness:
fixes \<psi> :: "'v ::linorder state"
assumes finite: "finite (fst \<psi>)"
and snd: "snd \<psi> = {}"
shows "(\<exists>\<psi>'. (resolution\<^sup>*\<^sup>* \<psi> \<psi>' \<and> {#} \<in> fst \<psi>')) \<longleftrightarrow> unsatisfiable (fst \<psi>)"
  using assms resolution_completeness resolution_soundness by metis

lemma simplified_falsity:
  assumes simp: "simplified \<psi>"
  and "{#} \<in> \<psi>"
  shows "\<psi> = {{#}}"
proof (rule ccontr)
  assume H: "\<not> ?thesis"
  then obtain \<chi> where "\<chi> \<in> \<psi>" and "\<chi> \<noteq> {#}" using assms(2) by blast
  hence "{#} \<subset># \<chi>" by (simp add: mset_less_empty_nonempty)
  hence "simplify \<psi> (\<psi> - {\<chi>})" using simplify.subsumption[OF assms(2) `{#} \<subset># \<chi>` `\<chi> \<in> \<psi>`] by blast
  thus False using simp by blast
qed


lemma simplify_falsity_in_preserved:
  assumes "simplify \<chi>s \<chi>s'"
  and "{#} \<in> \<chi>s"
  shows "{#} \<in> \<chi>s'"
  using assms
  by induction auto

lemma rtranclp_simplify_falsity_in_preserved:
  assumes "simplify\<^sup>*\<^sup>* \<chi>s \<chi>s'"
  and "{#} \<in> \<chi>s"
  shows "{#} \<in> \<chi>s'"
  using assms
  by induction (auto intro: simplify_falsity_in_preserved)

lemma resolution_falsity_get_falsity_alone:
  assumes "finite (fst \<psi>)"
  shows "(\<exists>\<psi>'. (resolution\<^sup>*\<^sup>* \<psi> \<psi>' \<and> {#} \<in> fst \<psi>')) \<longleftrightarrow> (\<exists>a_u_v. resolution\<^sup>*\<^sup>* \<psi> ({{#}}, a_u_v))" (is "?A \<longleftrightarrow> ?B")
proof
  assume ?B
  thus ?A by auto
next
  assume ?A
  then obtain \<chi>s a_u_v where \<chi>s: "resolution\<^sup>*\<^sup>* \<psi> (\<chi>s, a_u_v)" and F: "{#} \<in> \<chi>s" by auto
  { assume "simplified \<chi>s"
    hence ?B using simplified_falsity[OF _ F] \<chi>s by blast
  }
  also {
    assume "\<not> simplified \<chi>s"
    then obtain \<chi>s' where "full_simplifier \<chi>s \<chi>s'"
       by (metis \<chi>s assms finite_simplified_full_simp fst_conv rtranclp_resolution_finite)
    hence "{#} \<in> \<chi>s'"
      unfolding full_simplifier_def  by (meson F rtranclp_simplify_falsity_in_preserved tranclp_into_rtranclp)
    hence ?B
      by (metis \<chi>s `simp\<^sup>+\<^sup>\<down> \<chi>s \<chi>s'` fst_conv full_simp resolution_always_simplified rtranclp.rtrancl_into_rtrancl simplified_falsity)
  }
  ultimately show ?B by blast
qed

lemma resolution_soundness_and_completeness':
fixes \<psi> :: "'v ::linorder state"
assumes finite: "finite (fst \<psi>)"
and snd: "snd \<psi> = {}"
shows "(\<exists>a_u_v. (resolution\<^sup>*\<^sup>* \<psi> ({{#}}, a_u_v))) \<longleftrightarrow> unsatisfiable (fst \<psi>)"
  using assms resolution_completeness resolution_soundness resolution_falsity_get_falsity_alone by metis

end
