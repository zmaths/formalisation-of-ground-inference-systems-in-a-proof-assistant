watch:
	./test.sh
	fswatch -o Report/{*.thy,ROOT,document/{introduction.tex,concl.tex,biblio.bib,root.tex}} | xargs  -n1 ./test.sh


jedit:
	isabelle jedit -j -server -j -background -d thys -l CNF  Report/{*.thy,ROOT,document/{introduction.tex,concl.tex,root.tex}}


report:
	/Users/mathiasfleury/Documents/isabelle/isabelle/bin/isabelle build -d thys -D Report CNF
pres:
	python ~/Documents/osx-presentation/presentation.py -d 20 Presentation/presentation.pdf
clean:
	sed -i '' -E -e's/[ ]*$$//' thys/*.thy
